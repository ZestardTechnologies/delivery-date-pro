@extends('header')
@section('content')
<?php
	$shop = session('shop');
?>
<script type="text/javascript">
	ShopifyApp.ready(function(){
		ShopifyApp.Bar.initialize({
		  buttons: {			
			secondary: [{
			  label: 'HELP',
			  href : '{{ url('/help') }}',
			  loading: true
			},
			{
			  label: 'MANAGE ORDER',
			  href : "{{ url('order?store='.$shop) }}",
			  loading: true
			},{
			  label: 'SETTINGS',
			  href : "{{ url('dashboard?store='.$shop) }}",
			  loading: true
			}]
		  }
		});
  });
</script>
<div class="container formcolor formcolor_help">
    
    <div class=" row">
        <div class="help_page">
        <div class="col-md-12 col-sm-12 col-xs-12 need_help">
            <h2 class="dd-help">Need Help?</h2>
            <p class="col-md-12"><b>To customize any thing within the app or for other work just contact us on below details</b></p>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="dd-help-ul">
                    <li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
                    <li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
                    <li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
                </ul>
            </div>
        </div>         
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h2 class="dd-help">General Instruction</h2> 
            <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                 <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                                    <strong><span class="">Can Delivery Date Calendar be in different language?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Yes, delivery date calendar can be merchant specfic langauge, they can select language from <b>"Select Language"</b> in general setting. </p>                                                         
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
                                    <strong><span class="">How to configure Delivery datepicker in app?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="ul-help">
                                    <li>If you want to show delivery date label/title, then enable the <b>"Show Datepicker label on Website?"</b> option.</li>
                                    <li>If <b>"Datepicker label"</b> is enable then add a label which you want to show in your website cart page.</li>
                                    <li>If you want your customer to see the date format on your website then enable the <b>"Show Date Format on Website?"</b> option.</li>
                                    <li>Select the date format from <b>"Date Format"</b>.</li>
                                </ul>                                
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                                    <strong><span class="">How to make delivery date required in cart page?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="ul-help">
                                    <li>Enable the <b>"Delivery Date is Required Field?"</b>.</li>
                                    <li>Add a validation message in <b>"Validation Message When Delivery Date is Required"</b>. A alert message will show to the customer, if they have not selected the delivery date.</li>                                    
                                </ul>                                
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
                                    <strong><span class="">How to configure delivery time in app?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="ul-help">
                                    <li>If you want to keep delivery time on your website then enable the <b>"Show Time on Front?"</b> and follow below points.</li>
                                    <li>You can make delivery time field required by enable the <b>"Time is Required Field?"</b>.</li>
                                    <li>Add delivery time title on <b>"Delivery Time Title"</b> which will show on website cart page.</li>
                                    <li>Add delivery time label on <b>"Time Default option label"</b>, if not added it will take default label "Choose Time".</li>
                                    <li>Add Delivery time according to you with comma (",") separated values like: (12:00PM to 2:00PM, 4:00PM to 6:00PM)</li>
                                </ul>                                
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse5"> 
                                    <strong><span class="">Can merchant add notes on website through delivery date pro app?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="ul-help">
                                    <li>Yes, merchant can add some additional note on their website.</li>
                                    <li>To dispaly note section on website then enable the <b>"Show Notes on Website?"</b></li>
                                    <li>Add notes to <b>"Notes"</b> field here you can use the ck editor tools to make your note look attractive and good.</li>                                   
                                </ul>                                
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse6"> 
                                    <strong><span class="">What if Add Block Days Interval is enable?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>If Add Block Days Interval is enable, then extra days will be added for "Minimum Date Interval" due to Blocked Days.</p>                               
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse7"> 
                                    <strong><span class="">Can merchant block particular day for delivery like(sunday)?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse7" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="ul-help">
                                    <li>Yes, a merchant can block delivery days if they don't deliver on that day.</li>
                                    <li>Just have to click on days in <b>"Block Day(s)"</b> which they want to block. On blocked days user won't able to select it as a delivery date.</li>
                                </ul>                                
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse8"> 
                                    <strong><span class="">Can merchant block particular date?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse8" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Yes, a merchant can add dates in <b>"Blocked Date(s)"</b>. List of dates will be blocked for delivery so the user won't be able to select it for the order delivery.</p>                            
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse9"> 
                                    <strong><span class="">Is their any option to specify pre-order time?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse9" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Yes, If your store takes order before "X" months then you can specify the "X" in <b>"Allowed Pre-order Time"</b>. This option will allow user to book order before "X" number of Months on the Website.</p>                            
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse10"> 
                                    <strong><span class="">What is the purpose of Minimum Date Interval?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse10" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Purpose of Minimum Date Interval is that if you want your user to select delivery date after "X" day from the order date then you have to put "X" in <b>"Minimum Date Interval"</b> block. For "Today Delivery" it should be "0" otherwise it should be the interval of days from the current day to allow user to select delivery date.
For example: If you have put 1 day as minimum interval then the user have no the option to select the date when they are ordering, option for selecting delivery date will start from next day.</p>                            
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse11"> 
                                    <strong><span class="">How to show calendar visible on website?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse11" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>To show calendar visible on cart page of website, then enable the <b>"Show Calendar visible by default"</b>.</p>                            
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse12"> 
                                    <strong><span class="">Is their any option for by default next avaliable date is selected?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse12" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Enable the <b>"Next available date set as a selected"</b>, to show next avalivable date for delivery in cart page.</p>                            
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse13"> 
                                    <strong><span class="">How to show selected delivery date on admin order infromation?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse13" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>If a merchant want to see the selected delivery date by customer for particular order  in admin orders then enable the <b>"Add Delivery Information under Order Instructions"</b>.</p>                            
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse14"> 
                                    <strong><span class="">What if Merchant Activates Cut-Off Time?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse14" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>For <b>"Next Day Delivery"</b> if the cut off time is enabled & set then it will allow user to select day after the "Minimum Date Interval"(Explained above) as first available delivery date.</p>                            
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse15"> 
                                    <strong><span class="">What is Additional CSS for?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse15" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>If a merchant want to make different look for delivery date, they can add additional CSS.</p>                            
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse16"> 
                                    <strong><span class="">How a merchant can manage order within app?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse16" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="ul-help">
                                    <li>Yes a merchant can manage order from <b>"Manage Order"</b> button at the top of the app. In manage order page all the orders will display with selected date and time.</li>
                                    <li>On above table today's Delivery and This week's delivery number of orders will display. On click particular day or particular week orders will show.</li>
                                    <li>Here merchant can sort the orders by dates and by status.</li>                                   
                                    <li>A merchant can export Sheet of orders by click on <b>"Export to CSV"</b>.</li>
                                    <li>Show order data for all products means name and email of customer in order will be repeated for every product in CSV File.</li>
                                </ul>                                
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse17"> 
                                    <strong><span class="">What are Cut Off Settings?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse17" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Cut off Settings are for adding Cut Off Time for Each Individual Weekday i.e Multiple Cut Off Time.</p>                              
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse18"> 
                                    <strong><span class="">What is delivery time setting?</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse18" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>In this a merchant can set delivery time day wise and also add count of delivery on that time slot. Set Yes to <b>"Use Global Delivery Time"</b> if a merchant don't want to use individual delivery time for all days. If it is set to Yes, then delivery time in general setting will show for all days.</p>                              
                            </div>
                        </div>
                    </div>
                     
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse19"> 
                                    <strong><span class="">Notes for adding JQuery into Theme</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse19" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="ul-help">
                                    <li>In case if you don't see the datepicker in your cart page then check for the jquery link in your <a href="<?php echo 'https://'.session('shop').'/admin/themes/current/?key=layout/theme.liquid'?>" target="_blank"><b>Theme.liquid</b></a> file.</li>
                                    <li>If there is no jquery script then add below short-code in it and then refresh your cart page.</li>
                                    <li>Now try to open datepicker again.</li>    
                                </ul>
                                <p>
                                    Copy and paste the following code into head part of your <a href="<?php echo 'https://'.session('shop').'/admin/themes/current/?key=layout/theme.liquid'?>" target="_blank"><b>Theme.liquid</b></a> file.
                                </p>
                                <div class="copystyle_wrapper col-md-9">
                                    <textarea rows="1" class="form-control script_code" id="script_code" disabled><?php echo "{{ 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js' | script_tag }}" ?></textarea>
                                    <btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_script()"><i class="fa fa-check"></i> Copy</btn>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <p data-toggle="collapse" data-parent="#accordion" href="#collapse20"> 
                                    <strong><span class="">Notes for adding Datepicker into Popup cart</span>
                                    <span class="fa fa-chevron-down pull-right"></span></strong>  
                                </p>
                            </h4>
                        </div>
                        <div id="collapse20" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    In case you want to add datepicker in your  popup cart then copy and paste the following code into Your popup cart page.
                                </p>
                                <div class="copystyle_wrapper col-md-5">
                                    <textarea rows="1" class="form-control popup_code" id="popup_code" disabled><?php echo "{% include 'delivery-date' %}" ?></textarea>
                                    <btn id="copy_popup" name="copy_popup" value="Copy Popupcode" class="btn btn-info copycss_button" data-clipboard-target=".popup_code" style="display: block;" onclick="copy_popup()"><i class="fa fa-check"></i> Copy</btn>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>

    </div>
      <!--Version updates-->
    <div class="version_update_section">
          <div class="col-md-6" style="padding-right: 0;">
            <div class="feature_box">
				<h3 class="dd-help">Version Updates <span class="verison_no">2.0</span></h3>
				<div class="version_block">
					<div class="col-md-12">
						<div class="col-md-3 version_date">
                          <p><i class="glyphicon glyphicon-heart"></i></p>
                          <strong>22 Jan, 2018</strong>
                          <a href="#"><b>Update</b></a>
						</div>
						<div class="col-md-8 version_details version_details_2">
                          <strong>Version 2.0</strong>
                          <ul>
                              <li>Add Delivery Date & Time information under customer order Email</li>
                              <li>Auto Select for Next Available Delivery Date</li>
                              <li>Auto Tag Delivery Details to all the Orders</li>
                              <li>Manage Cut Off Time for Each Individual Weekday</li>
                              <li>Limit Number of Order Delivery during the given time slot for any day </li>
                          </ul>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-3 version_date">
							<p><i class="glyphicon glyphicon-globe"></i></p>
							<strong>20 Dec, 2017</strong>
							<a href="#"><b>Release</b></a>
						</div>
						<div class="col-md-8 version_details version_details_2">
							<strong>Version 1.0</strong>
							<ul>
                              <li>Delivery Date & Time Selection</li>
                              <li>Same Day & Next Day Delivery</li>
                              <li>Blocking Specific Days and Dates</li>
                              <li>Admin Order Manage & Export Based on Delivery Date</li>
                              <li>Option for Cut Off Time & Delivery Time</li>
                          </ul>
						</div>
                  </div>
              </div>
          </div>
        </div>
        <div class="col-md-6">
            <div class="feature_box">
              <h3 class="dd-help">Upcoming Features</h3>
				<div class="feature_block">
                    <div>   
                        <span class="checkboxFive">
                        <input type="checkbox" checked disabled value="1" id="checkbox0" name="exclude_block_date_status">
                        <label for="checkbox0"></label>
                        </span> 
                        <strong>Multiple Cutoff Time Option</strong>  
                    </div>
					<div>   
                        <span class="checkboxFive">
                        <input type="checkbox" checked disabled value="1" id="checkbox4" name="exclude_block_date_status">
                        <label for="checkbox4"></label>
                        </span> 
                        <strong>Multiple Delivery Time Option</strong>  
                    </div>
					<div>   
                        <span class="checkboxFive">
                        <input type="checkbox" checked disabled value="1" id="checkbox5" name="exclude_block_date_status">
                        <label for="checkbox5"></label>
                        </span> 
                        <strong>Auto Tag Delivery Details to all the Orders Within Interval of 1 hour from Order Placed Time</strong>  
                    </div>
                    <div>   
                        <span class="checkboxFive">
                        <input type="checkbox" checked disabled value="1" id="checkbox1" name="exclude_block_date_status">
                        <label for="checkbox1"></label>
                        </span> 
                        <strong>Auto Select for Next Available Delivery Date</strong>  
                    </div>
                    <div>   
                        <span class="checkboxFive">
                        <input type="checkbox" checked disabled value="1" id="checkbox2" name="exclude_block_date_status">
                        <label for="checkbox2"></label>
                        </span> 
                        <strong>Order Export in Excel</strong>  
                    </div>
                    <div>   
                        <span class="checkboxFive">
                        <input type="checkbox" checked disabled value="1" id="checkbox3" name="exclude_block_date_status">
                        <label for="checkbox3"></label>
                        </span> 
                        <strong>Filtering Orders by Delivery Date</strong>  
                    </div>                                        
				</div>
				<div>
					<p class="feature_text">
						New features are always welcome send us on : <a href="mailto:support@zestard.com"><b>support@zestard.com</b></a> 
					</p>
				</div>
			</div>			
		</div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".fa").addClass("fa-chevron-up").removeClass("fa-chevron-down");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find("span.fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find("span.fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        });
    });
</script>
@endsection
