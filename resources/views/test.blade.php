@extends('layouts.app')
@section('pageCss')
<style>
	#product_settings_wrapper
	{
		padding: 0 40px 0 40px;
	}
	#product_settings
	{
		width:auto !important;
	}
	a.addrow
	{
		padding: 4px !important;
		font-size: 12px !important;
	}
	#product_settings_filter
	{
		display: block !important;
	}
	#product_settings_paginate .pagination {
		float: right;
		margin-bottom: 15px;		
	}
	.global input:checked + label:after
	{
		content: 'Global' !important;
	}
	.global label:after {
    	content: 'Local' !important;
    }
	.global label:before
	{
		left:4px !important;
	}
	.global input:checked + label:before {
    	left: 45px !important;
	}
	.global
	{
		width:65px !important;
	}	
</style>
@endsection
@section('content')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	ShopifyApp.ready(function(){
		ShopifyApp.Bar.initialize({
			buttons: {			
				primary: {
					label: 'SAVE',
					message : 'form_submit',		
					callback:  function(event){  } 
				},
				secondary: [{
					label: 'SETTINGS',
					href : 'dashboard?shop={{$shop}}',
					loading: true
				},
				{
					label: 'MANAGE ORDER',
					href : 'order?shop={{$shop}}',
					loading: true
				},
				{
					label: 'HELP',
					href : 'help?shop={{$shop}}',
					loading: true
				}]
			}
		});
	});
</script>
<div class="container formcolor">
	<ul class="nav nav-tabs dashboard_tabs">
		<li><a href="dashboard?shop={{$shop}}">General Settings</a></li>		
        <li class="active"><a href="product-settings?shop={{$shop}}">Product Settings</a></li>
		<li><a href="help?shop={{$shop}}">Help</a></li>
	</ul>
<h2 class="sub-heading">Product Settings</h2>    
    <div class="row">
		<div class="col-md-12">	
			<form action="save-product-settings" name="config" method="post" id="product_settings_form" data-shopify-app-submit="form_submit">
				<input type="hidden" name="shop" value="{{ $shop }}" />
				<table id="product_settings" class="table table-striped table-bordered" cellspacing="0">
					<thead>
						<tr>
							<th>Enable/Disable</th>
							<th>Product Image</th>
							<th>Product Name</th>
							<th>Delivery Time<span data-toggle="tooltip" title="If Global, Then Delivery Time in General Settings will be considered else Delivery Time below will be considered" class='onoff global'><input name='global_product_delivery_time' type='checkbox' value='1' @if($configuration->global_product_delivery_time == 1) {{"checked"}} @endif id='global_product_delivery_time'><label for='global_product_delivery_time'></label></span></th>
							<th>Blocked Dates<span data-toggle="tooltip" title="If Global, Then Blocked Dates in General Settings will be considered else Blocked Dates below will be considered" class='onoff global'><input name='global_product_blocked_dates' type='checkbox' value='1' @if($configuration->global_product_blocked_dates == 1) {{"checked"}} @endif id='global_product_blocked_dates'><label for='global_product_blocked_dates'></label></span></th>
							<th>Blocked Days<span data-toggle="tooltip" title="If Global, Then Blocked Days in General Settings will be considered else Blocked Days below will be considered" class='onoff global'><input name='global_product_blocked_days' type='checkbox' value='1' @if($configuration->global_product_blocked_days == 1) {{"checked"}} @endif id='global_product_blocked_days'><label for='global_product_blocked_days'></label></span></th>
							<th>Allowed Pre-order Time<br><span  data-toggle="tooltip" title="If Global, Then Pre-order Time in General Settings will be considered else Pre-order Time below will be considered" class='onoff global'><input name='global_product_pre_order' type='checkbox' value='1' @if($configuration->global_product_pre_order == 1) {{"checked"}} @endif id='global_product_pre_order'><label for='global_product_pre_order'></label></span></th>
							<th>Minimum Date Interval<span  data-toggle="tooltip" title="If Global, Then Minimum Date Interval in General Settings will be considered else Minimum Date Interval below will be considered" class='onoff global'><input name='global_product_date_interval' type='checkbox' value='1' @if($configuration->global_product_date_interval == 1) {{"checked"}} @endif id='global_product_date_interval'><label for='global_product_date_interval'></label></span></th>
							<th>Cut-off Time<br><span data-toggle="tooltip" title="If Global, Then Cut-off Time in General Settings will be considered else Cut-off Time below will be considered"  class='onoff global'><input name='global_product_cut_off' type='checkbox' value='1' @if($configuration->global_product_cut_off == 1) {{"checked"}} @endif id='global_product_cut_off'><label for='global_product_cut_off'></label></span></th>
						</tr>
					</thead>
					<tbody>                
					</tbody>                                
				</table>					
			</form>					
		</div>
	</div>
</div>
@endsection
@section('pageScript')  
<script>    
//data table with custom filter and feature   
$(document).ready(function(){  
        var blocked_dates = '';
	$('[data-toggle="tooltip"]').tooltip();   
	var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var product_settings = <?php echo $product_settings ?>;
	var count = <?php echo $count ?>;        
	var table = $('#product_settings').DataTable({
		processing:   	true,
		searching: 		true,
		serverSide:   	true,
		lengthChange: 	true,
		stateSave: 		true,
		"ajax": {
			"url"	 	: "product-data",
			"dataSrc"	: "data",
			'data': {
           		shop: "{{ $shop }}",           		
        	}
		},
		"columns": [
			{ "className": "dt-center", "data": null, "name": "select_product", 
				fnCreatedCell: function (nTd, sData, oData, iRow, iCol)	
				{  					
					var checked = "";	
					if(count > 0)
					{    						
						if(product_settings[oData.product_id])
						{
							if(product_settings[oData.product_id].status == 1)
							{							
								var checked = "checked";	
							}
							$(nTd).html("<span class='onoff'><input name='selected_product[]'" + checked + " type='checkbox' value='"+ oData.product_id +"' id='checkbox_"+ oData.product_id +"'><label for='checkbox_"+ oData.product_id +"'></label></span>");  
						}
						else
						{
							$(nTd).html("<span class='onoff'><input name='selected_product[]'" + checked + " type='checkbox' value='"+ oData.product_id +"' id='checkbox_"+ oData.product_id +"'><label for='checkbox_"+ oData.product_id +"'></label></span>");
						}
					}
					else
					{
						$(nTd).html("<span class='onoff'><input name='selected_product[]' type='checkbox' value='"+ oData.product_id +"' id='checkbox_"+ oData.product_id +"'><label for='checkbox_"+ oData.product_id +"'></label></span>");
					}
				}
			},
			{ "className": "dt-center", "data": null, "name": "product_image", 
				fnCreatedCell: function (nTd, sData, oData, iRow, iCol)	
				{
					$(nTd).html("<img src='"+ oData.product_image +"' height=40 width=40>");
				}
			},
			{ "className": "dt-center", "data": "product_name", "name": "product_name" },	
			{ "className": "dt-center", "data": null, "name": "delivery_time", 
				fnCreatedCell: function (nTd, sData, oData, iRow, iCol){     						
					var checked="";
					var disabled;
					var global_delivery_time = {{ $configuration->global_product_delivery_time }} ;
					if(global_delivery_time == 1)
					{
						disabled = "disabled";
					}					
					var delivery_times = "";
					if(product_settings[oData.product_id])
					{
						delivery_times = product_settings[oData.product_id].delivery_times;		
					}	
					else
					{
						delivery_times = "";
					}				
					$(nTd).html('<textarea name="delivery_time_' + oData.product_id + '"' + disabled + '   class="form-control delivery_time_class" rows="5" >' + delivery_times + '</textarea>');
				}
			},
			{ "className": "dt-center", "data": null, "name": "blocked_dates",
				fnCreatedCell: function (nTd, sData, oData, iRow, iCol)	{     			
					if(count > 0)
					{
						if(product_settings[oData.product_id])
						{
							/* blocked_dates = $.parseJSON(product_settings[oData.product_id].blocked_dates); */
                                                        if(product_settings[oData.product_id].blocked_dates != null){
                                                            blocked_dates = (product_settings[oData.product_id].blocked_dates).split(",");
                                                        }
							
							
							var checked="";
							var disabled="";
							var global_blocked_dates = {{ $configuration->global_product_blocked_dates }} ;
							if(global_blocked_dates == 1)
							{
								disabled = "disabled";
							}
							if(blocked_dates.length > 0)
							{							
								dates = '<div class="blocked-dates blocked_dates_class" id="blocked-dates-' + oData.product_id + '"><div class="form-group date-box"><input type="text" ' + disabled + ' value="' + blocked_dates[0] + '" name="blockeddate_' + oData.product_id + '[]" class="blockeddate form-control" style="width:70%"/></div>';
								for(i=1; i < blocked_dates.length; i++)
								{
									/* dates = dates + '<div class="form-group date-box"><input value="' + blocked_dates[i] + '" type="text" name="blockeddate_' + oData.product_id + '[]" class="blockeddate form-control" style="width:70%"/></div><a class="removerow"><img src="{{ asset("/image/close-icon.png") }}"/></a>'; */
									dates = dates + '<div class="form-group date-box"><input value="' + blocked_dates[i] + '" type="text"  ' + disabled + ' name="blockeddate_' + oData.product_id + '[]" class="blockeddate form-control" style="width:70%;"><a class="removerow"><img src="{{ asset("/image/close-icon.png") }}"/></a></div>';
								}
								$(nTd).html(dates + '</div><div class="form-group"><a class="addrow" data-product-id="'+ oData.product_id +'"><i class="fa fa-plus"></i>Add Date(s)</a></div>');
							}
							else
							{							
								$(nTd).html('<div class="blocked-dates blocked_dates_class" id="blocked-dates-' + oData.product_id + '"><div class="form-group date-box"><input type="text" ' + disabled + '  name="blockeddate_' + oData.product_id + '[]" class="blockeddate form-control" style="width:100%"/></div></div><div class="form-group"><a class="addrow" data-product-id="'+ oData.product_id +'"><i class="fa fa-plus"></i>Add Date(s)</a></div>');
							}
						}
						else
						{
							$(nTd).html('<div class="blocked-dates blocked_dates_class" id="blocked-dates-' + oData.product_id + '"><div class="form-group date-box"><input type="text" ' + disabled + '  name="blockeddate_' + oData.product_id + '[]" class="blockeddate form-control" style="width:100%"/></div></div><div class="form-group"><a class="addrow" data-product-id="'+ oData.product_id +'"><i class="fa fa-plus"></i>Add Date(s)</a></div>');	
						}
                    }
					else
					{						
						$(nTd).html('<div class="blocked-dates blocked_dates_class" id="blocked-dates-' + oData.product_id + '"><div class="form-group date-box"><input type="text" ' + disabled + ' name="blockeddate_' + oData.product_id + '[]" class="blockeddate form-control" style="width:100%"/></div></div><div class="form-group"><a class="addrow" data-product-id="'+ oData.product_id +'"><i class="fa fa-plus"></i>Add Date(s)</a></div>');	
					}					
				}
			},
			{ "className": "dt-center", "data": null, "name": "blocked_days", 
				fnCreatedCell: function (nTd, sData, oData, iRow, iCol){     	
					var options, selected;
					var disabled="";
					var global_blocked_days = {{ $configuration->global_product_blocked_days }} ;
					if(global_blocked_days == 1)
					{
						disabled = "disabled";
					}
					if(product_settings[oData.product_id])
					{
						/* blocked_days = $.parseJSON(product_settings[oData.product_id].blocked_days); */
						if(product_settings[oData.product_id].blocked_days)
						{
                                                        if(product_settings[oData.product_id].blocked_days != null){                                                            
                                                            blocked_days = (product_settings[oData.product_id].blocked_days).split(",");
                                                        }
							if($.inArray("all_allow", blocked_days) != -1)
							{
								options = '<option selected value="all_allow">All Allows</option>';					
								for(i=0; i < days.length; i++)
								{							
									options = options + '<option value="' + days[i] + '">' + days[i] + '</option>';						
								}
							}
							else
							{
								options = '<option value="all_allow">All Allows</option>';
								for(i=0; i < days.length; i++)
								{
									selected = "";
									if($.inArray(days[i], blocked_days) != -1)
									{
										selected = "selected";
									}								
									options = options + '<option ' + selected + ' value="' + days[i] + '">' + days[i] + '</option>';						
								}
							}
						}
						else
						{
							options = '<option value="all_allow">All Allows</option>';	
							for(i=0; i < days.length; i++)
							{							
								options = options + '<option value="' + days[i] + '">' + days[i] + '</option>';						
							}
						}
					}
					else
					{
						options = '<option selected  value="all_allow">All Allows</option>';
						for(i=0; i < days.length; i++)
						{
							options = options + '<option value="' + days[i] + '">' + days[i] + '</option>';						
						}
					}											
					$(nTd).html('<input type="hidden" name="all_product_ids[]" value="' + oData.product_id + '"><select ' + disabled + ' name="days_'+ oData.product_id +'[]" size="8" multiple class="form-control blocked_days_class">' + options + '</select>');
				}
			},
			{ "className": "dt-center", "data": null, "name": "pre_order_time", 
				fnCreatedCell: function (nTd, sData, oData, iRow, iCol)	{                                    
					var disabled="";
					var global_product_pre_order = {{ $configuration->global_product_pre_order }} ;
					if(global_product_pre_order == 1)
					{
						disabled = "disabled";
					}
					var pre_order_time = "";
                    if(count > 0)
					{
					if(product_settings[oData.product_id])
					{	    
                                                pre_order_time = product_settings[oData.product_id].pre_order_time;   
//						if(product_settings[oData.product_id].status)
//						{
//                                                    
//							pre_order_time = product_settings[oData.product_id].pre_order_time;                                                        
//							
//							if(!pre_order_time)
//							{
//								pre_order_time = "";
//							}
//						}
						$(nTd).html('<input class="form-control pre_order_time_class" type="number" ' + disabled + ' value="'+ pre_order_time +'" name="allowed_month_' + oData.product_id + '" min="1" style="width:70px;" />');
						}
						else
						{
							$(nTd).html('<input class="form-control pre_order_time_class" ' + disabled + '  type="number" name="allowed_month_' + oData.product_id + '" min="1" style="width:70px;" />');
						}
					}
					else
					{
						$(nTd).html('<input class="form-control pre_order_time_class" ' + disabled + '  type="number" name="allowed_month_' + oData.product_id + '" min="1" style="width:70px;" />');
					}
				}
			},
			{ "className": "dt-center", "data": null, "name": "date_interval", 
				fnCreatedCell: function (nTd, sData, oData, iRow, iCol){
					var disabled="";
					var global_product_date_interval = {{ $configuration->global_product_date_interval }} ;
					if(global_product_date_interval == 1)
					{
						disabled = "disabled";
					} 
					var date_interval = "";
					if(product_settings[oData.product_id])
					{
						date_interval = product_settings[oData.product_id].date_interval;						
						if(!date_interval)
						{
							date_interval = "";
						}
					}
					$(nTd).html('<input class="form-control date_interval_class" ' + disabled + '  type="number" min="0" value="'+ date_interval +'" name="intervel_' + oData.product_id + '" style="width:70px;">');
				}
			},
			{ "className": "dt-center", "data": null, "name": "cut_off_time", 				
				fnCreatedCell: function (nTd, sData, oData, iRow, iCol){
					var disabled="";
					var global_product_cut_off = {{ $configuration->global_product_cut_off }} ;
					if(global_product_cut_off == 1)
					{
						disabled = "disabled";
					}  
					var cut_off_hours, cut_off_minutes, hours_options = "", minutes_options = "";					
					if(product_settings[oData.product_id])
					{
						cut_off_hours = product_settings[oData.product_id].cut_off_hours;
						cut_off_minutes = product_settings[oData.product_id].cut_off_minutes;
						for(h = 0; h < 24; h++) 
						{ 
							var selected = "";
							if(cut_off_hours == h)
							{
								selected = "selected";
							}	
							hours_options = hours_options + '<option ' + selected + ' value="' + pad(h) + '">' + pad(h) + '</option>';		
						}					
						for(m = 0; m < 60; m++) 
						{ 
							var selected = "";
							if(cut_off_minutes == m)
							{
								selected = "selected";
							}	
							minutes_options = minutes_options + '<option ' + selected + ' value="' + pad(m) + '">' + pad(m) + '</option>';
						}
					}
					else
					{		
						for(h = 0; h < 24; h++) 
						{ 
							hours_options = hours_options + '<option value="' + pad(h) + '">' + pad(h) + '</option>';		
						}					
						for(m = 0; m < 60; m++) 
						{ 
							minutes_options = minutes_options + '<option value="' + pad(m) + '">' + pad(m) + '</option>';
						}					
					}					
					$(nTd).html('<select ' + disabled + ' class="form-control cutoff_hour_class" name="hour_' + oData.product_id +'"><option value="">Select Hour</option>' + hours_options + '</select><br><select ' + disabled + '  class="form-control cutoff_minute_class" name="minute_' + oData.product_id +'"><option value="">Select Minute</option>' + minutes_options + '</select>');
				}			
			}
		],
		/* dom: '<Blf<t>ip>', */
		order: [[0, 'desc']],		
		pageLength: 10,
		lengthMenu: [10, 25, 50, 75, 100],
		initComplete: function(settings, json){
		},
		preDrawCallback: function(settings){
			$.ajax({
				url:"check-data",
				type:"POST",
				async:false,
				data:{data:$('#product_settings_form').serialize()},
				success:function()
				{
					$.ajax({
						url:"get-product-data",
						type:"POST",
						async:false,
						data:{data:$('#product_settings_form').serialize()},
						success:function(data)
						{
							product_settings = $.parseJSON(data);
						}					
					}); 
				}					
			}); 
		},
		drawCallback: function(settings) {
        	$(".addrow").click(function(){		
				var product_id = $(this).data("product-id");	
				var new_add = 	'<div class="form-group date-box">' +
								'<input type="text" name="blockeddate_' + product_id + '[]" class="blockeddate form-control" style="width:70%;">' +				
								'<a class="removerow"><img src="{{ asset("/image/close-icon.png") }}"/></a>' +
								'</div>';
				$('#blocked-dates-'+product_id).append(new_add);
				$(".blockeddate").datepicker({
					minDate: +0,
					dateFormat: 'd/m/yy',
					beforeShow: function() {
						setTimeout(function(){
								$('.ui-datepicker').css('z-index', 99999999999999);
						}, 0);
					}
				});
				$(".removerow").on('click', function () {
					$(this).parent('div').remove();
				});
			});	
			$(".blockeddate").datepicker({
				minDate: +0,
				dateFormat: 'd/m/yy',
				beforeShow: function() {
					setTimeout(function(){
							$('.ui-datepicker').css('z-index', 99999999999999);
					}, 0);
				}
			});
			$(".removerow").on('click', function () {
				$(this).parent('div').remove();
			});		
    	}
	});	
    var custom_div = "";
    $("#product_settings_filter").append(custom_div);
	$("#product_settings_filter").css("float","right");         	
	/* 
	$('#product_settings thead th').each(function(i){		
		var title = $(this).text();
		if(title == "Product Name")
		{
			$(this).css("max-width", "110px");
			$(this).html('<input type="text" style="max-width:110px;" placeholder="Search ' + title + '" />');
			$('input', this).on('keyup change', function(){
				if(table.column(i).search() !== this.value) 
				{
					table
						.column(i)
						.search(this.value)
						.draw();
				}
			});
		}
	}); 
	*/
	setTimeout(function(){
		$(".addrow").click(function(){		
		var product_id = $(this).data("product-id");	
		var new_add = 	'<div class="form-group date-box">' +
						'<input type="text" name="blockeddate_' + product_id + '[]" class="blockeddate form-control" style="width:70%;">' +				
						'<a class="removerow"><img src="{{ asset("/image/close-icon.png") }}"/></a>' +
						'</div>';
			$('#blocked-dates-'+product_id).append(new_add);
			$(".blockeddate").datepicker({
				minDate: +0,
				dateFormat: 'd/m/yy',
				beforeShow: function() {
					setTimeout(function(){
							$('.ui-datepicker').css('z-index', 99999999999999);
					}, 0);
				}
			});
			$(".removerow").on('click', function () {
				$(this).parent('div').remove();
			});
		});	
		$(".blockeddate").datepicker({
			minDate: +0,
			dateFormat: 'd/m/yy',
			beforeShow: function() {
				setTimeout(function(){
						$('.ui-datepicker').css('z-index', 99999999999999);
				}, 0);
			}
		});
		$(".removerow").on('click', function () {
			$(this).parent('div').remove();
		});		
	},6000);		
});
function pad(number)
{
	if(number < 10) 
		return "0" + number;
	else
		return number;
}

	setTimeout(function(){
		var length_content = $("#product_settings_length").html();
		var filter_content = $("#product_settings_filter").html();
		// $("#product_settings_length").html(filter_content);
		// $("#product_settings_filter").html(length_content);	
	},500);		
	$("#global_product_delivery_time").change(function(){		
		setTimeout(function(){
			var checked = $("#global_product_delivery_time").prop("checked");			
			if(checked)
			{
				$(".delivery_time_class").prop('disabled', true);
			}
			else
			{
				$(".delivery_time_class").prop('disabled', false);
			}
		}, 5000);
	});
	$("#global_product_blocked_dates").change(function(){
		setTimeout(function(){	
			var checked = $("#global_product_blocked_dates").prop("checked");			
			if(checked)
			{
				$(".blockeddate").prop("disabled", true);
			}
			else
			{
				$(".blockeddate").prop("disabled", false);
			}
		}, 5000);
	});
	$("#global_product_blocked_days").change(function(){
		setTimeout(function(){			
			var checked = $("#global_product_blocked_days").prop("checked");		
			if(checked)
			{
				$(".blocked_days_class").prop("disabled", true);
			}
			else
			{
				$(".blocked_days_class").prop("disabled", false);
			}
		}, 5000);
	});				
	$("#global_product_pre_order").change(function(){		
		setTimeout(function(){
			var checked = $("#global_product_pre_order").prop("checked");		
			if(checked)
			{
				$(".pre_order_time_class").prop("disabled", true);
			}
			else
			{
				$(".pre_order_time_class").prop("disabled", false);
			}		
		}, 5000);		
	});	
	$("#global_product_date_interval").change(function(){
		setTimeout(function(){
			var checked = $("#global_product_date_interval").prop("checked");		
			if(checked)
			{
				$(".date_interval_class").prop("disabled", true);
			}
			else
			{
				$(".date_interval_class").prop("disabled", false);
			}
		}, 5000);	
	});
	$("#global_product_cut_off").change(function(){
		setTimeout(function(){
			var checked = $("#global_product_cut_off").prop("checked");		
			if(checked)
			{
				$(".cutoff_hour_class").prop("disabled", true);
				$(".cutoff_minute_class").prop("disabled", true);
			}
			else
			{
				$(".cutoff_hour_class").prop("disabled", false);
				$(".cutoff_minute_class").prop("disabled", false);
			}
		}, 5000);	
	});
</script>
@endsection