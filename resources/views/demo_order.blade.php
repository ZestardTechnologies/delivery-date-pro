<?php
use Carbon\Carbon;
?>
@extends('header')
@section('content')   
	<table width="100%" class="display" id="example" cellspacing="0">
        <thead>
            <tr>
				<th>Number</th>
				<th>Date </th>
				<th>Shop</th>                   
            </tr>
        </thead>        
        <tbody>
            
        </tbody>
    </table>
<script>
$(document).ready(function(){			
	$('#example').DataTable({
		serverSide: true,
		processing:true,
		pagingType: "full_numbers",
		ajax:{
			url :"testing", 
			type: "post",
			data:{ _token: "{{ csrf_token() }}",start:"0",length:"10"},			
			error: function(){  							
			}
		},
		columns: [
			{ mData: "id" },
			{ mData: "date_format" },
			{ mData: "shop_id" }               
        ],
		dom: "frtiS",
		scrollY: 200,
		deferRender: true,
		scrollCollapse: true,
		scroller: {
			loadingIndicator: true
		}
	});
	/* var table = $('#example').DataTable({
		"processing": true,
		"serverSide": true,
		"type": "POST",        
		"PageLength" : 10,		
            "ajax":{
                    "url": "testing",
                    "dataType": "json",
                    "type": "POST",					
                    "data":{ _token: "{{ csrf_token() }}",start:"0",length:"5"}
                   },
				"columns": [
                 { mData: "id" },
				{ mData: "date_format" },
				{ mData: "shop_id" }               
            ]	
	}); */
	
	/* $.post("testing",{ _token: "{{ csrf_token() }}",start:"0",length:"5"},function(html){alert(html)});
	$('#example').DataTable({				
		"ajax":"testing",		
		"type":"post",
		"data":{ _token: "{{ csrf_token() }}",start:"0",length:"5"},
		"aocolumns": [
        { mData: "id" },
        { mData: "date_format" },
        { mData: "shop_id" }
		]		
		});*/	
	}); 
</script>
@endsection