@extends('header')
@section('content')
<style>
	.top-marquee{
		display:none;
	}
</style>
<script type="text/javascript">
ShopifyApp.ready(function(){
	ShopifyApp.Bar.initialize({
	  buttons: {			
		secondary: [{
		  label: 'HELP',
		  href : '{{ url('/help') }}',
		  loading: true
		},
		{
		  label: 'MANAGE ORDER',
		  href : '{{ url('/order') }}',
		  loading: true
		},{
		  label: 'SETTINGS',
		  href : '{{ url('/dashboard') }}',
		  loading: true
		}]
	  }
	});
});
$(document).ready(function(){    
  $('html, body').animate({
    scrollTop: $('.pricing_table_div').offset().top
  }, 'slow');
});
</script>
<link rel="stylesheet" href="{{ asset('css/custom.css') }}"/>
  <div class="container formcolor">		
	<br>		
		<div class="pricing_table_block">
			<div class="pricing_table_div">				
				<div class="pricing_table_item pricing_middle_item margin_right">
					<div class="ribbon ribbon-small text-white">					   
					</div>
					<h1 class="pricing_table_title">Ultimate</h1>
					<div class="pricing_table_price_div">
						<span class="doller_icon">$</span><h2>15.99</h2><span class="month_text"> / m</span>
					</div>
					<div class="pricing_table_list">
						<ul>
							<li>All Basic Features</li>		
							<li>Delivery Date & Time for each Product</li>		
						</ul>
					</div>
					<div class="select_btn">				
						<form action="ultimate" method="post">
							{{ csrf_field() }}
							<input name="four" type="hidden" value="4">	
            	<input name="shop" type="hidden" value="{{ $shop }}">
							</input>
							<button class="plan_select">Select</button>
						</form>
					</div>
				</div>				
			</div>
		</div>                       
  </div>
@endsection