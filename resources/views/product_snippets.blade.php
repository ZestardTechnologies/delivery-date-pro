<?php echo "{% if template == 'product' %}" ?>
<script>
  	product_id = <?php echo "{{ product.id }}" ?>  	    
</script>          	
<input type="hidden" id="ztpl-current-date" value=<?php echo "{{ 'now' | date: '%d' }}"?> />
<input type="hidden" id="ztpl-store-hour" value=<?php echo "{{ 'now' | date: '%H' }}"?> />
<input type="hidden" id="ztpl-store-minute" value=<?php echo "{{ 'now' | date: '%M' }}"?> />
<input type="hidden" id="ztpl-date-format-ddmmyy" value=<?php echo "{{ 'now' | date: '%d-%m-%Y' }}"?> />
<input type="hidden" id="ztpl-date-format-yymmdd" value=<?php echo "{{ 'now' | date: '%Y-%m-%d' }}"?> />
<div id="datepicker_box" class="additional-css">
	<p class="date-box">
		<label for="delivery-date-pro"></label>
		<input id="delivery-date-pro" type="text" name="properties[Delivery-Date]" readonly="readonly" disabled ></input>
	</p>
	<span id="selected_format"></span>
	<label id="delivery-time-label" style="display:none" ></label>
	<select id="delivery-time" name="properties[Delivery-Time]" style="display:none" readonly="readonly">
		<option id="time_option_label" value=""></option>
	</select>
	<span id="admin_notes"></span>
</div>
<?php echo "{{ 'https://zestardshop.com/shopifyapp/DeliveryDatePro/public/js/product_snippet.js' | script_tag }}" ?>
<?php echo "{{ 'https://zestardshop.com/shopifyapp/DeliveryDatePro/public/css/zestard-deliverydatepro-snippet.css' | stylesheet_tag }}" ?>
<?php echo "{{ 'https://zestardshop.com/shopifyapp/DeliveryDatePro/public/css/jquery-ui.css' | stylesheet_tag }}" ?>
<?php echo "{% endif %}" ?>    