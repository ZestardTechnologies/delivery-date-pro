@extends('header')
@section('content')
<?php
	$shop = session('shop');
?>
<script type="text/javascript">	
	ShopifyApp.ready(function(){
		ShopifyApp.Bar.initialize({
		  buttons: {
			primary: {
				label: 'SAVE',
				message : 'form_submit',		
				callback:  function(event){ validations(event) } 
			},
			secondary: [{
			  label: 'HELP',
			  href : '{{ url('/help') }}',
			  loading: true
			},
			{
			  label: 'MANAGE ORDER',
			  href : "{{ url('order?store='.$shop) }}",
			  loading: true
			},{
			  label: 'SETTINGS',
			  href : "{{ url('dashboard?shop='.$shop) }}",
			  loading: true
			},{
                        label: 'CHANGE VERSION',
                        href: '{{ url("change-plans") }}',
                        loading: true
            }]
		  }
		});
	});
</script>
<?php
$date = json_decode($config['block_date']);
$interval = $config['date_interval'];
$datepicker_display_on = $config['datepicker_display_on'];
$alloved_month = $config['alloved_month'];
$days = json_decode($config['days']);
$id = $config['shop_id'];
$hours = $config['hours'];
$minute = $config['minute'];
$app_title = $config['app_title'];
$show_date_format = $config['show_date_format'];
$date_format = $config['date_format'];
$app_status = $config['app_status'];
$datepicker_label = $config['datepicker_label'];
$cutoff_status = $config['cuttoff_status'];
$notes_message = $config['admin_order_note'];
$additional_css = $config['additional_css'];
$time_label = $config['time_label'];
$time__option_label = $config['time_default_option_label'];
$delivery_time = $config['delivery_time'];
$exclude_block_date_status = $config['exclude_block_date_status'];
if ($date_format == "mm/dd/yy") {
    $arrange_format = "m/d/Y";
} else if ($date_format == "yy/mm/dd") {
    $arrange_format = "Y/m/d";
} else if ($date_format == "dd/mm/yy") {
    $arrange_format = "d/m/Y";
}
?>

<div id="dashboard_div" class="container formcolor">  
    <p class="review">
        Are you loving this app? Please spend 2mins to help us <a href="https://apps.shopify.com/delivery-date-pro?reveal_new_review=true" target="_blank">write a review.</a>
    </p>
    @if(Session::has('success'))
    <p class="alert alert-success">{{ Session::get('success') }}<i class="fa fa-close" style="float: right;cursor: pointer;"></i></p>
    @endif
    @if(Session::has('error'))
    <p class="alert alert-danger">{{ Session::get('error') }}<i class="fa fa-close" style="float: right;cursor: pointer;"></i></p>
    @endif
    <ul class="nav nav-tabs dashboard_tabs">
        <li class="active"><a href="{{ url('dashboard?shop='.$shop) }}">General Settings</a></li>
        <li><a href="{{ url('cut-off?shop='.$shop) }}">Cut Off Settings</a></li>
        <li><a href="{{ url('delivery-time?shop='.$shop) }}">Delivery Time Settings</a></li>
    </ul>
    <form action="{{ url('edit/'.$id) }}" name="config" method="post" id="delivery_date_form" data-shopify-app-submit="">			                     
        <br/>
        <h2 class="sub-heading">General Settings</h2>
        <div class="row formcolor_row">
            <div class="col-sm-4 padding_left">
                <strong>App Active?</strong>
                <span class="onoff"><input name="app_status" @if($app_status == "Active") checked @endif type="checkbox" value="Active" id="checkboxID0"><label for="checkboxID0"></label></span>
            </div>                				
            <div class="col-sm-4 padding_middle"><strong>Blocked Date(s)</strong></div>	
            <div class="col-sm-4 padding_right">
                <strong>Cut Off Time&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; Active </strong>
                <span class="onoff" >
                    <input name="cuttoff_time" type="checkbox" value="cuttoff_on" @if($config['cuttoff_status'] == "1") checked @endif id="checkboxID11" name="cuttoff_time">
                           <label for="checkboxID11"></label>
                </span>
            </div>	
        </div>
        <div class="row formcolor_row">
            <div class="col-sm-4 padding_left">
                <div class="dateformat_option">
                    <div><strong>Select Datepicker Language</strong></div>
                    <div>                            
                        <select name="language_locale" class="form-control">       
                            <option value="sq" @if($config['language_locale'] == "sq") selected @endif >Albanian (Gjuha shqipe)</option>
                            <option value="ar" @if($config['language_locale'] == "ar") selected @endif >Arabic (&#8235;(&#1604;&#1593;&#1585;&#1576;&#1610;</option>
                            <option value="hy" @if($config['language_locale'] == "hy") selected @endif >Armenian (&#1344;&#1377;&#1397;&#1381;&#1408;&#1381;&#1398;)</option>
                            <option value="bg" @if($config['language_locale'] == "bg") selected @endif >Bulgarian (&#1073;&#1098;&#1083;&#1075;&#1072;&#1088;&#1089;&#1082;&#1080; &#1077;&#1079;&#1080;&#1082;)</option>
                            <option value="ca" @if($config['language_locale'] == "ca") selected @endif >Catalan (Catal&agrave;)</option>
                            <option value="zh-CN" @if($config['language_locale'] == "zh-CN") selected @endif >Chinese Simplified (&#31616;&#20307;&#20013;&#25991;)</option>
                            <option value="zh-TW" @if($config['language_locale'] == "zh-TW") selected @endif >Chinese Traditional (&#32321;&#39636;&#20013;&#25991;)</option>
                            <option value="hr" @if($config['language_locale'] == "hr") selected @endif >Croatian (Hrvatski jezik)</option>
                            <option value="cs" @if($config['language_locale'] == "cs") selected @endif >Czech (Ce&ouml;tina)</option>
                            <option value="da" @if($config['language_locale'] == "da") selected @endif >Danish (Dansk)</option>
                            <option value="nl" @if($config['language_locale'] == "nl") selected @endif >Dutch (Nederlands)</option>
                            <option value="eo" @if($config['language_locale'] == "eo") selected @endif >Esperanto</option>
                            <option value="en" @if($config['language_locale'] == "en") selected @endif >English</option>
                            <option value="fa" @if($config['language_locale'] == "fa") selected @endif >Farsi/Persian (&#8235;(&#1601;&#1575;&#1585;&#1587;&#1740;</option>
                            <option value="fi" @if($config['language_locale'] == "fi") selected @endif >Finnish (suomi)</option>
                            <option value="fr" @if($config['language_locale'] == "fr") selected @endif >French (Fran&ccedil;ais)</option>
                            <option value="de" @if($config['language_locale'] == "de") selected @endif >German (Deutsch)</option>
                            <option value="el" @if($config['language_locale'] == "el") selected @endif >Greek (&#917;&#955;&#955;&#951;&#957;&#953;&#954;&#940;)</option>
                            <option value="he" @if($config['language_locale'] == "he") selected @endif >Hebrew (&#8235;(&#1506;&#1489;&#1512;&#1497;&#1514;</option>
                            <option value="hu" @if($config['language_locale'] == "hu") selected @endif >Hungarian (Magyar)</option>
                            <option value="is" @if($config['language_locale'] == "is") selected @endif >Icelandic (&Otilde;slenska)</option>
                            <option value="id" @if($config['language_locale'] == "id") selected @endif >Indonesian (Bahasa Indonesia)</option>
                            <option value="it" @if($config['language_locale'] == "it") selected @endif >Italian (Italiano)</option>
                            <option value="ja" @if($config['language_locale'] == "ja") selected @endif >Japanese (&#26085;&#26412;&#35486;)</option>
                            <option value="ko" @if($config['language_locale'] == "ko") selected @endif >Korean (&#54620;&#44397;&#50612;)</option>
                            <option value="lv" @if($config['language_locale'] == "lv") selected @endif >Latvian (Latvie&ouml;u Valoda)</option>
                            <option value="lt" @if($config['language_locale'] == "lt") selected @endif >Lithuanian (lietuviu kalba)</option>
                            <option value="ms" @if($config['language_locale'] == "ms") selected @endif >Malaysian (Bahasa Malaysia)</option>
                            <option value="no" @if($config['language_locale'] == "no") selected @endif >Norwegian (Norsk)</option>
                            <option value="pl" @if($config['language_locale'] == "pl") selected @endif >Polish (Polski)</option>
                            <option value="pt-BR" @if($config['language_locale'] == "pt-BR") selected @endif >Portuguese/Brazilian (Portugu&ecirc;s)</option>
                            <option value="ro" @if($config['language_locale'] == "ro") selected @endif >Romanian (Rom&acirc;n&#259;)</option>
                            <option value="ru" @if($config['language_locale'] == "ru") selected @endif >Russian (&#1056;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081;)</option>
                            <option value="sr" @if($config['language_locale'] == "sr") selected @endif >Serbian (&#1089;&#1088;&#1087;&#1089;&#1082;&#1080; &#1112;&#1077;&#1079;&#1080;&#1082;)</option>
                            <option value="sr-SR" @if($config['language_locale'] == "sr-SR") selected @endif >Serbian (srpski jezik)</option>
                            <option value="sk" @if($config['language_locale'] == "sk") selected @endif >Slovak (Slovencina)</option>
                            <option value="sl" @if($config['language_locale'] == "sl") selected @endif >Slovenian (Slovenski Jezik)</option>
                            <option value="es" @if($config['language_locale'] == "es") selected @endif >Spanish (Espa&ntilde;ol)</option>
                            <option value="sv" @if($config['language_locale'] == "sv") selected @endif >Swedish (Svenska)</option>
                            <option value="th" @if($config['language_locale'] == "th") selected @endif >Thai (&#3616;&#3634;&#3625;&#3634;&#3652;&#3607;&#3618;)</option>
                            <option value="tr" @if($config['language_locale'] == "tr") selected @endif >Turkish (T&uuml;rk&ccedil;e)</option>
                            <option value="uk" @if($config['language_locale'] == "uk") selected @endif >Ukranian (&#1059;&#1082;&#1088;&#1072;&#1111;&#1085;&#1089;&#1100;&#1082;&#1072;)</option>
                        </select>		
                        </select> 
                    </div>
                </div>
                <div class="datepicker_label">
                    <strong>Show Datepicker label on Website?</strong>
                    <span class="onoff">
                        <input name="show_datepicker_label" type="checkbox" value="1" @if($config['show_datepicker_label'] == 1) checked @endif id="checkboxID6">
                               <label for="checkboxID6"></label>
                    </span>
                </div>
                <div class="datepicker_label">
                    <div><strong>Datepicker label</strong></div>
                    <div><input type="text" id="datepicker_label" name="datepicker_label" value="{{ $config['datepicker_label'] }}" class="form-control"></div>
                </div>
                <div class="datepicker_validate">
                    <div>
                        <strong>Delivery Date is Required Field?</strong>
                        <span class="onoff"><input name="require_option" type="checkbox" @if($config['require_option'] == 1) checked @endif value="1" id="checkboxID1"/>
                                                   <label for="checkboxID1"></label></span>
                    </div>                        
                </div>  
                <div class="datepicker_label">
                    <div><strong>Validation Message When Delivery Date is Required</strong></div>
                    <div><input type="text" id="date_error_message" name="date_error_message" value="{{ $config['date_error_message'] }}" class="form-control"></div>
                </div>                    
                <div class="datepicker_validate">                        
                    <strong>Show Date Format on Website?</strong>
                    <span class="onoff"><input name="show_date_format" type="checkbox" value="1" @if($config['show_date_format'] == 1) checked @endif id="checkboxID2"><label for="checkboxID2"></label></span>                         
                </div>
                <div class="dateformat_option">
                    <div><strong>Date Format</strong></div>
                    <div>
                        <select name="date_format" class="form-control">
                            <option value="mm/dd/yy" @if($date_format == "mm/dd/yy") selected @endif >mm/dd/yyyy</option>
                            <option value="yy/mm/dd" @if($date_format == "yy/mm/dd") selected @endif >yyyy/mm/dd</option>
                            <option value="dd/mm/yy" @if($date_format == "dd/mm/yy") selected @endif >dd/mm/yyyy</option>
                        </select> 
                    </div>
                </div>
                <div class="datepicker_validate">
                    <div>
                        <strong>Show Time on Front?</strong>
                        <span class="onoff"><input name="admin_time_status" type="checkbox" @if($config['admin_time_status'] == 1) checked @endif value="1" id="checkboxID5"><label for="checkboxID5"></label></span>
                    </div>                       
                </div>
                <div class="datepicker_validate">
                    <div><strong>Time is Required Field?</strong>
                        <span class="onoff"><input name="time_require_option" type="checkbox" @if($config['time_require_option'] == 1) checked @endif value="1" id="checkboxID4"><label for="checkboxID4"></label></span>
                    </div>                        
                </div>
                <div class="datepicker_label">
                    <div><strong>Validation Message When Delivery Time is Required</strong></div>
                    <div><input type="text" id="time_error_message" name="time_error_message" value="{{ $config['time_error_message'] }}" class="form-control"></div>
                </div>
                <div class="datepicker_label">
                    <div><strong>Delivery Time Title</strong></div>
                    <div><input type="text" name="time_label" value="{{ $time_label }}" class="form-control"></div>
                </div>
                <div class="datepicker_label">
                    <div><strong>Time Default option label</strong></div>
                    <div><input type="text" name="time_default_option_label" value="{{ $time__option_label }}" value="" class="form-control"></div>
                </div>
                <div class="order_note">
                    <div><span class="note"><strong>Delivery Time</strong> (Add <b>","</b>(comma) separated values)</span></div>
                    <div>
                        <textarea id="delivery_time" name="delivery_time" class="form-control" rows="5" >{{ $delivery_time }}</textarea>
                    </div>
                </div>					                     	
            </div>
            <div class="col-sm-4 padding_middle">	
                <div class="main-date" id="main-date">                                              
                    <?php if (!empty($date)) { ?>
                        @foreach ($date as $date_selected)
                        <?php if (!empty($date_selected)): ?>							
                            <div class="form-group date-box">								
                                @if($arrange_format == "m/d/Y")
                                <?php
                                $date_array = explode("-", $date_selected);
                                $new_date = $date_array[1] . "/" . $date_array[0] . "/" . $date_array[2];
                                ?>
                                <input type="text" name="blockdate[]" class="blockeddate form-control" value="{{ $new_date }}" style="width:70%;">
                                @else	
                                <input type="text" name="blockdate[]" class="blockeddate form-control" value="{{ date($arrange_format, strtotime($date_selected)) }}" style="width:70%;"/>
                                @endif
                                <!--input type="text" name="blockdate[]" class="blockeddate form-control" value="{{ date($arrange_format, strtotime($date_selected)) }}" style="width:70%;"-->
                                <input type="hidden" name="saveblockdate[]" class="saveblockeddate form-control" value="{{ date('d/m/Y', strtotime($date_selected)) }}">
                                <a class="removerow"><img src="{{ asset('/image/close-icon.png') }}"/></a>
                            </div>
                        <?php else: ?>
                            <div class="form-group date-box">
                                <input type="text" name="blockdate[]" class="saveblockdate form-control">
                                                                <!-- <input type="hidden" name="saveblockdate[]" class="saveblockeddate form-control"> -->
                            </div>
                        <?php endif; ?>
                        @endforeach
                    <?php } else { ?>
                        <div class="form-group date-box">
                            <input type="text" name="blockdate[]" class="blockeddate form-control" style="width:70%;"/>
                            <!--input type="text" name="saveblockdate[]" class="saveblockdate form-control"-->
                        </div>
                    <?php } ?>                      
                </div>
                <div class="form-group">
                    <a class="addrow"><i class="fa fa-plus"></i>Add Date(s)</a>
                </div>
                <div class="note">
                    <strong>Note:</strong>List of dates that will be blocked for delivery so user won't be able to select it for the order delivery.
                </div>						
                <div>   
                    <span class="onoff">
                        <input type="checkbox" value="1" @if($config['exclude_block_date_status'] == 1) checked @endif id="checkboxID12" name="exclude_block_date_status">
                               <label for="checkboxID12"></label>
                    </span> 
                    <strong>Add Block Days Interval</strong>  
                </div>   
                <div class="note" style="margin-top: 0;margin-bottom: 10px;">
                    <strong>Note:</strong> Use "Add Block Days", if you wish to add extra days for interval due to Block Days.
                </div>
                <br>
                <br>
                <strong>Block Day(s)</strong>
                <div class="form-group block_days">
                    <select name="days[]" size="8" multiple class="form-control">
                        <option value="all_allow"@if($days == "") selected @endif >All Allows</option>
                        <option value="Sunday" @if($days != "")@if(in_array("Sunday", $days)) selected @endif @endif>Sunday</option>
                        <option value="Monday" @if($days != "")@if(in_array("Monday", $days)) selected @endif @endif >Monday</option>
                        <option value="Tuesday" @if($days != "")@if(in_array("Tuesday", $days)) selected @endif @endif >Tuesday</option>
                        <option value="Wednesday" @if($days != "")@if(in_array("Wednesday", $days)) selected @endif @endif >Wednesday</option>
                        <option value="Thursday" @if($days != "")@if(in_array("Thursday", $days)) selected @endif @endif >Thursday</option>
                        <option value="Friday" @if($days != "")@if(in_array("Friday", $days)) selected @endif @endif >Friday</option>
                        <option value="Saturday" @if($days != "")@if(in_array("Saturday", $days)) selected @endif @endif >Saturday</option>
                    </select>
                </div>
                <div class="note">
                    <strong>Note:</strong> Selected day will be blocked and user won't be able to select it as a delivery date.
                </div>					
                <div><strong>Allowed Pre-order Time</strong></div>
                <div class="form-group margin_bottom_0">
                    <input class="form-control" type="number" value="{{ $config['alloved_month'] }}" name="allowed_month" min="1" id="example-number-input" style="width:70px;" />
                </div>
                <div class="note">
                    <strong>Note:</strong> This option will allow user to book order before "X" number of Months on the Website.
                </div> 					                                  
                <br />
                <div><strong>Minimum Date Interval</strong></div>
                <div class="form-group margin_bottom_0">
                    <input class="form-control" type="number" min="0" value="{{ $config['date_interval'] }}" name="intervel" id="example-number-input" style="width:70px;">
                </div>
                <div class="note">
                    <strong>Note:</strong> For "Today Delivery" it should be "0" otherwise it should be the interval of days from the current day to allow user to select delivery date.
                </div>	
                <br/>                    
                <div class="datepicker_validate">
                    <div>
                        <strong>Show Calendar visible by default</strong>
                        <span class="onoff">
                            <input name="datepicker_display_on" type="checkbox" @if($config['datepicker_display_on'] == "1") checked @endif value="1" id="checkboxID8"/>
                                   <label for="checkboxID8"></label>
                        </span>                            
                    </div>
                </div>
                <div class="main-date" id="main-date">
                    <div>
                        <strong>Next available date set as a selected</strong>
                        <span class="onoff">
                            <input name="default_date" type="checkbox" @if($config['default_date_option'] == "1") checked @endif value="1" id="checkboxID9" name="default_date"/>
                                   <label for="checkboxID9"></label>
                        </span>                            
                    </div>
                </div>
                <br>  
                <div class="">
                    <strong>Add Delivery Information under Order Instructions</strong>
                    <span class="onoff">
                        <input name="add_delivery_information" type="checkbox" value="1" @if($config['add_delivery_information'] == "1") checked @endif id="checkboxID10" name="add_delivery_information"/>
                               <label for="checkboxID10"></label>
                    </span>                            					                    	
                </div> 	
            </div>
            <div class="col-sm-4 padding_right">
                <div class="cuttofftime_box">
                    <div class="display_block">
                        <label for="hours">Hour</label>
                        <select name="hours" class="form-control">
                            <?php for ($h = 00; $h < 24; $h++) { ?>
                                <option value="<?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?>" @if($h == $hours) selected @endif ><?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="display_block">&nbsp;&nbsp;<strong>:</strong>&nbsp;&nbsp;</div>
                    <div class="display_block">
                        <label for="minute">Minute</label>
                        <select name="minute" class="form-control">
                            <?php for ($m = 00; $m <= 59; $m++) { ?>
                                <option value="<?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?>" @if($m == $minute) selected @endif><?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="note">
                    <strong>Note:</strong> For "Next Day Delivery" if the cut off time is enable & set then it will allow user to select day after the "Interval Date" as first available delivery date.
                </div>                
                <div class="order_note"><strong>Show Notes on Website?</strong>
                    <span class="onoff"><input name="admin_note_status" @if($config['admin_note_status'] == 1) checked @endif type="checkbox" value="1" id="checkboxID3"/>
                                               <label for="checkboxID3"></label></span>
                </div>
                <div class="order_note">
                    <div><strong>Notes</strong></div> 
                    <div>
                        <textarea id="admin_order_note" name="admin_order_note" class="form-control" rows="5" >{{ $notes_message }}</textarea>
                        @ckeditor('admin_order_note',['height' => 150])
                    </div>
                </div>
                <!--br>
                <div class="order_note">
                    <div><strong>Disable checkout button on cart page till app is loaded completely?</strong></div>
                    <div>
                        <span class="onoff" >
                                                        <input name="disable_checkout" type="checkbox" value="1" @if($config['disable_checkout'] == "1") checked @endif id="disable_checkout" name="cuttoff_time">
                                                        <label for="disable_checkout"></label>
                                                </span>
                    </div> 
                    <div class="note">
                        <strong>Note:</strong> If this option is enabled, Then customers won't be able to click on checkout button till app is loaded completely & it will not let customers to checkout without selecting delivery date(If delivery date is mandatory).
                    </div> 
                </div>
                <br-->
                <div class="order_note">
                    <div><strong>Additional CSS</strong></div>
                    <div>
                        <textarea name="additional_css" class="form-control" rows="4" >{{ $additional_css }}</textarea>
                    </div> 
                    <div class="note">
                        <strong>Note:</strong> You can add CSS For Example: .body {margin:0px;}
                    </div> 
                </div>
            </div>  
        </div>  
    </form>
    <div class="col-md-12 formcolor sticky_formcolor" >
        <div class="shortcode_heading col-sm-6">
            <h2 class="sub-heading subleft col-md-3">Shortcode</h2>
            <div class="col-sm-9">
                <div class="copystyle_wrapper">
                    <textarea rows="1" class="form-control script_code" id="script_code" disabled><?php echo "{% include 'delivery-date' %}" ?></textarea>
                    <btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_script()"><i class="fa fa-check"></i> Copy</btn>
                </div>
            </div>
        </div>
        <?php
        if (Session::has('shop')) {
            $url = "https://" . session('shop') . "/admin/themes/current/?key=templates/cart.liquid";
        } else {
            $url = "#";
        }
        ?>
        <?php
        if (Session::has('shop')) {
            $url1 = "https://" . session('shop') . "/admin/themes/current/?key=sections/cart-template.liquid";
        } else {
            $url1 = "#";
        }
        ?>                           
        <div class="shortcode_heading col-sm-6">
            <h2 class="slide_down sub-heading subleft col-md-12">Where to paste Shortcode?<i class="fa fa-chevron-up"></i></h2>
            <div class="col-sm-12" id="shortcode_info">
                <?php
                if (Session::has('shop')) {
                    $url = "https://" . session('shop') . "/admin/themes/current/?key=templates/cart.liquid";
                } else {
                    $url = "#";
                }
                ?>
                <?php
                if (Session::has('shop')) {
                    $url1 = "https://" . session('shop') . "/admin/themes/current/?key=sections/cart-template.liquid";
                } else {
                    $url1 = "#";
                }
                ?>
                <ul class="shortcode-note">
                    <li>Copy and paste the shortcode in your <a href="<?php echo $url1; ?>" target="_blank"><b>Cart Template</b></a> see <a class="info_css" href="{{ asset('image/info_delivery date.png') }}"><b>Example</b></a>
                    </li>
                    <li>If you dont find form tag in your cart page so you have to paste the short code in the 
                        <a href="<?php echo $url; ?>" target="_blank"><b>cart page</b></a> see <a class="info_css" href="{{ asset('image/Delivery date short code.png') }}"><b>Example</b></a>
                    </li>
                    <li>
                        If your cart page is not opening as url https://your-store-name/cart, Then Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right
                    </li>
                </ul>           
            </div>
        </div> 
        <div class="shortcode_heading col-sm-6">
            <h2 class="sub-heading subleft col-md-3">Do you want us to paste the Shortcode?</h2>
            <div class="col-sm-9">
                <div class="copystyle_wrapper">                        
                    <select class="validation form-control select_page cur_point select_template_page" name="select_template_page" id="select_template_page">
                        <option value="">Select Template Page</option>
                        <option value="1">Cart Page</option>                        
                    </select>
                </div>
            </div>
        </div>

        <div class="shortcode_heading col-sm-6">
            <input type="submit" class="btn btn-primary submit-loader" id="shortcode_pase_template" name="BtnPutShortcode" value="Put Shortcode in Template">
        </div> 

        <div style="display: none;">
            <p><b>Paste Shortcode in Cart Template</b></p>
            <div class="">
                <form action="{{ url('snippet-create-cart') }}" id="RemoveCartVariants" name="config" method="post" class="submitForm"> 
                    {{ csrf_field() }}
                    <button class="btn btn-primary" type="submit" name="snippet_cart">Paste Shortcode in Cart</button>
                </form>
            </div>

            <br>
        </div> 
    </div>      

</div>
<div class="modal fade" id="upgrade_modal">
    <div class="modal-dialog">          
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Note</b></h4>
            </div>
            <div class="modal-body">
                <p>Dear Customer, We have created one new version of the app <b>Ultimate</b>, which will let you to configure delivery date and time for each product. For upgrading to <b>Ultimate</b> version, click on <b>Upgrade</b> button below. If you face any issue(s) on your store, before uninstalling, Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP.</p>
                <div class="text-center"><a class="btn btn-info" href="feature-upgrade?shop={{$shop}}">Upgrade</a></div>
            </div>        
            <div class="modal-footer">			
                <div class="datepicker_validate" id="modal_div">
                    <div>
                        <strong>Show me this again</strong>
                        <span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again_upgrade"/>
                            <label for="dont_show_again_upgrade"></label></span>
                    </div>      
                </div>      
            </div>      
        </div>
    </div>
</div>
<script type="text/javascript">
    var upgrade_status = "{{ $upgrade_status }}";
    var upgrade_modal_status = "{{ $upgrade_modal_status }}";
    // if (upgrade_modal_status == "Y")
    // {
    //     if (upgrade_status == "N")
    //     {
    //         $('#upgrade_modal').modal('show');
    //     }
    // }
    $(document).ready(function () {
        $("#dont_show_again_upgrade").change(function () {
            var checked = $(this).prop("checked");
            var shop_name = "{{ session('shop') }}";
            if (!checked)
            {
                $.ajax({
                    url: 'upgrade-modal-status',
                    data: {shop_name: shop_name},
                    async: false,
                    type: 'POST',
                    success: function (result)
                    {

                    }
                });
                $('#upgrade_modal').modal('toggle');
            }
        });
    });
    $(function () {
        $(".blockeddate").datepicker({
            minDate: +0,
            dateFormat: '<?php echo $date_format; ?>',
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
        });

        $(".saveblockdate").datepicker({
            minDate: +0,
            dateFormat: '<?php echo 'dd/mm/yy'; ?>',
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
        });

        var table = $('#example').DataTable({
            lengthChange: false,
            dom: 'Bfrtip',
            buttons: ['csvHtml5']
        });

        table.buttons().container().appendTo('#example_wrapper .col-sm-6:eq(0)');
    });
    //for adding the new row
    $(".addrow").on('click', function () {
        var new_add = '<div class="form-group date-box">' +
                '<input type="text" name="blockdate[]" class="blockeddate form-control" style="width:70%;">' +
                '<a class="removerow"><img src="{{ asset("/image/close-icon.png") }}"/></a>' +
                '</div>';
        $('#main-date').append(new_add);
        rundatepicker();
        runremoverow();
    });
    function rundatepicker() {
        $(".blockeddate").datepicker({
            minDate: +0,
            dateFormat: '<?php echo $date_format; ?>',
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
        });

        $(".saveblockdate").datepicker({
            minDate: +0,
            dateFormat: '<?php echo 'dd/mm/yy'; ?>',
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
        });
    }
    function runremoverow() {
        $(".removerow").on('click', function () {
            $(this).parent('div').remove();
        });
    }
    $(".removerow").on('click', function () {
        $(this).parent('div').remove();
    });

    $("#shortcode_pase_template").click(function () {
        var template_value = $('#select_template_page').val();
        if (template_value == '1') {
            $("#RemoveCartVariants").submit();
            $('#select_template_page').css('border', '');
            $(".submit-loader").attr("disabled", "disabled");
            $(".submit-loader").css("padding-left", "25px");
        } else {
            $('#select_template_page').css('border', '1px solid red');
            return false;
        }
    });    
</script>
<style>
    .datepicker_validate{
        margin-top: 10px;
        margin-bottom: 10px;
    }  
    .validate_text{
        margin-top: 10px;
    }
</style>

@endsection