@extends('header')
@section('content')
<?php
	$shop = session('shop');
?>
<script type="text/javascript">
	ShopifyApp.ready(function(){
		ShopifyApp.Bar.initialize({
		  buttons: {
			primary: {
			  label: 'SAVE',
			  message: 'form_submit',
			  loading: true,
			},
			secondary: [{
			  label: 'HELP',
			  href : '{{ url('/help') }}',
			  loading: true
			},
			{
			  label: 'MANAGE ORDER',
			  href : "{{ url('order?store='.$shop) }}",
			  loading: true
			},{
			  label: 'SETTINGS',
			  href : "{{ url('dashboard?shop='.$shop) }}",
			  loading: true
			}]
		  }
		});
  });
</script>
<?php

$cutoff_status = $config['cuttoff_status'];

$disabled_status="";
if($cutoff_status==0)
{
	$disabled_status="disabled";	
}
$i=0;

?>
<div class="container formcolor cutoff_block">      
	<ul class="nav nav-tabs dashboard_tabs">
		<li><a href="{{ url('dashboard?shop='.$shop) }}">General Settings</a></li>
		<li class="active"><a href="{{ url('cut-off?shop='.$shop) }}">Cut Off Settings</a></li>
		<li><a href="{{ url('delivery-time?shop='.$shop) }}">Delivery Time Settings</a></li>
	</ul>				
	<?php $days=array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'); ?>
	<div class="row formcolor_row">	
	<h2 class="sub-heading">Cut Off Settings</h2>		
	<form method="post" action="{{ url('save-cut-off?shop='.$shop) }}" data-shopify-app-submit="form_submit" class="cutoff_div">
	{{ csrf_field() }}
	<div class="text-right">
		<div class=" cutoff_time_checkbox">
		<span class="onoff">
			<input type="checkbox" @if($global_flag==1) checked @endif name="global_cutoff_time" id="global_cutoff_time" value="1"/>
			<label for="global_cutoff_time"></label>
		</span>
		<strong>Use Global Cut Off Time</strong>
		<div class="note">
			<strong>Note:</strong> If You Enable Global Cut Off Time, Then Cut Off Time(In General Settings) Will Be Global For All Days
		</div>
		</div>
		<span style="font-weight:0;font-size:13px;">
		@if($cutoff_status==0)
			<div class=" note"><b style="font-size: 10px;">{{ "Cut Off is Inactive, If you want to Edit, Please Go to General Settings and Active Cut Off." }} </b></div>
		@endif
		</span>
	</div>
	<table id="cut_off" class="table table-bordered delivery_time_table cutoff_table" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th class="col-md-4"><span class="col-md-12">Day</span></th>							
			<th class="col-md-8"><span class="col-md-12">Cut Off Time</span></th>							          
		</tr>
	</thead>
	<tbody>					
	@foreach($days as $delivery_day)
	@if(empty($settings[$i]))
	<tr>
		<td class="col-md-4">
			<span class="col-md-12">{{ $delivery_day }}</span>
		</td>
		<td class="col-md-8">
		<div class="display_block col-md-3" >
				<label for="hours">Hour:   </label>
				<select {{ $disabled_status }} name="hour[]" class="form-control cutoff_hour">								
				<option selected value="">Select Hour</option>
					<?php
						for ($h = 00; $h < 24; $h++) 
						{		
					?>  	
						<option value="<?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?></option>
					<?php
						}										
					?>
				</select>
			</div>
			<div class="display_block col-md-3" >
				<label for="minute">Minute:   </label>
				<select {{ $disabled_status }} name="minute[]" class="form-control cutoff_minute">
				<option selected value="">Select Minute</option>
					<?php
						for ($m = 00; $m <= 59; $m++) 
						{
					?>								
						<option value="<?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?></option>								
					<?php
						}
					?>
				</select>
			</div>
		</div>
		</td>
	</tr>
	@else
		
	<?php   
		$day=$settings[$i];
		$hour = $day->cutoff_hour;
		$minute = $day->cutoff_minute; 
	?>								
	<tr>
		<td class="col-md-4">
			<span class="col-md-12">{{ $delivery_day }}</span>
		</td>
		<td class="col-md-8">
		<div class="" >
			<div class="display_block col-md-3" >
				<label for="hours">Hour:   </label>
				<select {{ $disabled_status }} name="hour[]" class="form-control cutoff_hour">								
				<option selected value="">Select Hour</option>
					<?php
						for ($h = 00; $h < 24; $h++) 
						{		
					?>  	
						<option @if(str_pad($h, 2, "0", STR_PAD_LEFT) == $hour) {{ "selected" }} @endif value="<?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?></option>
					<?php
						}										
					?>
				</select>
			</div>
			<div class="display_block col-md-3" >
				<label for="minute">Minute:   </label>
				<select {{ $disabled_status }} name="minute[]" class="form-control cutoff_minute">
				<option selected value="">Select Minute</option>
					<?php
					for ($m = 00; $m <= 59; $m++) {
						?>
							
						<option @if(str_pad($m, 2, "0", STR_PAD_LEFT) == $minute) {{ "selected" }} @endif value="<?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?></option>
						
						<?php
					}
					?>
				</select>
			</div>
		</div>
		</td>
	</tr>
	@endif
	<?php $i++; ?>
	@endforeach						
	</tbody>					
	</table>
	<!--div class="row text-center">
		<input type="submit" class="btn btn-primary" value="Save"/>
	</div-->
	</form>
</div>
<!--div class="container formcolor">      
		<ul class="nav nav-tabs dashboard_tabs">
			<li><a href="dashboard">General Settings</a></li>
			<li class="active"><a href="cut-off">Cut Off Settings</a></li>
			<li><a href="delivery-time">Delivery Time Settings</a></li>
		</ul>	
		<br>
            <h2 class="sub-heading">Cut Off Setting</h2>			
			<?php $days=array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'); ?>			
			<form method="post" action="save-cut-off" data-shopify-app-submit="form_submit">
			{{ csrf_field() }}
			<div class="text-right"><input type="checkbox" @if($global_flag==1) checked @endif name="global_cutoff_time" id="checkboxID0" value="1"/><label>&nbsp;Use Global Cut Off Time&nbsp;&nbsp;</label><br>
			<div class="note">If You Enable Global Cut Off Time, Then Cut Off Time(In General Settings) Will Be Global For All Days </div>
				<span style="font-weight:0;font-size:13px;"> @if($cutoff_status==0) <div class=" note"><b> {{ "Cut Off is Inactive, If you want to Edit, Please Go to General Settings and Active Cut Off." }} 
				</b>		
				</div> @endif
				</span>
			</div>
			<table id="cut_off" class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Day</th>							
					<th>Cut Off Time<span style="font-weight:0;font-size:12px;width:80%;left:1%;position:relative"><i>  </i></span></th>							          
				</tr>
			</thead>
			<tbody>					
			@foreach($days as $delivery_day)
			@if(empty($settings[$i]))
			<tr>
				<td>
				{{ $delivery_day }}
				</td>
				<td class="col-md-6">
				<div class="cuttofftime_box" >
					<div class="display_block " >
						<label for="hours">Hour:   </label>
						<select {{ $disabled_status }} name="hour[]" class="form-control cutoff_hour">								
						<option selected value="">Select Hour</option>
							<?php
								for ($h = 00; $h < 24; $h++) 
								{		
							?>  	
								<option value="<?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?></option>
							<?php
								}										
							?>
						</select>
					</div>
					<div class="display_block" >
						<label for="minute">Minute:   </label>
						<select {{ $disabled_status }} name="minute[]" class="form-control cutoff_minute">
						<option selected value="">Select Minute</option>
							<?php
								for ($m = 00; $m <= 59; $m++) 
								{
							?>								
								<option value="<?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?></option>								
							<?php
								}
							?>
						</select>
					</div>
				</div>
				</td>
			</tr>
			@else
				
			<?php   
				$day=$settings[$i];
				$hour = $day->cutoff_hour;
				$minute = $day->cutoff_minute; 
			?>											
			<tr>
				<td>
				{{ $delivery_day }}
				</td>
				<td class="col-md-6">
				<div class="cuttofftime_box" >
					<div class="display_block " >
						<label for="hours">Hour:   </label>
						<select {{ $disabled_status }} name="hour[]" class="form-control cutoff_hour">								
						<option selected value="">Select Hour</option>
							<?php
								for ($h = 00; $h < 24; $h++) 
								{		
							?>  	
								<option @if(str_pad($h, 2, "0", STR_PAD_LEFT) == $hour) {{ "selected" }} @endif value="<?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?></option>
							<?php
								}										
							?>
						</select>
					</div>
					<div class="display_block" >
						<label for="minute">Minute:   </label>
						<select {{ $disabled_status }} name="minute[]" class="form-control cutoff_minute">
						<option selected value="">Select Minute</option>
							<?php
							for ($m = 00; $m <= 59; $m++) {
								?>
									
								<option @if(str_pad($m, 2, "0", STR_PAD_LEFT) == $minute) {{ "selected" }} @endif value="<?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?></option>
								
								<?php
							}
							?>
						</select>
					</div>
				</div>
				</td>
			</tr>
			@endif
			<?php $i++; ?>
			@endforeach						
			</tbody>					
			</table>			
			</form>
</div-->		
<style>
    .datepicker_validate{
        margin-top: 10px;
        margin-bottom: 10px;
    }  
    .validate_text{
        margin-top: 10px;
    }
	#cut_off{
		width:100%;
		
	}
	#cut_off tr{
		height:10%;
		
	}
	.display_block{
		display:inline-block;
	}
</style>

@endsection