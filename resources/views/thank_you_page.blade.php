@extends('header')
@section('content')
<?php
	$shop = session('shop');
?>
<script type="text/javascript">	
	ShopifyApp.ready(function(){
		ShopifyApp.Bar.initialize({
		  buttons: {
			primary: {
				label: 'SAVE',
				message : 'form_submit',		
				callback:  function(event){ validations(event) } 
			},
			secondary: [{
			  label: 'HELP',
			  href : '{{ url('/help') }}',
			  loading: true
			},
			{
			  label: 'MANAGE ORDER',
			  href : "{{ url('order?store='.$shop) }}",
			  loading: true
			},{
			  label: 'SETTINGS',
			  href : "{{ url('dashboard?shop='.$shop) }}",
			  loading: true
			}]
		  }
		});
	});
</script>
<div id="thank_you_div" class="container formcolor">  
    <div class="col-md-12 formcolor">
        <div class="shortcode_heading col-sm-6">
        <!--h2 class="sub-heading subleft col-md-3">Code</h2-->
            <br>
            <div class="col-sm-12">
                <div class="copystyle_wrapper">
                    <textarea rows="23" class="form-control additional_script_code" id="additional_script_code" readonly><?php echo '<div class="content-box delivery-date-pro-box"><div id="datepicker_box" class="additional-css"><p class="date-box"><label for="delivery-date-pro"></label><input id="delivery-date-pro" type="text" name="attributes[Delivery-Date]" value="" readonly="readonly" disabled /></p><span id="selected_format"></span><br><br><label id="delivery-time-label" style="display:none"></label><select id="delivery-time" name="attributes[Delivery-Time]" style="display:none" readonly="readonly"><option id="time_option_label" value=""></option></select><span id="admin_notes"></span><button name="checkout" class="btn btn-info submit_button" >Submit</button></div><script>var delivery_date_attribute = "{{attributes.Delivery-Date}}";</script><script src="https://zestardshop.com/shopifyapp/DeliveryDatePro/public/js/zestard_jquery_3.3.1.js" type="text/javascript"></script><script type="text/javascript" src="https://zestardshop.com/shopifyapp/DeliveryDatePro/public/js/jquery-ui.js"></script><link href="https://zestardshop.com/shopifyapp/DeliveryDatePro/public/css/zestard-deliverydatepro-snippet.css"></link><link href="https://zestardshop.com/shopifyapp/DeliveryDatePro/public/css/jquery-ui.css"></link><link href="https://zestardshop.com/shopifyapp/DeliveryDatePro/public/css/order_confirmation.css"></link><script src="https://zestardshop.com/shopifyapp/DeliveryDatePro/public/js/snippet_order_confirmation.js"></script></div></div>' ?>
                  </textarea>
                </div>
              </div>    
              <div class="col-md-12 formcolor">
                <btn id="copy_additional_script" name="copy_additional_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target="#additional_script_code" style="display: block;"><i class="fa fa-check"></i> Copy</btn>
              </div>
            </div>
              <div class="col-md-6">
                <h2><b>Follow below steps to paste the copied code(on the left) on thank you page.</b></h2>

                <p><b>Step 1</b> :- Click on <b>Settings</b> on the bottom left.</p>
                <p><b>Step 2</b> :- Then click on <b>Checkout</b> under settings section.</a> Refer <a class="info_css" href="{{ asset('image/settings_checkout.png') }}"><b>Example</b></a></p>
                <p><b>Step 3</b> :- Scroll down to <b>Additional Script</b> Section and paste above the code. Refer <a class="info_css" href="{{ asset('image/additional_scripts.png') }}"><b>Example</b></a></p>
              </div>              
            </div>
            <script>
              toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
              }              
                var button = document.getElementById("copy_additional_script"),
                contentHolder = document.getElementById("additional_script_code");

                button.addEventListener("click", function() {
                  var copyText = document.getElementById("additional_script_code");
                  copyText.select();
                  console.log(copyText);              
                  console.log(document.execCommand("copy"));
                  toastr.success("Code Copied Successfully");
                });
            </script>
@endsection