<?php echo "{% assign hide_delivery_date = 0 %}
{% assign show_delivery_date = 0 %}
{% for item in cart.items %}
	{% if item.product.tags contains 'no-delivery-date' %}
   		{% assign hide_delivery_date = 0 %}           	
	{% else %}
        {% assign show_delivery_date = 1 %}
    {% endif %}              	
{% endfor %}"; ?>
<link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<?php echo "{% if hide_delivery_date == 0 and show_delivery_date == 1 %}"; ?>
<div id="datepicker_box">
  <p class="date-box">
    <label for="delivery-date-pro"></label>
    <input id="delivery-date-pro" type="text" name="attributes[Delivery-Date]" value="<?php echo "{{ cart.attributes.Delivery-Date }}"?>" readonly="readonly" disabled />
  </p>
  <span id="selected_format"></span>
  <label id="delivery-time-label" style="display:none" ></label>
  <select id="delivery-time" name="attributes[Delivery-Time]" style="display:none" readonly="readonly">
  <option id="time_option_label" value=""></option>
  </select>
  <span id="admin_notes"></span>
</div>
<?php echo "{% endif %}"; ?>
<script>
$zestard_jq = jQuery.noConflict();
$zestard_jq(document).ready(function(){
  var shop_name = "<?php echo "{{ shop.permanent_domain }}"?>";
  var unavailableDates = [];
  var unavailableDays = [];
  var start_from = '';
  var allowed_month = '';
  var date_formate = '';
  var check_out_form = $zestard_jq("#delivery-date-pro").closest("form").attr('class');
  
  if(check_out_form) {
    var check_out_class_array = check_out_form.split(' ');
    if($zestard_jq.inArray( "cart-form", check_out_class_array ) < 0)
    {
      $zestard_jq("#delivery-date-pro").closest("form").addClass( "cart-form" );
    }
  }
  else{
    $zestard_jq("#delivery-date-pro").closest("form").addClass( "cart-form" );
  }
  
  var is_date_required = false;
  var is_time_required = false;
  $zestard_jq.ajax({
		url: "https://zestardshop.com/shopifyapp/merge_delivery_date_demo/public/getconfig",
		dataType: "json",
    	data:{shop:shop_name},
		success: function(data) {
          var app_status = data[0].app_status;
          datepicker_on_default = data[0].datepicker_display_on;
          datepicker_default_date = data[0].default_date_option;
          if(app_status == "Deactive")
          {
            jQuery("#datepicker_box").remove();
          }
          if(data[0].require_option == 1)
          {
            is_date_required = true;
            $zestard_jq("#delivery-date-pro").attr("required", "true");
            $zestard_jq("#delivery-date-pro").closest("form").removeAttr('novalidate');
          }
          var date_label = data[0].datepicker_label;
          $zestard_jq('.date-box label').text(date_label);
          var dates = data[0].block_date;
		  var day = data[0].days;
		  start_from = '+'+data[0].date_interval;
		  allowed_month = '+'+data[0].alloved_month+'M';
		  unavailableDates = $zestard_jq.parseJSON(dates);
		  unavailableDays = $zestard_jq.parseJSON(day);
          date_formate = data[0].date_format;
          
          var cutoff_status = data[0].cuttoff_status;
          var cutoff_hours = data[0].hours;
          var cutoff_minute = data[0].minute;
          
          var current_date = "<?php echo "{{ 'now' | date".":"." '%d' }}"?>";
          
          var store_hour = "<?php echo "{{ 'now' | date".":"." '%H' }}"?>"; 
          
          var store_minute =  "<?php echo "{{ 'now' | date".":"." '%M' }}"?>";
         
          
          
          if(date_formate == "mm/dd/yy"){
            var display_format = "(mm/dd/yyyy)";
          }
          else if(date_formate == "yy/mm/dd")
          {
            var display_format = "(yyyy/mm/dd)";
          }
          else if(date_formate == "dd/mm/yy")
          {
            var display_format = "(dd/mm/yyyy)";
          }
          else
          {
            var display_format = "(mm/dd/yyyy)";
          }

          var show_date_format = data[0].show_date_format;
          if(show_date_format == 1){
            if(display_format != "")
            {
              $zestard_jq("#selected_format").text(display_format);
            }
            else
            {
              $zestard_jq("#selected_format").text('mm/dd/yyyy');
            }
          }
          var admin_note_status = data[0].admin_note_status;
          var notes_admin = data[0].admin_order_note;
          if(admin_note_status == 1)
          {
            $zestard_jq("#admin_notes").html($zestard_jq.parseHTML(notes_admin));
          }
          
          var admin_time_status = data[0].admin_time_status;
          var time_lable = data[0].time_label;
          var default_option_label = data[0].time_default_option_label;
          if(admin_time_status == 1)
          {
            var time_in_text = data[0].delivery_time;
          	var time_array = new Array();
            time_array = time_in_text.split(",");
            var time_count = 0;
            $zestard_jq("#delivery-time").show();
            $zestard_jq("#time_option_label").text(default_option_label);
            $zestard_jq(time_array).each(function() {
            $zestard_jq('#delivery-time').append("<option value='"+time_array[time_count]+"'"+">"+time_array[time_count]+"</option>");
              time_count++;
            });
            $zestard_jq("#delivery-time-label").show();
            $zestard_jq("#delivery-time-label").text(time_lable);
            
            if(data[0].time_require_option == 1)
            {
              is_time_required = true;
              $zestard_jq("#delivery-time").attr("required", "true");
              $zestard_jq("#delivery-date-pro").closest("form").removeAttr('novalidate');
            }
          }

          var str = parseInt(data[0].date_interval);
          
          if(parseInt(cutoff_status) == 0)
          {          
            if(parseInt(store_hour) <= parseInt(cutoff_hours))
            {
              if(parseInt(store_hour) < parseInt(cutoff_hours))
              {
                  	str = parseInt(data[0].date_interval);
              }
              else 
              {
                	if(parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute))
              		{
              			str = parseInt(data[0].date_interval);
              		}
                  else
                  {
                    	str = parseInt(data[0].date_interval)+1;
                  }
              }
            }
            else
            {
              str = parseInt(data[0].date_interval)+1;
            }
          }
          
          start_from = '+'+parseInt(str);

          if(parseInt(data[0].exclude_block_date_status) == 1)
          {
          //start
          var block_date_array = JSON.parse(dates);          
          var ddmmyy = "<?php echo "{{ 'now' | date: '%d-%m-%Y' }}" ?>";
          var yymmdd = "<?php echo "{{ 'now' | date: '%Y-%m-%d' }}" ?>";
          var date_index = parseInt(0);
          
          var reloop = function (ddmmyy,yymmdd){
            
            if(jQuery.inArray(ddmmyy,block_date_array) !== -1){              	
             	
              	start_from = parseInt(start_from)+1;
              
              	var NextDayData = new Date(new Date(yymmdd).getFullYear(),new Date(yymmdd).getMonth(),new Date(yymmdd).getDate()+1);
              
              	var date = new Date(NextDayData).getDate();
              	var month = new Date(NextDayData).getMonth()+1;
              	var year = new Date(NextDayData).getFullYear();            
              	
              	var NextDayDMY = date+'-'+month+'-'+year;
              	var NextDayYMD = year+'-'+month+'-'+date;              	
              	
              	return reloop(NextDayDMY,NextDayYMD);          	
            }            
          }  
          
          reloop(ddmmyy,yymmdd);
          }

          var additional_css = data[0].additional_css;
          $zestard_jq('.additional-css').html("<style>" +additional_css+ "</style>");
		}
	  });
setTimeout(function(){
		var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

		function unavailable(date) {
			ymd = date.getFullYear() + "/" + ("0"+(date.getMonth()+1)).slice(-2) + "/" + ("0"+date.getDate()).slice(-2);
            ymd1 = ("0"+date.getDate()).slice(-2) + "-" + ("0"+(date.getMonth()+1)).slice(-2) + "-" + date.getFullYear();
			day = new Date(ymd).getDay();
			if ($zestard_jq.inArray(ymd1, unavailableDates) < 0 && $zestard_jq.inArray(days[day], unavailableDays) < 0) {
				return [true, "enabled", "Book Now"];
			} else {
				return [false,"disabled","Booked Out"];
			}
		}
  setTimeout(function(){
    $zestard_jq("#delivery-date-pro").datepicker( { 
                        dateFormat: date_formate ,
			minDate: start_from, 
			maxDate: "'"+allowed_month+"'",
			beforeShowDay: unavailable
			});
                        
				if(datepicker_default_date == 1)
				{
				  $zestard_jq("#delivery-date-pro").datepicker("setDate", "today + start_from" );

				}

				if(datepicker_on_default == 1)
				{
				  $zestard_jq("#delivery-date-pro").datepicker("show");

				}
                        
  }, 300)
  $zestard_jq("#delivery-date-pro").prop( "disabled", false );   
	}, 3000);
        
        setTimeout(function(){ $zestard_jq("input[name=checkout]").removeAttr('disabled'); }, 3000);  
	  $zestard_jq('.cart-form').on('submit', function(){        
        //alert($('#delivery-date-pro').val());
		if(is_date_required == true && ((typeof $zestard_jq('#delivery-date-pro').val() == 'undefined') || ($zestard_jq('#delivery-date-pro').val() == '')))
		{
		  alert("Please Select Delivery Date");
		  $zestard_jq('#delivery-date-pro').focus();
		  return false;
		}
        if(is_time_required == true && ((typeof $zestard_jq('#delivery-time').val() == 'undefined') || ($zestard_jq('#delivery-time').val() == '')))
        {
          alert("Please Select Delivery Time");
          $zestard_jq('#delivery-time').focus();
          return false;
        }
	  });
        
});
</script>
<style>
  #selected_format{
        display: block;
  	    margin-left: 2px;
    	margin-top: 5px;
    	font-size: 11px;
    	font-style: italic;
        text-align: left;
  }
  #admin_notes{
        display: block;
    	margin-bottom: 20px;
    	font-size: 12px;
    	font-style: italic;
        text-align: left;
  }
  #datepicker_box {
    width: auto;
    text-align: center;
    display: inline-block;
  }
  #datepicker_box .date-box{
    display: inline-block;
    margin: 0px;
    text-align: left;
  }
</style>
<div class="additional-css"></div>