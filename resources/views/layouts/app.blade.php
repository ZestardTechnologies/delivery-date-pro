<!DOCTYPE html>
<html lang="en">
    <head>
       <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <title>{{ session('shop') }}</title>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}"> 
        <!--Import materialize.css-->
        <!--<link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"  media="screen,projection"/>-->
        <!-- Datepicker CSS -->
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
        <!-- Select2 CSS -->
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <!-- Notification -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <!-- magnificent popup CSS -->
        <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}"> 
        @yield('pageCss')
        <!-- jquery script -->
        <script src="{{ asset('js/zestard_jquery_3.3.1.js') }}"></script>

        <!-- Latest compiled JavaScript -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>    

        <!-- JavaScript for datepicker -->
        <script src="{{ asset('js/jquery-ui.js') }}"></script>

        <!-- jquery datatable script -->
        <script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/datatable/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('js/datatable/dataTables.buttons.min.js') }}"></script>
        <!--script src="{{ asset('js/datatable/date_range_plugin.js') }}"></script-->

        <!-- Select2 js -->
        <script src="{{ asset('js/select2.min.js') }}"></script>

        <!-- Notification -->
        <script src="{{ asset('js/toastr.min.js') }}"></script>

        <!-- Magnificent popup js -->
        <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>    

        <!-- Custom js -->
        <script src="{{ asset('js/javascript.js') }}"></script>

        <!-- shopify Script for fast load -->
        <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
        <script>
            ShopifyApp.init({
                apiKey: '2551a481974989b8ebaef60daccf5e76',
                shopOrigin: '<?php echo "https://" . session('
                shop '); ?>'
            });
            ShopifyApp.ready(function() {
                ShopifyApp.Bar.initialize({
                    icon: '',
                    title: 'Management',
                    buttons: {}
                });
        });
        </script>
        <!-- Js add by dipal (add general setting js in custom.js) -->
        <script src="{{ asset('js/custom.js') }}"></script>
    </head>
    <body>        
            @include('layouts.header1')
            @yield('content')
            @include('layouts.footer')   
       
<script>
//For showing modal for those who are using app for the first time
var one = new Date().getTime();
var two = new Date('2018-05-05').getTime();
/* @if(isset($new_install))
{ */
var new_install = "{{ $new_install }}";
if (new_install == "Y") {
    $('#new_note').modal('show');
} else {
    if (one <= two) {
        $('#notification').modal('show');
    }
}
/* }
@endif */
/* For Pagination */
$('#page_number').change(function() {
    this.form.submit();
});
$('#records_length').change(function() {
    this.form.submit();
});
@if(isset($page_number))

var page_number = {
    {
        $page_number
    }
}
var order_count = {
    {
        $order_count
    }
}
var last_page = Math.ceil(order_count / {
    {
        $records_per_page
    }
});
if (page_number >= last_page) {
    $(".next").hide();
    $(".last").hide();
} else {
    $(".next").show();
    $(".last").show();
}
if (page_number <= 1) {
    $(".previous").hide();
    $(".first").hide();
} else {
    $(".first").show();
    $(".previous").show();
}
$("input." + page_number).css('background', '#337ab7');
$("input." + page_number).css('color', 'white');
$(".next").click(function() {
    $(this).val(parseInt(page_number) + 1);
});
$(".previous").click(function() {
    if (page_number <= 1)
        $(this).val("1");
    else
        $(this).val(parseInt(page_number) - 1);
});
@endif
/* For Pagination */

//For showing success or error notification
@if(Session::has('notification'))
var type = "{{ Session::get('notification.alert-type', 'info') }}";
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
switch (type) {
    case 'info':
        toastr.info("{{ Session::get('notification.message') }}");
        break;
    case 'warning':
        toastr.warning("{{ Session::get('notification.message') }}");
        break;
    case 'success':
        toastr.success("{{ Session::get('notification.message') }}");
        break;
    case 'error':
        toastr.error("{{ Session::get('notification.message') }}");
        break;
    case 'options':
        toastr.warning("{{ Session::get('notification.message') }}");
        break;
}
@
else

    @if(!empty($notification))
var type = "{{ $notification['alert-type'] }}";
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
switch (type) {
    case 'info':
        toastr.info("{{ $notification['message'] }}");
        break;
    case 'warning':
        toastr.warning("{{ $notification['message'] }}");
        break;
    case 'success':
        toastr.success("{{ $notification['message'] }}");
        break;
    case 'error':
        toastr.error("{{ $notification['message'] }}");
        break;
    case 'options':
        toastr.warning("{{ $notification['message'] }}");
        break;
}
@endif
@endif
</script>
        @yield('pageScript')
    </body>
</html>