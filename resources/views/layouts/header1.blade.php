<header>
<!--@yield('navigation')-->
        <?
        if (!isset($active)) {
            $active = "";
        }
        ?>
        <div class="header clearfix">

        </div>
        <div class="text-right change_plans">
            <a href="change-plans" class="btn btn-success">Change Version</a>
        </div>
    <marquee class="marquee_code" onMouseOver="this.stop()" onMouseOut="this.start()">
        We have updated our app with a new feature called as <b>Delivery Date Pro on Thank You Page(i.e. Order Confirmation Page)</b>.In case, if Delivery information is not captured due to some reason, then this feature will help customers to select Delivery information again from Thank You Page. <b><a href="thank-you-page" target="_blank">Click here</a></b> to refer how you can implement this feature on your store.
    </marquee>
    <div class="modal fade" id="new_note">
        <div class="modal-dialog">          
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Note</b></h4>
                </div>
                <div class="modal-body">
                    <p>Dear Customer, As this is a paid app and hundreds of customers are using it, So if you face any issue(s) on your store before uninstalling, Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP.</p>
                </div>        
                <div class="modal-footer">			
                    <div class="datepicker_validate" id="modal_div">
                        <div>
                            <strong>Show me this again</strong>
                            <span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
                                <label for="dont_show_again"></label></span>
                        </div>      
                    </div>      
                </div>      
            </div>
        </div>
    </div>
    <div class="modal fade" id="notification">
        <div class="modal-dialog">          
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Note</b></h4>
                </div>
                <div class="modal-body">
                    <p>Dear Customer, We have upgraded our app with new features, minor changes & bug resolution so if you face any issue(s) on your store , Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP or Staff account access along with issue notes will help us to resolve your query soon.</p>
                </div>        			     
            </div>
        </div>
    </div>
</header>