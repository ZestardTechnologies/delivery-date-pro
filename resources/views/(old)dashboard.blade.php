@extends('header')
@section('content')
<?php
$date = json_decode($config['block_date']);
//dd($date);
$interval = $config['date_interval'];
$alloved_month = $config['alloved_month'];
$days = json_decode($config['days']);
$id = $config['shop_id'];
$hours = $config['hours'];
$minute = $config['minute'];
$app_title = $config['app_title'];
$date_format = $config['date_format'];
$app_status = $config['app_status'];
$datepicker_label = $config['datepicker_label'];
$notes_message = $config['admin_order_note'];

if($date_format == "mm/dd/yy")
{
    $arrange_format = "m/d/Y";
}
else if($date_format == "yy/mm/dd")
{
    $arrange_format = "Y/m/d";
}
else if($date_format == "dd/mm/yy")
{
    $arrange_format = "d/m/Y";
}
//dd($arrange_format);
?>
<div class="container formcolor">
    <?php
    if (!empty($config)) {
        ?>
        <form action="{{ url('edit/'.$id) }}" name="config" method="post">
            <?php
        } else {
            ?>
            <form action="{{ url('/saveconfig') }}" name="config" method="post">
            <?php
        }
        ?>
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-3 margin-left"><strong>App Active?</strong></div>
                <div class="col-sm-3 margin-left"><strong>Block Day(s)</strong></div>
                <div class="col-sm-4 margin-left"><strong>Allowed Pre-order Time</strong></div>                

            </div>
            <div class="row">
                <div class="col-sm-3 margin-left">
                    <div class="app_status">
                        <div>
                            <select name="app_status" class="form-control">
                                <option value="Active" @if($app_status == "Active") selected @endif >Active</option>
                                <option value="Deactive" @if($app_status == "Deactive") selected @endif >Deactive</option>
                           </select>
                        </div>
                    </div>
                    <div class="datepicker_label">
                        <div><strong>Datepicker label</strong></div>
                        <div><input type="text" name="datepicker_label" value="{{ $datepicker_label }}" class="form-control"></div>
                    </div>
                    <div class="dateformat_option">
                        <div><strong>Date Format</strong></div>
                        <div>
                           <select name="date_format" class="form-control">
                                <option value="mm/dd/yy" @if($date_format == "mm/dd/yy") selected @endif >mm/dd/yyyy</option>
                                <option value="yy/mm/dd" @if($date_format == "yy/mm/dd") selected @endif >yyyy/mm/dd</option>
                                <option value="dd/mm/yy" @if($date_format == "dd/mm/yy") selected @endif >dd/mm/yyyy</option>
                           </select> 
                        </div>
                    </div>
                    <div class="order_note">
                        <div><strong>Notes</strong></div>
                        <div>
                            <textarea name="admin_order_note" class="form-control" rows="5" >{{ $notes_message }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 margin-left">
                    <div class="form-group">
                        <select name="days[]" size="8" multiple class="form-control">
                            <option value="all_allow"@if($days == "") selected @endif >All Allows</option>
                            <option value="Sunday" @if($days != "")@if(in_array("Sunday", $days)) selected @endif @endif>Sunday</option>
                            <option value="Monday" @if($days != "")@if(in_array("Monday", $days)) selected @endif @endif >Monday</option>
                            <option value="Tuesday" @if($days != "")@if(in_array("Tuesday", $days)) selected @endif @endif >Tuesday</option>
                            <option value="Wednesday" @if($days != "")@if(in_array("Wednesday", $days)) selected @endif @endif >Wednesday</option>
                            <option value="Thursday" @if($days != "")@if(in_array("Thursday", $days)) selected @endif @endif >Thursday</option>
                            <option value="Friday" @if($days != "")@if(in_array("Friday", $days)) selected @endif @endif >Friday</option>
                            <option value="Saturday" @if($days != "")@if(in_array("Saturday", $days)) selected @endif @endif >Saturday</option>
                        </select>
                    </div>
                    <div class="note">
                        <strong>Note:</strong> Selected day will be blocked and user won't be able to select it as a delivery date.
                    </div>
                    <div class="main-date" id="main-date">
                        <div><strong>Blocked Dates</strong></div>
                        <?php
                        if (!empty($date)) {
                        ?>
                            @foreach ($date as $date_selected)
                            <div class="form-group date-box">
                                <?php                                     
                                    //echo $date_selected.'<br>';
                                    $dateFinal = str_replace("/","-",$date_selected);
                                    /*echo strtotime('27-09-2017').'<br>';
                                    09/27/2017
                                    27-09-2017
                                    y/m/d
                                    echo date($arrange_format, strtotime('27-09-2017'));*/
                                ?>
                                <input type="text" name="blockdate[]" class="blockeddate form-control" value="{{ date($arrange_format, strtotime($dateFinal)) }}" style="width:70%;">
                                <a class="removerow"><img src="{{ asset('/image/close-icon.png') }}"/></a>
                            </div>
                            @endforeach
                        <?php
                        }
                        else 
                            {
                        ?>
                            <div class="form-group date-box">
                                <input type="text" name="blockdate[]" class="blockeddate form-control">
                            </div>
                        <?php
                            }
                        ?>

                    </div>
                    <div class="form-group">
                        <a class="addrow">+Add Date(s)</a>
                    </div>
                    <div class="note">
                        <strong>Note:</strong>List of dates that will be blocked for delivery so user won't be able to select it for the order delivery.
                    </div>
                </div>
                <div class="col-sm-4 margin-left">
                    <div class="form-group">
                        <input class="form-control" type="number" name="allowed_month" min="0" id="example-number-input" style="width:70px;" @if(!empty($alloved_month))value="{{ $alloved_month }}"@else value=" " @endif >
                    </div>
                    <div class="note">
                        <strong>Note:</strong> This option will allow user to book order before "X" number of Months on the Website.
                    </div>                    
                    <br />
                    <div><strong>Minimum Date Interval</strong></div>
                    <div class="form-group">
                        <input class="form-control" type="number" min="0" name="intervel" id="example-number-input" style="width:70px;" @if(!empty($interval))value="{{ $interval }}"@else value="0" @endif>
                    </div>
                    <div class="note">
                        <strong>Note:</strong> For "Today Delivery" it should be "0" otherwise it should be the interval of days from the current day to allow user to select delivery date.
                    </div>
                    <br />
                    <div>
                        <div><strong>Cut Off Time</strong>&nbsp;&nbsp; - Active:&nbsp;<input type="checkbox" name="cuttoff_time" id="filled-in-box" class="filled-in" value="cuttoff_on" @if($hours > 0 OR $minute > 0) checked @endif></div>
                       
                        <div class="cuttofftime_box">
                            <div class="display_block">
                            <label for="hours">Hour</label>
                            <select name="hours" class="form-control">
<?php
for ($h = 00; $h <= 24; $h++) {
    ?>
                                    <option value="<?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?>" @if($h == $hours) selected @endif ><?php echo str_pad($h, 2, "0", STR_PAD_LEFT); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            </div>
                            <div class="display_block">&nbsp;&nbsp;<strong>:</strong>&nbsp;&nbsp;</div>
                            <div class="display_block">
                            <label for="minute">Minute</label>
                            <select name="minute" class="form-control">
                                <?php
                                for ($m = 00; $m <= 59; $m++) {
                                    ?>
                                    <option value="<?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?>" @if($m == $minute) selected @endif><?php echo str_pad($m, 2, "0", STR_PAD_LEFT); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            </div>
                        </div>
                        <div class="note">
                        <strong>Note:</strong> For "Next Day Delivery" if the cut off time is enable & set then it will allow user to select day after the "Interval Date" as first available delivery date.
                    </div>
                    </div>
                </div>  
            </div>
            <div class="row">
                    <input type="submit" name="savedate" value="Apply" class="btn btn-info margin-left" />
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="copystyle_wrapper">
                        <textarea rows="1" class="form-control script_code" id="script_code" disabled><?php echo "{% include 'delivery-date' %}" ?></textarea>
                        <btn id="copy_script" name="copy_script" value="Copy Shortcode" class="btn btn-info copycss_button" data-clipboard-target=".script_code" style="display: block;" onclick="copy_script()"><i class="fa fa-check"></i> Copy</btn>
                    </div>
                </div>
                <div class="col-sm-7">
                    <p>Copy and paste the shortcode in your <a href="<?php
                    if(Session::has('shop')){
                        echo "https://".session('shop')."/admin/themes/current/?key=templates/cart.liquid";
                    }
                    else{
                        echo"#";
                    }
                ?>" target="_blank">cart page</a> see <a class="info_css" href="{{ asset('image/Delivery date short code.png') }}">Example</a></p>
                    <p>If you dont find form tag in your cart page so you have to paste the short code in the <a href="
                    <?php
                    if(Session::has('shop')){
                        echo "https://".session('shop')."/admin/themes/current/?key=sections/cart-template.liquid";
                    }
                    else{
                        echo"#";
                    }
                    ?>" target="_blank"">catr-template</a> see <a class="info_css" href="{{ asset('image/info_delivery date.png') }}">Example</a></p>
                </div>
            </div>
            <div class="row"><hr></div>
        </form>
</div>
<script type="text/javascript">

    //for date pick
    $(function () {		
        $(".blockeddate").datepicker({
            minDate: +0,
            dateFormat: '<?php echo $date_format; ?>'
        });

        var table = $('#example').DataTable({
            lengthChange: false,
            dom: 'Bfrtip',
            buttons: ['csvHtml5']
        });

        table.buttons().container().appendTo('#example_wrapper .col-sm-6:eq(0)');
    });

    //for adding the new row
    $(".addrow").on('click', function () {
        var new_add = '<div class="form-group date-box">' +
                '<input type="text" name="blockdate[]" class="blockeddate form-control" style="width:70%;">' +
                '<a class="removerow"><img src="{{ asset("/image/close-icon.png") }}"/></a>' +
                '</div>';
        $('#main-date').append(new_add);
        rundatepicker();
        runremoverow();
    });
    function rundatepicker() {		
        $(".blockeddate").datepicker({
            minDate: +0,
            dateFormat: '<?php echo $date_format; ?>'
        });
    }
    function runremoverow() {
        $(".removerow").on('click', function () {
            $(this).parent('div').remove();
        });
    }
    $(".removerow").on('click', function () {
        $(this).parent('div').remove();
    });
</script>
<style>
    .date-box{
        display: -webkit-inline-box;

    }
    .date-box img{
        margin-top: 7px;
        margin-left: 10px;
    }
    .note{
        font-style: italic;
        font-size: 11px;
    }
    .margin-left{
        margin-left: 20px;
    }
    .title_option{
        margin-top: 10px;
    }
    .dateformat_option{
        margin-top: 10px;
    }
    .app_status{
        margin-top: 10px;
    }
    .datepicker_label{
        margin-top: 10px;
    }
    .cuttofftime_box{
        display: inline-block;
        margin-bottom: 20px;
    }
    .cutoff_lable{
        display: inline-block;
    }
    label[for=minute]{
        margin-bottom: 0px;
    }
    label[for=hours]{
        margin-bottom: 0px;
    }
    #main-date{
        margin-top: 20px;
    }
    .display_block{
        display: inline-block;
    }
    .copystyle_wrapper{
        position: relative;
        padding-bottom: 10px;
        padding-top: 10px;
    }
    .copycss_button{
        position: absolute;
        right: 0px;
        top: 10px;
        
    }
</style>

@endsection