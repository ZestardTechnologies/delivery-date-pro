@extends('header')
@section('content')
<script>
$(document).ready(function () {
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('.pricing_table_div').offset().top
    }, 'slow');
});
</script>
<link rel="stylesheet" href="{{ asset('css/custom.css') }}"/>
<div class="container formcolor">
	<div class="pricing_table_block">
		<div class="pricing_table_div">
			<div class="pricing_table_item margin_right">
				<h1 class="pricing_table_title">Basic</h1>
				<div class="pricing_table_price_div">
					<span class="doller_icon">$</span><h2>7.99</h2><span class="month_text"> / m</span>
				</div>
				<div class="pricing_table_list">
					<ul>
							<li>Delivery Date & Time Selection</li>
							<li>Cut Off Time (Same for Everyday)</li>
							<li>Same Day & Next Day Delivery</li>
							<li>Blocking Specific Days and Dates</li>
							<li>Admin Order Manage & Export Based on Delivery Date</li>
						</ul>
				</div>
				<div class="select_btn">				
					<form action="basic" method="post">                                            
					{{ csrf_field() }}
						<input name="one" type="hidden" value="1">
                                                <input name="shop" type="hidden" class="shop" value="{{ $shop }}">
						</input>
                                                <button type="submit" class="plan_select1">{{ ($app_version == 1)?'Continue':'Select' }}</button>
					</form>
				</div>
			</div>
			<div class="pricing_table_item pricing_middle_item margin_right">
				<div class="ribbon ribbon-small text-white">
				   <!--div class="ribbon-content bg_pink text-uppercase text_white">We Recommend</div-->
				</div>
				<h1 class="pricing_table_title">Professional</h1>
				<div class="pricing_table_price_div">
					<span class="doller_icon">$</span><h2>10.99</h2><span class="month_text"> / m</span>
				</div>
				<div class="pricing_table_list">
					<ul>
						<li>All Basic Features</li>				
						<li>Auto Select for Next Available Delivery Date</li>
						<li>Auto Tag Delivery Details to all the Orders Within Interval of 1 hour from Order Placed Time<br><br><br></li>
					</ul>					
				</div>
				<div class="select_btn">				
					<p><b>Note:</b> Usage charge incur during the free trial for this version.</p>
					<form action="profesional" method="post">
						{{ csrf_field() }}
						<input name="two" type="hidden" value="2">
                                                <input name="shop" type="hidden" class="shop" value="{{ $shop }}">
						</input>
						<!--button class="plan_select">Select</button-->
                                                @if($app_version != 2)
						<button data-version="two" type="button" class="plan_select">{{ ($app_version == 2)?'Continue':'Select' }}</button>
                                                @else
						<button type="submit" class="plan_select1">{{ ($app_version == 2)?'Continue':'Select' }}</button>
                                                @endif
					</form>
				</div>
			</div>
			<div class="pricing_table_item popup-with-zoom-anim">
				<h1 class="pricing_table_title">Enterprise</h1>
				<div class="pricing_table_price_div">
					<span class="doller_icon">$</span><h2>14.99</h2><span class="month_text"> / m</span>
				</div>
				<div class="pricing_table_list">
					<ul>
						<li>All Professional Features</li>
						<li>Set Cut Off Time for Each Individual Weekday</li>
						<li>Set Different Delivery Time Options for Each Weekday</li>
						<li>Set Limit for Customer Order Delivery Based on Delivery Time</li>
					</ul>					
				</div>
				<div class="select_btn">
					<p><b>Note:</b> Usage charge incur during the free trial for this version.</p>
                                        <form action="enterprise" method="post">
					{{ csrf_field() }}
						<input name="three" type="hidden" value="3">	
                                                <input name="shop" type="hidden" class="shop" value="{{ $shop }}">
						</input>
                                                @if($app_version != 3)
						<button data-version="three" type="button" class="plan_select">{{ ($app_version == 3)?'Continue':'Select' }}</button>
                                                @else
                                                <button type="submit" class="plan_select1">{{ ($app_version == 3)?'Continue':'Select' }}</button>
                                                @endif
					</form>
				</div>
			</div>
			<div class="pricing_table_item popup-with-zoom-anim">		<h1 class="pricing_table_title">Ultimate</h1>
				<div class="pricing_table_price_div">
					<span class="doller_icon">$</span><h2>15.99</h2><span class="month_text"> / m</span>
				</div>
				<div class="pricing_table_list">
					<ul>
						<li>All Basic Features</li>		
						<li>Delivery Date & Time for Each Product</li>		
					</ul>
					
				</div>
				<div class="select_btn">	
					<p><b>Note:</b> Usage charge incur during the free trial for this version.</p>			
					<form action="ultimate" method="post">
						{{ csrf_field() }}
						<input name="four" type="hidden" value="4">	
						<input name="shop" type="hidden"class="shop" value="{{ $shop }}">
						</input>
						<!--button class="plan_select">Select</button-->
                                                @if($app_version != 4)
                                                <button data-version="four" type="button" class="plan_select">{{ ($app_version == 4)?'Continue':'Select' }}</button>
                                                @else
                                                <button type="submit" class="plan_select1">{{ ($app_version == 4)?'Continue':'Select' }}</button>
                                                @endif
					</form>
				</div>
			</div>	
		</div>
	</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Note</h4>
			</div>
			<div class="modal-body">
				<p><b></b></p>
			</div>
			<div class="modal-footer">
				<div class="text-left">
					<input type="checkbox" name="confirm_usage_charge" id="confirm_usage_charge"></input> Check this box if you agree to proceed.
				</div>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script>
	var trial_days = "{{$trial_days}}";
	$(".plan_select").click(function(){		
		var version = $(this).data("version");
                var shop = $('.shop').val();                
		if(version == "two")
		{
			$(".modal-body").html("User charges will get apply for professional version during trial period according to the shopify terms and structure of charges. Develpement Merchants do not have any rights or hold on this Methodology.");
			url = "profesional?two=2&shop="+shop;
		}
		else if(version == "three")
		{
			$(".modal-body").html("User charges will get apply for enterprise version during trial period according to the shopify terms and structure of charges. Develpement Merchants do not have any rights or hold on this Methodology.");
			url = "enterprise?three=3&shop="+shop;
		}
		else if(version == "four")
		{
			$(".modal-body").html("User charges will get apply for ultimate version during trial period according to the shopify terms and structure of charges. Develpement Merchants do not have any rights or hold on this Methodology.");
			url = "ultimate?four=4&shop="+shop;
		}
		if(parseInt(trial_days) > 0)
		{
			$('#myModal').modal('show');
			$("#confirm_usage_charge").change(function(){
				if($(this).prop("checked"))
				{
					$.ajax({
						url:"send-confirm-email",
						async:false,
						data:{shop:"{{$shop}}"},
						success:function(result){

						}
					});		
					window.location.href = url;		
				}
			});							
		}
		else
		{
			window.location.href = url;	
		}
	});
</script>
<style>
	.pricing_table_div
	{
		display: flex !important;
	}
	.pricing_table_item
	{
		padding:10px !important;
	}
	.pricing_table_list {
        margin-bottom: 25px !important;
    }
	input#confirm_usage_charge {
    	margin-right: 4px;
	}
	#confirm_usage_charge {
    	visibility: initial !important;
	}
</style>
@endsection