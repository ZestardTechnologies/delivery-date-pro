<?php
use Carbon\Carbon;
?>
@extends('header')
@section('content')
<?php
	$product_delivery_date = "";
	$product_delivery_time = "";
	$shop = session('shop');
?>
<script type="text/javascript">
ShopifyApp.ready(function(){
	ShopifyApp.Bar.initialize({
		buttons: {			
			secondary: [{
				label: 'HELP',
				href : '{{ url('/help') }}',
				loading: true
			},
			{
				label: 'MANAGE ORDER',
				href : "{{ url('order?store='.$shop) }}",
				loading: true
			},
			{
				label: 'SETTINGS',
				href : "{{ url('dashboard?shop='.$shop) }}",
				loading: true
			}]
		}
	});
});
</script>
<?php	
$ipaddress = '';
if (getenv('HTTP_CLIENT_IP'))
	$ipaddress = getenv('HTTP_CLIENT_IP');
else if(getenv('HTTP_X_FORWARDED_FOR'))
	$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
else if(getenv('HTTP_X_FORWARDED'))
	$ipaddress = getenv('HTTP_X_FORWARDED');
else if(getenv('HTTP_FORWARDED_FOR'))
	$ipaddress = getenv('HTTP_FORWARDED_FOR');
else if(getenv('HTTP_FORWARDED'))
   $ipaddress = getenv('HTTP_FORWARDED');
else if(getenv('REMOTE_ADDR'))
	$ipaddress = getenv('REMOTE_ADDR');
else
	$ipaddress = 'UNKNOWN';
$field_data = collect($fields);
$fields_json = json_encode($fields);
$id = $shop_id;
$date_format = $settings['date_format'];

if($date_format == "mm/dd/yy")
{
    $arrange_format = "m/d/Y";
}
else if($date_format == "yy/mm/dd")
{
    $arrange_format = "Y/m/d";
}
else if($date_format == "dd/mm/yy")
{
    $arrange_format = "d/m/Y";
}
?>
<div class="container formcolor">
<ul class="nav nav-tabs dashboard_tabs">
	<li><a href="dashboard?shop={{$shop}}">General Settings</a></li>
	@if($app_version == 3)
		<li><a href="cut-off?shop={{$shop}}">Cut Off Settings</a></li>
		<li><a href="delivery-time?shop={{$shop}}">Delivery Time Settings</a></li>
	@endif
	@if($app_version == 4)
		<li><a href="product-settings?shop={{$shop}}">Product Settings</a></li>
	@endif
	<li><a href="help?shop={{$shop}}">Help</a></li>	
</ul>
<h2 class="sub-heading">Manage Order</h2><div class="text-right fixed-top" style='padding-right:20px'><a class="todays_delivery" href="javascript:;" ><b>Today's Delivery:</b></a> {{ $todays_orders }} <a class="weeks_delivery" href="javascript:;"><b>This Week's Delivery:</b></a> {{ $weeks_orders }}</div>
    <form action="{{url('update_order')}}" name="update_order" method="post">
        {{ csrf_field() }}
        <input type="submit" name="updateorder" value="Run" class="btn btn-info margin_zero" style="display:none;"/>
    </form>   
    <div class="row">
		<div class="col-md-12">				
			<form action='order-demo' method='post'><input type='hidden' name='_token' value='{{ csrf_token() }}'/>	
			<input type='hidden' value='{{ $arrange_format }}' name='current_format' />	
			<input type="hidden" name="shop" value="{{ $shop }}"/>			
			<table width="100%" class="inputs table table-striped table-bordered">
				<tbody>				
					<tr style="font-weight:900">						
						<td class="col-md-6">Start Delivery Date:<input style="width:50%" class='form-control' name="min" id="min" type="text" /></td>
						<td class="col-md-6"><span class="onoff"><input type='checkbox' name='repeat' id="repeat_details" value='1'></input><label for="repeat_details"></label></span>Show order data for all products (Export Sheet)</td>	
					</tr>
					<tr>
						<td  style="font-weight:900" class="col-md-6">End Delivery Date:<input style="width:50%" class='form-control' name="max" id="max" type="text" /></td>			<td><button type='reset' name='clear' value='' id='reset_button' class='btn btn-primary' onclick='reload()'>Reset</button>
						<button type='submit' id='export-csv' class='btn btn-primary'>Export to CSV</button><br><label style="font-size:13px; font-weight:normal">(Export Sheet will contain at max 250 orders)</label>
						</td>						
					</tr>
					<tr style="font-weight:400">
						<td colspan="2">
						<div class="row">
							<div class="col-sm-4"></div>
							<div class="col-sm-5">
								<select class="form-control status_filter">
									<option value="" Placeholder="">Select Status
									</option>
									<option value="Paid">Paid
									</option>				
									<option value="partially_paid">Partially Paid
									</option>				
									<option value="partially_refunded">Partially Refunded
									</option>
									<option value="refunded">Refunded
									</option>
									<option value="pending">Pending
									</option>
									<option value="authorized">Authorized
									</option>
									<option value="voided">Voided
									</option>									
								</select>
							</div>				
						</div> 
						</td>
					</tr>
				</tbody>
			</table>
			<br>
			</form>				
			<table id="example" class="table table-striped table-bordered" cellspacing="0">
            <thead>
                <tr>
                    <th>Order Number</th>
                    <th>Name</th>
                    <th data-visible="false">Email</th>
                    <th data-visible="false">Created at</th>
                    @if($shop == "the-bunched-co.myshopify.com")	
						<th>Product Name (Qty, Delivery Date, Delivery Time)</th>
					@else
						@if($app_version == 4)
							<th>Product Name (Qty, Delivery Date, Delivery Time)</th>
						@else
							<th>Product Name (Qty)</th>
						@endif
					@endif
                    <th data-visible="false">Product Quantity</th>
                    <th data-visible="false">Product Price</th>
                    <th data-visible="false">Product SKU</th>
                    <th data-visible="false">Shipping Name</th>
                    <th data-visible="false">Shipping Street</th>
                    <th data-visible="false">Shipping Address1</th>
                    <th data-visible="false">Shipping Company</th>
                    <th data-visible="false">Shipping City</th>
                    <th data-visible="false">Shipping Zip</th>
                    <th data-visible="false">Shipping Country</th>
                    <th data-visible="false">Gift Message</th>
                    <th>Delivery Date</th> 
                    <th>Delivery Time</th> 
					<th>Status</th>      	
					@if($shop == "the-bunched-co.myshopify.com")
						<th>Delivery Address</th>
					@endif	
                </tr>
            </thead>
            <tbody>                
                @foreach ($orderlist->orders as $order)
                    <?php               
						// If we want to show only orders with delivery date (if we enable it please make sure to check paginatiohn logic based on that)	
                       //if(!empty($order->note_attributes))
                       { 			
							
							$atributes="";	
							$time_attribute="";							
							foreach($order->note_attributes as $attribute)
							{
								if($attribute->name == "Delivery-Date" || $attribute->name == "Delivery-Date-Pro")
								{
									$atributes = $attribute;
								}	
								if($attribute->name == "Delivery-Time" || $attribute->name == "Delivery-Time-Pro")
								{
									$time_attribute = $attribute;
								}	
							}
                    ?>                
                <?php
                       //if($atributes->name == "Shipping-Date" OR $atributes->name == "date" OR $atributes->name == "Delivery-Date")
                       {
                           if($order->line_items>1)
                           {
                               $i = 0;
							   $products_array=array();
							?>
							@if($shop == "the-bunched-co.myshopify.com" || $app_version == 4)
								@foreach($order->line_items as $product_name)			
									<?php
										$product_delivery_date = " ";
										$product_delivery_time = " ";
									?>
									@foreach($product_name->properties as $property)
										@if($property->name == "Delivery-Date")
											<?php
												$product_delivery_date = ", ".$property->value;
											?>
										@endif
										@if($property->name == "Delivery-Time")
											<?php
												$product_delivery_time = ", ".$property->value;
											?>
										@endif
									@endforeach	
									<?php
										array_push($products_array," ".$product_name->title ."<b> (".$product_name->quantity ."" . $product_delivery_date ."" . $product_delivery_time . ")</b>");
									?> 
								@endforeach							
							@else
								@foreach ($order->line_items as $product_name)
								<?php
								/* if($i == 0)
								{ */
									array_push($products_array," ".$product_name->title ."<b> (".$product_name->quantity .")</b>");
								?> 
							@endforeach
						   @endif
							<tr>		
								<td><a href="<?php  echo "https://". session('shop')."/admin/orders/"; ?>{{ $order->id }}" target="_blank">{{"#".$order->order_number}}</a></td>
								@if(empty($order->customer))
									<td>-</td>
								@else
									<td>{{$order->customer->first_name}} {{$order->customer->last_name}}</td>
								@endif								
								<td>{{$order->email}}</td>
								<td>{{$order->created_at}}</td>
								<td class="row_column_scroll"><?php echo implode("<br>",$products_array); ?></td>
								<td>{{$product_name->title}}</td>
								<td>{{$product_name->price}}</td>
								<td>{{$product_name->sku}}</td>
								<td><?php echo (empty($order->shipping_lines) ? '' : $order->shipping_lines[0]->title); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->address2); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->address1); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->company); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->city); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->zip); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->country); ?></td>
								<td>{{$order->note}}</td>
								<td>
									@if(!empty($atributes))										
										<?php										
											$date_final = (string)$atributes->value;
											/* 
												if($arrange_format == "m/d/Y")
												{
													$date_string = (string)$atributes->value;
													$raw_date = explode('/', $date_string);
													$date_final = implode('/', array($raw_date['2'], $raw_date['0'], $raw_date['1']));
												}
												else if($arrange_format == "d/m/Y")
												{
													$date_string = (string)$atributes->value;
													$raw_date = explode('/', $date_string);
													$date_final = implode('/', array($raw_date['2'], $raw_date['1'], $raw_date['0']));	
												}
												else if($arrange_format == "Y/m/d")
												{
													$date_string = (string)$atributes->value;
													//$date_final = str_replace("/","-",$date_string);
												} 
											*/									
										?>										
										{{ $date_final }}														
									@else
										@if($shop == "the-bunched-co.myshopify.com" || $app_version == 4)
											<?php
												$delivery_date_array = array();
											?>				
											@foreach ($order->line_items as $product_name)			
												@foreach($product_name->properties as $property)
													@if($property->name == "Delivery-Date")
														<?php
															$product_delivery_date = $property->value;
														?>
														<?php
															array_push($delivery_date_array, $product_delivery_date);
														?>
													@endif												
												@endforeach												 
											@endforeach											
											@if(empty($delivery_date_array))
												{{ "-" }}	
											@else												
												<?php echo implode("<br>", $delivery_date_array); ?>
											@endif	
										@else
											{{	"-" }}
										@endif	
									@endif												
								</td>
								<td>
									@if(!empty($time_attribute))															
										@if($time_attribute->value != "" or !(empty($time_attribute->value)))
											{{ $time_attribute->value }}													
										@else												
											{{ "-" }}
										@endif
									@else
										@if($shop == "the-bunched-co.myshopify.com" || $app_version == 4)
											<?php
												$delivery_time_array = array();
												$product_delivery_time="";
											?>				
											@foreach ($order->line_items as $product_name)			
												@foreach($product_name->properties as $property)
													@if($property->name == "Delivery-Time")
														<?php
															$product_delivery_time = $property->value;
														?>
														<?php
															array_push($delivery_time_array, $product_delivery_time);
														?>
													@endif				
												@endforeach								@if(empty($product_delivery_time))
													<?php		
														array_push($delivery_time_array, "-");
													?>
												@endif				 
											@endforeach
											@if(empty($delivery_time_array))
												{{ "-" }}	
											@else	
												<?php echo implode("<br>", $delivery_time_array); ?>
											@endif	
										@else
											{{	"-" }}
										@endif	
									@endif	
								</td>
								<td>{{ $order->financial_status }}</td>
								@if($shop == "the-bunched-co.myshopify.com")
									<?php
										$address = empty($order->billing_address) ? '' : ($order->billing_address->address1 . ' ' . $order->billing_address->address2 . ' ' . $order->billing_address->city . ' ' . $order->billing_address->zip . ' ' . $order->billing_address->province . ' ' . $order->billing_address->country );
									?>
									<td>{{ $address }}</td>
								@endif
							</tr>
                                        
							<?php                              
							}
							else
                            {
							?>
							<tr>
								<td><a href="<?php  echo "https://". session('shop')."/admin/orders/"; ?>{{ $order->id }}" target="_blank">{{"#".$order->order_number}}</a></td>
								@if(empty($order->customer))
									<td>-</td>
								@else
									<td>{{$order->customer->first_name}} {{$order->customer->last_name}}</td>
								@endif
								<td>{{$order->email}}</td>
								<td>{{$order->created_at}}</td>
								<td class="row_column_scroll">{{" ".$order['line_items']['title']." (".$order['line_items']['quantity'].")"}}</td>
								<td>{{$order['line_items']['quantity']}}</td>
								<td>{{$order['line_items']['price']}}</td>
								<td>{{$order['line_items']['sku']}}</td>
								<td><?php echo (empty($order->shipping_lines) ? '' : $order->shipping_lines[0]->title); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->address2); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->address1); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->company); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->city); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->zip); ?></td>
								<td><?php echo (empty($order->billing_address) ? '' : $order->billing_address->country); ?></td>
								<td>{{$order->note}}</td>
								<td>								
									@if(!empty($atributes)) 
										@if(strtotime($atributes->value)=='')				
										
										{{ "-" }}
										
										@else
									
										<?php 
											$date_final = (string)$atributes->value;
											/* if($arrange_format == "m/d/Y")
											{
												$date_string = (string)$atributes->value;
												$raw_date = explode('/', $date_string);
												$date_final = implode('-', array($raw_date['2'], $raw_date['0'], $raw_date['1']));
											}
											else if($arrange_format == "d/m/Y")
											{
												$date_string = (string)$atributes->value;
												$raw_date = explode('/', $date_string);
												$date_final = implode('-', array($raw_date['2'], $raw_date['1'], $raw_date['0']));													
											}
											else if($arrange_format == "Y/m/d")
											{
												$date_string = (string)$atributes->value;
												$date_final = str_replace("/","-",$date_string);
											}	  */																				
										?>
										{{ $date_final }}	
										@endif																					
									@else 
									{{	"-" }}
									@endif												
								</td>
								<td>
									@if(!empty($time_attribute))															
										@if($time_attribute->value != "" or !(empty($time_attribute->value)))
											{{ $time_attribute->value }}		
										@else												
											{{ "-" }}
										@endif
									@else
										{{ "-" }}
									@endif	
								</td>
								<!--td>
								<?php if(strtotime($atributes['value']) == ''): ?>
								<?php
										
										$date_string = (string)$atributes['value'];
										$date_final = str_replace("/","-",$date_string);
								?>
								{{ date($arrange_format, strtotime($date_final)) }}
								<?php else: ?>                        
								{{ date($arrange_format, strtotime($atributes['value'])) }}
								<?php endif; ?>
								</td-->
								<td>{{ $order->financial_status }}</td>
								@if($shop == "the-bunched-co.myshopify.com")
									<?php
										$address = empty($order->billing_address) ? '' : ($order->billing_address->address1 . ' ' . $order->billing_address->address2 . ' ' . $order->billing_address->city . ' ' . $order->billing_address->zip . ' ' . $order->billing_address->province . ' ' . $order->billing_address->country );
									?>
									<td>{{ $address }}</td>
								@endif	
							</tr>     
							<?php
							}
							}							
							?>							
							<?php }							
							?>
							@endforeach
                
						</tbody>
					</table>
					<form action="orders">							
						<div class="container text-right" style="width:50%;float:right;padding:0;">  
							<ul class="pagination pagination-box">
								<li><button class="1 pagination-number previous" type="submit" name="page" value="1">Previous</button></li>
								<?php  $order = ceil($order_count / $records_per_page);
								for ($i=0; $i<$order; $i++)	{	?>	
								@if($i < 5)
								<li><input class="{{ $i + 1 }}  pagination-number" type="submit" name="page" value="{{ $i + 1 }}" style=""/></li>	
								@endif
								<?php }	?>
								<li><button class="6 pagination-number next" type="submit" name="page" value="6">Next</button></li>
							</ul>
						</div>	
						<input type="hidden" name="store" value="{{ $shop }}"/>			
					</form>					
					<div class="text-left container" style="float:left;padding:0;width:50%;color:#337ab7">		
						<form action="orders">	
							<div class="d-inline" style="float:left;width:45%;">
								<div class="text-left pagination pagination-box">
								Page No.
									<select style="border:1px solid #ddd" id="page_number" name="page">
										<?php $selected_flag=""; for ($i=0; $i<$order; $i++)	{
											if($page_number==$i+1)	$selected_flag="selected"?>
											<option {{ $selected_flag }} value="{{ $i + 1 }}" style="">{{ $i + 1 }}</option>			
										<?php 	$selected_flag="";	}	?>
									</select>
								</div>	
							</div>
							<input type="hidden" name="store" value="{{ $shop }}"/>		
						</form>	
						<br>
						<form action="orders">		
							<div class="d-inline" style="float:left;width:55%">	
							Show
							<select style="border:1px solid #ddd" id="records_length" name="records_length">
								<option <?php if($records_per_page == 10) echo "selected" ?>value="10" style="">10</option>
								<option <?php if($records_per_page == 25) echo "selected" ?> value="25" style="">25</option>
								<option <?php if($records_per_page == 50) echo "selected" ?> value="50" style="">50</option>
								<option <?php if($records_per_page == 100) echo "selected" ?> value="100" style="">100</option>
							</select> records.
							</div>
							<input type="hidden" name="store" value="{{ $shop }}"/>		
						</form>						
					</div>
				</div>
			</div>
	<script>
    function reload(){
        $("#min").val("");
        $("#max").val("");
        var table = $('#example').DataTable();
        table.search('').columns().search('').draw();      
    }   
    function convertdate(date)
    {
        var current_format = $('#date_format').val();
        var blank = "";
        if(current_format == "dd/mm/yy"){
            
            var day = date.substring(0, 2);
            var month = date.substring(3, 5);
            var year = date.substring(6, 10);
            
            return date.value = year + ',' + month + ',' + day;
        }
        else if(current_format == "mm/dd/yy")
        {
            
            var month = date.substring(0, 2);
            var day = date.substring(3, 5);
            var year = date.substring(6, 10);
            
            return date.value = year + ',' + month + ',' + day;
        }
        else if(current_format == "yy/mm/dd"){
            return date.split('/').join(',');
        }
        else{
            return blank;
        }
        
    }
     
    //data table with custom filter and feature   
    $(document).ready(function() {    
    //for data table
	
	var todays_date = "{{ $todays_date }}";
	
	var start_of_week = "{{ $start_of_week }}";
	var end_of_week = "{{ $end_of_week }}";	
	var arrange_format	='<?php echo $arrange_format ?>';
	if(arrange_format == "d/m/Y")
	{
		var day = todays_date.substring(0, 2);
		var month = todays_date.substring(3, 5);
		var year = todays_date.substring(6, 10);		
		todays_date=day + '/' + month + '/' + year;	
	}
	if(arrange_format == "m/d/Y")
	{
		var month = todays_date.substring(3, 5);
		var day = todays_date.substring(0, 2);
		var year = todays_date.substring(6, 10);		
		todays_date= month + '/' + day + '/' + year;	
	}
	if(arrange_format == "Y/m/d")
	{
		var month = todays_date.substring(5, 7);
		var day = todays_date.substring(8, 10);
		var year = todays_date.substring(0, 4);		
		//todays_date = year + '/' + month + '/' + day;
		parts_date=todays_date.split('-');
		todays_date=parts_date[2] + '/' + parts_date[1] + '/' + parts_date[0];					
	}	 	
	
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var min = $('#min').datepicker("getDate");
			var max = $('#max').datepicker("getDate");			
			/* var parts_date=data[16].split('/');
			var new_date=parts_date[2] + '/' + parts_date[1] + '/' + parts_date[0]; */
			//var startDate = new Date(data[16]);
			var arrange_format	='<?php echo $arrange_format ?>';
			var parts,new_date;	
			if(arrange_format == "d/m/Y")
			{
				parts_date=data[16].split('/');
				new_date=parts_date[2] + '/' + parts_date[1] + '/' + parts_date[0];	
			}
			if(arrange_format == "m/d/Y")
			{
				parts_date=data[16].split('/');
				new_date=parts_date[2] + '/' + parts_date[0] + '/' + parts_date[1];	
			}
			if(arrange_format == "Y/m/d")
			{
				parts_date=data[16].split('/');
				new_date=parts_date[0] + '/' + parts_date[1] + '/' + parts_date[2];	
			}						
			/* parts_date=data[16].split('/');
			new_date=parts_date[2] + '/' + parts_date[1] + '/' + parts_date[0]; */
			var startDate = new Date(new_date);							
			if (min == null && max == null) { return true; }
			if (min == null && startDate <= max) { return true;}
			if(max == null && startDate >= min) {return true;}
			if (startDate <= max && startDate >= min) { return true; }
			return false;
		}
	);
	
	$("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true,dateFormat: '<?php echo $date_format; ?>'
	});
	$("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true,dateFormat: '<?php echo $date_format; ?>' });
	
    var table = $('#example').DataTable({		
		"language": {
        search: "Search",
		info:"Showing _START_ to _END_ entries"//+" of "+{{ $order_count }}
        },
		"dom": "brtip",	
        "order": [ 0, "desc" ],
		"pageLength": 25,
		"bPaginate": false,	
			
	});

	$('#min, #max').change(function () {
		table.draw();
	});

	$('.todays_delivery').click(function () {
		$("#min").val(todays_date);
		$("#max").val(todays_date);
		table.draw();
	});	
	var ip = "{{ $ipaddress }}";
	/* if(ip == "103.254.244.134") */
	{ 		
		$('.weeks_delivery').click(function () {
			$("#min").val(start_of_week);
			$("#max").val(end_of_week);
			table.draw();
		});	
	}
	
    /* var table = $('#example').DataTable({
        language: {
        search: "Search"
        },
        "order": [[ 0, "desc" ]],
        lengthChange: false,
        dom: 'Bfrtip',
        "columnDefs": [
            {
                "targets": [ 1 ],
                "visible": true
            },
            {
                "targets": [ 3 ],
                "visible": false
            },
            {
                "targets": [ 4 ],
                "visible": false
            },
            {
                "targets": [ 6 ],
                "visible": false
            },
            {
                "targets": [ 5 ],
                "visible": false
            },
            {
                "targets": [ 7 ],
                "visible": false
            },
            {
                "targets": [ 8 ],
                "visible": false
            },
            {
                "targets": [ 9 ],
                "visible": false
            },
            {
                "targets": [ 10 ],
                "visible": false
            },
            {
                "targets": [ 11 ],
                "visible": false
            },
            {
                "targets": [ 12 ],
                "visible": false
            },
            {
                "targets": [ 13 ],
                "visible": false
            },
            {
                "targets": [ 14 ],
                "visible": false
            },
            {
                "targets": [ 15 ],
                "visible": false
            }
        ],
        buttons: [
            {  		
			  etend: 'csv',
              text: 'Export CSV',
              title: '<?php echo session('shop')." - Export Orders"; ?>'
            }
        ]
		
    }); 
	
    table.buttons().container()
        .appendTo('#example_wrapper .col-sm-6:eq(0)'); */
    <?php 
    if(count($fields)>0):
        $data = implode(',',$fields);
    ?>       
    <?php    
    endif;
    ?>    
    var custom_div = "";
    $("#example_filter").append(custom_div);
	$("#example_filter").css("float","right");
         	
	$("#display_field").select2({
        placeholder: "Select a Field"
    });
	$("select.status_filter").on('change', function(){
		table
		.search( this.value )
		.draw();
	});
});
</script>

@endsection