@extends('header')
@section('content')
<style>
.top-marquee{
	display:none;
}
</style>
<script type="text/javascript">
ShopifyApp.ready(function(){
	ShopifyApp.Bar.initialize({
	  buttons: {			
		secondary: [{
		  label: 'HELP',
		  href : '{{ url('/help') }}',
		  loading: true
		},
		{
		  label: 'MANAGE ORDER',
		  href : '{{ url('/order') }}',
		  loading: true
		},{
		  label: 'SETTINGS',
		  href : '{{ url('/dashboard') }}',
		  loading: true
		}]
	  }
	});
});
$(document).ready(function(){    
    $('html, body').animate({
        scrollTop: $('.pricing_table_div').offset().top
    }, 'slow');
});
</script>
<link rel="stylesheet" href="{{ asset('css/custom.css') }}"/>
    <div class="container formcolor">
		<h1 style="text-align:center">Our Plans</h1>
		<br>		
		<div class="pricing_table_block">
			<div class="pricing_table_div">
				<div class="pricing_table_item margin_right pricing_table_item_selected" style="background:#0086a0;color:white">
					<h1 class="pricing_table_title pricing_table_title_selected" style="color:white">Basic</h1>
					<div class="pricing_table_price_div" style="background:white;color:#0086a0">
						<span class="doller_icon">$</span><h2>7.99</h2><span class="month_text"> / m</span>
					</div>
					<div class="pricing_table_list">
						<ul>
							<li>Delivery Date & Time Selection</li>
							<li>Cut Off Time (Same for Everyday)</li>
							<li>Same Day & Next Day Delivery</li>
							<li>Blocking Specific Days and Dates</li>
							<li>Admin Order Manage & Export Based on Delivery Date</li>
						</ul>
					</div>
					<div class="select_btn">										
							<button style="background:white;color:#0086a0" class="plan_select">Selected</button>			
					</div>
				</div>
				<div class="pricing_table_item pricing_middle_item margin_right">
					<div class="ribbon ribbon-small text-white">					   
					</div>
					<h1 class="pricing_table_title">Professional</h1>
					<div class="pricing_table_price_div">
						<span class="doller_icon">$</span><h2>10.99</h2><span class="month_text"> / m</span>
					</div>
					<div class="pricing_table_list">
						<ul>
							<li>All Basic Features</li>									
							<li>Auto Select for Next Available Delivery Date</li>
							<li>Auto Tag Delivery Details to all the Orders Within Interval of 1 hour from Order Placed Time<br><br><br></li>							
						</ul>
					</div>
					<div class="select_btn">				
						<form action="profesional" method="post">
						{{ csrf_field() }}
							<input name="two" type="hidden" value="2">					
							</input>
							<button class="plan_select">Select</button>
						</form>
					</div>
				</div>
				<div class="pricing_table_item popup-with-zoom-anim">
					<h1 class="pricing_table_title">Enterprise</h1>
					<div class="pricing_table_price_div">
						<span class="doller_icon">$</span><h2>14.99</h2><span class="month_text"> / m</span>
					</div>
					<div class="pricing_table_list">
						<ul>
							<li>All Professional Features</li>
							<li>Set Cut Off Time for Each Individual Weekday</li>
							<li>Set Different Delivery Time Options for Each Weekday</li>
							<li>Set Limit for Customer Order Delivery Based on Delivery Time</li>
						</ul>
					</div>
					<div class="select_btn">
						<form action="enterprise" method="post">
						{{ csrf_field() }}
							<input name="three" type="hidden" value="3">					
							</input>
							<button class="plan_select">Select</button>
						</form>
					</div>
				</div>
			</div>
		</div>                       
    </div>
@endsection
