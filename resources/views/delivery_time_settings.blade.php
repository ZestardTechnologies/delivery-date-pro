@extends('header')
@section('content')
<?php
	$shop = session('shop');
?>
<script type="text/javascript">
	ShopifyApp.ready(function(){
		ShopifyApp.Bar.initialize({
		  buttons: {
			primary: {
			  label: 'SAVE',
			  message: 'form_submit',			  
			  callback:  function(event){ validate(event) } 
			},
			secondary: [{
			  label: 'HELP',
			  href : '{{ url('/help') }}',			  
			},
			{
			  label: 'MANAGE ORDER',
			  href : "{{ url('order?store='.$shop) }}",
			  loading: true
			},{
			  label: 'SETTINGS',
			  href : "{{ url('dashboard?shop='.$shop) }}",
			  loading: true
			}]
		  }
		});
	});
    
	function validate(event)
	{
		var checked = $("#count_flag").prop("checked");
		var flag=0;
		var d_time;
		var time_count;
		
		$(".delivery_time").each(function(){
			if($(this).val() == "")
			{				
			}
			else
			{				
				time_count = $(this).parent().parent().find(".times_count").val();
				if(time_count == "")
				{
					flag = 1;
				}
			}
		});
		if(checked)
		{
			if(flag==1)
			{
				alert('Since you checked "User Count" to manage deliveries based on count, Please make sure count value should not be empty for any delivery time.');
				//event.preventDefault();
			}
			else
			{
				$("#delivery_time_form").attr("data-shopify-app-submit","form_submit");	
			}
		}
		else
		{
			$("#delivery_time_form").attr("data-shopify-app-submit","form_submit");	
		}
	}
</script>
<?php
$i=0;
?>
<div class="container formcolor cutoff_block">    
	<ul class="nav nav-tabs dashboard_tabs">
		<li><a href="{{ url('dashboard?shop='.$shop) }}">General Settings</a></li>
		<li><a href="{{ url('cut-off?shop='.$shop) }}">Cut Off Settings</a></li>
		<li class="active"><a href="{{ url('delivery-time?shop='.$shop) }}">Delivery Time Settings</a></li>
	</ul>			            				
		<?php $days=array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'); ?>
		<div class="row formcolor_row">	
		<h2 class="sub-heading">Delivery Time Settings</h2>		
		<form method="post" id="delivery_time_form" action="save-delivery-times" data-shopify-app-submit="" class="cutoff_div">
		{{ csrf_field() }}		
		
		<div class="col-md-6 col-sm-6 col-xs-12 cutoff_time_checkbox delivery_time_checkbox ">
			<span class="onoff">
				<input type="checkbox" id="global_delivery_time" @if($global_flag==1) checked @endif name="global_delivery_time" value="1"/> 
				<label for="global_delivery_time"></label>
			</span>
			<strong>Use Global Delivery Time</strong>
			<div class="note">
				<strong>Note:</strong> If You Enable Global Delivery Time, Then Delivery Time(In General Settings) Will Be Global For All Days.
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12 cutoff_time_checkbox delivery_time_checkbox">
			<span class="onoff">
				<input id="count_flag" type="checkbox" @if($count_flag==1) checked @endif name="count_flag" id="count_flag" value="1"/>
				<label for="count_flag"></label>
			</span>
			<strong>Use Count</strong>
			<div class="note">
				<strong>Note:</strong> If You Enable Count,Then Each Delivery Time Will Be Available For Respective Number of Times That You Provide Below.
			</div>
		</div>

		<table id="cut_off" class="table table-bordered delivery_time_table" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="col-md-4"><span class="col-md-12">Day</span></th>							
				<th class="col-md-8">
					<span class="col-md-4">Delivery Time</span>
					<span class="col-md-3">Count</span>
				</th>
			</tr>
		</thead>
		<tbody>					
		@foreach($days as $day)			
		@if(empty($settings[$i]))
		<tr>
			<td class="col-md-4">
				<span class="col-md-12">{{ $day }}</span>
			</td>
			<td class="col-md-8">
				<div class="delivery_block">			
					<div class="delivery_time_box">								
						<div class="col-xs-4 col-md-4 col-sm-4 ">
							<input type="text" class="form-control delivery_time" name="delivery_time_{{ $i }}[]"/>
						</div>		
						<div class="col-xs-3 col-md-3 col-sm-3 ">							
							<input type="number" min="0" name="count_{{ $i }}[]" class="form-control times_count" />
						</div>	
						<div class="col-xs-12 col-md-12 col-sm-12 form-group dateformat_option">
							<a data-id="{{ $i }}" class="addrow"><i class="fa fa-plus"></i>Add Time Slot(s)</a>
						</div>						
					</div>
				</div>
			</td>
		</tr>
		@else	
		<?php $setting=$settings[$i]; ?>
		<tr>
			<td class="col-md-4">
				<span class="col-md-12">{{ $day }}</span>
			</td>
			<td class="col-md-8">
				<?php 
					$j=0;							 
					$counts=explode(",",$setting->count); 
					$delivery_times = json_decode($setting->delivery_times,true);
				?>
				@foreach($counts as $count)
				<div class="delivery_block">
					<div class="delivery_time_box">								
						<div class="col-xs-4 col-md-4 col-sm-4 ">							
							<input type="text" name="delivery_time_{{ $i }}[]" class="delivery_time form-control " value="{{ $delivery_times[$j] }}" />
						</div>
						<div class="col-xs-3 col-md-3 col-sm-3 ">										
							<input type="number" min="0" value="{{ $count }}" name="count_{{ $i }}[]" class="times_count form-control" />
						</div>														
						@if($j>0)
							<div class="col-xs-3">									
								<a class="removerow">
									<img src="{{ asset('/image/close-icon.png') }}" />
								</a>
							</div>
						@endif
					</div>
				</div>						
				<?php $j++; ?>						
				@endforeach
				<div class="col-xs-12 col-md-12 col-sm-12 form-group dateformat_option">
					<a data-id="{{ $i }}" class="addrow"><i class="fa fa-plus"></i>Add Time Slot(s)</a>
				</div>	
			</td>
		</tr>
		@endif
		<?php $i++; ?>
		@endforeach
		</tbody>					
		</table>			
		</form>				
	</div>
</div>
<script type="text/javascript">	
    //for adding the new row
	runremoverow();
    $(".addrow").on('click', function () {
		var id=$(this).attr('data-id');
        var new_add = '<div class="delivery_block"><div class="delivery_time_box"><div class="col-xs-4 col-md-4 col-sm-4 "><input required type="text" class="delivery_time form-control" name="delivery_time_'+id+'[]" ></input></div><div class="col-xs-3"><input required type="number" min="0" name="count_'+id+'[]" class="times_count form-control"/></div></div><div class="col-xs-3"><a class="removerow"><img src="{{ asset("/image/close-icon.png") }}"/></a></div></div>';
        $(new_add).insertBefore($(this).parent());        
        runremoverow();
    });    
	
	//for removing row
    function runremoverow() 
	{
        $(".removerow").on('click', function(){
            $(this).parent('div').parent('div').remove();
        });
		$(".times_count").keydown(function(e){
			if (e.shiftKey || e.ctrlKey || e.altKey) 
			{
				e.preventDefault();
			}
			else 
			{
				var key = e.keyCode;
				if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
				e.preventDefault();
				}
			}
		});
	}
    $(".removerow").on('click', function () {
        $(this).parent('div').parent('div').remove();
    });
	$(".times_count").keydown(function(e){
		if (e.shiftKey || e.ctrlKey || e.altKey) 
		{
			e.preventDefault();
		}
		else 
		{
			var key = e.keyCode;
			if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
			e.preventDefault();
			}
		}
	});
</script>
@endsection