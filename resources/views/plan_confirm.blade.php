@extends('header')
@section('content')
<script>
    $(document).ready(function () {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('.pricing_table_div').offset().top
        }, 'slow');
    });
</script>
<link rel="stylesheet" href="{{ asset('css/custom.css') }}"/>
@if($plan == 1)
<br>
<div class="container">
    <div class="row">
        <div class="col-lg-12 formcolor_plan">
            <h3>{{ "Dear Customer, You Have Selected Basic Plan. Total Payable Amount is $7.99 (Basic Plan)" }}</h3><br>
            <a href="{{ url('plan/1') }}"><button class="btn btn-info">Click Here to Pay</button></a>
            <a href="change-plans"><button class="btn btn-info">Go Back</button></a>
        </div></div></div>
@endif
@if($plan == 2)
<br>
<div class="container">
    <div class="row">
        <div class="col-lg-12 formcolor_plan">
            <h3>{{ "Dear Customer, You Have Selected Professional Plan. Total Payable Amount is $10.99 (Basic - $7.99 + Professional - $3.00)" }}</h3><br>
            <a href="{{ url('plan/2') }}"><button class="btn btn-info">Click Here to Pay</button></a>
            <a href="change-plans"><button class="btn btn-info">Go Back</button></a>
        </div></div></div>
@endif	
@if($plan == 3)
<br>
<div class="container">
    <div class="row">
        <div class="col-lg-12 formcolor_plan">
            <h3>{{ "Dear Customer, You Have Selected Enterprise Plan. Total Payable Amount is $14.99(Basic - $7.99 + Enterprise - $7.00)" }}</h3><br>
            <a href="{{ url('plan/3') }}"><button class="btn btn-info">Click Here to Pay</button></a>
            <a href="change-plans"><button class="btn btn-info">Go Back</button></a>	
        </div></div></div>
@endif
@if($plan == 4)
<br>
<div class="container">
    <div class="row">
        <div class="col-lg-12 formcolor_plan">
            <h3>{{ "Dear Customer, You Have Selected Ultimate Plan. Total Payable Amount is $15.99(Basic - $7.99 + Ultimate - $8.00)" }}</h3><br>
            <a href="plan/4?shop={{$shop}}"><button class="btn btn-info">Click Here to Pay</button></a>
            <a href="change-plans"><button class="btn btn-info">Go Back</button></a>	
        </div></div></div>
@endif
@endsection