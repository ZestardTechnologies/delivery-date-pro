<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
        <!-- HumDash -->
        <script type="text/javascript">
            var _ha = window._ha || [];
            / tracker methods like "setCustomDimension" should be called before "trackPageView" /
            _ha.push(['trackPageView']);
            _ha.push(['enableLinkTracking']);
            (function() {
                var u="https://app.humdash.com/";
                _ha.push(['setTrackerUrl', u+'humdash.php']);
                _ha.push(['setSiteId', '2049']);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'humdash.js'; s.parentNode.insertBefore(g,s);
            })();
            </script>
            <!-- End HumDash Code -->
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Be right back.</div>
            </div>
        </div>
    </body>
</html>
