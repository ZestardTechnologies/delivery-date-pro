<?php

$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$follow_up_headers = 'From: Zestard Technologies <shilpi@zestard.com>' . "\r\n";
$follow_up_headers .= 'MIME-Version: 1.0' . "\r\n";
$follow_up_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$json = '' . file_get_contents('php://input') . '';
$result = json_decode($json);
$connection = new mysqli("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_delivery_date_pro");
$store_name = $result->myshopify_domain;
$store_email = $result->email;
$sql = "SELECT * FROM usersettings where store_name = '$store_name'";
$data = $connection->query($sql);

if ($data->num_rows > 0) {
    $row = $data->fetch_assoc();
    $shop_id = $row['id'];
    $type_of_app = $row['type_of_app'];
    $app_version = $row['app_version'];
}

//$delete_shop = "DELETE FROM usersettings WHERE store_name= '".$result->myshopify_domain ."'";
$delete_shop = "DELETE FROM usersettings WHERE id= '" . $shop_id . "'";
$connection->query($delete_shop);
// order_details
$tables = array("trial_info", "product_settings", "product_settings", "field_config", "development_stores", "deliverytime_settings", "cutoff_settings", "block_config", "app_log", "app_config");
foreach ($tables as $table) {
    $query = "DELETE FROM $table WHERE shop_id='$shop_id'";
    $connection->query($query);
}

$owner_name = $result->shop_owner;
$app_name = "Delivery Date Pro";
$uninstallation_follow_up_msg = '<html>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i");
            @media only screen and (max-width:599px) {
                table {
                    width: 100% !important;
                }
            }
            
            @media only screen and (max-width:412px) {
                h2 {
                    font-size: 20px;
                }
                p {
                    font-size: 13px;
                }
                .easy-donation-icon img {
                    width: 120px;
                }
            }
        </style>
    
    </head>
    
    <body style="background: #f4f4f4; padding-top: 57px; padding-bottom: 57px;">
        <table class="main" border="0" cellspacing="0" cellpadding="0" width="600px" align="center" style="border: 1px solid #e6e6e6; background:#fff; ">
            <tbody>
                <tr>
                    <td style="padding: 30px 30px 10px 30px;" class="review-content">
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px; line-height: 25px; margin-top: 0px;"><b>Hi ' . $owner_name . '</b>,</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">You have Un-installed Shopify Application - "' . $app_name . '" from your store.</p>
                        <p style="font-family: \'Helvetica\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">If you have faced any issue with the application in terms of functional part or design related issues, we can assist you to resolve it for you.</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Some of the important features are Delivery Date for order, Delivery Time, Block Days, Block Dates, Cut Off time, Cut Off time for individual weekdays, Limit number of delivery for day or delivery time, order export based on delivery
                            date, Add information to customer order email, admin order notification, language support & Custom Design etc.</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Please have a look at below Live Stores to understand how you can use it for your website:</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">==============================</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">1) <a href="https://www.denheath.co.nz/cart" target="_blank">https://www.denheath.co.nz/cart</a></p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">2) <a href="http://maison-duffour.myshopify.com/cart" target="_blank">http://maison-duffour.myshopify.com/cart</a></p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">3) <a href="https://spotted-dog-bakery.myshopify.com/cart" target="_blank">https://spotted-dog-bakery.myshopify.com/cart</a></p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">4) <a href="https://lunchbunch.com.au/cart" target="_blank">https://lunchbunch.com.au/cart</a></p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">5) <a href="https://www.simplygifts.co.nz/cart" target="_blank">https://www.simplygifts.co.nz/cart</a></p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">6) <a href="https://www.fabulousflowers.co.za/cart" target="_blank">https://www.fabulousflowers.co.za/cart</a> (Florist)</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">7) <a href="https://ivyrosefloraldesign.com/cart" target="_blank">https://ivyrosefloraldesign.com/cart</a> (Florist)</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">8) <a href="https://www.equistar.store/cart" target="_blank">https://www.equistar.store/cart</a> (Japanse Store)</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">9) <a href="https://yonglekitchen.xyz/cart" target="_blank">https://yonglekitchen.xyz/cart</a> (Chinese Store)</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px; line-height: 25px; margin-top: 0px;">Also, do not forget to read the reviews of happy customers. We have 180+ active stores with the application.</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px; line-height: 25px; margin-top: 0px;">Please let us know if you have any query.</p>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px; line-height: 25px; margin-top: 0px;">PS: Don\'t forget to check our new App:: Shopify Product Matrix.</p>
    
                    </td>
                </tr>
    
                <tr>
                    <td style="padding: 20px 30px 30px 30px;">
    
                        <br>
                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 26px; margin-bottom:0px;">Thanks,<br>Zestard Support</p>
                    </td>
                </tr>
    
            </tbody>
        </table>
    </body>';
    mail($result->email, "Zestard Application :: Delivery Date Pro", $uninstallation_follow_up_msg, $follow_up_headers);
    mail("rdesoza98@gmail.com", "Zestard Application :: Delivery Date Pro", $uninstallation_follow_up_msg, $follow_up_headers);


$msg = '<table>
                <tr>
                    <th>Shop Name</th>
                    <td>' . $result->name . '</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>' . $result->email . '</td>
                </tr>
                <tr>
                    <th>Domain</th>
                    <td>' . $result->myshopify_domain . '</td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td>' . $result->phone . '</td>
                </tr>
                <tr>
                    <th>Shop Owner</th>
                    <td>' . $result->shop_owner . '</td>
                </tr>
                <tr>
                    <th>Country</th>
                    <td>' . $result->country_name . '</td>
                </tr>
                <tr>
                    <th>Plan</th>
                    <td>' . $result->plan_display_name . '</td>
                </tr>
            </table>';

$shop_name = $result->myshopify_domain;
$dev_store = $connection->query("SELECT * FROM development_stores WHERE dev_store_name = '$shop_name'");
if ($dev_store->num_rows <= 0) {    
    mail("support@zestard.com","Delivery Date Pro App Removed",$msg,$headers);
} else {
    mail("support@zestard.com", "Delivery Date Pro App Removed from Development Store", $msg, $headers);
}
?>