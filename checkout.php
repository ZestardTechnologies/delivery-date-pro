<?php
	$json = ''.file_get_contents('php://input').'';    
	
	
	$connection = new mysqli("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_delivery_date_pro");	
	$result = json_decode($json);
	$type = gettype($json);
	$order_id = $result->id;	
	$customer_email = $result->email;
	$customer_name = $result->customer->first_name . " " . $result->customer->last_name ;
	$order_name = $result->name;
	$other_note = $result->note; 	
	$url = explode("/", $result->order_status_url);		
	$store_name = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
	$sql = "SELECT * FROM usersettings where store_name = '$store_name'";
	$data = $connection->query($sql);		
	$date_format="";	
	$temp = json_encode($json);		
	$user_agent =  $result->client_details->user_agent;
	$ipaddress  =  $result->client_details->browser_ip;	
	
	if($store_name == 'davidaustinroses-dev.myshopify.com'){
		$myfile = fopen("debug.txt", "a") or die("Unable to open file!");			
		fwrite($myfile, "\n". $json);
		fclose($myfile);	
	}

	if ($data->num_rows > 0) 
	{
		$row = $data->fetch_assoc();
		$shop_id = $row['id'];		
		$type_of_app=$row['type_of_app'];
		$app_version=$row['app_version'];		
		$access_token = $row['access_token'];
		$sql = "SELECT * FROM block_config where shop_id = $shop_id";
		$data = $connection->query($sql);				
		$row = $data->fetch_assoc();
		$status = $row['app_status'];		
	}
	else
	{
		$shop_id = "";
		$type_of_app="";
		$app_version="";
		$status = "";
		$access_token = "";
	}	
	
	$delivery_date = "";
	$delivery_time = "";	
	$date="";
	foreach($result->note_attributes as $attributes)
	{
		if($attributes->name == "Shipping-Date" OR $attributes->name == "date" OR $attributes->name == "Delivery-Date")
		{				
			$delivery_date = $attributes->value;
		}
		if($attributes->name == "Delivery-Time")
		{				
			$delivery_time = $attributes->value;
		}						
	}	
	
	if($shop_id != "")
	{
		$sql = "SELECT * FROM block_config where shop_id = $shop_id";
		$data = $connection->query($sql);
		if ($data->num_rows > 0) 
		{
			$row = $data->fetch_assoc();
			$date_format = $row['date_format'];		
			$date_require_option = $row['require_option'];
			$time_require_option = $row['time_require_option'];			
		}					
	}	
	if($date_format != "")
	{		
		if($date_format == "dd/mm/yy")
		{
			$date = str_replace("/","-",$delivery_date);
			$date = date("Y-m-d", strtotime($date));			
		}
		if($date_format == "mm/dd/yy")
		{
			$date = str_replace("/","-",$delivery_date);
			$date = date("Y-m-d",strtotime($delivery_date));				
		}
		if($date_format == "yy/mm/dd")
		{
			$date = str_replace("/","-",$delivery_date);			
		}
	}	 
	
	$date_flag = 0;
	$time_flag = 0;
	
	$msg= $msg . "Hi There, Due to some Reason we have Found that ";
	if($date == "" || empty($date) || $date == "0000-00-00" || $date == "1969-12-31" || $date == "1970-01-01")
	{				
		if($date_require_option == '1')
		{
			$date_flag = 1;
			$msg = $msg . "Delivery Date";		
		}
	}	
	if($delivery_time == "" || empty($delivery_time))
	{		
		if($time_require_option == 1)
		{
			$time_flag = 1;
			if($date_flag == 1)
			{
				$msg = $msg . "&";
			}
			$msg = $msg . "Delivery Time ";		
		}
	}
	
	if($date_flag == 1 || $time_flag == 1)
	{	
		$msg = $msg . "has not been Captured for Order #$order_id on your Store $store_name
		from Device $user_agent";	
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";		
	}
	
	$delivery_info = array();
	$delivery_date_array = array();
	$delivery_time_array = array();
	$delivery_info = array();
	$delivery_date_temp = "";
	$delivery_time_temp = "";
	foreach($result->line_items as $item)
	{				
		foreach($item->properties as $property)
		{
			if($property->name == 'Delivery-Date')
			{
				$delivery_date_temp = $property->value;
				//array_push($delivery_date_array, $property->value);
			}
			if($property->name == 'Delivery-Time')
			{
				$delivery_time_temp = $property->value;
				// array_push($delivery_time_array, $property->value);
			}
		}
		$temp_delivery_info = array($delivery_date_temp, $delivery_time_temp);
		array_push($delivery_info, $temp_delivery_info);
	}
	$delivery_info_json = json_encode($delivery_info);
    
	if($date == "" || empty($date) || $date == "0000-00-00" || $date == "1969-12-31")
	{	
                
                
		$app_settings = "SELECT * FROM appsettings where id = 1";
		$app_settings_data = $connection->query($app_settings);
		$app_settings_row = $app_settings_data->fetch_assoc();
		$api_key = $app_settings_row['api_key'];		
		$shared_secret = $app_settings_row['shared_secret'];		
			
		$url = "https://$api_key:$shared_secret@$store_name/admin/shop.json";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token: $access_token"));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");		
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		curl_close ($curl);
			
		$shop_data_array = json_decode($response, true);	
		$admin_email = $shop_data_array['shop']['email'];

		// if($date_require_option == '1')
		{
			$date_flag = 1;
			$msg = $msg . "Delivery Date";	
			$insert_query = "insert into order_details(shop_id, order_id, order_name,  delivery_date, delivery_time, product_delivery_info ,ip_address, browser_info, customer_email, customer_name, admin_email, type_of_app,app_version,details) values($shop_id, '$order_id', '$order_name','$date', '$delivery_time','$delivery_info_json', '$ipaddress', '$user_agent', '$customer_email', '$customer_name', '$admin_email', $type_of_app, $app_version, '$store_name')";
		}
		// else
		// {
		// 	$insert_query = "insert into order_details(shop_id, order_id, delivery_date, delivery_time, product_delivery_info ,ip_address, browser_info, delivery_info_status, type_of_app,app_version,details) values($shop_id, '$order_id', '$date', '$delivery_time','$delivery_info_json', '$ipaddress', '$user_agent', '1' ,$type_of_app, $app_version, '$json')";				
		// }
	}
	else
	{
                
                
		$insert_query = "insert into order_details(shop_id, order_id, delivery_date, delivery_time, product_delivery_info ,ip_address, browser_info, customer_email, customer_name, admin_email, delivery_info_status, type_of_app,app_version,details) values($shop_id, '$order_id', '$date', '$delivery_time','$delivery_info_json', '$ipaddress', '$user_agent','$customer_email', '$customer_name', '$admin_email', '1' ,$type_of_app, $app_version, '$store_name')";	
	}
		//$insert_query = "insert into order_details(shop_id, order_id, delivery_date, delivery_time, product_delivery_info ,ip_address, browser_info, type_of_app,app_version,details) values($shop_id, '$order_id', '$date', '$delivery_time','$delivery_info_json', '$ipaddress', '$user_agent', $type_of_app, $app_version, '$store_name')";			
	
    if($connection->query($insert_query))
	{
			// $myfile = fopen("debug.txt", "a") or die("Unable to open file!");
			// $txt = "Test ".$other_note;
			// fwrite($myfile, "\n". $txt);
			// fclose($myfile);
	}
	else		
	{
		$insert_query = "insert into order_details(details) values('".$connection->error ."')";
		$connection->query($insert_query);
	}		
	if($type_of_app > 1 && $app_version > 1)
	{		
		$app_settings = "SELECT * FROM appsettings where id = 1";
		$app_settings_data = $connection->query($app_settings);
		$app_settings_row = $app_settings_data->fetch_assoc();
		$api_key = $app_settings_row['api_key'];		
		$shared_secret = $app_settings_row['shared_secret'];
		if($date == "" || empty($date) || $date == "0000-00-00" || $date == "1969-12-31" || $date == "1970-01-01"){

		}
		else{
			$url = "https://$api_key:$shared_secret@$store_name/admin/orders/$order_id.json";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token: $access_token"));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_VERBOSE, 0);
			curl_setopt($curl, CURLOPT_HEADER, 0);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");		
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($curl);
			curl_close ($curl);
			
			$order_data_array = json_decode($response);
			$previous_tags =  $order_data_array->order->tags;						
			$tags = "$previous_tags , $date , $delivery_time";		
			$note = '';
			if($store_name == 'virginiamaryflorist.myshopify.com'){
				$note = $other_note;
			} else {
				$note = $other_note." "."Delivery Date:- ".$date.' Delivery Time:- '.$delivery_time;
			}
			
			
			// $myfile = fopen("debug.txt", "a") or die("Unable to open file!");
			// $txt = "Test 2".$other_note;
			// fwrite($myfile, "\n". $txt);
			// fclose($myfile);

			$order_data =	[
								'order' =>[
									'id'   =>   "$order_id",
									'tags' =>   "$tags",
									'note' =>   $note,
								] 
							];
			$url = "https://$api_key:$shared_secret@$store_name/admin/orders/$order_id.json";
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token: $access_token"));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_VERBOSE, 0);
			curl_setopt($curl, CURLOPT_HEADER, 0);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($order_data));
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec ($curl);
			curl_close ($curl);				
		}        
		
		
		// $msg = $note;
		// $headers  = 'MIME-Version: 1.0' . "\r\n";
		// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";		
		// mail("dipal.zestard@gmail.com", "Regarging Order #".$order_id, $msg, $headers);	
		
	}               
	
?>