-- MySQL dump 10.13  Distrib 5.6.27-75.0, for Linux (x86_64)
--
-- Host: localhost    Database: anujdala_ddp_optimize
-- ------------------------------------------------------
-- Server version	5.6.40-84.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_config`
--

DROP TABLE IF EXISTS `app_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_format` text NOT NULL,
  `shop_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_config`
--

LOCK TABLES `app_config` WRITE;
/*!40000 ALTER TABLE `app_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_log`
--

DROP TABLE IF EXISTS `app_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(255) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `step1` varchar(255) DEFAULT NULL,
  `step1.2` varchar(255) DEFAULT NULL,
  `step2` varchar(255) DEFAULT NULL,
  `step3` varchar(255) DEFAULT NULL,
  `update_shopid_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_log`
--

LOCK TABLES `app_log` WRITE;
/*!40000 ALTER TABLE `app_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appsettings`
--

DROP TABLE IF EXISTS `appsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(300) DEFAULT NULL,
  `redirect_url` varchar(300) DEFAULT NULL,
  `permissions` text NOT NULL,
  `shared_secret` varchar(300) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appsettings`
--

LOCK TABLES `appsettings` WRITE;
/*!40000 ALTER TABLE `appsettings` DISABLE KEYS */;
INSERT INTO `appsettings` VALUES (1,'2551a481974989b8ebaef60daccf5e76','https://shopifydev.anujdalal.com/deliverydatepro_optimize/public/redirect','','07cc93ffb4baf1d0708c66595c7558f5',NULL,NULL);
/*!40000 ALTER TABLE `appsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_config`
--

DROP TABLE IF EXISTS `block_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_date` text,
  `alloved_month` int(11) NOT NULL,
  `date_interval` int(11) NOT NULL,
  `datepicker_display_on` int(11) DEFAULT '0',
  `default_date_option` int(11) DEFAULT '0',
  `days` text,
  `hours` int(11) NOT NULL,
  `minute` int(11) NOT NULL,
  `cuttoff_status` int(11) DEFAULT NULL,
  `shop_id` int(11) NOT NULL,
  `app_title` text,
  `date_format` text NOT NULL,
  `show_date_format` smallint(1) NOT NULL DEFAULT '0',
  `additional_css` text,
  `app_status` char(10) NOT NULL,
  `show_datepicker_label` int(1) NOT NULL DEFAULT '1',
  `datepicker_label` varchar(255) NOT NULL DEFAULT 'Delivery Date',
  `admin_order_note` text,
  `admin_note_status` int(11) DEFAULT '0',
  `require_option` char(3) DEFAULT NULL,
  `date_error_message` varchar(255) DEFAULT 'Please Select Delivery Date',
  `required_text` text,
  `admin_time_status` int(11) DEFAULT NULL,
  `time_require_option` int(11) DEFAULT '0',
  `time_error_message` varchar(255) DEFAULT 'Please Select Delivery Time',
  `time_label` varchar(255) DEFAULT NULL,
  `time_default_option_label` varchar(255) DEFAULT 'Delivery Time',
  `delivery_time` text,
  `exclude_block_date_status` int(11) DEFAULT '0',
  `add_delivery_information` int(1) DEFAULT '0',
  `language_locale` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT 'en',
  `global_product_delivery_time` int(1) DEFAULT '0',
  `global_product_blocked_dates` int(1) DEFAULT '0',
  `global_product_blocked_days` int(1) DEFAULT '0',
  `global_product_pre_order` int(1) DEFAULT '0',
  `global_product_date_interval` int(1) DEFAULT '0',
  `global_product_cut_off` int(1) DEFAULT '0',
  `disable_checkout` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_config`
--

LOCK TABLES `block_config` WRITE;
/*!40000 ALTER TABLE `block_config` DISABLE KEYS */;
INSERT INTO `block_config` VALUES (1,'[]',6,0,0,0,NULL,0,0,1,6,'Delivery Date','yy/mm/dd',0,'','Active',1,'Pick a delivery date:','',0,'0','Please Enter Delivery Date',NULL,1,0,'Please Select Delivery Time','','Delivery Time','drghdf',0,0,'en',0,0,0,0,0,0,0),(2,'[]',4,0,0,1,NULL,1,0,0,7,'Delivery Date','mm/dd/yy',1,'','Active',1,'Pick a delivery date:','<p>test</p>\r\n',1,'1','Please Enter Delivery Date',NULL,1,1,'Please Select Delivery Time','Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,'en',0,0,0,0,0,0,0),(3,'[\"19-06-2019\"]',1,0,0,0,NULL,12,45,0,8,'Delivery Date','yy/mm/dd',0,'','Active',1,'Pick a delivery date:','',0,'0','Please Enter Delivery Date',NULL,1,0,'Please Select Delivery Time','','Delivery Time','09:00 AM,10:00 AM',0,0,'en',0,1,1,1,1,1,0),(4,NULL,6,1,0,0,NULL,0,0,NULL,9,'Delivery Date','yy/mm/dd',0,NULL,'Active',1,'Pick a delivery date:',NULL,0,NULL,'Please Enter Delivery Date',NULL,NULL,0,'Please Select Delivery Time',NULL,'Delivery Time',NULL,0,0,'en',0,0,0,0,0,0,0),(5,NULL,6,1,0,0,NULL,0,0,NULL,10,'Delivery Date','yy/mm/dd',0,NULL,'Active',1,'Pick a delivery date:',NULL,0,NULL,'Please Enter Delivery Date',NULL,NULL,0,'Please Select Delivery Time',NULL,'Delivery Time',NULL,0,0,'en',0,0,0,0,0,0,0),(6,'[]',6,1,0,0,NULL,0,0,0,11,'Delivery Date','yy/mm/dd',0,'','Active',1,'Pick a delivery date:','',0,'0','Please Enter Delivery Date',NULL,0,0,'Please Select Delivery Time','','Delivery Time','',0,0,'en',0,0,0,0,0,0,0),(7,'[]',6,1,0,0,NULL,0,0,1,12,'Delivery Date','yy/mm/dd',0,'','Active',1,'Pick a delivery date:','',0,'1','Please Enter Delivery Date',NULL,1,0,'Please Select Delivery Time','','Delivery Time','10:00,11:00',0,0,'en',0,0,0,0,0,0,0),(8,NULL,6,1,0,0,NULL,0,0,NULL,13,'Delivery Date','yy/mm/dd',0,NULL,'Active',1,'Pick a delivery date:',NULL,0,NULL,'Please Enter Delivery Date',NULL,NULL,0,'Please Select Delivery Time',NULL,'Delivery Time',NULL,0,0,'en',0,0,0,0,0,0,0),(9,'[]',6,0,0,0,NULL,12,33,0,14,'Delivery Date','yy/mm/dd',0,'','Active',1,'Pick a delivery date:','',0,'0','Please Enter Delivery Date',NULL,0,0,'Please Select Delivery Time','','Delivery Time','',0,0,'en',0,0,0,0,0,0,0),(10,NULL,6,1,0,0,NULL,0,0,NULL,15,'Delivery Date','yy/mm/dd',0,NULL,'Active',1,'Pick a delivery date:',NULL,0,NULL,'Please Enter Delivery Date',NULL,NULL,0,'Please Select Delivery Time',NULL,'Delivery Time',NULL,0,0,'en',0,0,0,0,0,0,0),(11,NULL,6,1,0,0,NULL,0,0,NULL,16,'Delivery Date','yy/mm/dd',0,NULL,'Active',1,'Pick a delivery date:',NULL,0,NULL,'Please Enter Delivery Date',NULL,NULL,0,'Please Select Delivery Time',NULL,'Delivery Time',NULL,0,0,'en',0,0,0,0,0,0,0),(14,'[\"20-06-2019\"]',6,0,0,0,NULL,0,0,1,20,'Delivery Date','yy/mm/dd',0,'','Active',1,'Pick a delivery date:','',0,'0','Please Enter Delivery Date',NULL,0,0,'Please Select Delivery Time','','Delivery Time','',0,0,'en',0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `block_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cutoff_settings`
--

DROP TABLE IF EXISTS `cutoff_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cutoff_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `store` text,
  `shop_id` int(11) DEFAULT NULL,
  `cutoff_day` varchar(255) DEFAULT NULL COMMENT '0=sunday,....,6=saturday',
  `cutoff_hour` varchar(50) DEFAULT NULL,
  `cutoff_minute` varchar(50) DEFAULT NULL,
  `update_shopid_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cutoff_settings`
--

LOCK TABLES `cutoff_settings` WRITE;
/*!40000 ALTER TABLE `cutoff_settings` DISABLE KEYS */;
INSERT INTO `cutoff_settings` VALUES (1,'dipal-test.myshopify.com',8,'0','01','00',1),(2,'dipal-test.myshopify.com',8,'1','-','-',1),(3,'dipal-test.myshopify.com',8,'2','-','-',1),(4,'dipal-test.myshopify.com',8,'3','01','00',1),(5,'dipal-test.myshopify.com',8,'4','-','-',1),(6,'dipal-test.myshopify.com',8,'5','-','-',1),(7,'dipal-test.myshopify.com',8,'6','-','-',1),(8,'vijay-test.myshopify.com',12,'0','01','00',1),(9,'vijay-test.myshopify.com',12,'1','-','-',1),(10,'vijay-test.myshopify.com',12,'2','-','-',1),(11,'vijay-test.myshopify.com',12,'3','-','-',1),(12,'vijay-test.myshopify.com',12,'4','-','-',1),(13,'vijay-test.myshopify.com',12,'5','-','-',1),(14,'vijay-test.myshopify.com',12,'6','-','-',1);
/*!40000 ALTER TABLE `cutoff_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverytime_settings`
--

DROP TABLE IF EXISTS `deliverytime_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverytime_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `store` text,
  `shop_id` int(11) DEFAULT NULL,
  `day` int(10) DEFAULT NULL COMMENT '0=sunday,....,6=saturday',
  `delivery_times` longtext,
  `count` longtext,
  `update_shopid_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverytime_settings`
--

LOCK TABLES `deliverytime_settings` WRITE;
/*!40000 ALTER TABLE `deliverytime_settings` DISABLE KEYS */;
INSERT INTO `deliverytime_settings` VALUES (1,'dipal-test.myshopify.com',8,0,'[\"12:00\",\"13:00\"]','1,',1),(2,'dipal-test.myshopify.com',8,1,'[\"10:00\"]','1',1),(3,'dipal-test.myshopify.com',8,2,'[\"10:00\"]','',1),(4,'dipal-test.myshopify.com',8,3,'[\"05:00\"]','2',1),(5,'dipal-test.myshopify.com',8,4,'[\"05:00\"]','1',1),(6,'dipal-test.myshopify.com',8,5,'[\"04:00\",\"05:00\"]','1,',1),(7,'dipal-test.myshopify.com',8,6,'[\"05:00\"]','',1),(8,'vijay-test.myshopify.com',12,0,'[\"10:00 AM\"]','',1),(9,'vijay-test.myshopify.com',12,1,'[\"\"]','',1),(10,'vijay-test.myshopify.com',12,2,'[\"1:00\"]','1',1),(11,'vijay-test.myshopify.com',12,3,'[\"\"]','',1),(12,'vijay-test.myshopify.com',12,4,'[\"\"]','',1),(13,'vijay-test.myshopify.com',12,5,'[\"\"]','',1),(14,'vijay-test.myshopify.com',12,6,'[\"\"]','',1);
/*!40000 ALTER TABLE `deliverytime_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `development_stores`
--

DROP TABLE IF EXISTS `development_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `development_stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dev_store_name` varchar(255) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `update_shopid_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `development_stores`
--

LOCK TABLES `development_stores` WRITE;
/*!40000 ALTER TABLE `development_stores` DISABLE KEYS */;
/*!40000 ALTER TABLE `development_stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_config`
--

DROP TABLE IF EXISTS `field_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `fields` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_config`
--

LOCK TABLES `field_config` WRITE;
/*!40000 ALTER TABLE `field_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `field_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shop_id` int(10) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `order_name` varchar(255) DEFAULT '',
  `delivery_date` date DEFAULT NULL,
  `delivery_time` varchar(255) DEFAULT NULL,
  `product_delivery_info` longtext,
  `ip_address` varchar(255) DEFAULT NULL,
  `browser_info` text,
  `date_and_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tag_flag` int(1) DEFAULT '0',
  `delivery_info_status` int(1) DEFAULT '0',
  `admin_email` varchar(255) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_name` text,
  `type_of_app` int(1) DEFAULT NULL,
  `app_version` int(1) DEFAULT NULL,
  `details` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_settings`
--

DROP TABLE IF EXISTS `product_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop` varchar(255) DEFAULT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `delivery_times` text,
  `blocked_dates` text,
  `blocked_days` text,
  `pre_order_time` int(10) DEFAULT NULL,
  `date_interval` int(10) DEFAULT NULL,
  `cut_off_hours` int(10) DEFAULT NULL,
  `cut_off_minutes` int(10) DEFAULT NULL,
  `update_shopid_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_settings`
--

LOCK TABLES `product_settings` WRITE;
/*!40000 ALTER TABLE `product_settings` DISABLE KEYS */;
INSERT INTO `product_settings` VALUES (1,'dipal-test.myshopify.com',8,1234127126575,1,'10:00','13-6-2019','Thursday',1,2,0,0,1),(2,'dipal-test.myshopify.com',8,1234127061039,0,'null','','all_allow',1,0,0,0,1),(3,'dipal-test.myshopify.com',8,1234127159343,0,'null','','all_allow',1,0,0,0,1),(4,'dipal-test.myshopify.com',8,1234128568367,0,'null','','all_allow',1,0,0,0,1),(5,'dipal-test.myshopify.com',8,1234128732207,0,'null','','all_allow',1,0,0,0,1),(6,'dipal-test.myshopify.com',8,1234127224879,0,'null','','all_allow',1,0,0,0,1),(7,'dipal-test.myshopify.com',8,1234126929967,0,'null','','all_allow',1,0,0,0,1),(8,'dipal-test.myshopify.com',8,1234126962735,0,'null','','all_allow',1,0,0,0,1),(9,'dipal-test.myshopify.com',8,1234126995503,0,'null','','all_allow',1,0,0,0,1),(10,'dipal-test.myshopify.com',8,1234127323183,0,'null','','all_allow',1,0,0,0,1);
/*!40000 ALTER TABLE `product_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings_log`
--

DROP TABLE IF EXISTS `settings_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `block_date` text,
  `alloved_month` int(11) NOT NULL,
  `date_interval` int(11) NOT NULL,
  `datepicker_display_on` int(11) DEFAULT '0',
  `default_date_option` int(11) DEFAULT '0',
  `days` text,
  `hours` int(11) NOT NULL,
  `minute` int(11) NOT NULL,
  `cuttoff_status` int(11) DEFAULT NULL,
  `app_title` text,
  `date_format` text NOT NULL,
  `show_date_format` smallint(1) NOT NULL DEFAULT '0',
  `additional_css` text,
  `app_status` char(10) NOT NULL,
  `datepicker_label` varchar(255) NOT NULL DEFAULT 'Delivery Date',
  `admin_order_note` text,
  `admin_note_status` int(11) DEFAULT '0',
  `require_option` char(3) DEFAULT NULL,
  `required_text` text,
  `admin_time_status` int(11) DEFAULT NULL,
  `time_require_option` int(11) DEFAULT '0',
  `time_label` varchar(255) DEFAULT NULL,
  `time_default_option_label` varchar(255) DEFAULT 'Delivery Time',
  `delivery_time` text,
  `exclude_block_date_status` int(11) DEFAULT '0',
  `add_delivery_information` int(1) DEFAULT '0',
  `type_of_app` int(1) DEFAULT NULL,
  `app_version` int(1) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `date_and_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings_log`
--

LOCK TABLES `settings_log` WRITE;
/*!40000 ALTER TABLE `settings_log` DISABLE KEYS */;
INSERT INTO `settings_log` VALUES (1,6,NULL,6,1,0,0,NULL,0,0,NULL,NULL,'yy/mm/dd',0,NULL,'Active','Pick a delivery date:',NULL,0,NULL,NULL,NULL,0,NULL,'Delivery Time',NULL,0,0,1,1,'103.254.244.134','2019-06-05 12:12:04'),(2,6,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,1,1,1,'103.254.244.134','2019-06-05 12:24:06'),(3,6,'[\"2019-06-05\"]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-05 12:46:23'),(4,6,'[\"05-06-2019\"]',6,1,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-05 12:48:34'),(5,6,'[\"05-06-2019\"]',6,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-05 12:48:35'),(6,6,'[\"05-06-2019\"]',6,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-05 12:49:37'),(7,6,'[]',6,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-05 12:55:48'),(8,6,'[]',6,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-05 12:56:19'),(9,7,NULL,6,1,0,0,NULL,0,0,NULL,NULL,'yy/mm/dd',0,NULL,'Active','Pick a delivery date:',NULL,0,NULL,NULL,NULL,0,NULL,'Delivery Time',NULL,0,0,3,3,'103.254.244.134','2019-06-05 13:09:02'),(10,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:21:37'),(11,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Deactive','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:22:40'),(12,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:24:23'),(13,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Deactive','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:24:44'),(14,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:25:00'),(15,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:25:23'),(16,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:25:41'),(17,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:26:06'),(18,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:26:26'),(19,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:27:06'),(20,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:27:39'),(21,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,0,0,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:27:58'),(22,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:29:25'),(23,7,'[]',6,1,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:29:58'),(24,7,'[\"06-06-2019\",\"09-06-2019\"]',6,1,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:30:34'),(25,7,'[\"06-06-2019\",\"09-06-2019\"]',6,1,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:31:04'),(26,7,'[\"06-06-2019\",\"09-06-2019\"]',6,1,0,0,'[\"Sunday\",\"Saturday\"]',0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:32:04'),(27,7,'[\"06-06-2019\",\"09-06-2019\"]',6,1,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:32:39'),(28,7,'[\"06-06-2019\"]',6,1,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:32:45'),(29,7,'[\"06-06-2019\"]',6,0,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:33:05'),(30,7,'[\"06-06-2019\"]',6,0,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:33:26'),(31,7,'[\"06-06-2019\"]',4,0,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:33:55'),(32,7,'[\"06-06-2019\"]',4,0,1,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:34:17'),(33,7,'[\"06-06-2019\"]',4,0,0,0,NULL,0,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:34:40'),(34,7,'[\"06-06-2019\"]',4,0,0,0,NULL,10,0,1,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:35:28'),(35,7,'[\"06-06-2019\"]',4,0,0,0,NULL,10,0,1,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','<p>test</p>\r\n',1,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 05:37:20'),(36,7,'[\"06-06-2019\"]',4,0,0,0,NULL,10,0,1,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','<p>test</p>\r\n',1,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,1,1,1,'103.254.244.134','2019-06-06 05:39:14'),(37,7,'[\"06-06-2019\"]',4,0,0,0,NULL,10,0,1,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','<p>test</p>\r\n',1,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 06:42:10'),(38,7,'[]',4,0,0,0,NULL,10,0,1,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','<p>test</p>\r\n',1,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 06:49:47'),(39,7,'[]',4,0,0,0,NULL,1,0,1,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','<p>test</p>\r\n',1,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 06:51:18'),(40,7,'[]',4,0,0,0,NULL,1,0,0,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','<p>test</p>\r\n',1,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,1,1,'103.254.244.134','2019-06-06 06:51:34'),(41,7,'[]',4,0,0,0,NULL,1,0,1,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','<p>test</p>\r\n',1,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,2,2,'103.254.244.134','2019-06-06 06:59:14'),(42,7,'[]',4,0,0,1,NULL,1,0,1,NULL,'mm/dd/yy',1,'','Active','Pick a delivery date:','<p>test</p>\r\n',1,'1',NULL,1,1,'Delivery available','Delivery Time','9:00 AM to 10:00 PM',0,0,2,2,'103.254.244.134','2019-06-06 07:00:15'),(43,8,NULL,6,1,0,0,NULL,0,0,NULL,NULL,'yy/mm/dd',0,NULL,'Active','Pick a delivery date:',NULL,0,NULL,NULL,NULL,0,NULL,'Delivery Time',NULL,0,0,2,2,'103.254.244.134','2019-06-06 07:35:23'),(44,8,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,2,2,'103.254.244.134','2019-06-06 07:36:08'),(45,8,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-06 07:38:49'),(46,8,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 07:40:41'),(47,8,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 08:26:50'),(48,8,'[]',6,1,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 08:27:38'),(49,8,'[]',6,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 09:16:54'),(50,8,'[]',6,0,1,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 09:18:22'),(51,8,'[]',6,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 10:06:11'),(52,8,'[]',6,1,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 12:38:24'),(53,8,'[]',6,1,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 13:12:28'),(54,8,'[]',6,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 13:20:04'),(55,8,'[]',6,1,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 13:21:40'),(56,8,'[]',6,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-06 13:31:58'),(57,8,'[]',6,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','dfdfhdfhfh',0,0,3,3,'103.254.244.134','2019-06-06 13:32:42'),(58,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-07 09:13:42'),(59,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-07 09:32:35'),(60,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,1,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-07 09:48:28'),(61,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,1,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-07 09:49:23'),(62,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,1,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-07 09:54:55'),(63,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,1,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-07 10:01:10'),(64,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,1,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-07 10:01:34'),(65,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,1,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-07 10:03:00'),(66,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,1,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-07 10:03:09'),(67,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,1,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-07 10:11:12'),(68,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,1,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-07 10:18:13'),(69,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,1,'','Delivery Time','dsffsdf',0,0,3,3,'103.254.244.134','2019-06-07 10:34:47'),(70,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'','Delivery Time','dsffsdf',0,0,3,3,'103.254.244.134','2019-06-07 10:39:45'),(71,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'','Delivery Time','dsffsdf',0,0,3,3,'103.254.244.134','2019-06-07 10:47:21'),(72,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'','Delivery Time','dsffsdf',0,0,3,3,'103.254.244.134','2019-06-07 10:48:04'),(73,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'','Delivery Time','dsffsdf',0,0,3,3,'103.254.244.134','2019-06-07 12:14:23'),(74,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,1,1,'','Delivery Time','Morning',0,0,4,4,'103.254.244.134','2019-06-08 05:00:10'),(75,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,1,0,'','Delivery Time','Morning',0,0,4,4,'103.254.244.134','2019-06-08 05:00:40'),(76,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,0,0,'','Delivery Time','Morning',0,0,4,4,'103.254.244.134','2019-06-08 05:30:25'),(77,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,0,0,'','Delivery Time','Morning',0,0,4,4,'103.254.244.134','2019-06-08 05:31:26'),(78,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Deactive','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','Morning',0,0,4,4,'103.254.244.134','2019-06-08 05:31:47'),(79,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','Morning',0,0,4,4,'103.254.244.134','2019-06-08 05:32:14'),(80,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','Morning',0,0,4,4,'103.254.244.134','2019-06-08 05:34:56'),(81,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','Morning',0,0,4,4,'103.254.244.134','2019-06-08 08:43:27'),(82,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','Morning',0,0,4,4,'103.254.244.134','2019-06-08 09:52:38'),(83,8,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-08 10:20:40'),(84,11,NULL,6,1,0,0,NULL,0,0,NULL,NULL,'yy/mm/dd',0,NULL,'Active','Pick a delivery date:',NULL,0,NULL,NULL,NULL,0,NULL,'Delivery Time',NULL,0,0,1,1,'103.254.244.134','2019-06-08 11:35:10'),(85,12,NULL,6,1,0,0,NULL,0,0,NULL,NULL,'yy/mm/dd',0,NULL,'Active','Pick a delivery date:',NULL,0,NULL,NULL,NULL,0,NULL,'Delivery Time',NULL,0,0,3,3,'103.254.244.134','2019-06-08 11:52:33'),(86,8,'[\"15-06-2019\"]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 08:50:21'),(87,8,'[\"15-06-2019\"]',6,0,0,0,'[\"Monday\",\"Friday\"]',0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 08:52:44'),(88,8,'[\"15-06-2019\"]',3,0,0,0,'[\"Monday\",\"Friday\"]',0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 08:54:54'),(89,8,'[\"15-06-2019\"]',3,2,0,0,'[\"Monday\",\"Friday\"]',0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 09:06:13'),(90,8,'[\"15-06-2019\"]',3,2,0,0,'[\"Monday\",\"Friday\"]',10,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 09:06:41'),(91,8,'[\"15-06-2019\"]',3,2,0,0,'[\"Monday\",\"Friday\"]',10,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 09:09:16'),(92,8,'[]',0,0,0,0,NULL,10,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 09:09:59'),(93,8,'[]',1,0,0,0,NULL,10,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 09:22:16'),(94,8,'[\"19-06-2019\"]',1,0,0,0,NULL,10,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 10:23:14'),(95,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 10:37:12'),(96,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 10:40:06'),(97,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 11:17:12'),(98,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 11:18:33'),(99,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 11:55:23'),(100,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 11:55:54'),(101,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 12:01:37'),(102,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 12:21:06'),(103,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 12:26:17'),(104,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 12:27:12'),(105,8,'[\"19-06-2019\"]',1,0,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-10 12:42:26'),(106,12,'[]',6,1,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-11 05:29:55'),(107,12,'[]',6,1,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,0,0,'','Delivery Time','',0,0,3,3,'103.254.244.134','2019-06-11 05:35:28'),(108,12,'[]',6,1,0,0,NULL,0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'1',NULL,1,0,'','Delivery Time','10:00',0,0,3,3,'103.254.244.134','2019-06-11 05:35:56'),(109,8,'[\"19-06-2019\"]',1,0,0,0,NULL,1,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-11 06:05:06'),(110,8,'[\"19-06-2019\"]',1,0,0,0,NULL,1,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-11 06:05:20'),(111,8,'[\"19-06-2019\"]',1,0,0,0,NULL,1,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-11 06:06:25'),(112,14,NULL,6,1,0,0,NULL,0,0,NULL,NULL,'yy/mm/dd',0,NULL,'Active','Pick a delivery date:',NULL,0,NULL,NULL,NULL,0,NULL,'Delivery Time',NULL,0,0,1,1,'103.254.244.134','2019-06-11 06:18:23'),(113,14,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:18:45'),(114,8,'[\"19-06-2019\"]',1,0,0,0,NULL,1,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-11 06:23:47'),(115,14,'[]',6,0,0,0,NULL,1,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:38:53'),(116,14,'[]',6,0,0,0,NULL,10,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:39:49'),(117,14,'[]',6,0,0,0,NULL,10,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:44:41'),(118,14,'[]',6,0,0,0,NULL,15,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:48:16'),(119,14,'[]',6,0,0,0,NULL,15,18,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:49:15'),(120,14,'[]',6,0,0,0,NULL,15,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:49:41'),(121,14,'[]',6,0,0,0,NULL,11,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:50:29'),(122,14,'[]',6,0,0,0,NULL,18,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:54:04'),(123,14,'[]',6,0,0,0,NULL,18,24,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:54:22'),(124,14,'[]',6,0,0,0,NULL,11,24,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:57:06'),(125,14,'[]',6,0,0,0,NULL,13,10,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:57:53'),(126,14,'[]',6,0,0,0,NULL,13,30,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 06:58:59'),(127,14,'[]',6,0,0,0,NULL,12,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-11 07:01:08'),(128,8,'[\"19-06-2019\"]',1,0,0,0,NULL,10,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-11 07:16:42'),(129,8,'[\"19-06-2019\"]',1,0,0,0,NULL,15,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-11 07:20:16'),(130,8,'[\"19-06-2019\"]',1,0,0,0,NULL,15,45,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-11 07:27:58'),(131,8,'[\"19-06-2019\"]',1,0,0,0,NULL,15,45,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-11 07:30:42'),(132,8,'[\"19-06-2019\"]',1,0,0,0,NULL,12,45,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-11 07:31:52'),(133,14,'[]',6,0,0,0,NULL,12,33,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,2,2,'103.254.244.134','2019-06-11 10:18:12'),(152,8,'[\"19-06-2019\"]',1,0,0,0,NULL,12,45,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,4,4,'103.254.244.134','2019-06-13 05:46:36'),(153,19,NULL,6,1,0,0,NULL,0,0,NULL,NULL,'yy/mm/dd',0,NULL,'Active','Pick a delivery date:',NULL,0,NULL,NULL,NULL,0,NULL,'Delivery Time',NULL,0,0,1,1,'103.254.244.134','2019-06-13 11:16:51'),(154,19,'[\"2019-06-13\"]',6,1,0,0,'[\"Sunday\"]',0,0,0,NULL,'dd/mm/yy',1,'','Active','Pick a delivery date:','',0,'1',NULL,1,0,'Delivery available','Delivery Time','10:00',0,0,2,2,'103.254.244.134','2019-06-13 11:22:58'),(155,20,NULL,6,1,0,0,NULL,0,0,NULL,NULL,'yy/mm/dd',0,NULL,'Active','Pick a delivery date:',NULL,0,NULL,NULL,NULL,0,NULL,'Delivery Time',NULL,0,0,1,1,'103.254.244.134','2019-06-13 12:27:13'),(156,20,'[]',6,0,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 12:28:06'),(157,20,'[]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 12:30:13'),(158,20,'[\"2019-06-20\"]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 12:30:39'),(159,20,'[\"20-06-2019\"]',6,1,0,0,NULL,0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 12:31:19'),(160,20,'[\"20-06-2019\"]',6,1,0,0,'[\"Saturday\"]',0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 12:31:52'),(161,20,'[\"20-06-2019\"]',6,2,0,0,'[\"Saturday\"]',0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 13:08:07'),(162,20,'[\"20-06-2019\"]',6,2,0,0,'[\"Saturday\"]',0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 13:08:24'),(163,20,'[\"20-06-2019\"]',6,0,0,0,'[\"Saturday\"]',0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 13:08:41'),(164,20,'[\"20-06-2019\"]',6,0,0,0,'[\"Saturday\"]',0,0,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 13:11:23'),(165,20,'[\"20-06-2019\"]',6,0,0,0,'[\"Saturday\"]',0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 13:11:47'),(166,20,'[\"20-06-2019\"]',6,0,0,0,'[\"Saturday\"]',1,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 13:12:05'),(167,20,'[\"20-06-2019\"]',6,0,0,0,'[\"Saturday\"]',23,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 13:14:44'),(168,20,'[\"20-06-2019\"]',6,0,0,0,'[\"Saturday\"]',0,0,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,0,0,'','Delivery Time','',0,0,1,1,'103.254.244.134','2019-06-13 13:24:52'),(169,8,'[\"19-06-2019\"]',1,0,0,0,NULL,12,45,0,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,2,2,'103.254.244.134','2019-06-17 08:42:05'),(170,8,'[\"19-06-2019\"]',1,0,0,0,NULL,12,45,1,NULL,'yy/mm/dd',0,'','Active','Pick a delivery date:','',0,'0',NULL,1,0,'','Delivery Time','09:00 AM,10:00 AM',0,0,2,2,'103.254.244.134','2019-06-18 07:29:07');
/*!40000 ALTER TABLE `settings_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trial_info`
--

DROP TABLE IF EXISTS `trial_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trial_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(255) NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `trial_days` int(11) NOT NULL,
  `activated_on` varchar(255) DEFAULT NULL,
  `trial_ends_on` varchar(255) DEFAULT NULL,
  `update_shopid_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trial_info`
--

LOCK TABLES `trial_info` WRITE;
/*!40000 ALTER TABLE `trial_info` DISABLE KEYS */;
INSERT INTO `trial_info` VALUES (1,'dipal-test.myshopify.com',8,1,'2019-06-06','2019-06-07',1),(2,'vijay-test.myshopify.com',12,3,'2019-06-08','2019-06-11',1),(4,'ishita-test.myshopify.com',20,3,'2019-06-13','2019-06-16',0);
/*!40000 ALTER TABLE `trial_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersettings`
--

DROP TABLE IF EXISTS `usersettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` text NOT NULL,
  `store_name` varchar(255) NOT NULL,
  `store_encrypt` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `charge_id` varchar(255) DEFAULT NULL,
  `api_client_id` int(11) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `billing_on` varchar(255) DEFAULT NULL,
  `payment_created_at` varchar(255) DEFAULT NULL,
  `activated_on` varchar(255) DEFAULT NULL,
  `trial_ends_on` varchar(255) DEFAULT NULL,
  `cancelled_on` varchar(255) DEFAULT NULL,
  `trial_days` varchar(255) DEFAULT NULL,
  `decorated_return_url` varchar(255) DEFAULT NULL,
  `confirmation_url` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `write_orders_access` int(1) DEFAULT '1',
  `records_per_page` int(10) DEFAULT '25',
  `delivery_time_count_status` int(1) NOT NULL DEFAULT '0',
  `global_delivery_time_status` int(1) NOT NULL DEFAULT '1',
  `global_cutoff_time_status` int(1) NOT NULL DEFAULT '1',
  `app_version` int(1) NOT NULL DEFAULT '0' COMMENT '1 = Basic, 2 = Pro, 3 = Enterprise',
  `type_of_app` int(1) NOT NULL DEFAULT '0' COMMENT '1 = Basic, 2 = Pro, 3 = Enterprise',
  `usage_charge_id` int(11) DEFAULT NULL,
  `usage_price` varchar(255) DEFAULT NULL,
  `new_install` char(1) DEFAULT 'Y',
  `upgrade_status` char(1) DEFAULT 'N',
  `upgrade_modal_status` char(1) DEFAULT 'Y',
  `language_locale` varchar(255) DEFAULT 'en',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersettings`
--

LOCK TABLES `usersettings` WRITE;
/*!40000 ALTER TABLE `usersettings` DISABLE KEYS */;
INSERT INTO `usersettings` VALUES (8,'0f78032b4bb581a6aba852c1c8a0059f','dipal-test.myshopify.com','ze1XoD9UZ92vg',NULL,'2019-06-07 17:24:08','3671916591',2944143,'0.01','active',NULL,'2019-06-06T13:03:12+05:30','2019-06-06','2019-06-07',NULL,'1','https://shopifydev.anujdalal.com/deliverydatepro_optimize/public/payment_success?charge_id=3671916591','https://dipal-test.myshopify.com/admin/charges/3671916591/confirm_recurring_application_charge?signature=BAh7BzoHaWRsKwcvAN3aOhJhdXRvX2FjdGl2YXRlRg%3D%3D--d31233cb2175b298cc3d50aa4ca4dfa5755fde5c','dipal-test.myshopify.com',1,25,0,0,1,1,1,14944873,'0.01','N','Y','N','en'),(12,'9a675018e9e5a6d6a625e08023b918eb','vijay-test.myshopify.com','ze7iYV6OQkc4c',NULL,'2019-06-11 10:37:29','4900126771',2944143,'0.01','active',NULL,'2019-06-08T07:43:04-04:00','2019-06-08','2019-06-11',NULL,'3','https://shopifydev.anujdalal.com/deliverydatepro_optimize/public/payment_success?charge_id=4900126771','https://vijay-test.myshopify.com/admin/charges/4900126771/confirm_recurring_application_charge?signature=BAh7BzoHaWRsKwgzABIkAQA6EmF1dG9fYWN0aXZhdGVG--90290d7d3e504d7da7a9915f4ca83115e25d7aca','vijay-test.myshopify.com',1,25,0,0,0,3,3,14816901,'7.00','N','Y','N','en'),(20,'29e7200dfe6dc49dd051125ef513db1c','ishita-test.myshopify.com','ze7WuIIhYJu7w',NULL,'2019-06-13 17:13:21','9994862637',2944143,'0.01','active',NULL,'2019-06-13T17:42:51+05:30','2019-06-13','2019-06-16',NULL,'3','https://shopifydev.anujdalal.com/deliverydatepro_optimize/public/payment_success?charge_id=9994862637','https://ishita-test.myshopify.com/admin/charges/9994862637/confirm_recurring_application_charge?signature=BAh7BzoHaWRsKwgtgL1TAgA6EmF1dG9fYWN0aXZhdGVG--705a53c304c4fe210b47af52f13d6eed543a5bcb','ishita-test.myshopify.com',1,25,0,1,1,1,1,NULL,'0.01','N','N','N','en');
/*!40000 ALTER TABLE `usersettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visitors_log`
--

DROP TABLE IF EXISTS `visitors_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitors_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` text,
  `shop_id` int(11) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `browser_info` text,
  `date_and_time` varchar(255) DEFAULT NULL,
  `update_shopid_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visitors_log`
--

LOCK TABLES `visitors_log` WRITE;
/*!40000 ALTER TABLE `visitors_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `visitors_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-18  4:52:30
