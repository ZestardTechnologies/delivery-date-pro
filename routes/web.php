<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::get('dashboard', 'dateController@index')->name('dashboard');

Route::any('save-visitors-log', 'dateController@save_visitors_log')->name('save-visitors-log');

Route::any('save-orders-log', 'orderController@save_orders_log')->name('save-orders-log');

Route::any('order','orderController@index')->name('order');

Route::any('orders','orderController@orders')->name('orders');

Route::any('order-demo','orderController@order_export')->name('order-demo');

Route::any('cut-off','cutoffController@cut_off')->middleware('check_permission')->name('cut-off');

Route::any('save-cut-off','cutoffController@save_cut_off')->middleware('check_permission')->name('save-cut-off');

Route::any('delivery-time','deliverytimeController@delivery_time')->middleware('check_permission')->name('delivery-time');

Route::any('save-delivery-times','deliverytimeController@save_delivery_times')->middleware('check_permission')->name('save-delivery-times');

Route::any('delivery-times','dateController@get_delivery_times')->middleware('cors')->name('delivery-times');

Route::any('get-cutoff-time','dateController@get_cutoff_time')->middleware('cors')->name('get-cutoff-time');

Route::any('check-cuttoff','dateController@check_cutoff_time')->middleware('cors')->name('check-cuttoff');

Route::any('check-blocked-day','dateController@check_blocked_day')->middleware('cors')->name('check-blocked-day');

Route::any('check-blocked-date','dateController@check_blocked_date')->middleware('cors')->name('check-blocked-date');

Route::any('check-delivery-time','dateController@check_delivery_time')->middleware('cors')->name('check-delivery-time');

Route::any('get-type-and-version','dateController@get_type_and_version')->middleware('cors')->name('get-type-and-version');

Route::any('update-modal-status','dateController@update_modal_status')->middleware('cors')->name('update-modal-status');

Route::any('upgrade-modal-status','dateController@update_upgrade_modal_status')->middleware('cors')->name('upgrade-modal-status');

Route::any('todays-orders','orderController@todays_view')->name('todays-orders');

Route::any('weeks-orders','orderController@weeks_view')->name('weeks-orders');

Route::any('todays','orderController@todays_orders')->name('todays');

Route::any('weeks','orderController@weeks_orders')->name('weeks');

Route::any('denied','dateController@permission_denied')->name('denied');

Route::any('acknowledge','dateController@acknowledge')->middleware('cors')->name('acknowledge');

Route::any('testing','testing_controller@testing')->name('testing');

Route::any('get-orders','testing_controller@get_orders')->middleware('cors')->name('get-orders');

Route::any('save-delivery-info','dateController@save_delivery_info')->middleware('cors')->name('save-delivery-info');

Route::any('check-delivery-info','dateController@check_delivery_info')->middleware('cors')->name('check-delivery-info');	

Route::any('testing-view','testing_controller@testing_view')->name('testing-view');

Route::post('editorder/{id}', 'orderController@update')->name('edit');

Route::post('save_selected_fields', 'orderController@update_fields')->name('save_selected_fields');

Route::post('update_order', 'orderController@update_order')->name('update_order');

Route::post('saveconfig','dateController@store')->name('saveconfig');

Route::get('getconfig','dateController@selectdate')->middleware('cors')->name('getconfig');

Route::get('getconfigandtype','dateController@getconfigandtype')->middleware('cors')->name('getconfigandtype');

Route::any('delivery-date-pro','dateController@delivery_date_pro')->middleware('cors')->name('delivery-date-pro');

Route::any('edit/{id}','dateController@update')->name('edit');

Route::any('plans','callbackController@plans')->name('plans');

Route::any('change-plans','callbackController@change_plan')->name('change-plans');

Route::any('send-confirm-email','callbackController@send_confirm_email')->name('send-confirm-email');

Route::any('save-order-tag','testing_controller@save_order_tag')->name('save-order-tag');

Route::any('basic','callbackController@basic')->name('basic');

Route::any('profesional','callbackController@professional')->name('profesional');

Route::any('enterprise','callbackController@enterprise')->name('enterprise');

Route::any('check-cart', 'dateController@check_cart')->middleware('cors')->name('check-cart');
Route::any('help', 'callbackController@help')->name('help');

// Route::get('help', function(){
//     return view('help')->with('active', 'help');
// })->name('help');

Route::any('new-features', function(){
    return view('features');
})->name('new-features');

Route::any('snippet', function(){
    return view('snippets');
});

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');

Route::get('plan/{plan_id}','callbackController@usage_charge')->name('plan');

// Product Settings Routes
Route::any('check-data', 'dateController@check_data')->name('check-data');

Route::any('get-product-data', 'dateController@get_product_data')->name('get-product-data');

Route::any('product-settings', 'Product_Controller@product_settings')->middleware('check_permission_product')->name('product-settings');

Route::any('save-product-settings', 'Product_Controller@save_product_settings')->middleware('check_permission_product')->name('save-product-settings');

Route::any('save-product-data', 'Product_Controller@check_data')->middleware('check_permission_product')->name('save-product-data');

Route::any('product-data','Product_Controller@product_data')->name('product-data');

Route::any('get-product-settings','Product_Controller@get_product_settings')->middleware('cors')->name('get-product-settings');
// Product Settings Routes

// Upgrade
Route::any('feature-upgrade','callbackController@feature_upgrade')->name('feature_upgrade');

Route::any('ultimate','callbackController@ultimate')->name('ultimate');
// Upgrade

Route::any('check-delivery-info','dateController@check_delivery_info')->middleware('cors')->name('check-delivery-info');

Route::any('thank-you-page','dateController@thank_you_page')->middleware('cors')->name('thank-you-page');

Route::any('update_db','callbackController@update_db')->name('update_db');

//Route::any('newdash','dateController@newdash')->name('newdash');
//Route::any('notification_kindnotes', 'notificationController@today_order_nitification')->name('notification_kindnotes');

//Route::any('get_product_status','ProductStatusController@get_product_status')->middleware('cors')->name('get_product_status');
Route::post('snippet-create-product', 'callbackController@SnippetCreateProduct')->name('snippet_create_product');
Route::post('snippet-create-cart', 'callbackController@SnippetCreateCart')->name('snippet_create_cart');