function validations(event)
{
    var show_time = $('#checkboxID5').prop("checked");
    var show_datepicker_label = $('#checkboxID6').prop("checked");
    var delivery_date_required = $('#checkboxID1').prop("checked");
    var delivery_time = $("#delivery_time").val();
    var datepicker_label = $("#datepicker_label").val();
    var date_error_message = $("#date_error_message").val();
    var delivery_time_required = $('#checkboxID4').prop("checked");
    var time_error_message = $("#time_error_message").val();

    if (!delivery_time || delivery_time == "")
    {
        if (show_time == true)
        {
            alert("Please enter delivery time");
            $("#delivery_time").focus();
            return false;
        }

    }

    if (!datepicker_label || datepicker_label == "")
    {
        if (show_datepicker_label)
        {
            alert("Please enter datepicker label");
            $("#datepicker_label").focus();
            return false;
            //event.preventDefault();
        }
    }
    if (!date_error_message || date_error_message == "")
    {
        if (delivery_date_required)
        {
            alert("Delivery Date is required please enter validation message");
            $("#date_error_message").focus();
            return false;
            //event.preventDefault();
        }
    }
    if (!time_error_message || time_error_message == "")
    {
        if (delivery_time_required)
        {
            alert("Delivery Time is required please enter validation message");
            $("#time_error_message").focus();
            return false;
            //event.preventDefault();
        }
    }
    if ($("#example-number-input").val() < 0)
    {
        alert("Allowed Pre-order Time should be atleast 1");
        $("#example-number-input").focus();
        return false;
        //event.preventDefault();
    }
    $("#delivery_date_form").attr("data-shopify-app-submit", "form_submit");
}

$(document).ready(function () {
    $("#dont_show_again").change(function () {
        var checked = $(this).prop("checked");
        var shop_name = "{{ session('shop') }}";
        if (!checked)
        {
            $.ajax({
                url: 'update-modal-status',
                data: {shop_name: shop_name},
                async: false,
                type: 'POST',
                success: function (result)
                {

                }
            });
            $('#new_note').modal('toggle');
        }
    });

    $(".slide_down").click(function () {
        var display = $("#shortcode_info").css("display");
        $("#shortcode_info").slideToggle();
        if (display == "none")
        {
            $(".slide_down i").removeClass("fa fa-chevron-up");
            $(".slide_down i").addClass("fa fa-chevron-down");
        } else
        {
            $(".slide_down i").removeClass("fa fa-chevron-down");
            $(".slide_down i").addClass("fa fa-chevron-up");
        }
    });
    $(".slide_down_product").click(function () {
        var display = $("#shortcode_info_product").css("display");
        $("#shortcode_info_product").slideToggle();
        if (display == "none")
        {
            $(".slide_down_product i").removeClass("fa fa-chevron-up");
            $(".slide_down_product i").addClass("fa fa-chevron-down");
        } else
        {
            $(".slide_down_product i").removeClass("fa fa-chevron-down");
            $(".slide_down_product i").addClass("fa fa-chevron-up");
        }
    });   
});