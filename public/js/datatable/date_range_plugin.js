var min="24/12/2017";
var max="24/12/2017";

$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {        
		min = $('#min').val();
        max = $('#max').val();   
        var iStartDateCol = 16;
        var iEndDateCol = 16;
		
        min=min.substring(6,10) + min.substring(3,5)+ min.substring(0,2);
        max=max.substring(6,10) + max.substring(3,5)+ max.substring(0,2);
 
        var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
        var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);
 
        if ( min === "" && max === "" )
        {
            return true;
        }
        else if ( min <= datofini && max === "")
        {
            return true;
        }
        else if ( max >= datoffin && min === "")
        {
            return true;
        }
        else if (min <= datofini && max >= datoffin)
        {
            return true;
        }
        return false;
    }
); 