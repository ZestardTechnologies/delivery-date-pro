var shop_name = Shopify.shop;


var all_path = "https://shopifydev.anujdalal.com/DevDeliveryDatePro/public/";
$zestard_jq = "";

//Check if Jquery is Undefined or Not. If Undefined, Then Load Jquery
if (!jQuery) {
    function jQuery_load() { return { ready: function(func) { drh_callbacks.push(func); } }; };
    var drh_callbacks = [];
    setTimeout(function() {
        var scr = document.createElement('script');
        scr.src = all_path + 'js/jquery_3.2.1.js';
        document.head.appendChild(scr);
        scr.onload = function() {
            $.each(drh_callbacks, function(i, func) { $(func); });
        };
    }, 2000);
}

//Assigning Jquery Object to Zestard_jq
$zestard_jq = jQuery;

//Check if Jquery UI is Undefined or Not. If Undefined, Then Load Jquery UI
if (typeof jQuery.ui === 'undefined') {
    function jQuery_ui() { return { ready: function(func) { drh_callbacks.push(func); } }; };
    var drh_callbacks = [];
    setTimeout(function() {
        var scr = document.createElement('script');
        scr.src = all_path + 'js/jquery-ui.js';
        document.head.appendChild(scr);

        scr.onload = function() {
            $zestard_jq.each(drh_callbacks, function(i, func) { $zestard_jq(func); });
        };
    }, 2000);

    var ready_flag = false;
    jQuery(document).ready(function(jQuery) {
        ready_flag = true;
    });

    //iphone 7
    if (ready_flag == false) {
        $(window).load(function() {
            var s = document.createElement("script");
            s.type = "text/javascript";
            s.src = "http://code.jquery.com/jquery-3.3.1.min.js";
            $("head").append(s);
            $zestard_jq = jQuery;
            deliveryDatePro();
        });
    }
}

/* if(typeof jQuery === 'undefined')
{
	$zestard_jq.ajax({
			url:all_path + "acknowledge",
			async:false,
			data:{shop_name:shop_name, type:1},
			success:function(result)
			{
						
			}
		});
}
	
if(typeof jQuery.ui === 'undefined')
{
	$zestard_jq.ajax({
			url:all_path + "acknowledge",
			async:false,
			data:{shop_name:shop_name, type:2},
			success:function(result)
			{
						
			}
		});
} */

//Calling Function	
deliveryDatePro();

//Global Variable Declarations
var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var cutoff_hours, cutoff_minute, store_hour, store_minute, datepicker_default_date, deliverytime_global_status, app_version, type_of_app, cutoff_global_status, cutoff_status, cutoff_global_hours, cutoff_global_minute, start_from, is_time_required, is_date_required, time_array, time_count, deliverytime_response, cutoff_response, date_formate, add_delivery_information, order_note;

function deliveryDatePro() {

    $zestard_jq.ajax({
        url: all_path + "get-type-and-version",
        async: false,
        data: { shop_name: shop_name },
        success: function(result) {
            app_version = result[0].app_version;
            type_of_app = result[0].type_of_app;
        }
    });

    var unavailableDates = [];
    var unavailableDays = [];
    start_from = '';
    var allowed_month = '';
    date_formate = '';
    var check_out_form = $zestard_jq("#delivery-date-pro").closest("form").attr('class');

    if (check_out_form) {
        var check_out_class_array = check_out_form.split(' ');
        if ($zestard_jq.inArray("cart-form", check_out_class_array) < 0) {
            $zestard_jq("#delivery-date-pro").closest("form").addClass("cart-form");
        }
    } else {
        $zestard_jq("#delivery-date-pro").closest("form").addClass("cart-form");
    }

    is_date_required = false;
    is_time_required = false;
    $zestard_jq.ajax({
        url: all_path + "getconfig",
        dataType: "json",
        async: false,
        data: {
            shop: shop_name
        },
        success: function(data) {

            app_status = data[0].app_status;
            datepicker_on_default = data[0].datepicker_display_on;
            datepicker_default_date = data[0].default_date_option;
            add_delivery_information = data[0].add_delivery_information;
            if (app_status == "Deactive") {
                $zestard_jq("#datepicker_box").remove();
            }

            if (data[0].require_option == 1) {
                is_date_required = true;
                $zestard_jq("#delivery-date-pro").attr("required", "true");
                $zestard_jq("#delivery-date-pro").closest("form").removeAttr('novalidate');
            }
            var date_label = data[0].datepicker_label;
            $zestard_jq('.date-box label').text(date_label);
            var dates = data[0].block_date;
            var day = data[0].days;
            start_from = '+' + data[0].date_interval;
            allowed_month = '+' + data[0].alloved_month + 'M';
            unavailableDates = $zestard_jq.parseJSON(dates);
            unavailableDays = $zestard_jq.parseJSON(day);
            date_formate = data[0].date_format;

            cutoff_status = data[0].cuttoff_status;
            cutoff_global_status = data[0].cutoff_global_status;
            deliverytime_global_status = data[0].deliverytime_global_status;

            cutoff_global_hours = data[0].hours;
            cutoff_global_minute = data[0].minute;
            var current_date = $zestard_jq("#ztpl-current-date").val();

            store_hour = $zestard_jq("#ztpl-store-hour").val();

            store_minute = $zestard_jq("#ztpl-store-minute").val();
            if (date_formate == "mm/dd/yy") {
                var display_format = "(mm/dd/yyyy)";
            } else if (date_formate == "yy/mm/dd") {
                var display_format = "(yyyy/mm/dd)";
            } else if (date_formate == "dd/mm/yy") {
                var display_format = "(dd/mm/yyyy)";
            } else {
                var display_format = "(mm/dd/yyyy)";
            }

            var show_date_format = data[0].show_date_format;
            if (show_date_format == 1) {
                if (display_format != "") {
                    $zestard_jq("#selected_format").text(display_format);
                } else {
                    $zestard_jq("#selected_format").text('mm/dd/yyyy');
                }
            }
            var admin_note_status = data[0].admin_note_status;
            var notes_admin = data[0].admin_order_note;
            if (admin_note_status == 1) {
                $zestard_jq("#admin_notes").html($zestard_jq.parseHTML(notes_admin));
            }

            var admin_time_status = data[0].admin_time_status;
            var time_lable = data[0].time_label;
            var default_option_label = data[0].time_default_option_label;
            if (admin_time_status == 1) {
                var time_in_text = data[0].delivery_time;
                time_array = new Array();
                time_array = time_in_text.split(",");
                time_count = 0;
                $zestard_jq("#delivery-time").show();
                $zestard_jq("#delivery-time").css('border', 'none');
                $zestard_jq("#time_option_label").text(default_option_label);
                if (app_version <= 2 && type_of_app <= 2) {
                    $zestard_jq(time_array).each(function() {
                        $zestard_jq('#delivery-time').append("<option value='" + time_array[time_count] + "'" + ">" + time_array[time_count] + "</option>");
                        time_count++;
                    });
                }
                $zestard_jq("#delivery-time-label").show();
                $zestard_jq("#delivery-time-label").text(time_lable);

                if (data[0].time_require_option == 1) {
                    is_time_required = true;
                    $zestard_jq("#delivery-time").attr("required", "true");
                    $zestard_jq("#delivery-date-pro").closest("form").removeAttr('novalidate');
                }
            }

            var str = parseInt(data[0].date_interval);
            if (parseInt(cutoff_status) == 0) {

            } else {
                //Check App Version if it is 3(i.e Enterprise) or more then it will Exceute Following Code For Multiple Cut Off and Delivery Time  
                if (app_version > 2 && type_of_app > 2) {
                    if (cutoff_global_status == 1) {
                        cutoff_hours = cutoff_global_hours;
                        cutoff_minute = cutoff_global_minute;
                        if (parseInt(store_hour) <= parseInt(cutoff_hours)) {
                            if (parseInt(store_hour) < parseInt(cutoff_hours)) {
                                str = parseInt(data[0].date_interval);
                            } else {
                                if (parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                    str = parseInt(data[0].date_interval);
                                } else {
                                    str = parseInt(data[0].date_interval) + 1;
                                }
                            }
                        } else {
                            str = parseInt(data[0].date_interval) + 1;
                        }
                    } else {
                        var day = new Date().getDay();
                        $zestard_jq.ajax({
                            url: all_path + "get-cutoff-time",
                            data: { day: day, shop_name: shop_name },
                            async: false,
                            success: function(response) {
                                var json_data = JSON.parse(response);
                                cutoff_hours = json_data['cutoff_hour'];
                                cutoff_minute = json_data['cutoff_minute'];
                            }
                        });
                        if (parseInt(store_hour) <= parseInt(cutoff_hours)) {
                            if (parseInt(store_hour) < parseInt(cutoff_hours)) {
                                str = parseInt(data[0].date_interval);
                            } else {
                                if (parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                    str = parseInt(data[0].date_interval);
                                } else {
                                    str = parseInt(data[0].date_interval) + 1;
                                }
                            }
                        } else {
                            str = parseInt(data[0].date_interval) + 1;
                        }
                    }
                } else {
                    cutoff_hours = cutoff_global_hours;
                    cutoff_minute = cutoff_global_minute;
                    if (parseInt(store_hour) <= parseInt(cutoff_hours)) {
                        if (parseInt(store_hour) < parseInt(cutoff_hours)) {
                            str = parseInt(data[0].date_interval);
                        } else {
                            if (parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                str = parseInt(data[0].date_interval);
                            } else {
                                str = parseInt(data[0].date_interval) + 1;
                            }
                        }
                    } else {
                        str = parseInt(data[0].date_interval) + 1;
                    }
                }
            }
            start_from = '+' + parseInt(str);
            //alert('A== '+start_from);
            if (parseInt(data[0].exclude_block_date_status) == 1) {
                //start
                var block_date_array = JSON.parse(dates);
                var block_days_array = JSON.parse(day);
                var ddmmyy = $zestard_jq("#ztpl-date-format-ddmmyy").val();
                var yymmdd = $zestard_jq("#ztpl-date-format-yymmdd").val();
                var date_index = parseInt(0);
                var reloop = function(ddmmyy, yymmdd) {
                    var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                    var current_day = new Date(yymmdd);
                    var current_available_day = weekday[current_day.getDay()];
                    //if(jQuery.inArray(ddmmyy,block_date_array) !== -1)
                    if ($zestard_jq.inArray(ddmmyy, block_date_array) !== -1) {
                        start_from = parseInt(start_from) + 1;
                        var NextDayData = new Date(new Date(yymmdd).getFullYear(), new Date(yymmdd).getMonth(), new Date(yymmdd).getDate() + 1);
                        var date = new Date(NextDayData).getDate();
                        var month = new Date(NextDayData).getMonth() + 1;
                        var year = new Date(NextDayData).getFullYear();

                        var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                        var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                        //alert(NextDayDMY+'=='+NextDayYMD);
                        return reloop(NextDayDMY, NextDayYMD);
                    } else if ($zestard_jq.inArray(current_available_day, block_days_array) !== -1) {
                        start_from = parseInt(start_from) + 1;
                        //alert('days== '+start_from);
                        var NextDayData = new Date(new Date(yymmdd).getFullYear(), new Date(yymmdd).getMonth(), new Date(yymmdd).getDate() + 1);
                        var date = new Date(NextDayData).getDate();
                        var month = new Date(NextDayData).getMonth() + 1;
                        var year = new Date(NextDayData).getFullYear();
                        var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                        var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                        return reloop(NextDayDMY, NextDayYMD);
                    }
                }
                reloop(ddmmyy, yymmdd);
                //alert('total== '+start_from);
                function pad(n) {
                    return (n < 10) ? ("0" + n) : n;
                }
            }
            var additional_css = data[0].additional_css;
            $zestard_jq('.additional-css').after("<style>" + additional_css + "</style>");
        }
    });

    setTimeout(function() {
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        function unavailable(date) {
            ymd = date.getFullYear() + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2);
            ymd1 = ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear();
            day = new Date(ymd).getDay();
            if ($zestard_jq.inArray(ymd1, unavailableDates) < 0 && $zestard_jq.inArray(days[day], unavailableDays) < 0) {
                return [true, "enabled", "Book Now"];
            } else {
                return [false, "disabled", "Booked Out"];
            }
        }
        setTimeout(function() {
            datepicker_var = undefined;
            if (datepicker_on_default == 1) {
                datepicker_var = $zestard_jq("#datepicker_box .date-box").append("<div class='visible_datepicker'></div>");
                $zestard_jq("#delivery-date-pro").attr("type", "hidden");
                $zestard_jq(".visible_datepicker").datepicker({
                    dateFormat: date_formate,
                    minDate: start_from,
                    maxDate: "'" + allowed_month + "'",
                    beforeShowDay: unavailable,
                    beforeShow: function() {
                        setTimeout(function() {
                            $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
                        }, 0);
                    }
                });
            } else {
                datepicker_var = $zestard_jq("#delivery-date-pro").datepicker({
                    dateFormat: date_formate,
                    minDate: start_from,
                    maxDate: "'" + allowed_month + "'",
                    beforeShowDay: unavailable,
                    beforeShow: function() {
                        setTimeout(function() {
                            $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
                        }, 0);
                    }
                });
            }
            alert(datepicker_var);
            if (typeof datepicker_var === 'undefined') {
                $zestard_jq.ajax({
                    url: all_path + "acknowledge",
                    async: false,
                    data: { shop_name: shop_name, type: 3 },
                    success: function(result) {

                    }
                });
            }
            if (app_version > 1 && type_of_app > 1) {
                if (datepicker_default_date == 1) {
                    $zestard_jq("#delivery-date-pro").datepicker("setDate", "today + start_from");
                }
            }
        }, 300)
        $zestard_jq("#delivery-date-pro").prop("disabled", false);
    }, 3000);

    //Check App Version if it is 3(i.e Enterprise) or more then it will Exceute Following Code For Multiple Cut Off and Delivery Time
    if (app_version > 2 && type_of_app > 2) {
        if (deliverytime_global_status == 0) {
            delivery_date_pro();
            var result = "";
            $zestard_jq.ajax({
                url: all_path + "delivery-times",
                data: { shop_name: shop_name, start_from: start_from, date_format: date_formate },
                async: false,
                success: function(response) {
                    if (response == 0) {
                        $zestard_jq("#delivery-time").html("");
                    } else {
                        $zestard_jq("#delivery-time").html("");
                        $zestard_jq("#delivery-time").append("<option value=''>Choose Time</option>");
                        for (var key in response) {
                            $zestard_jq("#delivery-time").append("<option value='" + response[key] + "'> " + response[key] + " </option>");
                        }
                    }
                }

            });
        } else {
            $zestard_jq(time_array).each(function() {
                $zestard_jq('#delivery-time').append("<option value='" + time_array[time_count] + "'" + ">" + time_array[time_count] + "</option>");
                time_count++;
            });
        }
    }

    setTimeout(function() { $zestard_jq("input[name=checkout]").removeAttr('disabled'); }, 3000);

    setTimeout(function() {
        old_order_note = $zestard_jq('textarea[name=note]').val();
        if (old_order_note) {
            open_index = old_order_note.indexOf("(Delivery Date:");
            if (open_index > -1) {
                $zestard_jq('textarea[name=note]').val(old_order_note.substring(0, open_index - 1));
            }
        }
    }, 500);

    setTimeout(function() {
        $zestard_jq('input[name=checkout], button[name=checkout]').on('click', function(event) {

            if (datepicker_on_default == 1) {
                var temp_date = $zestard_jq(".visible_datepicker").val();
                $zestard_jq("input[name=attributes[Delivery-Date]]").val(temp_date);
            }
            /* alert($zestard_jq("#delivery-date-pro").val()); 
            event.preventDefault();	 */

            order_note = '';
            old_order_note = '';
            if (app_status == 'Active') {
                if (is_date_required == true && ((typeof $zestard_jq('#delivery-date-pro').val() == 'undefined') || ($zestard_jq('#delivery-date-pro').val() == ''))) {
                    alert("Please Select Delivery Date");
                    $zestard_jq('#delivery-date-pro').focus();
                    return false;
                }
                if (is_time_required == true && ((typeof $zestard_jq('#delivery-time').val() == 'undefined') || ($zestard_jq('#delivery-time').val() == ''))) {
                    alert("Please Select Delivery Time");
                    $zestard_jq('#delivery-time').focus();
                    return false;
                }
                if (add_delivery_information == 1) {
                    order_note = $zestard_jq('textarea[name=note]').val();
                    old_order_note = $zestard_jq('textarea[name=note]').val();
                }
                //Check Blocked Days and Dates					
                if ($zestard_jq('#delivery-date-pro').val() != '') {
                    var blocked_status, json_response, blocked_date_status;
                    var todays_date = $zestard_jq('#delivery-date-pro').val();
                    var today = new Date(todays_date).getDay();
                    $zestard_jq.ajax({
                        url: all_path + "check-blocked-day",
                        data: { todays_date: todays_date, date_format: date_formate },
                        async: false,
                        success: function(result) {
                            json_response = $zestard_jq.parseJSON(result);
                        }
                    });
                    blocked_status = json_response.result;

                    $zestard_jq.ajax({
                        url: all_path + "check-blocked-date",
                        data: { delivery_date: todays_date, date_format: date_formate },
                        async: false,
                        success: function(response) {
                            blocked_date_status = response;
                        }
                    });

                    if (blocked_status == 0) {
                        alert('Sorry Delivery is Not Possible for ' + json_response.today + ' . Please Select Other Day');
                        event.preventDefault();
                    }
                    if (blocked_date_status == 0) {
                        alert('Sorry, The Delivery Date You Selected is Blocked. Please Select Other Delivery Date');
                        event.preventDefault();
                    }
                    if (blocked_date_status == 1 && blocked_status == 1) {
                        $zestard_jq.ajax({
                            url: all_path + "check-cuttoff",
                            data: { start_from: start_from, date_format: date_formate, delivery_date: $zestard_jq("#delivery-date-pro").val(), store_hour: store_hour, store_minute: store_minute, shop_name: shop_name },
                            async: false,
                            success: function(result) {
                                cutoff_response = result;
                            }
                        });
                        if (add_delivery_information == 1) {
                            order_note = order_note + " (Delivery Date: " + $zestard_jq('#delivery-date-pro').val();
                            if ($zestard_jq('#delivery-time').val() != '') {
                                order_note = order_note + ", Delivery Time: " + $zestard_jq('#delivery-time').val() + ")";
                            }
                            $zestard_jq('textarea[name=note]').val(order_note);
                            //console.log($zestard_jq('textarea[name=note]').val());
                        }
                        if (cutoff_response == 1) {
                            if ($zestard_jq("#delivery-time").val() == "" || typeof $zestard_jq('#delivery-date-pro').val() === 'undefined') {
                                if (is_time_required == true) {
                                    alert("Please Select Delivery Time");
                                    $zestard_jq('#delivery-time').focus();
                                    event.preventDefault();
                                }
                            } else {
                                if (deliverytime_global_status == 0) {
                                    $zestard_jq.ajax({
                                        url: all_path + "check-delivery-time",
                                        data: { delivery_time: $zestard_jq("#delivery-time").val(), date_format: date_formate, delivery_date: $zestard_jq("#delivery-date-pro").val(), shop_name: shop_name },
                                        async: false,
                                        success: function(result) {
                                            deliverytime_response = result;
                                        }
                                    });
                                    if (deliverytime_response == 1) {

                                    } else {
                                        alert("Selected Delivery Time Slot is Full. Plese Select Other Delivery Time");
                                        event.preventDefault();
                                    }
                                }
                            }
                        }
                        if (cutoff_response == 0) {
                            alert("Cut Off Time is Over Please Select Other Delivery Date");
                            event.preventDefault();
                        }
                        if (cutoff_response == 2) {
                            alert("Sorry You Cannot Select " + $zestard_jq("#delivery-date-pro").val() + " As Delivery Date");
                            event.preventDefault();
                        }
                    }
                }
            }
        });
    });
}
//Following Function is for Showing Delivery Time Dynamically Based on Days
function delivery_date_pro() {
    var day;
    $zestard_jq("#delivery-date-pro").change(function(dateText) {
        var result = "";

        day = new Date(this.value).getDay();
        $zestard_jq.ajax({
            url: all_path + "delivery-times",
            data: { todays_date: this.value, date_format: date_formate, shop_name: shop_name },
            async: false,
            success: function(response) {
                if (response == 0) {
                    $zestard_jq("#delivery-time").html("");
                    if (is_time_required == true) {
                        alert('Sorry No Time Slots Available For ' + $zestard_jq("#delivery-date-pro").val() + ' Please Select Other Date');
                        $zestard_jq("#delivery-date-pro").val("");
                        $zestard_jq("#delivery-date-pro").focus();
                    }
                } else {
                    $zestard_jq("#delivery-time").html("");
                    $zestard_jq("#delivery-time").append("<option value=''>Choose Time</option>");
                    for (var key in response) {
                        $zestard_jq("#delivery-time").append("<option value='" + response[key] + "'> " + response[key] + " </option>");
                    }
                }
            }
        });
    });
}