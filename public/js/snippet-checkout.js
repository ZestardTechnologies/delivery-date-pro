// Use this file for david store shopify plus for checkout page
//Global Variable Declarations 
var base_path_delivery_date = "https://zestardshop.com/shopifyapp/DeliveryDatePro/public/";
var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var cutoff_hours, cutoff_minute, store_hour, store_minute, datepicker_default_date, deliverytime_global_status, app_version, type_of_app, cutoff_global_status, cutoff_status, cutoff_global_hours, cutoff_global_minute, start_from, is_time_required, is_date_required, time_array, time_count, deliverytime_response, cutoff_response, date_formate, add_delivery_information, order_note, locale, date_error_message, store_date, delivery_date_array, formatted_delivery_date, selected_delivery_date, datepicker_on_default, textarea_variable, original_start_from, time_error_message, disable_checkout;
var xhttp, global_settings, datepicker_language;
var shop_name = Shopify.shop;
var $zestard_jq = "";
var unavailableDates = [];
var unavailableDays = [];
var start_from = '';
var allowed_month = '';
var date_formate = '';

//Check if Jquery is Undefined or Not available.
if (typeof jQuery == 'undefined') {
    //If Undefined or Not available, Then Load	
    (function() {
        var jscript = document.createElement("script");
        jscript.src = base_path_delivery_date + "js/zestard_jquery_3.3.1.js";
        jscript.type = 'text/javascript';
        jscript.async = false;

        var el = document.getElementById('datepicker_box_checkout');
        if (el == '' || el == 'undefined' || el == null) {
            elChild = document.createElement('div');
            elChild.id = 'datepicker_box_jquery_checkout';
            el.insertBefore(elChild, el.firstChild);
        } else {
            elChild = document.createElement('div');
            elChild.id = 'datepicker_box_jquery_checkout';
            el.insertBefore(elChild, el.firstChild);
            document.getElementById('datepicker_box_jquery_checkout').appendChild(jscript);
        }
        jscript.onload = function() {
            //Assigning Jquery Object to Zestard_jq
            $zestard_jq = window.jQuery;

            // Load the general settings for the datepicker
            $zestard_jq.ajax({
                url: base_path_delivery_date + "getconfig",
                dataType: "json",
                async: false,
                data: {
                    shop: shop_name
                },
                success: function(settings_info) {
                    global_settings = settings_info;
                    datepicker_language = global_settings[0].language_locale;
                }
            });

            if (global_settings != 0) {
                //If Undefined or Not available, Then Load Ui
                uiscript = document.createElement('script');
                uiscript.type = "text/javascript";
                uiscript.src = base_path_delivery_date + 'js/jquery-ui.js';
                document.getElementById('datepicker_box_jquery_checkout').appendChild(uiscript);
                uiscript.onload = function() {

                    // Check If jQuery UI Object is Undefined
                    //Send Email that jQuery is Still not Working
                    if (typeof jQuery.ui == 'undefined') {
                        //If undefined then send email                        
                        xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {

                            }
                        };
                        xhttp.open("GET", base_path_delivery_date + "acknowledge?shop_name=" + shop_name + "&type=2", true);
                        xhttp.send();
                    } else {
                        // if language is not english then load datepicker language js
                        if (datepicker_language != "en") {
                            jscript = document.createElement("script");
                            jscript.src = base_path_delivery_date + "js/locale_js/datepicker-" + datepicker_language + ".js";
                            jscript.type = 'text/javascript';
                            jscript.async = false;
                            var el = document.getElementById('datepicker_box_checkout'),
                                elChild = document.createElement('div');
                            elChild.id = 'datepicker_locale_jquery';
                            el.insertBefore(elChild, el.firstChild);
                            el.appendChild(elChild);
                            document.getElementById('datepicker_box_jquery_checkout').appendChild(jscript);
                            jscript.onload = function() {
                                $zestard_jq = window.jQuery;
                            };
                        }
                        //Calling Delivery Date Function
                        load_deliveryDatePro();
                    }
                }
            } else {
                //Do not display zestard delivery date pro (as settings are not available)
                //may be uninstalled or data is not available
                hide_deliveryDatePro();

            }
        };
    })();
}

// Hide the datepicker
function hide_deliveryDatePro() {
    //remove div from cart page
    $zestard_jq("input[name=checkout]").css('pointer-events', "auto");
    $zestard_jq("button[name=checkout]").css('pointer-events', "auto");
    $zestard_jq("#datepicker_box_checkout").remove();
}

// Show the datepicker
function load_deliveryDatePro() {
    store_date = $zestard_jq("#ztpl-date-format-ddmmyy").val();
    $zestard_jq("#delivery-date-pro-checkout").val("");
    $zestard_jq.ajax({
        url: base_path_delivery_date + "get-type-and-version",
        async: false,
        data: {
            shop_name: shop_name
        },
        success: function(result) {
            // data of general setting and user setting (blockconfig and user model)         
            general_user_settings = result;
            if (general_user_settings != 0) { // Find the general setting
                app_version = general_user_settings.app_version;
                type_of_app = general_user_settings.type_of_app;
                var check_out_form = $zestard_jq("#delivery-date-pro-checkout").closest("form").attr('class');

                if (check_out_form) {
                    var check_out_class_array = check_out_form.split(' ');
                    if ($zestard_jq.inArray("cart-form", check_out_class_array) < 0) {
                        if (shop_name != "petaljet.myshopify.com")
                            $zestard_jq("#delivery-date-pro-checkout").closest("form").addClass("cart-form");
                    }
                } else {
                    if (shop_name != "petaljet.myshopify.com")
                        $zestard_jq("#delivery-date-pro-checkout").closest("form").addClass("cart-form");
                }

                // Assign variable for datepikcer default settings
                is_date_required = false;
                is_time_required = false;
                app_status = general_user_settings.block_config.app_status;
                datepicker_language = general_user_settings.block_config.language_locale;
                datepicker_on_default = general_user_settings.block_config.datepicker_display_on;
                datepicker_default_date = general_user_settings.block_config.default_date_option;
                add_delivery_information = general_user_settings.block_config.add_delivery_information;
                date_error_message = general_user_settings.block_config.date_error_message;
                time_error_message = general_user_settings.block_config.time_error_message;

                // App is disable then delivery date pro hide
                if (app_status == "Deactive") {
                    hide_deliveryDatePro();
                }

                /* Date related settings*/
                // if datepicker is required
                if (general_user_settings.block_config.require_option == 1) {
                    is_date_required = true;
                    $zestard_jq("#delivery-date-pro-checkout").attr("required", "true");
                    $zestard_jq("#delivery-date-pro-checkout").closest("form").removeAttr('novalidate');
                }

                // Show the label of datepicker
                var date_label = general_user_settings.block_config.datepicker_label;
                show_datepicker_label = general_user_settings.block_config.show_datepicker_label;
                if (show_datepicker_label == 1) { // datepicker label true
                    $zestard_jq('.date-box label').text(date_label);
                    if (shop_name == "mitcham-central-flowers.myshopify.com") {
                        setTimeout(function() {
                            $zestard_jq('.date-box label').text(date_label);
                        }, 3000);
                    }
                } else { // datepicker label fasle
                    $zestard_jq('.date-box label').hide();
                }

                // assign variable for block dates, days, date interval and preorder month
                var dates = general_user_settings.block_config.block_date;
                var day = general_user_settings.block_config.days;
                start_from = '+' + general_user_settings.block_config.date_interval;
                allowed_month = '+' + general_user_settings.block_config.alloved_month + 'M';
                unavailableDates = $zestard_jq.parseJSON(dates);
                unavailableDays = $zestard_jq.parseJSON(day);

                // assign variable for date format, and cutoff settings and access the store date and time
                date_formate = general_user_settings.block_config.date_format;
                cutoff_status = general_user_settings.block_config.cuttoff_status;
                cutoff_global_status = general_user_settings.global_cutoff_time_status;
                deliverytime_global_status = general_user_settings.global_delivery_time_status;
                cutoff_global_hours = general_user_settings.block_config.hours;
                cutoff_global_minute = general_user_settings.block_config.minute;
                var current_date = $zestard_jq("#ztpl-current-date").val();
                store_hour = $zestard_jq("#ztpl-store-hour").val();
                store_minute = $zestard_jq("#ztpl-store-minute").val();

                if (date_formate == "mm/dd/yy") {
                    var display_format = "(mm/dd/yyyy)";
                } else if (date_formate == "yy/mm/dd") {
                    var display_format = "(yyyy/mm/dd)";
                } else if (date_formate == "dd/mm/yy") {
                    var display_format = "(dd/mm/yyyy)";
                } else {
                    var display_format = "(mm/dd/yyyy)";
                }

                // display the date format
                var show_date_format = general_user_settings.block_config.show_date_format;
                if (show_date_format == 1) {
                    if (display_format != "") {
                        $zestard_jq("#selected_format_checkout").text(display_format);
                    } else {
                        $zestard_jq("#selected_format_checkout").text('mm/dd/yyyy');
                    }
                }

                // display the delivery notes on frontend
                var admin_note_status = general_user_settings.block_config.admin_note_status;
                var notes_admin = general_user_settings.block_config.admin_order_note;
                if (admin_note_status == 1) {
                    $zestard_jq("#admin_notes").html($zestard_jq.parseHTML(notes_admin));
                }
                /* End date settings */

                /* Time related settings */
                var admin_time_status = general_user_settings.block_config.admin_time_status; //time display is true or not
                var time_lable = general_user_settings.block_config.time_label; //time label display is true or not
                var default_option_label = general_user_settings.block_config.time_default_option_label; //time option label
                if (admin_time_status == 1) { // diplay time on front true
                    var time_in_text = general_user_settings.block_config.delivery_time;
                    time_array = new Array();
                    time_array = time_in_text.split(",");
                    time_count = 0;
                    $zestard_jq("#delivery-time-checkout").css('display', 'block');
                    $zestard_jq("#delivery-time-checkout").css('border', 'none');
                    $zestard_jq("#time_option_label_checkout").text(default_option_label);
                    if (app_version <= 2 && type_of_app <= 2) { //for basic verison and professional version
                        $zestard_jq(time_array).each(function() {
                            $zestard_jq('#delivery-time-checkout').append("<option value='" + time_array[time_count] + "'" + ">" + time_array[time_count] + "</option>");
                            time_count++;
                        });
                    }
                    $zestard_jq("#delivery-time-label-checkout").show();
                    $zestard_jq("#delivery-time-label-checkout").text(time_lable);
                    if (general_user_settings.block_config.time_require_option == 1) { //time is required true
                        is_time_required = true;
                        $zestard_jq("#delivery-time-checkout").attr("required", "true");
                        $zestard_jq("#delivery-date-pro-checkout").closest("form").removeAttr('novalidate');
                    }
                }
                /* End time settings */

                /* add addtinal css if any add */
                var additional_css = general_user_settings.block_config.additional_css;
                if (shop_name == "balance80.myshopify.com") {
                    $zestard_jq('.datepicker_wrapper_box').css("cssText", "display: inline-block;background: #f5f5f5;padding: 20px 30px;color:black !important;font-weight:700;");
                } else {
                    $zestard_jq('#datepicker_box_checkout').after("<style>" + additional_css + "</style>");
                }
                /* end*/

                //Change Variable Name
                var str = parseInt(general_user_settings.block_config.date_interval);
                if (parseInt(cutoff_status) == 1) { // if cutoff status yes
                    //Check App Version if it is 3(i.e Enterprise) or more then it will Exceute Following Code For Multiple Cut Off and Delivery Time  
                    if (app_version > 2 && type_of_app > 2) {
                        if (cutoff_global_status == 1) { // if cutoff time use as global settings global = 1, local = 0                                       
                            cutoff_hours = cutoff_global_hours;
                            cutoff_minute = cutoff_global_minute;
                            if (parseInt(store_hour) <= parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                str = parseInt(general_user_settings.block_config.date_interval);
                            } else {
                                str = parseInt(general_user_settings.block_config.date_interval) + 1;
                            }
                        } else { // if cutoff settings use as local settings then display day wise cutoff time as we set as cutoff settings                                  
                            var c_day = new Date().getDay();
                            $zestard_jq.ajax({
                                url: base_path_delivery_date + "get-cutoff-time",
                                data: {
                                    day: c_day,
                                    shop_name: shop_name
                                },
                                async: false,
                                success: function(response) {
                                    var json_data = JSON.parse(response);
                                    cutoff_hours = json_data['cutoff_hour'];
                                    cutoff_minute = json_data['cutoff_minute'];
                                }
                            });
                            if (parseInt(store_hour) <= parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                str = parseInt(general_user_settings.block_config.date_interval);
                            } else if ((parseInt(cutoff_hours) == "00" && parseInt(cutoff_minute == "00")) || (cutoff_hours == "-" && cutoff_minute == "-")) {
                                str = parseInt(general_user_settings.block_config.date_interval);
                            } else {
                                str = parseInt(general_user_settings.block_config.date_interval) + 1;
                            }
                        }
                    } else { // for basic and professinal version
                        cutoff_hours = cutoff_global_hours;
                        cutoff_minute = cutoff_global_minute;
                        if (parseInt(store_hour) <= parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                            str = parseInt(general_user_settings.block_config.date_interval);
                        } else {
                            str = parseInt(general_user_settings.block_config.date_interval) + 1;
                        }
                    }
                }
                start_from = '+' + parseInt(str);
                original_start_from = start_from;

                function pad(n) {
                    return (n < 10) ? ("0" + n) : n;
                }
                var tempdate = new Date();
                tempdate.setDate(tempdate.getDate());
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var block_date_array = JSON.parse(dates);
                var block_days_array = unavailableDays;
                var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                var current_available_day = weekday[tempdate.getDay()];
                exit_flag = 0;
                temp_count = start_from;
                if (!block_days_array) {
                    block_days_array = new Array();
                }
                if (!block_date_array) {
                    block_date_array = new Array();
                }


                if (parseInt(general_user_settings.block_config.exclude_block_date_status) == 0) {
                    tempdate.setDate(tempdate.getDate() + parseInt(start_from));
                    while (temp_count != -1) {
                        var date = tempdate.getDate();
                        var month = tempdate.getMonth() + 1;
                        var year = tempdate.getFullYear();
                        NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                        NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                        current_available_day = weekday[tempdate.getDay()];
                        if ($zestard_jq.inArray(NextDayDMY, block_date_array) !== -1) {
                            index_of = block_date_array.indexOf(NextDayDMY);
                            block_date_array.splice(index_of, 1);
                            start_from = parseInt(start_from) + 1;
                            temp_count = start_from;
                        } else if ($zestard_jq.inArray(current_available_day, block_days_array) !== -1) {
                            index_of = block_days_array.indexOf(current_available_day);
                            start_from = parseInt(start_from) + 1;
                            temp_count = start_from;
                        } else {
                            temp_count = -1;
                        }
                        tempdate.setDate(tempdate.getDate() + 1);
                    }
                } else {
                    if (temp_count == 0) {
                        if (block_date_array.length > 0 || block_days_array.length > 0) {
                            while ((block_date_array.length > 0 || block_days_array.length > 0) && temp_count > -1) {
                                var date = tempdate.getDate();
                                var month = tempdate.getMonth() + 1;
                                var year = tempdate.getFullYear();
                                NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                                NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                                current_available_day = weekday[tempdate.getDay()];

                                if ($zestard_jq.inArray(NextDayDMY, block_date_array) !== -1) {
                                    index_of = block_date_array.indexOf(NextDayDMY);
                                    block_date_array.splice(index_of, 1);
                                    start_from = parseInt(start_from) + 1;
                                    temp_count = start_from;

                                } else if ($zestard_jq.inArray(current_available_day, block_days_array) !== -1) {
                                    index_of = block_days_array.indexOf(current_available_day);

                                    start_from = parseInt(start_from) + 1;
                                    temp_count = start_from;

                                } else {
                                    if (block_date_array.length <= 0 || block_days_array.length <= 0) {
                                        temp_count = -1;
                                    } else if (temp_count == 0) {
                                        temp_count = -1;

                                    } else {
                                        temp_count--;
                                    }
                                }
                                tempdate.setDate(tempdate.getDate() + 1);
                            }
                        }
                    } else {
                        while ((block_date_array.length > 0 || block_days_array.length > 0) && temp_count != -1) {
                            var date = tempdate.getDate();
                            var month = tempdate.getMonth() + 1;
                            var year = tempdate.getFullYear();
                            NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                            NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                            current_available_day = weekday[tempdate.getDay()];
                            if ($zestard_jq.inArray(NextDayDMY, block_date_array) !== -1) {
                                temp_count = 0;
                                index_of = block_date_array.indexOf(NextDayDMY);
                                block_date_array.splice(index_of, 1);
                                start_from = parseInt(start_from) + 1;
                                temp_count = start_from;

                            } else if ($zestard_jq.inArray(current_available_day, block_days_array) !== -1) {
                                index_of = block_days_array.indexOf(current_available_day);
                                temp_count = 0;

                                start_from = parseInt(start_from) + 1;
                                temp_count = start_from;

                            } else {

                                var start_from_date = new Date();
                                start_from_date.setDate(start_from_date.getDate() + parseInt(original_start_from));
                                if (tempdate.getTime() >= start_from_date.getTime()) {
                                    temp_count = -1;
                                    block_days_array = [];
                                }

                            }
                            tempdate.setDate(tempdate.getDate() + 1);

                        }
                    }
                }

                if (parseInt(general_user_settings.block_config.exclude_block_date_status) == 1 && 1 == 2) {
                    var block_date_array = JSON.parse(dates);
                    var block_days_array = JSON.parse(day);
                    var ddmmyy = $zestard_jq("#ztpl-date-format-ddmmyy").val();
                    var yymmdd = $zestard_jq("#ztpl-date-format-yymmdd").val();
                    var date_index = parseInt(0);
                    var reloop = function(ddmmyy, yymmdd) {
                        var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                        var current_day = new Date(yymmdd);
                        var current_available_day = weekday[current_day.getDay()];
                        //if(jQuery.inArray(ddmmyy,block_date_array) !== -1)							
                        //Here ddmmyy
                        if ($zestard_jq.inArray(ddmmyy, block_date_array) != -1) {
                            start_from = parseInt(start_from) + 1;

                            //And Here yymmdd??
                            var NextDayData = new Date(new Date(yymmdd).getFullYear(), new Date(yymmdd).getMonth(), new Date(yymmdd).getDate() + 1);

                            var date = new Date(NextDayData).getDate();
                            var month = new Date(NextDayData).getMonth() + 1;
                            var year = new Date(NextDayData).getFullYear();

                            var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                            var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                            //alert(NextDayDMY+'=='+NextDayYMD);
                            return reloop(NextDayDMY, NextDayYMD);
                        } else if ($zestard_jq.inArray(current_available_day, block_days_array) != -1) {
                            start_from = parseInt(start_from) + 1;
                            //alert('days== '+start_from);
                            var NextDayData = new Date(new Date(yymmdd).getFullYear(), new Date(yymmdd).getMonth(), new Date(yymmdd).getDate() + 1);
                            var date = new Date(NextDayData).getDate();
                            var month = new Date(NextDayData).getMonth() + 1;
                            var year = new Date(NextDayData).getFullYear();
                            var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                            var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                            return reloop(NextDayDMY, NextDayYMD);
                        }
                    }
                    reloop(ddmmyy, yymmdd);

                    function pad(n) {
                        return (n < 10) ? ("0" + n) : n;
                    }
                }


                setTimeout(function() {
                    setTimeout(function() {
                        datepicker_var = undefined;
                        if (datepicker_on_default == 1) { //For keeping datepicker always open	                            
                            if (datepicker_language == "en") { //datepikcer language is english
                                $zestard_jq("#datepicker_box_checkout .date-box").append("<div class='visible_datepicker'></div>");
                                $zestard_jq("#delivery-date-pro-checkout").attr("type", "hidden");
                                datepicker_var = $zestard_jq(".visible_datepicker").datepicker({
                                    dateFormat: date_formate,
                                    minDate: start_from,
                                    maxDate: "'" + allowed_month + "'",
                                    beforeShowDay: unavailable,
                                    onSelect: function(dateText) {
                                        if (deliverytime_global_status == 0) { //delivery time is set as local then call function to display delivery time based on days                                               
                                            delivery_date_pro();
                                        }
                                    },
                                    beforeShow: function() {
                                        setTimeout(function() {
                                            $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
                                        }, 0);
                                    }
                                });
                                if (datepicker_default_date == 1) {
                                    datepicker_var.datepicker("setDate", start_from);
                                }
                            } else { // load if language is not en
                                $zestard_jq("#datepicker_box_checkout .date-box").append("<div class='visible_datepicker'></div>");
                                $zestard_jq("#delivery-date-pro-checkout").attr("type", "hidden");
                                datepicker_var = $zestard_jq(".visible_datepicker").datepicker({
                                    dateFormat: date_formate,
                                    minDate: start_from,
                                    maxDate: "'" + allowed_month + "'",
                                    beforeShowDay: unavailable,
                                    defaultDate: null,
                                    onSelect: function(dateText) {
                                        if (deliverytime_global_status == 0) { //delivery time is set as local then call function to display delivery time based on days
                                            delivery_date_pro();
                                        }
                                        $('.zAddToCart').prop('disabled', false);


                                    },
                                    beforeShow: function() {
                                        setTimeout(function() {
                                            $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
                                        }, 0);
                                    }
                                });
                                if (datepicker_default_date == 1) {
                                    console.log($('.visible_datepicker').datepicker('setDate', null));
                                }
                            }
                        } else { // datepicker is hide default
                            if (datepicker_language == "en") { // language is en
                                datepicker_var = $zestard_jq("#delivery-date-pro-checkout").datepicker({
                                    dateFormat: date_formate,
                                    minDate: start_from,
                                    maxDate: "'" + allowed_month + "'",
                                    onSelect: function() {
                                        $('.zAddToCart').prop('disabled', false);
                                        //console.log($zestard_jq("#delivery-date-pro-checkout").val());

                                    },
                                    beforeShowDay: unavailable,
                                    defaultDate: null,
                                    beforeShow: function() {
                                        setTimeout(function() {
                                            $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
                                        }, 0);
                                    }
                                });
                            } else { // language is not en
                                datepicker_var = $zestard_jq("#delivery-date-pro-checkout").datepicker({
                                    dateFormat: date_formate,
                                    minDate: start_from,
                                    maxDate: "'" + allowed_month + "'",
                                    onSelect: function() {
                                        //$('#AddToCart').prop('disabled', false);
                                        $('.zAddToCart').prop('disabled', false);
                                        //console.log($zestard_jq("#delivery-date-pro-checkout").val());
                                    },
                                    beforeShowDay: unavailable,
                                    beforeShow: function() {
                                        setTimeout(function() {
                                            $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
                                        }, 0);
                                    }
                                });
                            }
                        }
                        $zestard_jq("#delivery-date-pro-checkout").val("");
                        // Check If Datepicker Object is Undefined
                        if (app_version > 1 && type_of_app > 1) { //app vesrion is not basic version
                            if (datepicker_default_date == 1) { // if datepicker default option true
                                $zestard_jq("#delivery-date-pro-checkout").datepicker("setDate", "today + start_from");
                            } else {
                                $('.visible_datepicker').datepicker('setDate', null);
                            }
                        }
                        if (typeof datepicker_var === 'undefined') {
                            if (shop_name != "purpinkg.myshopify.com") {
                                //If Undefined, Then send email
                                $zestard_jq.ajax({
                                    url: base_path_delivery_date + "acknowledge",
                                    async: false,
                                    data: {
                                        shop_name: shop_name,
                                        type: 3
                                    },
                                    success: function(result) {}
                                });
                            }
                        }
                    }, 300)
                    if (shop_name == "purpinkg.myshopify.com" || shop_name == "virginiamaryflorist.myshopify.com") {
                        setTimeout(function() {
                            $(".visible_datepicker").find(".ui-state-active").css("border", "1px solid #c3c3c3");
                            $(".visible_datepicker").find(".ui-state-active").removeClass("ui-state-active");
                        }, 2000);
                    }
                    $zestard_jq("#delivery-date-pro-checkout").prop("disabled", false);
                }, 100);
                $zestard_jq("#delivery-date-pro-checkout").prop("disabled", false);
                //Check App Version if it is 3(i.e Enterprise) or more then it will Exceute Following Code For Multiple Cut Off and Delivery Time
                if (app_version > 2 && type_of_app > 2) {
                    if (deliverytime_global_status == 0) { // if deliverytime set as local = 0                        
                        delivery_date_pro();
                    } else { // delivery time set as global = 1
                        $zestard_jq(time_array).each(function() {
                            $zestard_jq('#delivery-time-checkout').append("<option value='" + time_array[time_count] + "'" + ">" + time_array[time_count] + "</option>");
                            time_count++;
                        });
                    }
                }
                setTimeout(function() {
                    $zestard_jq("input[name=checkout]").css('pointer-events', "auto");
                    $zestard_jq("button[name=checkout]").css('pointer-events', "auto");
                }, 3000);
                setTimeout(function() {
                    if (shop_name == "cheeseschool.myshopify.com") {
                        textarea_variable = $zestard_jq('.delivery_date_pro textarea[name=note]');
                    } else {
                        textarea_variable = $zestard_jq('textarea[name=note]');
                    }
                    old_order_note = textarea_variable.val();
                    if (old_order_note) {
                        open_index = old_order_note.indexOf("(Delivery Date:");
                        if (open_index > -1) {
                            textarea_variable.html(old_order_note.substring(0, open_index - 1));
                        }
                    }
                }, 500);
                setTimeout(function() {
                    $zestard_jq('input[name=checkout], button[name=checkout], .googlepay, button[name=button]').on('click', function(event) {
                        if (shop_name == "bokis-florist-newbury-2.myshopify.com") {
                            $zestard_jq("#delivery-date-pro-checkout").removeClass("error");
                        }
                        if (datepicker_on_default == 1) {
                            var temp_date = $zestard_jq(".visible_datepicker").val();
                            $zestard_jq("#delivery-date-pro-checkout").val(temp_date);
                        }
                        order_note = '';
                        old_order_note = '';
                        if (app_status == 'Active') {
                            if (is_date_required == true && ((typeof $zestard_jq('#delivery-date-pro-checkout').val() == 'undefined') || ($zestard_jq('#delivery-date-pro-checkout').val() == ''))) { // alert for select delivery date
                                {
                                    alert(date_error_message);
                                }
                                $zestard_jq('#delivery-date-pro-checkout').focus();
                                return false;
                                event.preventDefault();
                            }
                            if (is_time_required == true && ((typeof $zestard_jq('#delivery-time-checkout').val() == 'undefined') || ($zestard_jq('#delivery-time-checkout').val() == ''))) { // alert for select delivery time
                                {
                                    alert(time_error_message);
                                    $zestard_jq('#delivery-time-checkout').focus();
                                    return false;
                                }
                            }
                            if (add_delivery_information == 1) {
                                order_note = textarea_variable.val();
                                old_order_note = textarea_variable.val();
                            }

                            //Check Blocked Days and Dates	
                            if ($zestard_jq('#delivery-date-pro-checkout').val() != '') {
                                selected_delivery_date = $zestard_jq('#delivery-date-pro-checkout').val(); {
                                    if (date_formate == "dd/mm/yy") {
                                        formatted_delivery_date = selected_delivery_date;
                                    }
                                    if (date_formate == "mm/dd/yy") {
                                        delivery_date_array = selected_delivery_date.split('/');
                                        formatted_delivery_date = delivery_date_array[1] + '/' + delivery_date_array[0] + '/' + delivery_date_array[2];
                                    }
                                    if (date_formate == "yy/mm/dd") {
                                        delivery_date_array = selected_delivery_date.split('/');
                                        formatted_delivery_date = delivery_date_array[2] + '/' + delivery_date_array[1] + '/' + delivery_date_array[0];
                                    }
                                }
                                var blocked_status, json_response, blocked_date_status;
                                var todays_date = $zestard_jq('#delivery-date-pro-checkout').val();
                                var today = new Date(todays_date).getDay();
                                $zestard_jq.ajax({
                                    url: base_path_delivery_date + "check-blocked-day",
                                    data: {
                                        todays_date: todays_date,
                                        date_format: date_formate,
                                        shop_name: shop_name
                                    },
                                    async: false,
                                    success: function(result) {
                                        json_response = $zestard_jq.parseJSON(result);
                                    }
                                });
                                blocked_status = json_response.result;

                                $zestard_jq.ajax({
                                    url: base_path_delivery_date + "check-blocked-date",
                                    data: {
                                        delivery_date: todays_date,
                                        date_format: date_formate,
                                        shop_name: shop_name
                                    },
                                    async: false,
                                    success: function(response) {
                                        blocked_date_status = response;
                                    }
                                });

                                if (blocked_status == 0) {
                                    alert('Sorry Delivery is Not Possible for ' + json_response.today + ' . Please Select Other Day');
                                    event.preventDefault();
                                }
                                if (blocked_date_status == 0) {
                                    alert('Sorry, The Delivery Date You Selected is Blocked. Please Select Other Delivery Date');
                                    event.preventDefault();
                                }
                                if (blocked_date_status == 1 && blocked_status == 1) {
                                    $zestard_jq.ajax({
                                        url: base_path_delivery_date + "check-cuttoff",
                                        data: {
                                            start_from: start_from,
                                            date_format: date_formate,
                                            delivery_date: formatted_delivery_date,
                                            store_hour: store_hour,
                                            store_minute: store_minute,
                                            shop_name: shop_name,
                                            store_date: store_date
                                        },
                                        async: false,
                                        success: function(result) {
                                            cutoff_response = result;
                                        }
                                    });


                                    if (add_delivery_information == 1) {
                                        // date,time and note bind
                                        if (shop_name == "wimkoelman.myshopify.com") {
                                            var delivery_date = $zestard_jq("#delivery-date-pro-checkout").val();
                                            var delivery_time = $zestard_jq('#delivery-time-checkout').val();
                                            var note = $zestard_jq('#cartSpecialInstructions').val();
                                            order_note = note + ' ' + delivery_date + ' ' + delivery_time;
                                        }
                                        // end date,time and note bind

                                        old_order_note = textarea_variable.val();
                                        if (old_order_note) {
                                            open_index = old_order_note.indexOf("(Delivery Date:");
                                            if (open_index > -1) {
                                                textarea_variable.html(old_order_note.substring(0, open_index - 1));
                                            }
                                        }
                                        order_note = old_order_note;
                                        if (order_note == "" || typeof order_note === "undefined") {
                                            order_note = " (Delivery Date: " + $zestard_jq('#delivery-date-pro-checkout').val() + ")";
                                        } else {
                                            order_note = order_note + " (Delivery Date: " + $zestard_jq('#delivery-date-pro-checkout').val();
                                        }
                                        if ($zestard_jq('#delivery-time-checkout').val() != '') {
                                            order_note = order_note + ", Delivery Time: " + $zestard_jq('#delivery-time-checkout').val() + ")";
                                        }

                                    }
                                    if (cutoff_response == 1) {
                                        if ($zestard_jq("#delivery-time-checkout").val() == "" || typeof $zestard_jq('#delivery-date-pro-checkout').val() === 'undefined') {
                                            if (is_time_required == true) {
                                                if (shop_name == "alacena-de-monica-prueba-12345.myshopify.com") {
                                                    alert("Para continuar selecciona el horario de envío");
                                                    $zestard_jq('#delivery-time-checkout').focus();
                                                    event.preventDefault();
                                                } else {
                                                    alert("Please Select Delivery Time");
                                                    $zestard_jq('#delivery-time-checkout').focus();
                                                    event.preventDefault();
                                                }
                                            } else {
                                                if (add_delivery_information == 1) {

                                                    if (shop_name == "wimkoelman.myshopify.com") {
                                                        var delivery_date = $zestard_jq("#delivery-date-pro-checkout").val();
                                                        var delivery_time = $zestard_jq('#delivery-time-checkout').val();
                                                        var note = $zestard_jq('#cartSpecialInstructions').val();
                                                        order_note = note + ' ' + delivery_date + ' ' + delivery_time;
                                                    }

                                                    old_order_note = textarea_variable.val();
                                                    if (old_order_note) {
                                                        open_index = old_order_note.indexOf("(Delivery Date:");
                                                        if (open_index > -1) {
                                                            textarea_variable.html(old_order_note.substring(0, open_index - 1));
                                                        }
                                                    }
                                                    order_note = old_order_note;
                                                    if (order_note == "" || typeof order_note === "undefined") {
                                                        order_note = " (Delivery Date: " + $zestard_jq('#delivery-date-pro-checkout').val() + ")";
                                                    } else {
                                                        order_note = order_note + " (Delivery Date: " + $zestard_jq('#delivery-date-pro-checkout').val();
                                                    }
                                                    if ($zestard_jq('#delivery-time-checkout').val() != '') {
                                                        order_note = order_note + ", Delivery Time: " + $zestard_jq('#delivery-time-checkout').val() + ")";
                                                    }
                                                    textarea_variable.val(order_note);
                                                }
                                            }
                                        } else {
                                            if (add_delivery_information == 1) {
                                                if (shop_name == "wimkoelman.myshopify.com") {
                                                    var delivery_date = $zestard_jq("#delivery-date-pro-checkout").val();
                                                    var delivery_time = $zestard_jq('#delivery-time-checkout').val();
                                                    var note = $zestard_jq('#cartSpecialInstructions').val();
                                                    order_note = note + ' ' + delivery_date + ' ' + delivery_time;
                                                }

                                                old_order_note = textarea_variable.val();
                                                if (old_order_note) {
                                                    open_index = old_order_note.indexOf("(Delivery Date:");
                                                    if (open_index > -1) {
                                                        textarea_variable.html(old_order_note.substring(0, open_index - 1));
                                                    }
                                                }
                                                order_note = old_order_note;
                                                if (order_note == "" || typeof order_note === "undefined") {
                                                    order_note = " (Delivery Date: " + $zestard_jq('#delivery-date-pro-checkout').val() + ")";
                                                } else {
                                                    order_note = order_note + " (Delivery Date: " + $zestard_jq('#delivery-date-pro-checkout').val();
                                                }
                                                if ($zestard_jq('#delivery-time-checkout').val() != '') {
                                                    order_note = order_note + ", Delivery Time: " + $zestard_jq('#delivery-time-checkout').val() + ")";
                                                }
                                                textarea_variable.val(order_note);
                                            }
                                            if (deliverytime_global_status == 0) {

                                                $zestard_jq.ajax({
                                                    url: base_path_delivery_date + "check-delivery-time",
                                                    data: {
                                                        delivery_time: $zestard_jq("#delivery-time-checkout").val(),
                                                        date_format: date_formate,
                                                        delivery_date: formatted_delivery_date,
                                                        shop_name: shop_name
                                                    },
                                                    async: false,
                                                    success: function(result) {
                                                        deliverytime_response = result;
                                                    }
                                                });
                                                if (deliverytime_response == 1) {

                                                } else {
                                                    alert("Selected Delivery Time Slot is Full. Plese Select Other Delivery Time");
                                                    event.preventDefault();
                                                }
                                            } else {}
                                        }
                                    }
                                    if (cutoff_response == 0) {
                                        alert("Cut Off Time is Over Please Select Other Delivery Date");
                                        event.preventDefault();
                                    }
                                    if (cutoff_response == 2) {
                                        alert("Sorry You Cannot Select " + $zestard_jq("#delivery-date-pro-checkout").val() + " As Delivery Date");
                                        event.preventDefault();
                                    }
                                }
                            }
                        }
                    });
                });
            } else {
                // setting not found or data not found for store
                hide_deliveryDatePro();
            }
        }
    });
}

// Following function is for showing delivery time based on date change or day selection
function delivery_date_pro() {
    var day;

    $zestard_jq("#delivery-date-pro-checkout").datepicker({
        dateFormat: date_formate,
        minDate: start_from,
        maxDate: "'" + allowed_month + "'",
        beforeShowDay: unavailable,
        onSelect: function(dateText) {
            $zestard_jq(".ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active").css("cssText", "background:#000 !important;color:#fff !important;font-weight:700 !important;");
            var result = "";
            day = new Date(this.value).getDay();
            $zestard_jq.ajax({
                url: base_path_delivery_date + "delivery-times",
                data: {
                    todays_date: dateText,
                    date_format: date_formate,
                    shop_name: shop_name,
                    day: day
                },
                async: false,
                success: function(response) {
                    if (response == 0) {
                        $zestard_jq("#delivery-time-checkout").html("");
                        if (is_time_required == true) {
                            alert('Sorry No Time Slots Available For ' + $zestard_jq("#delivery-date-pro-checkout").val() + ' Please Select Other Date');
                            $zestard_jq("#delivery-date-pro-checkout").val("");
                            $zestard_jq("#delivery-date-pro-checkout").focus();
                        }
                    } else {
                        $zestard_jq("#delivery-time-checkout").html("");
                        $zestard_jq("#delivery-time-checkout").append("<option value=''>Choose Time</option>");
                        for (var key in response) {
                            $zestard_jq("#delivery-time-checkout").append("<option value='" + response[key] + "'> " + response[key] + " </option>");
                        }
                    }
                }
            });
        },
        beforeShow: function() {
            setTimeout(function() {
                $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
            }, 0);
        }
    });
}

// Function for unavailable dates or blocked dates
function unavailable(date) {
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    ymd = date.getFullYear() + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2);
    ymd1 = ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear();
    day = new Date(ymd).getDay();
    if ($zestard_jq.inArray(ymd1, unavailableDates) < 0 && $zestard_jq.inArray(days[day], unavailableDays) < 0) {
        return [true, "enabled", "Book Now"];
    } else {
        return [false, "disabled", "Booked Out"];
    }
}