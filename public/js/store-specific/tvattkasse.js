$(document).ready(function(){
  var shop_name = Shopify.shop;
  var unavailableDates = [];
  var unavailableDays = [];
  var start_from = '';
  var allowed_month = '';
  var date_formate = '';
  var check_out_form = $("#delivery-date-pro").closest("form").attr('class');     
  if(check_out_form) {
    var check_out_class_array = check_out_form.split(' ');
    if($.inArray( "cart-form", check_out_class_array ) < 0)
    {
      $("#delivery-date-pro").closest("form").addClass( "cart-form" );
    }
    
  }
  else{
    $("#delivery-date-pro").closest("form").addClass( "cart-form" );
  }
  
  var is_date_required = false;
  var is_time_required = false;
  $.ajax({
		url: "https://zestardshop.com/shopifyapp/DeliveryDatePro/public/getconfig",
		dataType: "json",
    	data:{shop:shop_name},
		success: function(data) {
          var app_status = data[0].app_status;
          if(app_status == "Deactive")
          {
            jQuery("#datepicker_box").remove();
          }
          if(data[0].require_option == 1)
          {
            is_date_required = true;
            $("#delivery-date-pro").attr("required", "true");
            $("#delivery-date-pro").closest("form").removeAttr('novalidate');
          }
          var date_label = data[0].datepicker_label;
          $('.date-box label').text(date_label);
          var dates = data[0].block_date;
		  var day = data[0].days;
		  start_from = '+'+data[0].date_interval;
		  allowed_month = '+'+data[0].alloved_month+'M';
		  unavailableDates = $.parseJSON(dates);
		  unavailableDays = $.parseJSON(day);
          date_formate = data[0].date_format;
          
          var cutoff_status = data[0].cuttoff_status;
          var cutoff_hours = data[0].hours;
          var cutoff_minute = data[0].minute;
          
          var current_date = "{{ 'now' | date: '%d' }}";
          
          var store_hour = "{{ 'now' | date: '%H' }}"; 
          
          var store_minute =  "{{ 'now' | date: '%M' }}";
         
          
          
          if(date_formate == "mm/dd/yy"){
            var display_format = "(mm/dd/yyyy)";
          }
          else if(date_formate == "yy/mm/dd")
          {
            var display_format = "(yyyy/mm/dd)";
          }
          else if(date_formate == "dd/mm/yy")
          {
            var display_format = "(dd/mm/yyyy)";
          }
          else
          {
            var display_format = "(mm/dd/yyyy)";
          }

          var show_date_format = data[0].show_date_format;
          if(show_date_format == 1){
            if(display_format != "")
            {
              $("#selected_format").text(display_format);
            }
            else
            {
              $("#selected_format").text('mm/dd/yyyy');
            }
          }
          var admin_note_status = data[0].admin_note_status;
          var notes_admin = data[0].admin_order_note;
          if(admin_note_status == 1)
          {
            $("#admin_notes").html($.parseHTML(notes_admin));
          }
          
          var admin_time_status = data[0].admin_time_status;
          var time_lable = data[0].time_label;
          var default_option_label = data[0].time_default_option_label;
          if(admin_time_status == 1)
          {
            var time_in_text = data[0].delivery_time;
          	var time_array = new Array();
            time_array = time_in_text.split(",");
            var time_count = 0;
            $("#delivery-time").show();
            $("#time_option_label").text(default_option_label);
            $(time_array).each(function() {
            $('#delivery-time').append("<option value='"+time_array[time_count]+"'"+">"+time_array[time_count]+"</option>");
              time_count++;
            });
            $("#delivery-time-label").show();
            $("#delivery-time-label").text(time_lable);
            
            if(data[0].time_require_option == 1)
            {
              is_time_required = true;
              $("#delivery-time").attr("required", "true");
              $("#delivery-date-pro").closest("form").removeAttr('novalidate');
            }
          }

          var str = parseInt(data[0].date_interval);
          
          if(parseInt(cutoff_status) == 0)
          {          
            if(parseInt(store_hour) <= parseInt(cutoff_hours))
            {
              if(parseInt(store_hour) < parseInt(cutoff_hours))
              {
                  	str = parseInt(data[0].date_interval);
              }
              else 
              {
                	if(parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute))
              		{
              			str = parseInt(data[0].date_interval);
              		}
                  else
                  {
                    	str = parseInt(data[0].date_interval)+1;
                  }
              }
            }
            else
            {
              str = parseInt(data[0].date_interval)+1;
            }
          }
          
          start_from = '+'+parseInt(str);

          var additional_css = data[0].additional_css;
          $('.additional-css').html("<style>" +additional_css+ "</style>");
		}
	  });
setTimeout(function(){
		var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

		function unavailable(date) {
			ymd = date.getFullYear() + "/" + ("0"+(date.getMonth()+1)).slice(-2) + "/" + ("0"+date.getDate()).slice(-2);
            ymd1 = ("0"+date.getDate()).slice(-2) + "-" + ("0"+(date.getMonth()+1)).slice(-2) + "-" + date.getFullYear();
			day = new Date(ymd).getDay();
			if ($.inArray(ymd1, unavailableDates) < 0 && $.inArray(days[day], unavailableDays) < 0) {
				return [true, "enabled", "Book Now"];
			} else {
				return [false,"disabled","Booked Out"];
			}
		}
  setTimeout(function(){
    $("#delivery-date-pro").datepicker( { 
      dateFormat: date_formate ,
			minDate: start_from, 
			maxDate: "'"+allowed_month+"'",
			beforeShowDay: unavailable
			});
  }, 300)
  $("#delivery-date-pro").prop( "disabled", false );   
	}, 3000);
  setTimeout(function(){ $("input[name=checkout]").removeAttr('disabled'); }, 3000);  
  
  
    $('.cart-form').on('submit', function(){        
        //alert($('#delivery-date-pro').val());
		if(is_date_required == true && ((typeof $('#delivery-date-pro').val() == 'undefined') || ($('#delivery-date-pro').val() == '')))
		{
		  alert("Please Select Delivery Date");
		  $('#delivery-date-pro').focus();
		  return false;
		}
        if(is_time_required == true && ((typeof $('#delivery-time').val() == 'undefined') || ($('#delivery-time').val() == '')))
        {
          alert("Please Select Delivery Time");
          $('#delivery-time').focus();
          return false;
        }
	  });
});
