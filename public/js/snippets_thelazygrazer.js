//Global Variable Declarations
var base_path_delivery_date = "https://zestardshop.com/shopifyapp/DeliveryDatePro/public/";
var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var cutoff_hours, cutoff_minute, store_hour, store_minute, datepicker_default_date, deliverytime_global_status, app_version, type_of_app, cutoff_global_status, cutoff_status, cutoff_global_hours, cutoff_global_minute, start_from, is_time_required, is_date_required, time_array, time_count, deliverytime_response, cutoff_response, date_formate, add_delivery_information, order_note, locale, date_error_message, store_date, delivery_date_array, formatted_delivery_date, selected_delivery_date, datepicker_on_default, textarea_variable, original_start_from, time_error_message;
var xhttp, data_length;
var shop_name = Shopify.shop;
//document.getElementsByName("checkout").disabled = true;
//var $ = "";
//Check if Jquery is Undefined or Not available.
//if (typeof jQuery == 'undefined') 

if (shop_name != "amour-et-rose.myshopify.com" && shop_name != "dettaglio.myshopify.com" && shop_name != "enarxi.myshopify.com" && shop_name != "fisherscheese.myshopify.com" && shop_name != "alacena-de-monica-prueba-12345.myshopify.com" && shop_name != "shopify-dev-app.myshopify.com" && shop_name != "cheeseschool.myshopify.com" && shop_name != "sgrocerie.myshopify.com" && shop_name != "maison-duffour.myshopify.com" && shop_name != "do-it-center-panama.myshopify.com" && shop_name != "pheasants-hill-farm.myshopify.com") {
    if ($("#datepicker_box_jquery").length == 0) {
        //If Undefined or Not available, Then Load	
        (function() {
            var jscript = document.createElement("script");
            jscript.src = base_path_delivery_date + "js/zestard_jquery_3.3.1.js";
            jscript.type = 'text/javascript';
            jscript.async = false;

            var el = document.getElementById('datepicker_box'),
                elChild = document.createElement('div');
            elChild.id = 'datepicker_box_jquery';
            el.insertBefore(elChild, el.firstChild);
            document.getElementById('datepicker_box_jquery').appendChild(jscript);
            jscript.onload = function() {
                //Assigning Jquery Object to 
                $zestard_jq = window.jQuery;
                $.ajax({
                    url: base_path_delivery_date + "getconfig",
                    dataType: "json",
                    async: false,
                    data: {
                        shop: shop_name
                    },
                    success: function(data) {
                        if (data == 0) {
                            data_length = data;
                        } else {
                            locale = data[0].language_locale;
                        }
                    }
                });
                if (data_length != 0) {
                    //If Undefined or Not available, Then Load				
                    script = document.createElement('script');
                    script.type = "text/javascript";
                    script.src = base_path_delivery_date + 'js/jquery-ui.js';
                    document.getElementById('datepicker_box_jquery').appendChild(script);
                    script.onload = function() {

                        // Check If jQuery UI Object is Undefined
                        //Send Email that jQuery is Still not Working
                        if (typeof jQuery.ui == 'undefined') {
                            //If undefined then send email
                            xhttp = new XMLHttpRequest();
                            xhttp.onreadystatechange = function() {
                                if (this.readyState == 4 && this.status == 200) {

                                }
                            };
                            xhttp.open("GET", base_path_delivery_date + "acknowledge?shop_name=" + shop_name + "&type=2", true);
                            xhttp.send();
                        } else {
                            if (locale == "en") {
                                $zestard_jq = window.jQuery;
                                //Calling Delivery Date Function
                                deliveryDatePro();
                            } else {
                                jscript = document.createElement("script");
                                jscript.src = "https://zestardshop.com/shopifyapp/DeliveryDatePro/public/" + "js/locale_js/datepicker-" + locale + ".js";
                                jscript.type = 'text/javascript';
                                jscript.async = false;
                                var el = document.getElementById('datepicker_box'),
                                    elChild = document.createElement('div');
                                elChild.id = 'datepicker_locale_jquery';
                                el.insertBefore(elChild, el.firstChild);
                                el.appendChild(elChild);
                                document.getElementById('datepicker_box_jquery').appendChild(jscript);
                                jscript.onload = function() {
                                    $zestard_jq = window.jQuery;
                                    //Calling Delivery Date Function
                                    deliveryDatePro();
                                }
                            }
                        }
                    }
                } else {
                    $zestard_jq("input[name=checkout]").css('pointer-events', "auto");
                    $zestard_jq("button[name=checkout]").css('pointer-events', "auto");
                    $("#datepicker_box").remove();
                }
            };
        })();
    }
} else {
    $zestard_jq = window.jQuery;
    //Calling Delivery Date Function
    deliveryDatePro();
}
/* else
{	
	$ = window.jQuery;						
	//Load jQuery UI
	var jscript = document.createElement("script");
	jscript.src = base_path_delivery_date + "js/jquery-ui.js";
	jscript.type = 'text/javascript';
	jscript.async = false;
	var el = document.getElementById('datepicker_box');
	elChild = document.createElement('div');
	elChild.id = 'datepicker_box_jquery';		
	el.insertBefore(elChild, el.firstChild); 	
	
	document.getElementById('datepicker_box_jquery').append(jscript);
	//document.getElementById('datepicker_box').append(jscript);
	jscript.onload = function(){
		$ = window.jQuery;
		$.ajax({
			url: base_path_delivery_date + "getconfig",
			dataType: "json",
			async: false,
			data: {
				shop: shop_name
			},
			success: function(data){			
				locale = data[0].language_locale;
			}
		});		
		// Check If jQuery UI Object is Undefined
		if(typeof jQuery.ui == 'undefined')
		{
			//If undefined, Then send email
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200){
				
				}
			};
			xhttp.open("GET", base_path_delivery_date + "acknowledge?shop_name=" + shop_name + "&type=2", true);
			xhttp.send();	
		}
		else
		{	
			if(locale == "en")
			{
				$ = window.jQuery;	
				//Calling Delivery Date Function
				deliveryDatePro();
			}
			else
			{
				var jscript = document.createElement("script");
				jscript.src = "https://zestardshop.com/shopifyapp/DeliveryDatePro/public/" + "js/locale_js/datepicker-" + locale + ".js";					
				jscript.type = 'text/javascript';
				jscript.async = false;						
				var el = document.getElementById('datepicker_box'),
				elChild = document.createElement('div');
				elChild.id = 'datepicker_locale_jquery';		
				el.insertBefore(elChild, el.firstChild); 	
				el.append(elChild);
				document.getElementById('datepicker_box_jquery').append(jscript);						
				jscript.onload = function(){ 
					$ = window.jQuery;					
					//Calling Delivery Date Function
					deliveryDatePro();
				}
			}
		}		
	} 		
}*/

function deliveryDatePro() {
    /* 
	$("input[name=checkout]").attr('disabled','disabled');
    $("button[name=checkout]").attr('disabled','disabled'); 
    */
    var disable_checkout;
    store_date = $("#ztpl-date-format-ddmmyy").val();
    $("#delivery-date-pro").val("");
    $.ajax({
        url: base_path_delivery_date + "get-type-and-version",
        async: false,
        data: { shop_name: shop_name },
        success: function(result) {
            data_length = result;
            if (data_length != 0) {
                app_version = result[0].app_version;
                type_of_app = result[0].type_of_app;
            } else {
                $zestard_jq("input[name=checkout]").css('pointer-events', "auto");
                $zestard_jq("button[name=checkout]").css('pointer-events', "auto");
                $("#datepicker_box").remove();
            }
        }
    });
    if (data_length != 0) {
        var unavailableDates = [];
        var unavailableDays = [];
        start_from = '';
        var allowed_month = '';
        date_formate = '';
        var check_out_form = $("#delivery-date-pro").closest("form").attr('class');

        if (check_out_form) {
            var check_out_class_array = check_out_form.split(' ');
            if ($.inArray("cart-form", check_out_class_array) < 0) {
                if (shop_name != "petaljet.myshopify.com")
                    $("#delivery-date-pro").closest("form").addClass("cart-form");
            }
        } else {
            if (shop_name != "petaljet.myshopify.com")
                $("#delivery-date-pro").closest("form").addClass("cart-form");
        }
        is_date_required = false;
        is_time_required = false;
        $.ajax({
            url: base_path_delivery_date + "getconfig",
            dataType: "json",
            async: false,
            data: {
                shop: shop_name
            },
            success: function(data) {
                app_status = data[0].app_status;
                locale = data[0].language_locale;
                datepicker_on_default = data[0].datepicker_display_on;
                datepicker_default_date = data[0].default_date_option;
                add_delivery_information = data[0].add_delivery_information;
                date_error_message = data[0].date_error_message;
                time_error_message = data[0].time_error_message;
                if (app_status == "Deactive") {
                    $zestard_jq("input[name=checkout]").css('pointer-events', "auto");
                    $zestard_jq("button[name=checkout]").css('pointer-events', "auto");
                    $("#datepicker_box").remove();
                } else {
                    //      disable_checkout = data[0].disable_checkout;
                    //     if (disable_checkout == 1) {
                    //     console.log('here');
                    //     $zestard_jq("input[name=checkout]").css('pointer-events', "none");
                    //     $zestard_jq("button[name=checkout]").css('pointer-events', "none");
                    // }
                }

                if (data[0].require_option == 1) {
                    is_date_required = true;
                    $("#delivery-date-pro").attr("required", "true");
                    $("#delivery-date-pro").closest("form").removeAttr('novalidate');
                }
                var date_label = data[0].datepicker_label;
                show_datepicker_label = data[0].show_datepicker_label;
                if (show_datepicker_label == 1) {
                    $('.date-box label').text(date_label);
                } else {
                    $('.date-box label').hide();
                }
                var dates = data[0].block_date;
                var day = data[0].days;
                start_from = '+' + data[0].date_interval;
                allowed_month = '+' + data[0].alloved_month + 'M';
                unavailableDates = $.parseJSON(dates);
                unavailableDays = $.parseJSON(day);
                date_formate = data[0].date_format;
                cutoff_status = data[0].cuttoff_status;
                cutoff_global_status = data[0].cutoff_global_status;
                deliverytime_global_status = data[0].deliverytime_global_status;

                cutoff_global_hours = data[0].hours;
                cutoff_global_minute = data[0].minute;
                var current_date = $("#ztpl-current-date").val();

                store_hour = $("#ztpl-store-hour").val();

                store_minute = $("#ztpl-store-minute").val();
                if (date_formate == "mm/dd/yy") {
                    var display_format = "(mm/dd/yyyy)";
                } else if (date_formate == "yy/mm/dd") {
                    var display_format = "(yyyy/mm/dd)";
                } else if (date_formate == "dd/mm/yy") {
                    var display_format = "(dd/mm/yyyy)";
                } else {
                    var display_format = "(mm/dd/yyyy)";
                }

                var show_date_format = data[0].show_date_format;
                if (show_date_format == 1) {
                    if (display_format != "") {
                        $("#selected_format").text(display_format);
                    } else {
                        $("#selected_format").text('mm/dd/yyyy');
                    }
                }
                var admin_note_status = data[0].admin_note_status;
                var notes_admin = data[0].admin_order_note;
                if (admin_note_status == 1) {
                    $("#admin_notes").html($.parseHTML(notes_admin));
                }

                var admin_time_status = data[0].admin_time_status;
                var time_lable = data[0].time_label;
                var default_option_label = data[0].time_default_option_label;
                if (admin_time_status == 1) {
                    var time_in_text = data[0].delivery_time;
                    time_array = new Array();
                    time_array = time_in_text.split(",");
                    time_count = 0;
                    //$("#delivery-time").show();
                    $("#delivery-time").css('display', 'block');
                    //document.getElementById("delivery-time").style.display = "block";
                    $("#delivery-time").css('border', 'none');
                    $("#time_option_label").text(default_option_label);
                    if (app_version <= 2 && type_of_app <= 2) {
                        $(time_array).each(function() {
                            $('#delivery-time').append("<option value='" + time_array[time_count] + "'" + ">" + time_array[time_count] + "</option>");
                            time_count++;
                        });
                    }
                    $("#delivery-time-label").show();
                    $("#delivery-time-label").text(time_lable);

                    if (data[0].time_require_option == 1) {
                        is_time_required = true;
                        $("#delivery-time").attr("required", "true");
                        $("#delivery-date-pro").closest("form").removeAttr('novalidate');
                    }
                }
                var additional_css = data[0].additional_css;

                if (shop_name == "balance80.myshopify.com") {
                    $('.datepicker_wrapper_box').css("cssText", "display: inline-block;background: #f5f5f5;padding: 20px 30px;color:black !important;font-weight:700;");
                } else {
                    $('#datepicker_box').after("<style>" + additional_css + "</style>");
                }
                //Change Variable Name
                var str = parseInt(data[0].date_interval);
                if (parseInt(cutoff_status) == 0) {} else {
                    //Check App Version if it is 3(i.e Enterprise) or more then it will Exceute Following Code For Multiple Cut Off and Delivery Time  
                    if (app_version > 2 && type_of_app > 2) {
                        if (cutoff_global_status == 1) {
                            cutoff_hours = cutoff_global_hours;
                            cutoff_minute = cutoff_global_minute;
                            if (parseInt(store_hour) <= parseInt(cutoff_hours)) {
                                if (parseInt(store_hour) < parseInt(cutoff_hours)) {
                                    str = parseInt(data[0].date_interval);
                                } else {
                                    if (parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                        str = parseInt(data[0].date_interval);
                                    } else {
                                        str = parseInt(data[0].date_interval) + 1;
                                    }
                                }
                            } else {
                                str = parseInt(data[0].date_interval) + 1;
                            }
                        } else {
                            var c_day = new Date().getDay();
                            $.ajax({
                                url: base_path_delivery_date + "get-cutoff-time",
                                data: { day: c_day, shop_name: shop_name },
                                async: false,
                                success: function(response) {
                                    var json_data = JSON.parse(response);
                                    cutoff_hours = json_data['cutoff_hour'];
                                    cutoff_minute = json_data['cutoff_minute'];
                                }
                            });
                            console.log(cutoff_hours + " " + cutoff_minute);
                            console.log(store_hour + " " + store_minute);
                            if (parseInt(store_hour) <= parseInt(cutoff_hours)) {
                                if (parseInt(store_hour) < parseInt(cutoff_hours)) {
                                    str = parseInt(data[0].date_interval);
                                } else {
                                    console.log('here');
                                    if (parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                        str = parseInt(data[0].date_interval);
                                    } else {
                                        str = parseInt(data[0].date_interval) + 1;
                                    }
                                }
                            } else {
                                str = parseInt(data[0].date_interval) + 1;
                            }
                        }
                    } else {
                        cutoff_hours = cutoff_global_hours;
                        cutoff_minute = cutoff_global_minute;
                        if (parseInt(store_hour) <= parseInt(cutoff_hours)) {
                            if (parseInt(store_hour) < parseInt(cutoff_hours)) {
                                str = parseInt(data[0].date_interval);
                            } else {
                                if (parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                    str = parseInt(data[0].date_interval);
                                } else {
                                    str = parseInt(data[0].date_interval) + 1;
                                }
                            }
                        } else {
                            str = parseInt(data[0].date_interval) + 1;
                        }
                    }
                }
                start_from = '+' + parseInt(str);
                original_start_from = start_from;

                function pad(n) {
                    return (n < 10) ? ("0" + n) : n;
                }
                var tempdate = new Date();
                tempdate.setDate(tempdate.getDate());
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var block_date_array = JSON.parse(dates);
                var block_days_array = unavailableDays;
                var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                var current_available_day = weekday[tempdate.getDay()];
                exit_flag = 0;
                temp_count = start_from;
                if (!block_days_array) {
                    block_days_array = new Array();
                }
                if (!block_date_array) {
                    block_date_array = new Array();
                }

                /* if (shop_name == "sunshine-milk-bread.myshopify.com" || shop_name == "greenox.myshopify.com" || shop_name == "petaljet.myshopify.com" || shop_name == "alacena-de-monica-prueba-12345.myshopify.com" || shop_name == "hello-cheese.myshopify.com" || shop_name == "sweetworthy.myshopify.com" || shop_name == "the-cornish-scone-company.myshopify.com" || shop_name == "the-cornish-scone-company.myshopify.com" || shop_name == "sgrocerie.myshopify.com" || shop_name == "maison-duffour.myshopify.com") //  */
                if (parseInt(data[0].exclude_block_date_status) == 0) {
                    tempdate.setDate(tempdate.getDate() + parseInt(start_from));
                    console.log(tempdate);
                    while (temp_count != -1) {
                        var date = tempdate.getDate();
                        var month = tempdate.getMonth() + 1;
                        var year = tempdate.getFullYear();
                        NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                        NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                        current_available_day = weekday[tempdate.getDay()];
                        if ($zestard_jq.inArray(NextDayDMY, block_date_array) !== -1) {
                            index_of = block_date_array.indexOf(NextDayDMY);
                            block_date_array.splice(index_of, 1);
                            start_from = parseInt(start_from) + 1;
                            temp_count = start_from;
                        } else if ($zestard_jq.inArray(current_available_day, block_days_array) !== -1) {
                            index_of = block_days_array.indexOf(current_available_day);
                            start_from = parseInt(start_from) + 1;
                            temp_count = start_from;
                        } else {
                            temp_count = -1;
                        }
                        tempdate.setDate(tempdate.getDate() + 1);
                    }
                } else {
                    if (temp_count == 0) {
                        if (block_date_array.length > 0 || block_days_array.length > 0) {
                            while ((block_date_array.length > 0 || block_days_array.length > 0) && temp_count > -1) {
                                var date = tempdate.getDate();
                                var month = tempdate.getMonth() + 1;
                                var year = tempdate.getFullYear();
                                NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                                NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                                current_available_day = weekday[tempdate.getDay()];
                                /* console.log(NextDayDMY);
                                console.log(temp_count);
                                console.log(block_date_array.length + " " + " " + block_days_array.length);	 */
                                if ($.inArray(NextDayDMY, block_date_array) !== -1) {
                                    index_of = block_date_array.indexOf(NextDayDMY);
                                    block_date_array.splice(index_of, 1);
                                    start_from = parseInt(start_from) + 1;
                                    temp_count = start_from;
                                    /* console.log(start_from + " if " +NextDayDMY);	
                                    console.log(block_date_array.length + " " + " " + block_days_array.length);	 */
                                } else if ($.inArray(current_available_day, block_days_array) !== -1) {
                                    index_of = block_days_array.indexOf(current_available_day);
                                    //block_days_array.splice(index_of, 1);
                                    start_from = parseInt(start_from) + 1;
                                    temp_count = start_from;
                                    /* console.log(start_from + " else if" +NextDayDMY);			
                                    console.log(block_date_array.length + " " + " " + block_days_array.length);	 */
                                } else {
                                    if (block_date_array.length <= 0 || block_days_array.length <= 0) {
                                        temp_count = -1;
                                    } else if (temp_count == 0) {
                                        temp_count = -1;
                                        //console.log(NextDayDMY);
                                        //console.log(temp_count);
                                    } else {
                                        temp_count--;
                                    }
                                }
                                tempdate.setDate(tempdate.getDate() + 1);
                            }
                        }
                    } else {
                        while ((block_date_array.length > 0 || block_days_array.length > 0) && temp_count != -1) {
                            var date = tempdate.getDate();
                            var month = tempdate.getMonth() + 1;
                            var year = tempdate.getFullYear();
                            NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                            NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                            current_available_day = weekday[tempdate.getDay()];
                            if ($.inArray(NextDayDMY, block_date_array) !== -1) {
                                temp_count = 0;
                                index_of = block_date_array.indexOf(NextDayDMY);
                                block_date_array.splice(index_of, 1);
                                start_from = parseInt(start_from) + 1;
                                temp_count = start_from;
                                //console.log(start_from + " if " +NextDayDMY);
                            } else if ($.inArray(current_available_day, block_days_array) !== -1) {
                                index_of = block_days_array.indexOf(current_available_day);
                                temp_count = 0;
                                //block_days_array.splice(index_of, 1);
                                start_from = parseInt(start_from) + 1;
                                temp_count = start_from;
                                //console.log(start_from + " else if" +NextDayDMY);								
                            } else {
                                //console.log(start_from + " else");
                                /* 1 Aug 2018 */
                                var start_from_date = new Date();
                                start_from_date.setDate(start_from_date.getDate() + parseInt(original_start_from));
                                if (tempdate.getTime() >= start_from_date.getTime()) {
                                    temp_count = -1;
                                    block_days_array = [];
                                }
                                /* 1 Aug 2018 */
                                //temp_count--;

                                /* 31 july 2018 */
                                /* tempdate.setDate(tempdate.getDate() + parseInt(start_from));
                                console.log(tempdate);
                                var date = tempdate.getDate();
                                var month = tempdate.getMonth() + 1;
                                var year = tempdate.getFullYear();
                                NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                                NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                                current_available_day = weekday[tempdate.getDay()];
                                console.log(block_date_array);
                                console.log(block_days_array);
                                if ($.inArray(NextDayDMY, block_date_array) !== -1) {
                                	temp_count = 0;
                                	index_of = block_date_array.indexOf(NextDayDMY);
                                	block_date_array.splice(index_of, 1);
                                	start_from = parseInt(start_from) + 1;
                                	temp_count = start_from;
                                	console.log(start_from + " if " + NextDayDMY);
                                } 
                                else if($.inArray(current_available_day, block_days_array) !== -1) {
                                	index_of = block_days_array.indexOf(current_available_day);
                                	temp_count = 0;
                                	block_days_array.splice(index_of, 1);
                                	start_from = parseInt(start_from) + 1;
                                	temp_count = start_from;
                                	console.log(start_from + " else " + NextDayDMY);								
                                }
                                else
                                {
                                	temp_count = -1;
                                } */
                                /* 31 july 2018 */
                            }
                            tempdate.setDate(tempdate.getDate() + 1);
                            //console.log(tempdate);
                        }
                    }
                }
                //console.log(start_from);	
                /* else
                {					
                	if(temp_count == 0)
                	{
                		if(block_date_array.length > 0 || block_days_array.length > 0)
                		{
                			if($.inArray(NextDayDMY,block_date_array) !== -1)
                			{										
                				index_of = block_date_array.indexOf(NextDayDMY);
                				block_date_array.splice(index_of, 1);							
                				start_from = parseInt(start_from) + 1;														   															
                			} 
                			else if($.inArray(current_available_day,block_days_array) !== -1)
                			{								
                				index_of = block_date_array.indexOf(current_available_day);								
                				block_days_array.splice(index_of, 1);							
                				start_from = parseInt(start_from) + 1;																																																					
                			}
                		}
                	}
                	else 
                	{						
                		while((block_date_array.length > 0 || block_days_array.length > 0) && temp_count > -1)
                		{								
                			console.log(temp_count);
                			var date  = tempdate.getDate();
                			var month = tempdate.getMonth() + 1;
                			var year  = tempdate.getFullYear();
                			NextDayDMY = pad(date)+'-'+pad(month)+'-'+year;
                			NextDayYMD = year+'-'+pad(month)+'-'+pad(date);	
                			current_available_day = weekday[tempdate.getDay()];																			
                			if($.inArray(NextDayDMY,block_date_array) !== -1)
                			{		
                				temp_count = 0;
                				index_of = block_date_array.indexOf(NextDayDMY);
                				block_date_array.splice(index_of, 1);							
                				start_from = parseInt(start_from) + 1;														   							
                				//temp_count = parseInt(start_from) + 1;
                			} 
                			else if($.inArray(current_available_day,block_days_array) !== -1)
                			{								
                				index_of = block_date_array.indexOf(current_available_day);
                				temp_count = 0;
                				block_days_array.splice(index_of, 1);							
                				start_from = parseInt(start_from) + 1;																								
                				//temp_count = parseInt(start_from) + 1;							
                			}
                			else
                			{
                				temp_count = -1;
                			}												
                			tempdate.setDate(tempdate.getDate() + 1);												
                		}
                	}				
                }*/
                if (parseInt(data[0].exclude_block_date_status) == 1 && 1 == 2) {
                    var block_date_array = JSON.parse(dates);
                    var block_days_array = JSON.parse(day);
                    var ddmmyy = $("#ztpl-date-format-ddmmyy").val();
                    var yymmdd = $("#ztpl-date-format-yymmdd").val();
                    var date_index = parseInt(0);
                    var reloop = function(ddmmyy, yymmdd) {
                        var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                        var current_day = new Date(yymmdd);
                        var current_available_day = weekday[current_day.getDay()];
                        //if(jQuery.inArray(ddmmyy,block_date_array) !== -1)							
                        //Here ddmmyy
                        if ($.inArray(ddmmyy, block_date_array) != -1) {
                            start_from = parseInt(start_from) + 1;

                            //And Here yymmdd??
                            var NextDayData = new Date(new Date(yymmdd).getFullYear(), new Date(yymmdd).getMonth(), new Date(yymmdd).getDate() + 1);

                            var date = new Date(NextDayData).getDate();
                            var month = new Date(NextDayData).getMonth() + 1;
                            var year = new Date(NextDayData).getFullYear();

                            var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                            var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                            //alert(NextDayDMY+'=='+NextDayYMD);
                            return reloop(NextDayDMY, NextDayYMD);
                        } else if ($.inArray(current_available_day, block_days_array) != -1) {
                            start_from = parseInt(start_from) + 1;
                            //alert('days== '+start_from);
                            var NextDayData = new Date(new Date(yymmdd).getFullYear(), new Date(yymmdd).getMonth(), new Date(yymmdd).getDate() + 1);
                            var date = new Date(NextDayData).getDate();
                            var month = new Date(NextDayData).getMonth() + 1;
                            var year = new Date(NextDayData).getFullYear();
                            var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                            var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                            return reloop(NextDayDMY, NextDayYMD);
                        }
                    }
                    reloop(ddmmyy, yymmdd);
                    //alert('total== '+start_from);
                    function pad(n) {
                        return (n < 10) ? ("0" + n) : n;
                    }
                }
            }
        });

        //setTimeout(function() {
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        function unavailable(date) {
            ymd = date.getFullYear() + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2);
            ymd1 = ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear();
            day = new Date(ymd).getDay();
            if ($.inArray(ymd1, unavailableDates) < 0 && $.inArray(days[day], unavailableDays) < 0) {
                return [true, "enabled", "Book Now"];
            } else {
                return [false, "disabled", "Booked Out"];
            }
        }
        setTimeout(function() {
            datepicker_var = undefined;
            //For keeping datepicker always open	
            if (datepicker_on_default == 1) {
                if (locale == "en") {
                    $("#datepicker_box .date-box").append("<div class='visible_datepicker'></div>");
                    $("#delivery-date-pro").attr("type", "hidden");
                    datepicker_var = $(".visible_datepicker").datepicker({
                        dateFormat: date_formate,
                        minDate: start_from,
                        maxDate: "'" + allowed_month + "'",
                        beforeShowDay: unavailable,
                        onSelect: function(dateText) {
                            if (deliverytime_global_status == 0) {
                                delivery_date_pro_div(dateText);
                            }
                        },
                        beforeShow: function() {
                            setTimeout(function() {
                                $('.ui-datepicker').css('z-index', 99999999999999);
                            }, 0);
                        }
                    });
                    $('.visible_datepicker').datepicker('setDate', null);
                } else {
                    //$.datepicker.setDefaults($.datepicker.regional[locale]); 
                    $("#datepicker_box .date-box").append("<div class='visible_datepicker'></div>");
                    $("#delivery-date-pro").attr("type", "hidden");
                    datepicker_var = $(".visible_datepicker").datepicker({
                        dateFormat: date_formate,
                        minDate: start_from,
                        maxDate: "'" + allowed_month + "'",
                        beforeShowDay: unavailable,
                        defaultDate: null,
                        onSelect: function(dateText) {
                            if (deliverytime_global_status == 0) {
                                delivery_date_pro_div(dateText);
                            }
                        },
                        beforeShow: function() {
                            setTimeout(function() {
                                $('.ui-datepicker').css('z-index', 99999999999999);
                            }, 0);
                        }
                    });
                    $('.visible_datepicker').datepicker('setDate', null);
                }
            } else {
                if (locale == "en") {
                    datepicker_var = $("#delivery-date-pro").datepicker({
                        dateFormat: date_formate,
                        minDate: start_from,
                        maxDate: "'" + allowed_month + "'",
                        beforeShowDay: unavailable,
                        defaultDate: null,
                        beforeShow: function() {
                            setTimeout(function() {
                                $('.ui-datepicker').css('z-index', 99999999999999);
                            }, 0);
                        }
                    });
                } else {
                    /* $.datepicker.setDefaults($.datepicker.regional[locale]); 						 */
                    datepicker_var = $("#delivery-date-pro").datepicker({
                        dateFormat: date_formate,
                        minDate: start_from,
                        maxDate: "'" + allowed_month + "'",
                        beforeShowDay: unavailable,
                        beforeShow: function() {
                            setTimeout(function() {
                                $('.ui-datepicker').css('z-index', 99999999999999);
                            }, 0);
                        }
                    });
                }
            }
            $("#delivery-date-pro").val("");
            // Check If Datepicker Object is Undefined            
            /* if (app_version == 1 && type_of_app == 1) {                    
					{
                        $("#delivery-date-pro").datepicker("setDate", "today - 1");
                    }
                } */
            if (app_version > 1 && type_of_app > 1) {
                if (datepicker_default_date == 1) {
                    $("#delivery-date-pro").datepicker("setDate", "today + start_from");
                }
            }
            if (typeof datepicker_var === 'undefined') {
                //If Undefined, Then send email
                $.ajax({
                    url: base_path_delivery_date + "acknowledge",
                    async: false,
                    data: { shop_name: shop_name, type: 3 },
                    success: function(result) {}
                });
            }

        }, 300)
        $("#delivery-date-pro").prop("disabled", false);
        //});
        $("#delivery-date-pro").prop("disabled", false);
        //Check App Version if it is 3(i.e Enterprise) or more then it will Exceute Following Code For Multiple Cut Off and Delivery Time
        if (app_version > 2 && type_of_app > 2) {
            if (deliverytime_global_status == 0) {
                delivery_date_pro();
                var result = "";
                $.ajax({
                    url: base_path_delivery_date + "delivery-times",
                    data: { shop_name: shop_name, start_from: start_from, date_format: date_formate },
                    async: false,
                    success: function(response) {
                        if (response == 0) {
                            $("#delivery-time").html("");
                        } else {
                            $("#delivery-time").html("");
                            $("#delivery-time").append("<option value=''>Choose Time</option>");
                            for (var key in response) {
                                $("#delivery-time").append("<option value='" + response[key] + "'> " + response[key] + " </option>");
                            }
                        }
                    }

                });
            } else {
                $(time_array).each(function() {
                    $('#delivery-time').append("<option value='" + time_array[time_count] + "'" + ">" + time_array[time_count] + "</option>");
                    time_count++;
                });
            }
        }
        //setTimeout(function() {            
        $("input[name=checkout]").css('pointer-events', "auto");
        $("button[name=checkout]").css('pointer-events', "auto");
        //$("#datepicker_box").show();	
        //}, 3000);


        setTimeout(function() {

            if (shop_name == "cheeseschool.myshopify.com") {
                textarea_variable = $('.delivery_date_pro textarea[name=note]');
            } else {
                textarea_variable = $('textarea[name=note]');
            }
            old_order_note = textarea_variable.val();
            if (old_order_note) {
                open_index = old_order_note.indexOf("(Delivery Date:");
                if (open_index > -1) {
                    textarea_variable.html(old_order_note.substring(0, open_index - 1));
                }
            }
        }, 500);

        setTimeout(function() {
            $('input[name=checkout], button[name=checkout]').on('click', function(event) {

                if (shop_name == "bokis-florist-newbury-2.myshopify.com") {
                    $("#delivery-date-pro").removeClass("error");
                }
                if (datepicker_on_default == 1) {
                    var temp_date = $(".visible_datepicker").val();
                    $("#delivery-date-pro").val(temp_date);
                }
                console.log($("#delivery-date-pro").val());
                if (shop_name == "buy-honeymelts.myshopify.com") {
                    /* console.log($("#delivery-date-pro").val());
                    console.log(temp_date); */
                    //return false;
                }
                order_note = '';
                old_order_note = '';
                if (app_status == 'Active') {
                    if (is_date_required == true && ((typeof $('#delivery-date-pro').val() == 'undefined') || ($('#delivery-date-pro').val() == ''))) {
                        /* if(shop_name == "siauliuorchideja.myshopify.com")
					{
						alert("Pasirinkite datą ir laiką.");	
					}
					
					else */
                        {
                            alert(date_error_message);
                        }
                        $('#delivery-date-pro').focus();
                        return false;
                        event.preventDefault();
                    }
                    if (is_time_required == true && ((typeof $('#delivery-time').val() == 'undefined') || ($('#delivery-time').val() == ''))) {
                        /* if(shop_name == "alacena-de-monica-prueba-12345.myshopify.com")
                        {														
                        }
                        else */
                        {
                            alert(time_error_message);
                            $('#delivery-time').focus();
                            return false;
                        }
                    }
                    /* if(shop_name == "alacena-de-monica-prueba-12345.myshopify.com")
                    {
                    	if($('#enable-express').prop("checked"))
                    	{
                    		if((typeof $('#delivery-time').val() == 'undefined') || ($('#delivery-time').val() == ''))
                    		{
                    			$('#delivery-time').focus();
                    			return false;	
                    		}
                    	}
                    } */
                    if (add_delivery_information == 1) {
                        order_note = textarea_variable.val();
                        old_order_note = textarea_variable.val();
                    }
                    //Check Blocked Days and Dates	

                    if ($('#delivery-date-pro').val() != '') {
                        selected_delivery_date = $('#delivery-date-pro').val();
                        //if(shop_name == "debut-shopify.myshopify.com")
                        {
                            if (date_formate == "dd/mm/yy") {
                                formatted_delivery_date = selected_delivery_date;
                            }
                            if (date_formate == "mm/dd/yy") {
                                delivery_date_array = selected_delivery_date.split('/');
                                formatted_delivery_date = delivery_date_array[1] + '/' + delivery_date_array[0] + '/' + delivery_date_array[2];
                            }
                            if (date_formate == "yy/mm/dd") {

                                delivery_date_array = selected_delivery_date.split('/');
                                formatted_delivery_date = delivery_date_array[2] + '/' + delivery_date_array[1] + '/' + delivery_date_array[0];
                            }
                        }
                        var blocked_status, json_response, blocked_date_status;
                        var todays_date = $('#delivery-date-pro').val();
                        var today = new Date(todays_date).getDay();
                        $.ajax({
                            url: base_path_delivery_date + "check-blocked-day",
                            data: { todays_date: todays_date, date_format: date_formate, shop_name: shop_name },
                            async: false,
                            success: function(result) {
                                json_response = $.parseJSON(result);
                            }
                        });
                        blocked_status = json_response.result;

                        $.ajax({
                            url: base_path_delivery_date + "check-blocked-date",
                            data: { delivery_date: todays_date, date_format: date_formate, shop_name: shop_name },
                            async: false,
                            success: function(response) {
                                blocked_date_status = response;
                            }
                        });

                        if (blocked_status == 0) {
                            alert('Sorry Delivery is Not Possible for ' + json_response.today + ' . Please Select Other Day');
                            event.preventDefault();
                        }
                        if (blocked_date_status == 0) {
                            alert('Sorry, The Delivery Date You Selected is Blocked. Please Select Other Delivery Date');
                            event.preventDefault();
                        }
                        if (blocked_date_status == 1 && blocked_status == 1) {
                            $.ajax({
                                url: base_path_delivery_date + "check-cuttoff",
                                data: { start_from: start_from, date_format: date_formate, delivery_date: formatted_delivery_date, store_hour: store_hour, store_minute: store_minute, shop_name: shop_name, store_date: store_date },
                                async: false,
                                success: function(result) {
                                    cutoff_response = result;
                                }
                            });

                            /* if (add_delivery_information == 1)
                            {
                                order_note = order_note + " (Delivery Date: " + $('#delivery-date-pro').val();
                                if ($('#delivery-time').val() != '')
                                {
                                    order_note = order_note + ", Delivery Time: " + $('#delivery-time').val() + ")";
                                }
                                textarea_variable.val(order_note);
                                console.log(textarea_variable.val());
                            } */
                            if (add_delivery_information == 1) {
                                old_order_note = textarea_variable.val();
                                if (old_order_note) {
                                    open_index = old_order_note.indexOf("(Delivery Date:");
                                    if (open_index > -1) {
                                        textarea_variable.html(old_order_note.substring(0, open_index - 1));
                                    }
                                }
                                order_note = old_order_note;
                                if (order_note == "" || typeof order_note === "undefined") {
                                    order_note = " (Delivery Date: " + $('#delivery-date-pro').val() + ")";
                                } else {
                                    order_note = order_note + " (Delivery Date: " + $('#delivery-date-pro').val();
                                }
                                if ($('#delivery-time').val() != '') {
                                    order_note = order_note + ", Delivery Time: " + $('#delivery-time').val() + ")";
                                }
                                //console.log(textarea_variable.val());
                            }
                            if (cutoff_response == 1) {
                                if ($("#delivery-time").val() == "" || typeof $('#delivery-date-pro').val() === 'undefined') {
                                    if (is_time_required == true) {
                                        if (shop_name == "alacena-de-monica-prueba-12345.myshopify.com") {
                                            alert("Para continuar selecciona el horario de envío");
                                            $('#delivery-time').focus();
                                            event.preventDefault();
                                        } else {
                                            alert("Please Select Delivery Time");
                                            $('#delivery-time').focus();
                                            event.preventDefault();
                                        }
                                    } else {
                                        if (add_delivery_information == 1) {
                                            textarea_variable.val(order_note);
                                        }
                                    }
                                } else {
                                    if (add_delivery_information == 1) {
                                        textarea_variable.val(order_note);
                                    }
                                    if (deliverytime_global_status == 0) {
                                        $.ajax({
                                            url: base_path_delivery_date + "check-delivery-time",
                                            data: { delivery_time: $("#delivery-time").val(), date_format: date_formate, delivery_date: formatted_delivery_date, shop_name: shop_name },
                                            async: false,
                                            success: function(result) {
                                                deliverytime_response = result;
                                            }
                                        });
                                        if (deliverytime_response == 1) {

                                        } else {
                                            alert("Selected Delivery Time Slot is Full. Plese Select Other Delivery Time");
                                            event.preventDefault();
                                        }
                                    } else {}
                                }
                            }
                            if (cutoff_response == 0) {
                                alert("Cut Off Time is Over Please Select Other Delivery Date");
                                event.preventDefault();
                            }
                            if (cutoff_response == 2) {
                                alert("Sorry You Cannot Select " + $("#delivery-date-pro").val() + " As Delivery Date");
                                event.preventDefault();
                            }
                        }
                    }
                }
            });
        });
    }
}
//Following Function is for Showing Delivery Time Dynamically Based on Days
function delivery_date_pro_div(dateText) {

    var result = "";
    day = new Date(dateText).getDay();
    $.ajax({
        url: base_path_delivery_date + "delivery-times",
        data: { todays_date: dateText, date_format: date_formate, shop_name: shop_name },
        async: false,
        success: function(response) {
            if (response == 0) {
                $("#delivery-time").html("");
                if (is_time_required == true) {
                    alert('Sorry No Time Slots Available For ' + dateText + ' Please Select Other Date');
                    $("#delivery-date-pro").val("");
                    $("#delivery-date-pro").focus();
                }
            } else {
                $("#delivery-time").html("");
                $("#delivery-time").append("<option value=''>Choose Time</option>");
                for (var key in response) {
                    $("#delivery-time").append("<option value='" + response[key] + "'> " + response[key] + " </option>");
                }
            }
        }
    });

}

function delivery_date_pro() {
    var day;
    $("#delivery-date-pro").change(function(dateText) {
        $(".ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active").css("cssText", "background:#000 !important;color:#fff !important;font-weight:700 !important;")

        var result = "";

        day = new Date(this.value).getDay();
        $.ajax({
            url: base_path_delivery_date + "delivery-times",
            data: { todays_date: this.value, date_format: date_formate, shop_name: shop_name },
            async: false,
            success: function(response) {
                if (response == 0) {
                    $("#delivery-time").html("");
                    if (is_time_required == true) {
                        alert('Sorry No Time Slots Available For ' + $("#delivery-date-pro").val() + ' Please Select Other Date');
                        $("#delivery-date-pro").val("");
                        $("#delivery-date-pro").focus();
                    }
                } else {
                    $("#delivery-time").html("");
                    $("#delivery-time").append("<option value=''>Choose Time</option>");
                    for (var key in response) {
                        $("#delivery-time").append("<option value='" + response[key] + "'> " + response[key] + " </option>");
                    }
                }
            }
        });
    });
}