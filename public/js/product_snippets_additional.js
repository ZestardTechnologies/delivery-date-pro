//Global Variable Declarations
var base_path_delivery_date = "https://zestardshop.com/shopifyapp/DeliveryDatePro/public/";
var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var cutoff_hours, cutoff_minute, store_hour, store_minute, datepicker_default_date, deliverytime_global_status, app_version, type_of_app, cutoff_global_status, cutoff_status, cutoff_global_hours, cutoff_global_minute, start_from, is_time_required, is_date_required, time_array, time_count, deliverytime_response, cutoff_response, date_formate, add_delivery_information, order_note, locale, date_error_message, store_date, delivery_date_array, formatted_delivery_date, selected_delivery_date, time_in_text, day, dates;
var xhttp;
var shop_name = Shopify.shop,
    DDP_product_id = product_id;
var $zestard_jq = "";

//Check if Jquery is Undefined or Not available.
if (typeof jQuery == 'undefined') {
    //If Undefined or Not available, Then Load	    
    var jscript = document.createElement("script");
    jscript.src = base_path_delivery_date + "js/zestard_jquery_3.3.1.js";
    jscript.type = 'text/javascript';
    jscript.async = false;

    var el = document.getElementById('datepicker_box'),
        elChild = document.createElement('div');
    elChild.id = 'datepicker_box_jquery';
    el.insertBefore(elChild, el.firstChild);
    document.getElementById('datepicker_box_jquery').append(jscript);
    jscript.onload = function() {
        //Assigning Jquery Object to Zestard_jq
        $zestard_jq = window.jQuery;
        $zestard_jq.ajax({
            url: base_path_delivery_date + "getconfig",
            dataType: "json",
            async: false,
            data: {
                shop: shop_name
            },
            success: function(data) {
                locale = data[0].language_locale;
            }
        });
        //If Undefined or Not available, Then Load				
        script = document.createElement('script');
        script.type = "text/javascript";
        script.async = false;
        script.src = base_path_delivery_date + 'js/jquery-ui.js';
        document.getElementById('datepicker_box_jquery').append(script);
        script.onload = function() {
            // Check If jQuery UI Object is Undefined
            //Send Email that jQuery is Still not Working
            if (typeof jQuery.ui == 'undefined') {
                //If undefined then send email
                // xhttp = new XMLHttpRequest();
                // xhttp.onreadystatechange = function() {
                //     if (this.readyState == 4 && this.status == 200) {

                //     }
                // };
                // xhttp.open("GET", base_path_delivery_date + "acknowledge?shop_name=" + shop_name + "&type=2", true);
                // xhttp.send();
            } else {
                if (locale == "en") {
                    $zestard_jq = window.jQuery;
                    //Calling Delivery Date Function
                    deliveryDatePro();
                } else {
                    jscript = document.createElement("script");
                    jscript.src = "https://zestardshop.com/shopifyapp/DeliveryDatePro/public/" + "js/locale_js/datepicker-" + locale + ".js";
                    jscript.type = 'text/javascript';
                    jscript.async = false;
                    var el = document.getElementById('datepicker_box'),
                        elChild = document.createElement('div');
                    elChild.id = 'datepicker_locale_jquery';
                    el.insertBefore(elChild, el.firstChild);
                    el.append(elChild);
                    document.getElementById('datepicker_box_jquery').append(jscript);
                    jscript.onload = function() {
                        $zestard_jq = window.jQuery;
                        //Calling Delivery Date Function
                        deliveryDatePro();
                    }
                }
            }
        }
    };
} else {
    $zestard_jq = window.jQuery;
    //Calling Delivery Date Function
    deliveryDatePro();
}

function deliveryDatePro() {
    var delivery_product_id = 17489099849779;
    //alert(delivery_product_id);
    $zestard_jq('input[type="radio"]').on("click", function() {
        //alert($zestard_jq(this).val());
        if ($zestard_jq(this).val() == "ztpl_free_delivery") {
            $zestard_jq("#datepicker_box").hide();
        }
        if ($zestard_jq(this).val() == "ztpl_specific_delivery") {
            //$zestard_jq("#datepicker_box").show();
            //var cart_data = jQuery.get('/cart.js');
            //console.log(cart_data.responseText);

        }
    });
    store_date = $zestard_jq("#ztpl-date-format-ddmmyy").val();
    var all_settings, product_settings, store_settings;
    $zestard_jq.ajax({
        url: base_path_delivery_date + "get-type-and-version",
        async: false,
        data: { shop_name: shop_name },
        success: function(result) {
            app_version = result[0].app_version;
            type_of_app = result[0].type_of_app;
        }
    });
    $zestard_jq.ajax({
        url: base_path_delivery_date + "get-product-settings",
        async: false,
        data: { shop_name: shop_name, product_id: DDP_product_id },
        success: function(result) {
            all_settings = $.parseJSON(result);
        }
    });
    if (all_settings == 0) {
        $zestard_jq("#datepicker_box").remove();
        $zestard_jq("input[name=add]").css('pointer-events', "auto");
        $zestard_jq("button[name=add]").css('pointer-events', "auto");
        $zestard_jq("#AddToCart").css('pointer-events', "auto");
    } else {
        product_settings = all_settings.product_settings;
        app_config = all_settings.app_config;
        var unavailableDates = [];
        var unavailableDays = [];
        start_from = '';
        var allowed_month = '';
        date_formate = '';
        var check_out_form = $zestard_jq("#delivery-date-pro").closest("form").attr('class');
        if (check_out_form) {
            var check_out_class_array = check_out_form.split(' ');
            if ($zestard_jq.inArray("cart-form", check_out_class_array) < 0) {
                $zestard_jq("#delivery-date-pro").closest("form").addClass("cart-form");
            }
        } else {
            $zestard_jq("#delivery-date-pro").closest("form").addClass("cart-form");
        }
        is_date_required = false;
        is_time_required = false;
        $zestard_jq.ajax({
            url: base_path_delivery_date + "getconfig",
            dataType: "json",
            async: false,
            data: {
                shop: shop_name
            },
            success: function(data) {
                app_status = data[0].app_status;
                locale = data[0].language_locale;
                datepicker_on_default = data[0].datepicker_display_on;
                datepicker_default_date = data[0].default_date_option;
                add_delivery_information = data[0].add_delivery_information;
                date_error_message = data[0].date_error_message;
                if (app_status == "Deactive") {
                    $zestard_jq("#datepicker_box").remove();
                    $zestard_jq("input[name=add]").css('pointer-events', "auto");
                    $zestard_jq("button[name=add]").css('pointer-events', "auto");
                    $zestard_jq("#AddToCart").css('pointer-events', "auto");
                }

                $zestard_jq.ajax({
                    url: base_path_delivery_date + "get_product_status",
                    async: false,
                    data: {
                        store: shop_name,
                        product: product_id
                    },
                    success: function(status_product) {
                        if (status_product == 0) {
                            $zestard_jq("#datepicker_box").remove();
                            $zestard_jq("input[name=add]").css('pointer-events', "auto");
                            $zestard_jq("button[name=add]").css('pointer-events', "auto");
                            $zestard_jq("#AddToCart").css('pointer-events', "auto");

                        } else if (status_product == 1) {
                            $zestard_jq("#ztpl_delivery_options_wrapper").css('display', "block");

                        } else {

                        }
                    }
                });
                /* If setting are not saved for particular product then use default settings */
                if (product_settings == null) {
                    /* 
                        dates = data[0].block_date;
                        day = data[0].days;
                        start_from = '+' + data[0].date_interval;
                        allowed_month = '+' + data[0].alloved_month + 'D';                
                        cutoff_status = data[0].cuttoff_status;
                        cutoff_global_status = data[0].cutoff_global_status;
                        cutoff_global_hours	 = data[0].hours;
                        cutoff_global_minute = data[0].minute;
                        time_in_text = data[0].delivery_time;	 
                    */
                    $zestard_jq("#datepicker_box").remove();
                    $zestard_jq("input[name=add]").css('pointer-events', "auto");
                    $zestard_jq("button[name=add]").css('pointer-events', "auto");
                    $zestard_jq("#AddToCart").css('pointer-events', "auto");
                } else {
                    if (app_config.global_product_blocked_dates == 0) {
                        dates = product_settings.blocked_dates;
                        unavailableDates = dates.split(",");
                    } else {
                        dates = data[0].block_date;
                        unavailableDates = $.parseJSON(dates);
                    }

                    if (app_config.global_product_blocked_days == 0) {
                        day = product_settings.blocked_days;
                        if (day) {
                            unavailableDays = day.split(",");
                        }
                    } else {
                        day = data[0].days;
                        if (day) {
                            unavailableDays = $.parseJSON(day);
                        }
                    }
                    if (app_config.global_product_date_interval == 0) {
                        start_from = '+' + product_settings.date_interval;
                    } else {
                        start_from = '+' + data[0].date_interval;
                    }
                    if (app_config.global_product_pre_order == 0) {
                        allowed_month = '+' + product_settings.pre_order_time + 'M';
                    } else {
                        allowed_month = '+' + data[0].alloved_month + 'M';
                    }
                    if (app_config.global_product_cut_off == 0) {
                        cutoff_global_hours = product_settings.cut_off_hours;
                        cutoff_global_minute = product_settings.cut_off_minutes;
                    } else {
                        cutoff_global_hours = data[0].hours;
                        cutoff_global_minute = data[0].minute;
                    }
                    if (app_config.global_product_delivery_time == 0) {
                        time_in_text = product_settings.delivery_times;
                    } else {
                        time_in_text = data[0].delivery_time;
                    }
                    if (data[0].require_option == 1) {
                        //console.log('test');
                        is_date_required = true;
                        $zestard_jq("#delivery-date-pro").attr("required", "true");
                        $zestard_jq("#delivery-date-pro").closest("form").removeAttr('novalidate');
                    }
                    var date_label = data[0].datepicker_label;
                    show_datepicker_label = data[0].show_datepicker_label;
                    if (show_datepicker_label == 1) {
                        $zestard_jq('.date-box label').text(date_label);
                    } else {
                        $zestard_jq('.date-box label').hide();
                    }
                    time_array = new Array();
                    if (time_in_text) {
                        time_array = time_in_text.split(",");
                    }
                    date_formate = data[0].date_format;
                    cutoff_status = data[0].cuttoff_status;
                    cutoff_global_status = data[0].cutoff_global_status;
                    deliverytime_global_status = data[0].deliverytime_global_status;
                    var current_date = $zestard_jq("#ztpl-current-date").val();
                    store_hour = $zestard_jq("#ztpl-store-hour").val();
                    store_minute = $zestard_jq("#ztpl-store-minute").val();
                    if (date_formate == "mm/dd/yy") {
                        var display_format = "(mm/dd/yyyy)";
                    } else if (date_formate == "yy/mm/dd") {
                        var display_format = "(yyyy/mm/dd)";
                    } else if (date_formate == "dd/mm/yy") {
                        var display_format = "(dd/mm/yyyy)";
                    } else {
                        var display_format = "(mm/dd/yyyy)";
                    }
                    var show_date_format = data[0].show_date_format;
                    if (show_date_format == 1) {
                        if (display_format != "") {
                            $zestard_jq("#selected_format").text(display_format);
                        } else {
                            $zestard_jq("#selected_format").text('mm/dd/yyyy');
                        }
                    }
                    var admin_note_status = data[0].admin_note_status;
                    var notes_admin = data[0].admin_order_note;
                    if (admin_note_status == 1) {
                        $zestard_jq("#admin_notes").html($zestard_jq.parseHTML(notes_admin));
                    }
                    var admin_time_status = data[0].admin_time_status;
                    var time_lable = data[0].time_label;

                    var default_option_label = data[0].time_default_option_label;
                    if (admin_time_status == 1) {
                        time_count = 0;
                        $zestard_jq("#delivery-time").show();
                        $zestard_jq("#delivery-time").css('border', 'none');
                        $zestard_jq("#time_option_label").text(default_option_label);
                        if (app_version <= 2 && type_of_app <= 2) {
                            $zestard_jq(time_array).each(function() {
                                $zestard_jq('#delivery-time').append("<option value='" + time_array[time_count] + "'" + ">" + time_array[time_count] + "</option>");
                                time_count++;
                            });
                        }
                        $zestard_jq("#delivery-time-label").show();
                        $zestard_jq("#delivery-time-label").text(time_lable);
                        if (data[0].time_require_option == 1) {
                            is_time_required = true;
                            $zestard_jq("#delivery-time").attr("required", "true");
                            $zestard_jq("#delivery-date-pro").closest("form").removeAttr('novalidate');
                        }
                    }
                    //Change Variable Name
                    var str = parseInt(start_from);
                    if (parseInt(cutoff_status) == 0) {} else {
                        //Check App Version if it is 3(i.e Enterprise) or more then it will Exceute Following Code For Multiple Cut Off and Delivery Time  
                        if (app_version > 2 && type_of_app > 2) {
                            if (cutoff_global_status == 1) {
                                cutoff_hours = cutoff_global_hours;
                                cutoff_minute = cutoff_global_minute;
                                if (parseInt(store_hour) <= parseInt(cutoff_hours)) {
                                    if (parseInt(store_hour) < parseInt(cutoff_hours)) {
                                        str = parseInt(product_settings.date_interval);
                                    } else {
                                        if (parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                            str = parseInt(product_settings.date_interval);
                                        } else {
                                            str = parseInt(product_settings.date_interval) + 1;
                                        }
                                    }
                                } else {
                                    str = parseInt(product_settings.date_interval) + 1;
                                }
                            } else {
                                var day = new Date().getDay();
                                $zestard_jq.ajax({
                                    url: base_path_delivery_date + "get-cutoff-time",
                                    data: { day: day, shop_name: shop_name },
                                    async: false,
                                    success: function(response) {
                                        var json_data = JSON.parse(response);
                                        cutoff_hours = json_data['cutoff_hour'];
                                        cutoff_minute = json_data['cutoff_minute'];
                                    }
                                });
                                if (parseInt(store_hour) <= parseInt(cutoff_hours)) {
                                    if (parseInt(store_hour) < parseInt(cutoff_hours)) {
                                        str = parseInt(product_settings.date_interval);
                                    } else {
                                        if (parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                            str = parseInt(product_settings.date_interval);
                                        } else {
                                            str = parseInt(product_settings.date_interval) + 1;
                                        }
                                    }
                                } else {
                                    str = parseInt(product_settings.date_interval) + 1;
                                }
                            }
                        } else {
                            cutoff_hours = cutoff_global_hours;
                            cutoff_minute = cutoff_global_minute;
                            if (parseInt(store_hour) <= parseInt(cutoff_hours)) {
                                if (parseInt(store_hour) < parseInt(cutoff_hours)) {
                                    str = parseInt(product_settings.date_interval);
                                } else {
                                    if (parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute)) {
                                        str = parseInt(product_settings.date_interval);
                                    } else {
                                        str = parseInt(product_settings.date_interval) + 1;
                                    }
                                }
                            } else {
                                str = parseInt(product_settings.date_interval) + 1;
                            }
                        }
                    }
                    start_from = '+' + parseInt(str);

                    function pad(n) {
                        return (n < 10) ? ("0" + n) : n;
                    }
                    var tempdate = new Date();
                    tempdate.setDate(tempdate.getDate());
                    var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                    var block_date_array = dates.split(",");
                    if (day) {
                        var block_days_array = day.split(",");
                    } else {
                        block_days_array = new Array()
                    }
                    var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                    var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                    var current_available_day = weekday[tempdate.getDay()];
                    exit_flag = 0;
                    temp_count = start_from;
                    if (!block_days_array) {
                        block_days_array = new Array();
                    }
                    if (!block_date_array) {
                        block_date_array = new Array();
                    }
                    if (parseInt(data[0].exclude_block_date_status) == 1) {
                        if (temp_count == 0) {
                            if (block_date_array.length > 0 || block_days_array.length > 0) {
                                if ($zestard_jq.inArray(NextDayDMY, block_date_array) !== -1) {
                                    index_of = block_date_array.indexOf(NextDayDMY);
                                    block_date_array.splice(index_of, 1);
                                    start_from = parseInt(start_from) + 1;
                                } else if ($zestard_jq.inArray(current_available_day, block_days_array) !== -1) {
                                    index_of = block_date_array.indexOf(current_available_day);
                                    block_days_array.splice(index_of, 1);
                                    start_from = parseInt(start_from) + 1;
                                }
                            }
                        } else {
                            while ((block_date_array.length > 0 || block_days_array.length > 0) && temp_count > 0) {
                                var date = tempdate.getDate();
                                var month = tempdate.getMonth() + 1;
                                var year = tempdate.getFullYear();
                                NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                                NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                                current_available_day = weekday[tempdate.getDay()];
                                if ($zestard_jq.inArray(NextDayDMY, block_date_array) !== -1) {
                                    temp_count = 0;
                                    index_of = block_date_array.indexOf(NextDayDMY);
                                    block_date_array.splice(index_of, 1);
                                    start_from = parseInt(start_from) + 1;
                                    temp_count = start_from;
                                } else if ($zestard_jq.inArray(current_available_day, block_days_array) !== -1) {
                                    index_of = block_date_array.indexOf(current_available_day);
                                    temp_count = 0;
                                    block_days_array.splice(index_of, 1);
                                    start_from = parseInt(start_from) + 1;
                                    temp_count = start_from;
                                } else {
                                    temp_count--;
                                }
                                tempdate.setDate(tempdate.getDate() + 1);
                            }
                        }
                    }

                    if (parseInt(data[0].exclude_block_date_status) == 1 && 1 == 2) {
                        var block_date_array = JSON.parse(dates);
                        var block_days_array = JSON.parse(day);
                        var ddmmyy = $zestard_jq("#ztpl-date-format-ddmmyy").val();
                        var yymmdd = $zestard_jq("#ztpl-date-format-yymmdd").val();
                        var date_index = parseInt(0);
                        var reloop = function(ddmmyy, yymmdd) {
                            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                            var current_day = new Date(yymmdd);
                            var current_available_day = weekday[current_day.getDay()];

                            //Here ddmmyy
                            if ($zestard_jq.inArray(ddmmyy, block_date_array) != -1) {
                                start_from = parseInt(start_from) + 1;
                                //And Here yymmdd??
                                var NextDayData = new Date(new Date(yymmdd).getFullYear(), new Date(yymmdd).getMonth(), new Date(yymmdd).getDate() + 1);
                                var date = new Date(NextDayData).getDate();
                                var month = new Date(NextDayData).getMonth() + 1;
                                var year = new Date(NextDayData).getFullYear();
                                var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                                var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                                //alert(NextDayDMY+'=='+NextDayYMD);
                                return reloop(NextDayDMY, NextDayYMD);
                            } else if ($zestard_jq.inArray(current_available_day, block_days_array) != -1) {
                                start_from = parseInt(start_from) + 1;
                                //alert('days== '+start_from);
                                var NextDayData = new Date(new Date(yymmdd).getFullYear(), new Date(yymmdd).getMonth(), new Date(yymmdd).getDate() + 1);
                                var date = new Date(NextDayData).getDate();
                                var month = new Date(NextDayData).getMonth() + 1;
                                var year = new Date(NextDayData).getFullYear();
                                var NextDayDMY = pad(date) + '-' + pad(month) + '-' + year;
                                var NextDayYMD = year + '-' + pad(month) + '-' + pad(date);
                                return reloop(NextDayDMY, NextDayYMD);
                            }
                        }
                        reloop(ddmmyy, yymmdd);
                        //alert('total== '+start_from);
                        function pad(n) {
                            return (n < 10) ? ("0" + n) : n;
                        }
                    }
                    var additional_css = data[0].additional_css;
                    $zestard_jq('.additional-css').after("<style>" + additional_css + "</style>");
                    setTimeout(function() {
                        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                        var highlight_flag = 1;

                        function unavailable(date) {
                            if (shop_name == "purpinkg.myshopify.com") {
                                if (highlight_flag == 1) {
                                    setTimeout(function() {
                                        $(".visible_datepicker").find(".ui-state-active").removeClass("ui-state-active");
                                        $(".visible_datepicker").find(".ui-state-highlight").removeClass("ui-state-highlight");
                                        ($(".visible_datepicker").find(".ui-state-hover").removeClass("ui-state-hover"));
                                        highlight_flag = 0;
                                    }, 500);
                                }
                            }
                            ymd = date.getFullYear() + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2);
                            ymd1 = ("0" + date.getDate()).slice(-2) + "-" + ("" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear();
                            day = new Date(ymd).getDay();
                            if ($zestard_jq.inArray(ymd1, unavailableDates) < 0 && $zestard_jq.inArray(days[day], unavailableDays) < 0) {
                                return [true, "enabled", "Book Now"];
                            } else {
                                return [false, "disabled", "Booked Out"];
                            }
                        }

                        setTimeout(function() {
                            datepicker_var = undefined;
                            //For keeping datepicker always open	
                            if (datepicker_on_default == 1) {
                                if (locale == "en") {
                                    //console.log('tes');
                                    $zestard_jq("#datepicker_box .date-box").append("<div class='visible_datepicker'></div>");
                                    $zestard_jq("#delivery-date-pro").attr("type", "hidden");
                                    datepicker_var = $zestard_jq(".visible_datepicker").datepicker({
                                        dateFormat: date_formate,
                                        minDate: parseInt(start_from),
                                        maxDate: allowed_month,
                                        onSelect: function() {
                                            $('#AddToCart').prop('disabled', false);
                                            $('.zAddToCart').prop('disabled', false);
                                        },
                                        beforeShowDay: unavailable,
                                        beforeShow: function() {
                                            setTimeout(function() {
                                                $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
                                            }, 0);
                                        }
                                    });
                                } else {
                                    console.log('tes1');
                                    $zestard_jq("#datepicker_box .date-box").append("<div class='visible_datepicker'></div>");
                                    $zestard_jq("#delivery-date-pro").attr("type", "hidden");
                                    datepicker_var = $zestard_jq(".visible_datepicker").datepicker( //$.datepicker.regional[locale], 
                                        {
                                            dateFormat: date_formate,
                                            minDate: start_from,
                                            maxDate: "'" + allowed_month + "'",
                                            beforeShowDay: unavailable,
                                            beforeShow: function() {
                                                setTimeout(function() {
                                                    $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
                                                }, 0);
                                            }
                                        });
                                    $zestard_jq.datepicker.setDefaults($zestard_jq.datepicker.regional[locale]);

                                }
                            } else {
                                if (locale == "en") {
                                    datepicker_var = $zestard_jq("#delivery-date-pro").datepicker({
                                        dateFormat: date_formate,
                                        minDate: start_from,
                                        maxDate: "'" + allowed_month + "'",
                                        beforeShowDay: unavailable,
                                        beforeShow: function() {
                                            setTimeout(function() {
                                                $zestard_jq('.ui-datepicker').css('z-index', 99999999999999999999);
                                            }, 500);
                                        }
                                    });
                                } else {
                                    datepicker_var = $zestard_jq("#delivery-date-pro").datepicker({
                                        dateFormat: date_formate,
                                        minDate: start_from,
                                        maxDate: "'" + allowed_month + "'",
                                        beforeShowDay: unavailable,
                                        beforeShow: function() {
                                            setTimeout(function() {
                                                $zestard_jq('.ui-datepicker').css('z-index', 99999999999999);
                                            }, 0);
                                        }
                                    });
                                    $zestard_jq.datepicker.setDefaults($zestard_jq.datepicker.regional[locale]);
                                }
                            }
                        }, 100);
                        if (shop_name == "purpinkg.myshopify.com") {
                            setTimeout(function() {
                                $zestard_jq('.visible_datepicker').datepicker("setDate", null);
                            }, 500);
                        }
                        // Check If Datepicker Object is Undefined            
                        if (app_version > 1 && type_of_app > 1) {
                            if (datepicker_default_date == 1) {
                                $zestard_jq("#delivery-date-pro").datepicker("setDate", "today + start_from");
                            }
                        }

                        // }, 300)
                        $zestard_jq("#delivery-date-pro").prop("disabled", false);
                    });

                    //Check App Version if it is 3(i.e Enterprise) or more then it will Exceute Following Code For Multiple Cut Off and Delivery Time
                    if (app_version > 2 && type_of_app > 2) {
                        if (deliverytime_global_status == 0) {
                            delivery_date_pro();
                            var result = "";
                            $zestard_jq.ajax({
                                url: base_path_delivery_date + "delivery-times",
                                data: { shop_name: shop_name, start_from: start_from, date_format: date_formate },
                                async: false,
                                success: function(response) {
                                    if (response == 0) {
                                        $zestard_jq("#delivery-time").html("");
                                    } else {
                                        $zestard_jq("#delivery-time").html("");
                                        $zestard_jq("#delivery-time").append("<option value=''>Choose Time</option>");
                                        for (var key in response) {
                                            $zestard_jq("#delivery-time").append("<option value='" + response[key] + "'> " + response[key] + " </option>");
                                        }
                                    }
                                }

                            });
                        } else {
                            $zestard_jq(time_array).each(function() {
                                $zestard_jq('#delivery-time').append("<option value='" + time_array[time_count] + "'" + ">" + time_array[time_count] + "</option>");
                                time_count++;
                            });
                        }
                    }
                    setTimeout(function() {
                        $zestard_jq("input[name=add]").css('pointer-events', "auto");
                        $zestard_jq("button[name=add]").css('pointer-events', "auto");
                        $zestard_jq("#AddToCart").css('pointer-events', "auto");
                    }, 3000);
                    setTimeout(function() {
                        old_order_note = $zestard_jq('textarea[name=note]').val();
                        if (old_order_note) {
                            open_index = old_order_note.indexOf("(Delivery Date:");
                            if (open_index > -1) {
                                $zestard_jq('textarea[name=note]').val(old_order_note.substring(0, open_index - 1));
                            }
                        }
                    }, 500);
                    setTimeout(function() {
                        $zestard_jq('input[name=add], button[name=add], #AddToCart').on('click', function(event) {
                            if (datepicker_on_default == 1) {
                                var temp_date = $zestard_jq(".visible_datepicker").val();
                                $zestard_jq("#delivery-date-pro").val(temp_date);
                            }
                            (temp_date);
                            order_note = '';
                            old_order_note = '';
                            if (app_status == 'Active') {
                                if (is_date_required == true && ((typeof $zestard_jq('#delivery-date-pro').val() == 'undefined') || ($zestard_jq('#delivery-date-pro').val() == ''))) {
                                    alert(date_error_message);
                                    $zestard_jq('#delivery-date-pro').focus();
                                    return false;
                                }
                                if (is_time_required == true && ((typeof $zestard_jq('#delivery-time').val() == 'undefined') || ($zestard_jq('#delivery-time').val() == ''))) {
                                    alert("Please Select Delivery Time");
                                    $zestard_jq('#delivery-time').focus();
                                    return false;
                                }
                                if (add_delivery_information == 1) {
                                    order_note = $zestard_jq('textarea[name=note]').val();
                                    old_order_note = $zestard_jq('textarea[name=note]').val();
                                }
                                //Check Blocked Days and Dates					
                                if ($zestard_jq('#delivery-date-pro').val() != '') {
                                    selected_delivery_date = $zestard_jq('#delivery-date-pro').val();
                                    if (datepicker_on_default == 1) {
                                        selected_delivery_date = $zestard_jq(".visible_datepicker").val();
                                    }
                                    if (shop_name == "debut-shopify.myshopify.com") {
                                        if (date_formate == "dd/mm/yy") {
                                            formatted_delivery_date = selected_delivery_date;
                                        }
                                        if (date_formate == "mm/dd/yy") {
                                            delivery_date_array = selected_delivery_date.split('/');
                                            formatted_delivery_date = delivery_date_array[1] + '/' + delivery_date_array[0] + '/' + delivery_date_array[2];
                                        }
                                        if (date_formate == "yy/mm/dd") {

                                            delivery_date_array = selected_delivery_date.split('/');
                                            formatted_delivery_date = delivery_date_array[2] + '/' + delivery_date_array[1] + '/' + delivery_date_array[0];
                                        }
                                    }
                                    formatted_delivery_date = selected_delivery_date;
                                    if (app_version > 2 && type_of_app > 2) {
                                        if (shop_name == "apps-testing-store.myshopify.com" || shop_name == "the-bunched-co.myshopify.com") {
                                            if (delivery_info.length > 0) {
                                                $zestard_jq.ajax({
                                                    url: base_path_delivery_date + "check-cart",
                                                    data: { delivery_info: JSON.stringify(delivery_info), delivery_time: $zestard_jq("#delivery-time").val(), date_format: date_formate, delivery_date: formatted_delivery_date, shop_name: shop_name },
                                                    async: false,
                                                    success: function(result) {
                                                        if (result == 1) {

                                                        } else {
                                                            alert("Selected Delivery Time Slot is Full. Plese Select Other Delivery Time");
                                                            event.preventDefault();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                    var blocked_status, json_response, blocked_date_status;
                                    var todays_date = $zestard_jq('#delivery-date-pro').val();
                                    var today = new Date(todays_date).getDay();
                                    $zestard_jq.ajax({
                                        url: base_path_delivery_date + "check-blocked-day",
                                        data: { todays_date: todays_date, date_format: date_formate, shop_name: Shopify.shop },
                                        async: false,
                                        success: function(result) {
                                            json_response = $zestard_jq.parseJSON(result);
                                        }
                                    });
                                    blocked_status = json_response.result;
                                    $zestard_jq.ajax({
                                        url: base_path_delivery_date + "check-blocked-date",
                                        data: { delivery_date: todays_date, date_format: date_formate },
                                        async: false,
                                        success: function(response) {
                                            blocked_date_status = response;
                                        }
                                    });
                                    if (blocked_status == 0) {
                                        alert('Sorry Delivery is Not Possible for ' + json_response.today + ' . Please Select Other Day');
                                        event.preventDefault();
                                    }
                                    if (blocked_date_status == 0) {
                                        alert('Sorry, The Delivery Date You Selected is Blocked. Please Select Other Delivery Date');
                                        event.preventDefault();
                                    }
                                    if (blocked_date_status == 1 && blocked_status == 1) {
                                        $zestard_jq.ajax({
                                            url: base_path_delivery_date + "check-cuttoff",
                                            data: { start_from: start_from, date_format: date_formate, delivery_date: formatted_delivery_date, store_hour: store_hour, store_minute: store_minute, shop_name: shop_name },
                                            async: false,
                                            success: function(result) {
                                                cutoff_response = result;
                                            }
                                        });
                                        if (add_delivery_information == 1) {
                                            order_note = order_note + " (Delivery Date: " + $zestard_jq('#delivery-date-pro').val();
                                            if ($zestard_jq('#delivery-time').val() != '') {
                                                order_note = order_note + ", Delivery Time: " + $zestard_jq('#delivery-time').val() + ")";
                                            }
                                            $zestard_jq('textarea[name=note]').val(order_note);
                                        }
                                        if (cutoff_response == 1) {
                                            if ($zestard_jq("#delivery-time").val() == "" || typeof $zestard_jq('#delivery-date-pro').val() === 'undefined') {
                                                if (is_time_required == true) {
                                                    alert("Please Select Delivery Time");
                                                    $zestard_jq('#delivery-time').focus();
                                                    event.preventDefault();
                                                }
                                            } else {
                                                if (deliverytime_global_status == 0) {
                                                    $zestard_jq.ajax({
                                                        url: base_path_delivery_date + "check-delivery-time",
                                                        data: { delivery_time: $zestard_jq("#delivery-time").val(), date_format: date_formate, delivery_date: formatted_delivery_date, shop_name: shop_name },
                                                        async: false,
                                                        success: function(result) {
                                                            deliverytime_response = result;
                                                        }
                                                    });
                                                    if (deliverytime_response == 1) {

                                                    } else {
                                                        alert("Selected Delivery Time Slot is Full. Plese Select Other Delivery Time");
                                                        event.preventDefault();
                                                    }
                                                }
                                            }
                                        }
                                        if (cutoff_response == 0) {
                                            alert("Cut Off Time is Over Please Select Other Delivery Date");
                                            event.preventDefault();
                                        }
                                        if (cutoff_response == 2) {
                                            alert("Sorry You Cannot Select " + $zestard_jq("#delivery-date-pro").val() + " As Delivery Date");
                                            event.preventDefault();
                                        }
                                    }
                                }
                            }
                        });
                    });
                }
            }
        });
    }
}
//Following Function is for Showing Delivery Time Dynamically Based on Days
function delivery_date_pro() {
    var day;
    $zestard_jq("#delivery-date-pro").change(function(dateText) {
        var result = "";
        day = new Date(this.value).getDay();
        $zestard_jq.ajax({
            url: base_path_delivery_date + "delivery-times",
            data: { todays_date: this.value, date_format: date_formate, shop_name: shop_name },
            async: false,
            success: function(response) {
                if (response == 0) {
                    $zestard_jq("#delivery-time").html("");
                    if (is_time_required == true) {
                        alert('Sorry No Time Slots Available For ' + $zestard_jq("#delivery-date-pro").val() + ' Please Select Other Date');
                        $zestard_jq("#delivery-date-pro").val("");
                        $zestard_jq("#delivery-date-pro").focus();
                    }
                } else {
                    $zestard_jq("#delivery-time").html("");
                    $zestard_jq("#delivery-time").append("<option value=''>Choose Time</option>");
                    for (var key in response) {
                        $zestard_jq("#delivery-time").append("<option value='" + response[key] + "'> " + response[key] + " </option>");
                    }
                }
            }
        });
    });
}