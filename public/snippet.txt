<link rel="stylesheet" href="https://zestardshop.com/shopifyapp/DeliveryDatePro/public/css/jquery-ui.css">
<script src="https://zestardshop.com/shopifyapp/DeliveryDatePro/public/js/jquery-ui.js"></script>

<div id="datepicker_box">
  <p class="date-box">
    <label for="delivery-date-pro"></label>
    <input id="delivery-date-pro" type="text" name="attributes[Delivery-Date]" value="{{ cart.attributes.date }}" disabled />
  </p>
  <span id="selected_format"></span>
  <span id="admin_notes"></span>
</div>
<script>
$(document).ready(function(){
  var shop_name = "{{ shop.permanent_domain }}";
  var unavailableDates = [];
  var unavailableDays = [];
  var start_from = '';
  var allowed_month = '';
  var date_formate = '';
  $.ajax({
		url: "https://zestardshop.com/shopifyapp/DeliveryDatePro/public/getconfig",
		dataType: "json",
    	data:{shop:shop_name},
		success: function(data) {
          var app_status = data[0].app_status;
          if(app_status == "Deactive")
          {
            jQuery("#datepicker_box").remove();
          }
          if(data[0].require_option == 1)
          {
            $("#delivery-date-pro").attr("required", "true");
            $("#delivery-date-pro").closest("form").removeAttr('novalidate');
            //$("#date").attr("oninvalid", "setCustomValidity('"+data[0].required_text+"')");
          }
          var date_label = data[0].datepicker_label;
          jQuery('.date-box label').text(date_label);
          var dates = data[0].block_date;
		  var day = data[0].days;
		  start_from = '+'+data[0].date_interval;
		  allowed_month = '+'+data[0].alloved_month+'M';
		  unavailableDates = $.parseJSON(dates);
		  unavailableDays = $.parseJSON(day);
          date_formate = data[0].date_format;
          
          var cutoff_status = data[0].cuttoff_status;
          var cutoff_hours = data[0].hours;
          var cutoff_minute = data[0].minute;
          
          var current_date = "{{ 'now' | date: '%d' }}";
          
          var store_hour = "{{ 'now' | date: '%H' }}"; 
          
          var store_minute =  "{{ 'now' | date: '%M' }}";
         
          
          
          if(date_formate == "mm/dd/yy"){
            var display_format = "(mm/dd/yyyy)";
          }
          else if(date_formate == "yy/mm/dd")
          {
            var display_format = "(yyyy/mm/dd)";
          }
          else if(date_formate == "dd/mm/yy")
          {
            var display_format = "(dd/mm/yyyy)";
          }
          else
          {
            var display_format = "(mm/dd/yyyy)";
          }

          var show_date_format = data[0].show_date_format;
          if(show_date_format == 1){
            if(display_format != "")
            {
              $("#selected_format").text(display_format);
            }
            else
            {
              $("#selected_format").text('mm/dd/yyyy');
            }
          }
          var admin_note_status = data[0].admin_note_status;
          var notes_admin = data[0].admin_order_note;
          if(admin_note_status == 1)
          {
            $("#admin_notes").html($.parseHTML(notes_admin));
          }
          
          var str = parseInt(data[0].date_interval);
          
          if(parseInt(cutoff_status) == 0)
          {          
            if(parseInt(store_hour) <= parseInt(cutoff_hours))
            {
              if(parseInt(store_hour) < parseInt(cutoff_hours))
              {
                  	str = parseInt(data[0].date_interval);
              }
              else 
              {
                	if(parseInt(store_hour) == parseInt(cutoff_hours) && parseInt(store_minute) <= parseInt(cutoff_minute))
              		{
              			str = parseInt(data[0].date_interval);
              		}
                  else
                  {
                    	str = parseInt(data[0].date_interval)+1;
                  }
              }
            }
            else
            {
              str = parseInt(data[0].date_interval)+1;
            }
          }
          
          start_from = '+'+parseInt(str);

          var additional_css = data[0].additional_css;
          jQuery('.additional-css').html("<style>" +additional_css+ "</style>");
		}
	  });
setTimeout(function(){
		var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

		function unavailable(date) {
			ymd = date.getFullYear() + "/" + ("0"+(date.getMonth()+1)).slice(-2) + "/" + ("0"+date.getDate()).slice(-2);
            ymd1 = ("0"+date.getDate()).slice(-2) + "-" + ("0"+(date.getMonth()+1)).slice(-2) + "-" + date.getFullYear();
			day = new Date(ymd).getDay();
			if ($.inArray(ymd1, unavailableDates) < 0 && $.inArray(days[day], unavailableDays) < 0) {
				return [true, "enabled", "Book Now"];
			} else {
				return [false,"disabled","Booked Out"];
			}
		}
  setTimeout(function(){
    jQuery("#delivery-date-pro").datepicker( { 
      dateFormat: date_formate ,
			minDate: start_from, 
			maxDate: "'"+allowed_month+"'",
			beforeShowDay: unavailable
			});
  }, 300)
     $("#delivery-date-pro").prop( "disabled", false );   
	}, 3000);
});
</script>
<style>
  #selected_format{
        display: block;
  	    margin-left: 2px;
    	margin-top: 5px;
    	font-size: 11px;
    	font-style: italic;
        text-align: left;
  }
  #admin_notes{
        display: block;
    	margin-bottom: 20px;
    	font-size: 12px;
    	font-style: italic;
        text-align: left;
  }
  #datepicker_box {
    width: auto;
    text-align: center;
    display: inline-block;
  }
  #datepicker_box .date-box{
    display: inline-block;
    margin: 0px;
    text-align: left;
  }
</style>
<div class="additional-css"></div>