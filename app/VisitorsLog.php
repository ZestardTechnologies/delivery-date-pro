<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorsLog extends Model
{
  protected $table = 'visitors_log';
  public $timestamps = false;
  //protected $primaryKey = 'id';
  protected $fillable =[
    'store_name', 'shop_id', 'ip_address', 'browser_info', 'date_and_time', 'update_shopid_status'
  ];
  
}
