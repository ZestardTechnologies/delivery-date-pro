<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class app_log extends Model
{
    protected $table = 'app_log';
    
      //public $timestamps = false;
        
      protected $fillable =[
        'store_name',
        'shop_id',
        'step1',
        'step1.2',
        'step2',
        'step3',
        'update_shopid_status'
      ];
}
