<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class settings_log extends Model

{

  protected $table = 'settings_log';

  public $timestamps = false;

  //protected $primaryKey = 'id';

  protected $fillable =[
     
    'shop_id',

    'block_date',

    'alloved_month',

    'date_interval',
      
    'datepicker_display_on',
      
    'default_date_option',

    'days',

    'hours',

    'minute',

    'cuttoff_status',

    'app_title',

    'date_format',

    'app_status',

    'datepicker_label',

    'admin_note_status',

    'require_option',

    'required_text',

    'admin_time_status',
      
    'time_require_option',

    'time_label',
      
    'time_default_option_label',

    'delivery_time',
      
    'exclude_block_date_status'

  ];

}

