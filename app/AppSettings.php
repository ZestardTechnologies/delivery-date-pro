<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class AppSettings extends Model
{
  protected $table = 'appsettings';
  public $timestamps = false;
  //protected $primaryKey = 'id';
  protected $fillable =[
    'api_key', 'redirect_url', 'permissions', 'shared_secret'
  ];
  
}
