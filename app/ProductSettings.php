<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSettings extends Model
{
  protected $table = 'product_settings';
  public $timestamps = false;
  //protected $primaryKey = 'id';
  protected $fillable =[
    'shop', 'shop_id', 'product_id', 'status', 'delivery_times', 'blocked_dates', 'blocked_days', 'pre_order_time', 'date_interval', 'cut_off_hours', 'cut_off_minutes', 'update_shopid_status'
  ];
  
}
