<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\settings_log;
use App\block_config;
use App\AppSettings;

class notificationController extends Controller
{
	
    public function today_order_nitification(Request $request)
    {	
        $sh = App::make('ShopifyAPI');
        $shop = session('shop');
        $app_settings = AppSettings::where('id', 1)->first();     
        $shop_find = ShopModel::where('store_name', $shop)->first();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $order_data = $sh->call(['URL' => '/admin/orders.json?limit=250', 'METHOD' => 'GET']);
        //dd($order_data);
        foreach($order_data->orders as $single_order){            
            foreach($single_order->note_attributes as $attribute){
                if($attribute->name == "Delivery-Date"){
                    $current = strtotime(date("Y-m-d"));
                    $delivery_date    = strtotime($attribute->value);
                    $datediff = $delivery_date - $current;
                    $difference = floor($datediff/(60*60*24));
                    if($difference==0)
                    {
                        $mail_html ='<table>
                        <tr>
                            <th>Order id</th>
                            <td>'.$single_order->name.'</td>
                        </tr>
                        <tr>
                            <th>Delivery date</th>
                            <td>'.$attribute->value.'</td>
                        </tr>
                        <tr>
                            <th>Domain</th>
                            <td>''</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>''</td>
                        </tr>
                        <tr>
                            <th>Shop Owner</th>
                            <td>''</td>
                        </tr>
                        <tr>
                            <th>Country</th>
                            <td>''</td>
                        </tr>
                        <tr>
                            <th>Plan</th>
                            <td>''</td>
                        </tr>
                    </table>';   
                        echo 'today </br>';
                    }
                    else{
                        echo $attribute->value.' other </br>';
                    }
                    //dd($difference);

                }
            }
        }		
		
	}
}