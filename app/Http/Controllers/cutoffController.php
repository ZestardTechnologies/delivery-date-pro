<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\block_config;
use App\AppSettings;
use App\ProductSettings;
use App\DeliveryTimeSettings;
use App\CutoffSettings;
use App\VisitorsLog;

class cutoffController extends Controller
{   	
	public function __construct(Request $request)
	{		
	}
	public function cut_off(Request $request)
	{				
		$shop = session('shop');
		if(empty($shop))
		{
		  $shop = $_GET['shop'];
		}		
		$app_settings = AppSettings::where('id', 1)->first();
		$shop_find = ShopModel::where('store_name' , $shop)->first();
		$shop_id = $shop_find->id;
		$config = [];
		$config = block_config::where('shop_id' , $shop_id)->first();
		$data['config'] = $config;
		$data['active'] = 'date';
		$settings=CutoffSettings::where('shop_id', $shop_id)->get();
		$data['settings'] = $settings;
		$data['global_flag']=$shop_find->global_cutoff_time_status;	
		return view('cutoff_settings',$data);					
	}
	
	public function save_cut_off(Request $request)	{			
		
		$shop = session('shop');		
		if(empty($shop))
		{
		  $shop = $_GET['shop'];
		}
		$temp=ShopModel::where('store_name' , $shop)->first();
                $shop_id = $temp->id;
		if($request->input('global_cutoff_time'))
		{						
			$temp->global_cutoff_time_status=1;
			$temp->save();
		}
		else
		{					
			$temp->global_cutoff_time_status=0;
			$temp->save();
		}		
		$count=count($request->input('hour'));					
		for($i=0;$i<$count;$i++)
		{			
			$hour=$request->input('hour')[$i];
			$minute=$request->input('minute')[$i];
			if($hour=="" || $minute=="")
			{
				$hour="-";
				$minute="-";
			}
			$info=array('cutoff_day' => $i, 'cutoff_hour' => $hour, 'cutoff_minute' =>$minute, 'store' => $shop, 'shop_id' => $shop_id);	
			$row_count=CutoffSettings::where(['cutoff_day' => $i, 'store' => $shop])->count();
			if($row_count > 0)
			{
				CutoffSettings::where(['cutoff_day' => $i, 'store' => $shop])->update($info);	
			}	
			else
			{
				CutoffSettings::insert($info);
			}							
		}			
		$notification = array(
		'message' => 'Settings Saved Successfully.',
		'alert-type' => 'success');     						
		return redirect()->route('cut-off')->with('notification',$notification);	
	}		
	
}