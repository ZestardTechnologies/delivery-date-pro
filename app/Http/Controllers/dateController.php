<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\settings_log;
use App\block_config;
use App\AppSettings;
use App\ProductSettings;
use App\DeliveryTimeSettings;
use App\CutoffSettings;
use App\VisitorsLog;

class dateController extends Controller {

    public function index(Request $request) {
        $shop_model = new ShopModel;
        $shop = session('shop');
        if (session('shop') == "shopify-dev-app.myshopify.com") {
            //dd($_GET);
        }

        if (empty($shop)) {
            $shop = $_GET['shop'];
            //$shop = $request->input('shop');
            session(['shop' => $shop]);
        }
        if (isset($_GET['notification'])) {
            $notification = $_GET['notification'];
        } else {
            $notification = array();
        }
        //session(['shop' => $shop]);		
        $app_settings = AppSettings::where('id', 1)->first();
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_id = $shop_find->id;
        $type_of_app = $shop_find->type_of_app;
        $app_version = $shop_find->app_version;
        $new_install = $shop_find->new_install;
        $upgrade_status = $shop_find->upgrade_status;
        $upgrade_modal_status = $shop_find->upgrade_modal_status;
        if ($shop == "free-theme-test.myshopify.com") {
            // dd($upgrade_status . " " . $upgrade_modal_status);
        }
        $install_date = $shop_find->payment_created_at;
        $date_time = explode("T", $install_date);
        $date = strtotime($date_time[0]);
        $date = strtotime("+7 day", $date);
        $modal_date = date('Y-m-d', $date);
        $todays_date = date('Y-m-d');
        if (strtotime($todays_date) > strtotime($modal_date)) {
            $new_install = 'N';
            $shop_find->new_install = 'N';
            $shop_find->save();
        }
        $config = [];
        $config = block_config::where('shop_id', $shop_id)->first();

        //For Basic Version, Return to Basic Dashboard	
        if (($type_of_app == 1 ) && ($app_version == 1)) {
            return view('dashboard_basic', ['config' => $config, 'active' => 'date', 'new_install' => $new_install, 'notification' => $notification, 'shop' => $shop, 'upgrade_status' => $upgrade_status, 'upgrade_modal_status' => $upgrade_modal_status]);
        }
        //For Professional Version, Return to Professional Dashboard	
        if (($type_of_app == 2 ) && ($app_version == 2)) {
            return view('dashboard_pro', ['config' => $config, 'active' => 'date', 'new_install' => $new_install, 'notification' => $notification, 'shop' => $shop, 'upgrade_status' => $upgrade_status, 'upgrade_modal_status' => $upgrade_modal_status]);
        }
        //For EnterPrise Version, Return to EnterPrise Dashboard	
        if ($type_of_app == 3 && $app_version == 3) {
            return view('dashboard', ['config' => $config, 'active' => 'date', 'new_install' => $new_install, 'notification' => $notification, 'shop' => $shop, 'upgrade_status' => $upgrade_status, 'upgrade_modal_status' => $upgrade_modal_status]);
        }
        //For Ultimate Version, Return to Ultimate Dashboard	
        if ($type_of_app == 4 && $app_version == 4) {
            return view('dashboard_product', ['config' => $config, 'active' => 'date', 'new_install' => $new_install, 'shop' => $shop, 'upgrade_status' => $upgrade_status, 'upgrade_modal_status' => $upgrade_modal_status]);
        }
    }

    public function check_data(Request $request) {
        $product_data = array();
        parse_str($request->input('data'), $product_data);
        if (isset($product_data['selected_product'])) {
            $selected_products_count = count($product_data['selected_product']);
            $selected_products = $product_data['selected_product'];
            $shop = session('shop');
            $data = array();
            $data['global_product_delivery_time'] = isset($product_data['global_product_delivery_time']) ? 1 : 0;
            $data['global_product_blocked_dates'] = isset($product_data['global_product_blocked_dates']) ? 1 : 0;
            $data['global_product_blocked_days'] = isset($product_data['global_product_blocked_days']) ? 1 : 0;
            $data['global_product_pre_order'] = isset($product_data['global_product_pre_order']) ? 1 : 0;
            $data['global_product_date_interval'] = isset($product_data['global_product_date_interval']) ? 1 : 0;
            $data['global_product_cut_off'] = isset($product_data['global_product_cut_off']) ? 1 : 0;

            $user_settings = ShopModel::where('store_name', $shop)->first();
            block_config::where(['shop_id' => $user_settings->id])->update($data);
            /* DB::table('product_settings')->truncate(); */
            /* dd($selected_products);	 */
            for ($i = 0; $i < $selected_products_count; $i++) {
                $data = array();
                $product_id = $selected_products[$i];               
                $product_settings = ProductSettings::where(['shop_id' => $user_settings->id, 'product_id' => $product_id])->first();
                if (isset($product_data['delivery_time_' . $product_id])) {
                    $data['delivery_times'] = $product_data['delivery_time_' . $product_id];
                } else {
                    $data['delivery_times'] = $product_settings->delivery_times;
                }
                if (isset($product_data['blockeddate_' . $product_id])) {
                    $data['blocked_dates'] = str_replace("/", "-", implode(",", $product_data['blockeddate_' . $product_id]));
                }
                if (isset($product_data['days_' . $product_id])) {
                    if (count($product_data['days_' . $product_id]) > 0) {
                        $data['blocked_days'] = implode(",", $product_data['days_' . $product_id]);
                    }
                }
                if (isset($product_data['allowed_month_' . $product_id])) {
                    $data['pre_order_time'] = $product_data['allowed_month_' . $product_id];
                }
                if (isset($product_data['intervel_' . $product_id])) {
                    $data['date_interval'] = $product_data['intervel_' . $product_id];
                }
                $data['cut_off_hours'] = empty($product_data['hour_' . $product_id]) ? 0 : $product_data['hour_' . $product_id];
                $data['cut_off_minutes'] = empty($product_data['minute_' . $product_id]) ? 0 : $product_data['minute_' . $product_id];

                $count = ProductSettings::where(['shop' => $shop, 'product_id' => $product_id])->count();
                if ($count > 0) {
                    $data = ProductSettings::where(['shop' => $shop, 'product_id' => $product_id])->update($data);
                } else {
                    $data['shop'] = $shop;
                    $data['product_id'] = $product_id;
                    $data = ProductSettings::insert($data);
                }
            }
        }
    }

    public function get_product_data(Request $request) {
        $product_settings = ProductSettings::select('product_id')->get();
        $product_settings_array = array();
        foreach ($product_settings as $product) {
            $product_id = $product->product_id;
            /* $product_settings_array[$product_id] = DB::table('product_settings')->select('product_id', 'blocked_dates', 'blocked_days', 'pre_order_time', 'cut_off_hours', 'cut_off_minutes')->where('product_id', $product_id)->first();	 */
            $product_settings_array[$product_id] = ProductSettings::where('product_id', $product_id)->first();
        }
        return json_encode($product_settings_array);
    }

   //For Updating General Settings
    public function update(Request $recuestdata, $id) {
        $data = block_config::where('shop_id', $id)->first();
        $shop = session('shop');
        $old_data = new settings_log;
        $old_format = $data->date_format;
        //dd($recuestdata->all());	
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        //For Logging Old Data Before Storing New Changes
        $shop_find = ShopModel::where('id', $id)->first();
        $old_data->shop_id = $data->shop_id;
        $old_data->app_status = $data->app_status;
        $old_data->block_date = $data->block_date;
        $old_data->alloved_month = $data->alloved_month;
        $old_data->date_interval = $data->date_interval;
        $old_data->datepicker_display_on = $data->datepicker_display_on;
        $old_data->default_date_option = $data->default_date_option;
        $old_data->days = $data->days;
        $old_data->hours = $data->hours;
        $old_data->minute = $data->minute;
        $old_data->cuttoff_status = $data->cuttoff_status;
        $old_data->date_format = $data->date_format;
        $old_data->show_date_format = $data->show_date_format;
        $old_data->datepicker_label = $data->datepicker_label;
        $old_data->admin_order_note = $data->admin_order_note;
        $old_data->additional_css = $data->additional_css;
        $old_data->admin_note_status = $data->admin_note_status;
        $old_data->require_option = $data->require_option;
        $old_data->admin_time_status = $data->admin_time_status;
        $old_data->time_require_option = $data->time_require_option;
        $old_data->add_delivery_information = $data->add_delivery_information;
        $old_data->time_label = $data->time_label;
        $old_data->time_default_option_label = $data->time_default_option_label;
        $old_data->delivery_time = $data->delivery_time;
        $old_data->type_of_app = $shop_find->type_of_app;
        $old_data->app_version = $shop_find->app_version;
        $old_data->ip_address = $ip;
        $old_data->save();

        if ($recuestdata['blockdate']) {
            $dateConverted = array();
            if ($recuestdata['blockdate'][0] != "") {
                foreach ($recuestdata['blockdate'] as $dateConvert) {
                    if (session('shop') == "ivyrose-floral-designs.myshopify.com") {
                        //echo "blocked date ".count(trim($dateConvert)) . "<br>";
                    }
                    $dateConvert = trim($dateConvert);
                    if ($dateConvert != '') {
                        if ($old_format == "dd/mm/yy") {
                            $dateConverted[] = str_replace("/", "-", $dateConvert);
                        }
                        if ($old_format == "mm/dd/yy") {
                            $date_array = explode("/", $dateConvert);
                            $dateConverted[] = $date_array[1] . "-" . $date_array[0] . "-" . $date_array[2];
                        }
                        if ($old_format == "yy/mm/dd") {
                            $date_array = explode("/", $dateConvert);
                            $dateConverted[] = $date_array[2] . "-" . $date_array[1] . "-" . $date_array[0];
                        }
                    }
                }
                if (session('shop') == "ivyrose-floral-designs.myshopify.com") {
                    //die;
                }
            }
        } else {
            $dateConverted = array();
        }
        $data->block_date = json_encode($dateConverted);

        if ($recuestdata['allowed_month']) {
            $data->alloved_month = $recuestdata['allowed_month'];
        } else {
            $data->alloved_month = 0;
        }
        if ($recuestdata['intervel']) {
            $data->date_interval = $recuestdata['intervel'];
        } else {
            $data->date_interval = 0;
        }
        if ($recuestdata['datepicker_display_on']) {
            $data->datepicker_display_on = $recuestdata['datepicker_display_on'];
        } else {
            $data->datepicker_display_on = 0;
        }
        if ($recuestdata['default_date']) {
            $default_date = $recuestdata['default_date'];
        } else {
            $default_date = 0;
        }
        $data->default_date_option = $default_date;
        if ($recuestdata['days']) {
            if (in_array("all_allow", $recuestdata['days'])) {
                $data->days = null;
            } else {
                $data->days = json_encode($recuestdata['days']);
            }
        } else {
            $data->days = null;
        }
        if ($recuestdata['cuttoff_time'] == "cuttoff_on") {
            $data->hours = $recuestdata['hours'];
            $data->minute = $recuestdata['minute'];
            $data->cuttoff_status = 1;
        } else {
            $data->hours = $recuestdata['hours'];
            $data->minute = $recuestdata['minute'];
            $data->cuttoff_status = 0;
        }
        if ($recuestdata['app_status']) {
            $data->app_status = $recuestdata['app_status'];
        } else {
            $data->app_status = "Deactive";
        }
        if ($recuestdata['require_option']) {
            $data->require_option = $recuestdata['require_option'];
        } else {
            $data->require_option = 0;
        }
        if ($recuestdata['show_datepicker_label']) {
            $data->show_datepicker_label = $recuestdata['show_datepicker_label'];
        } else {
            $data->show_datepicker_label = 0;
        }

        $data->datepicker_label = $recuestdata['datepicker_label'];

        if ($recuestdata['show_date_format']) {
            $data->show_date_format = $recuestdata['show_date_format'];
        } else {
            $data->show_date_format = 0;
        }
        if ($recuestdata['admin_time_status']) {
            $data->admin_time_status = $recuestdata['admin_time_status'];
        } else {
            $data->admin_time_status = 0;
        }
        $data->date_format = $recuestdata['date_format'];
        $data->admin_order_note = $recuestdata['admin_order_note'];
        $data->additional_css = $recuestdata['additional_css'];
        if ($recuestdata['admin_note_status']) {
            $data->admin_note_status = $recuestdata['admin_note_status'];
        } else {
            $data->admin_note_status = 0;
        }
        if ($recuestdata['time_require_option']) {
            $data->time_require_option = $recuestdata['time_require_option'];
        } else {
            $data->time_require_option = 0;
        }
        if ($recuestdata['add_delivery_information']) {
            $data->add_delivery_information = $recuestdata['add_delivery_information'];
        } else {
            $data->add_delivery_information = 0;
        }
        $data->time_label = $recuestdata['time_label'];
        $data->time_default_option_label = $recuestdata['time_default_option_label'];
        $data->delivery_time = $recuestdata['delivery_time'];
        if ($recuestdata['datepicker_display_on']) {
            $data->datepicker_display_on = $recuestdata['datepicker_display_on'];
        } else {
            $data->datepicker_display_on = 0;
        }
        $data->language_locale = $recuestdata['language_locale'];
        $data->date_error_message = $recuestdata['date_error_message'];
        $data->time_error_message = $recuestdata['time_error_message'];
        if ($recuestdata['exclude_block_date_status']) {
            $data->exclude_block_date_status = $recuestdata['exclude_block_date_status'];
        } else {
            $data->exclude_block_date_status = 0;
        }
        $data->save();
        $current_setting = ShopModel::where('id', $id)->first()->toArray();
        $new_install = $current_setting['new_install'];
        if ($new_install == 'Y') {
            ShopModel::where('id', $id)->update(['new_install' => 'N']);
        }
        $notification = array(
            'message' => 'Settings Saved Successfully.',
            'alert-type' => 'success'
        );
        //return redirect()->route('dashboard', ['shop' => $shop_find->store_name])->with('notification',$notification);
        //return redirect()->route('dashboard', ['shop' => $shop_find->store_name , 'notification' => $notification])->with('notification',$notification);
        return redirect()->route('dashboard', ['shop' => $shop_find->store_name, 'notification' => $notification])->with('notification', $notification);
    }

    public function selectdate(Request $recuestdata) {
        $config = [];
        $shop = $recuestdata['shop'];    
        if(empty($shop)){
            $shop = session('shop');
        }    
        $shop_find = ShopModel::where('store_name', $shop)->first();                 
        if (count($shop_find) > 0) {
            $shop_id = $shop_find->id;
            $global_cutoff_flag = $shop_find->global_cutoff_time_status;
            $global_deliverytime_flag = $shop_find->global_delivery_time_status;
            $config = block_config::where('shop_id', $shop_id)->get();                    
            $config[0]['cutoff_global_status'] = $global_cutoff_flag;
            $config[0]['deliverytime_global_status'] = $global_deliverytime_flag;            
            return $config;
        } else {             
            return 0;
        }
    }

    // For PetalJet	
    public function getconfigandtype(Request $recuestdata) {
        $config = [];
        $shop = $recuestdata['shop'];
        $shop_find = ShopModel::where('store_name', $shop)->first();
        if (count($shop_find) > 0) {
            $shop_id = $shop_find->id;
            $global_cutoff_flag = $shop_find->global_cutoff_time_status;
            $global_deliverytime_flag = $shop_find->global_delivery_time_status;
            $config = block_config::where('shop_id', $shop_id)->get();
            $config[0]['cutoff_global_status'] = $global_cutoff_flag;
            $config[0]['deliverytime_global_status'] = $global_deliverytime_flag;
            $config[0]['app_version'] = $shop_find->app_version;
            $config[0]['type_of_app'] = $shop_find->type_of_app;
            return $config;
        } else {
            return 0;
        }
    }

    // For PetalJet
    // Get Delivery Time Dynamically Based on Day
    public function get_delivery_times(Request $request) {
        //For Getting Delivery Time Each Time Date is Changed
        $todays_date = "";
        if ($request->input('todays_date')) {
            if ($request->input('date_format') == "mm/dd/yy") {
                $date = date("d-m-Y", strtotime($request->input('todays_date')));
                $todays_date = str_replace("/", "-", $date);
                $day = date("w", strtotime($todays_date));
            } else {
                $todays_date = str_replace("/", "-", $request->input('todays_date'));
                $day = date("w", strtotime($todays_date));
            }
        }
        //For Getting Delivery Time on Page Load
        else {
            $start_from = $request->input('start_from');
            $date = date("d-m-Y");
            $next_date = date('d-m-Y H:i:s', strtotime($date . $start_from . ' day'));
            $day = date("w", strtotime($next_date));
        }
        $options = "";
        $shop = $request->input('shop_name');
        $temp = ShopModel::where('store_name', $shop)->first();
        $flag = $temp->delivery_time_count_status;
        $app_version = $temp->app_version;
        $current_time = date("Y-m-d", time());
        /* Code by dipal product global setting off for ultimate version */
        if ($app_version == 4 && $request->input('product_id') != '' && $request->input('global_time') == 0) {
            $product_id = $request->input('product_id');
            $result = ProductSettings::where(['shop' => $shop, 'status' => 1, 'product_id' => $product_id])->get();
            $count = ProductSettings::where(['shop' => $shop, 'status' => 1, 'product_id' => $product_id])->count();
            if ($count > 0) {
                $options_array = array();
                $delivery_times = $result[0]->delivery_times;
                $delivery_times = explode(',', $delivery_times);
                for ($i = 0; $i < count($delivery_times); $i++) {
                    array_push($options_array, $delivery_times[$i]);
                }
            } else {
                $options_array = 0;
            }
        }
        /* product global setting on for ultimate version */
        else if ($app_version == 4) {
            $result = block_config::where(['shop_id' => $temp->id])->first();
            if (!empty($result)) {
                $options_array = array();
                $delivery_times = $result->delivery_time;
                $delivery_times = explode(',', $delivery_times);
                for ($i = 0; $i < count($delivery_times); $i++) {
                    array_push($options_array, $delivery_times[$i]);
                }
            } else {
                $options_array = 0;
            }
        }
        /* end code */ 
        else {        
                $result = DeliveryTimeSettings::where(['store' => $shop, 'day' => $day])->get();
                $count = DeliveryTimeSettings::where(['store' => $shop, 'day' => $day])->count();
                //If Delivery Time exists for selected day then Return Array of Delivery Time
                if ($count > 0) {
                    $counts = explode(",", $result[0]->count);
                    $options_array = array();
                    $delivery_times = json_decode($result[0]->delivery_times, true);
                    if ($flag == 1) {
                        for ($i = 0; $i < count($counts); $i++) {
                            if (intval($counts[$i]) > 0) {
                                if ($shop == "the-bunched-co.myshopify.com") {
                                    $order_count = 0;
                                    $orders = DB::table('order_details')->where(['details' => $shop])->where('product_delivery_info', '!=', '')->get();
                                    $flag = 1;
                                    //dd($todays_date);							
                                    foreach ($orders as $order) {
                                        $temp = json_decode($order->product_delivery_info, true);
                                        if (empty($temp)) {
                                            $temp = array();
                                        }
                                        if (count($temp) > 0) {
                                            foreach ($temp as $info) {
                                                if ($info[0] == str_replace("-", "/", $todays_date) && $info[1] == $delivery_times[$i]) {
                                                    //$flag = 0;
                                                    $order_count++;
                                                }
                                            }
                                        }
                                    }
                                    //echo $counts ." ". intval($count);				
                                    if ($order_count >= intval($counts[$i])) {
                                        $flag = 0;
                                    }
                                    if ($flag == 1) {
                                        array_push($options_array, $delivery_times[$i]);
                                    }
                                } else {
                                    array_push($options_array, $delivery_times[$i]);
                                }
                            }
                        }
                    } else {
                        for ($i = 0; $i < count($counts); $i++) {
                            array_push($options_array, $delivery_times[$i]);
                        }
                    }
                }
                //If there are no Delivery Time For Selected Day Return 0
                else {
                    $options_array = 0;
                }            
        }
        return $options_array;
    }

    // Get Cutoff Time Dynamically Based on Day
    public function get_cutoff_time(Request $request) {        
        $day = $request->input('day');
        $shop = $request->input('shop_name');
        $result = CutoffSettings::where(['store' => $shop, 'cutoff_day' => $day])->get();
        return json_encode($result[0]);
    }

    // Validating Cut Off Time	
    public function check_cutoff_time(Request $request) {
        $start_from = $request->input('start_from') - 1;
        $date = $request->input('store_date');
        $cutoff_date = str_replace("/", "-", $request->input('delivery_date'));
        $cutoff_flag = 0;
        $invalid_date = 0;
        $date = strtotime("$start_from day", strtotime($date));
        $min_date = date('d-m-Y', $date);

        // Compare Next Available Date With Selected Date and Set Cut Off Flag 	
        $cutoff_timestamp = strtotime($cutoff_date);
        $mindate_timestamp = strtotime($min_date);
        //echo $cutoff_timestamp ." ". $mindate_timestamp;
        if ($cutoff_timestamp == $mindate_timestamp) {
            $cutoff_flag = 1;
        }
        if ($cutoff_timestamp < $mindate_timestamp) {
            $invalid_date = 1;
        }
        $cutoff_flag;
        $shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_id = $shop_find->id;
        $day = date('w');

        $cutoff_time_settings = CutoffSettings::where(['store' => $shop, 'cutoff_day' => $day])->get();

        $delivery_time_status = $shop_find->global_delivery_time_status;
        $cutoff_global_status = $shop_find->global_cutoff_time_status;
        $config = [];
        $config = block_config::where('shop_id', $shop_id)->first();
        $cutoff_status = $config->cuttoff_status;
        $cutoff_hours = $config->hours;
        $cutoff_minute = $config->minute;
        $store_hour = $request->input('store_hour');
        $store_minute = $request->input('store_minute');

        //Check Cut Off Status 
        if ($cutoff_status == 0) {
            echo 1;
        } else {
            //Check Global Cut Off Status is Active, Then Compare with Global Cut Off time				
            if ($invalid_date == 1) {
                echo 2;
            } else {
                if ($cutoff_global_status == 1) {
                    if (($store_hour) <= ($cutoff_hours)) {
                        if (($store_hour) < ($cutoff_hours)) {
                            echo 1;
                        } else {
                            if (($store_hour) == ($cutoff_hours) && ($store_minute) <= ($cutoff_minute)) {
                                echo 1;
                            } else {
                                // If User Selected Next Available Date After Cut Off Time Return 0 Else 1
                                if ($cutoff_flag == 1) {
                                    echo 0;
                                } else {
                                    echo 1;
                                }
                            }
                        }
                    } else {
                        if ($cutoff_flag == 1) {
                            echo 0;
                        } else {
                            echo 1;
                        }
                    }
                }
                //If Global Cut Off Status if Inactive, Then Compare with Current Day's Cut Off time
                else {
                    $cutoff_hours = $cutoff_time_settings[0]->cutoff_hour;
                    $cutoff_minute = $cutoff_time_settings[0]->cutoff_minute;

                    if (($store_hour) <= ($cutoff_hours)) {
                        if (($store_hour) < ($cutoff_hours)) {
                            echo 1;
                        } else {
                            if (($store_hour) == ($cutoff_hours) && ($store_minute) <= ($cutoff_minute)) {
                                echo 1;
                            } else {
                                // If User Selected Next Available Date After Cut Off Time Return 0 Else 1
                                if ($cutoff_flag == 1) {
                                    echo 0;
                                } else {
                                    echo 1;
                                }
                            }
                        }
                    } else {
                        // If User Selected Next Available Date After Cut Off Time Return 0 Else 1
                        if ($cutoff_flag == 1) {
                            echo 0;
                        } else {
                            echo 1;
                        }
                    }
                }
            }
        }
    }

    // Validating Delivery Time Time	
    public function check_delivery_time(Request $request) {
        $shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettings::where('id', 1)->first();
        $shop_id = $shop_find->id;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

        $delivery_date = $request->input('delivery_date');
        $delivery_time = $request->input('delivery_time');
        $format = $request->input('date_format');

        //Converting Date According to Store's Date Format
        if ($format == "dd/mm/yy") {
            $date = str_replace("/", "-", $delivery_date);
            $date = date("Y-m-d", strtotime($date));
        }
        if ($format == "mm/dd/yy") {
            $date = str_replace("/", "-", $delivery_date);
            $date = date("Y-m-d", strtotime($delivery_date));
        }
        if ($format == "yy/mm/dd") {
            $date = str_replace("/", "-", $delivery_date);
        }
        $day = date('w', strtotime($date));
        $result = DeliveryTimeSettings::where(['store' => $shop, 'day' => $day])->get();
        $delivery_times = json_decode($result[0]->delivery_times, true);
        $delivery_time_count = explode(",", $result[0]->count);
        $date_atrributes = '';
        $time_attributes = '';
        $index = array_search("$delivery_time", $delivery_times);
        if (isset($index)) {
            $count = $delivery_time_count[$index];
        }
        $counts = 0;
        $flag = 0;        
        $delivery_time_settings = DeliveryTimeSettings::where(['store' => $shop, 'day' => $day])->get();
        $global_delivery_time_status = $shop_find->global_delivery_time_status;
        $delivery_time_count_status = $shop_find->delivery_time_count_status;
        $config = [];
        $config = block_config::where('shop_id', $shop_id)->first();

        //If Global Delivery Time Status Is Active Then No Need to Check Count Just Return 1	
        if ($global_delivery_time_status == 1) {
            echo 1;
        }
        //If Global Delivery Time Status is Inactive Then Check Count
        else {
            //If Count is Inactive Return 1
            if ($delivery_time_count_status == 0) {
                echo 1;
            }
            //If Count is Active Check Count
            else {                
                //If Order's Have Reached Count Limit, Then Return 0 Else 1			
                if ($shop == "the-bunched-co.myshopify.com") {
                    $counts = 0;
                    $orders = DB::table('order_details')->where(['details' => $shop])->where('product_delivery_info', '!=', '')->get();
                    $flag = 1;
                    foreach ($orders as $order) {
                        $temp = json_decode($order->product_delivery_info, true);
                        if (empty($temp)) {
                            $temp = array();
                        }
                        if (count($temp) > 0) {
                            foreach ($temp as $info) {
                                if ($info[0] == $delivery_date && $info[1] == $delivery_time) {
                                    //$flag = 0;
                                    $counts++;
                                }
                            }
                        }
                    }
                    //echo $counts ." ". intval($count);				
                    if ($counts >= intval($count)) {
                        $flag = 0;
                    }
                    echo $flag;
                } else {
                    if ($shop == "chill-and-shop.myshopify.com") {
                        $new_date = date("Y-d-m", strtotime($date));
                        $counts = DB::table('order_details')->where(['delivery_date' => $new_date, 'delivery_time' => $delivery_time, 'details' => $shop])->count();
                        //dd('$count='.$count.'and $counts='.$counts);
                        if ($count > 0) {
                            if (intval($counts) < intval($count)) {
                                echo 1;
                            } else {
                                echo 0;
                            }
                        } else {

                            echo 1;
                        }
                    } else {
                        $counts = DB::table('order_details')->where(['delivery_date' => $date, 'delivery_time' => $delivery_time, 'details' => $shop])->count();

                        if (!empty($count)) {
                            if (intval($counts) < intval($count)) {
                                //dd('test');
                                echo 1;
                            } else {
                                //dd('test1');
                                echo 0;
                            }
                        } else {
                            echo 1;
                        }
                    }
                }
            }
        }
    }

    public function check_cart(Request $request) {
        $shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettings::where('id', 1)->first();
        $shop_id = $shop_find->id;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

        $delivery_count = 0;
        $delivery_date = $request->input('delivery_date');
        $delivery_time = $request->input('delivery_time');
        $format = $request->input('date_format');
        $delivery_info_array = json_decode($request->input('delivery_info'), true);
        //dd($delivery_info_array);
        foreach ($delivery_info_array as $info) {
            // echo "<br>" . $info[0] . " " . $delivery_date . "<br>";
            // echo $info[1] . " " . $delivery_time ;
            if ($info[0] == $delivery_date && $info[1] == $delivery_time) {
                $delivery_count++;
            }
        }
        //Converting Date According to Store's Date Format
        if ($format == "dd/mm/yy") {
            $date = str_replace("/", "-", $delivery_date);
            $date = date("Y-m-d", strtotime($date));
        }
        if ($format == "mm/dd/yy") {
            $date = str_replace("/", "-", $delivery_date);
            $date = date("Y-m-d", strtotime($delivery_date));
        }
        if ($format == "yy/mm/dd") {
            $date = str_replace("/", "-", $delivery_date);
        }

        $day = date('w', strtotime($date));
        $result = DeliveryTimeSettings::where(['store' => $shop, 'day' => $day])->get();
        $delivery_times = json_decode($result[0]->delivery_times, true);
        $delivery_time_count = $result[0]->count;
        $date_atrributes = '';
        $time_attributes = '';
        $delivery_time_count = explode(",", $result[0]->count);
        $date_atrributes = '';
        $time_attributes = '';
        $index = array_search("$delivery_time", $delivery_times);
        if (isset($index)) {
            $count = $delivery_time_count[$index];
        }
        $counts = 0;
        $flag = 1;
        /* $min_date=date("Y-m-d",strtotime($date));
          $max_date=date("Y-m-d",strtotime("+1 days",strtotime($min_date))); */

        $delivery_time_settings = DeliveryTimeSettings::where(['store' => $shop, 'day' => $day])->get();

        $global_delivery_time_status = $shop_find->global_delivery_time_status;
        $delivery_time_count_status = $shop_find->delivery_time_count_status;
        $config = [];
        $config = block_config::where('shop_id', $shop_id)->first();

        //If Global Delivery Time Status Is Active Then No Need to Check Count Just Return 1	
        if ($global_delivery_time_status == 1) {
            echo 1;
        }
        //If Global Delivery Time Status is Inactive Then Check Count
        else {
            //If Count is Inactive Return 1
            if ($delivery_time_count_status == 0) {
                echo 1;
            }
            //If Count is Active Check Count
            else {
                if ($delivery_count < $count) {
                    echo 1;
                } else {
                    echo 0;
                }
            }
        }
    }

    //Check Blocked Dates
    public function check_blocked_date(Request $request) {
        //Converting Date According to Store's Date Format
        if ($request->input('date_format') == "mm/dd/yy") {
            $date = date("d-m-Y", strtotime($request->input('delivery_date')));
        }
        if ($request->input('date_format') == "dd/mm/yy") {
            $date = str_replace("/", "-", $request->input('delivery_date'));
        }
        if ($request->input('date_format') == "yy/mm/dd") {
            $date = date("d-m-Y", strtotime($request->input('delivery_date')));
        }
        $delivery_date = strtotime($date);
        $shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_id = $shop_find->id;
        $config = block_config::where('shop_id', $shop_id)->first();
        $blocked_dates = json_decode($config->block_date, true);
               
        //If there are No Blocked Dates Simply Return 1 else Check in Blocked Dates
        if (count($blocked_dates) > 0) {
            $blocked_dates_timestamp = array();
            foreach ($blocked_dates as $date) {
                array_push($blocked_dates_timestamp, strtotime($date));
            }
            $delivery_time_stamp = strtotime($delivery_date);

            // If Selected Delivery Date Exists in Blocked Dates Then Return False
            if (in_array($delivery_date, $blocked_dates_timestamp)) {
                echo 0;
            } else {
                echo 1;
            }
        } else {
            echo 1;
        }
    }

    //Check Blocked Days
    public function check_blocked_day(Request $request) {
        $days = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        $json_response = array();
        $shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_id = $shop_find->id;
        $config = block_config::where('shop_id', $shop_id)->first();
        //Converting Date According to Store's Date Format
        if ($request->input('date_format') == "mm/dd/yy") {
            $todays_date = str_replace("/", "-", $request->input('todays_date'));
            $date = date("d-m-Y", strtotime($request->input('todays_date')));
            $today = date("w", strtotime($date));
        }
        if ($request->input('date_format') == "dd/mm/yy") {
            $todays_date = str_replace("/", "-", $request->input('todays_date'));
            $today = date("w", strtotime($todays_date));
        }
        if ($request->input('date_format') == "yy/mm/dd") {
            $todays_date = str_replace("/", "-", $request->input('todays_date'));
            $today = date("w", strtotime($todays_date));
        }
        if (empty($config->days)) {
            $json_response['result'] = 1;
        } else {
            $blocked_days = json_decode($config->days, true);
            if (in_array($days[$today], $blocked_days)) {
                $json_response['result'] = 0;
                $json_response['today'] = $days[$today];
            } else {
                $json_response['result'] = 1;
                $json_response['today'] = $days[$today];
            }
        }
        return json_encode($json_response);
    }

    // For Redirecting To Permission Denied Page if App Version is Less Than 3	
    public function permission_denied() {
        return view('permission_denied');
    }

    // For Logging Details Such as Store, IP Address, Browser and OS Info, Date & Time
    public function save_visitors_log(Request $request) {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $info = array(
            'store_name' => session('shop'),
            'ip_address' => $ip,
            'browser_info' => $request->input('user_agent'),
            'date_and_time' => date("d-m-Y H:i:s"));
        VisitorsLog::insert($info);
    }

    //For getting app type and version
    public function get_type_and_version(Request $request) {
        $shop = $request->input('shop_name');
        if(empty($shop)){
            $shop = session('shop');
        }
        
        $data = array();
        if($shop == 'davidaustinroses-dev.myshopify.com'){
            $data_info = ShopModel::where('store_name', $shop)->first();                    
            $shop_id = $data_info->id;    
            $data = ShopModel::with('block_config')->where('store_name', $shop)->whereHas('block_config', function($query) use ($shop_id) {
                $query->where('shop_id', '=', $shop_id);
            })->first();  
        } else {
            $data = ShopModel::where('store_name', $shop)->get();        
        }
        
        if (count($data) > 0) {
            return $data;
        } else {
            return 0;
        }
    }

    public function update_modal_status(Request $request) {
        $shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_find->new_install = 'N';
        $shop_find->save();
    }

    public function update_upgrade_modal_status(Request $request) {
        $shop = $request->input('shop_name');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_find->upgrade_modal_status = 'N';
        $shop_find->save();
    }

    //For sending email according to issue(jQuery or Datepicker Object or jQuery UI Undefined)
    public function acknowledge(Request $request) {
        $type = $request->input('type');
        $store = $request->input('shop_name');
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        if ($type == 1) {
            $msg = "jQuery Not Loaded in Store $store";
        }

        if ($type == 2) {
            $msg = "jQuery UI Not Loaded in Store $store";
        }

        if ($type == 3) {
            $msg = "Datepicker Object Not Created in Store $store";
        }

        mail("girish.zestard@gmail.com", "Type $type Error in Store $store", $msg, $headers);
    }

    public function delivery_date_pro(Request $request) {
        
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $order_number = $request->input('order_number');
        $customer_id = $request->input('customer_id');
        $delivery_date = $request->input('delivery_date');
        $delivery_time = $request->input('delivery_time');
        $shop = $request->input('shop_name');
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name', $shop)->first();               
        $shop_id = $shop_find->id;        
        // if(empty($shop))
        // {
        // 	$shop=$_GET['shop'];
        // 	session(['shop' => $shop]);
        // }
        $app_settings = AppSettings::where('id', 1)->first();
        $config_settings = block_config::where('shop_id', $shop_id)->first();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $order_details = $sh->call(["URL" => "/admin/orders/$order_number.json", "METHOD" => "GET"]);
        $customer_email = $order_details->order->email;
        $store_details = $sh->call(['URL' => 'shop.json', 'METHOD' => 'GET']);
        $email_address = $store_details->shop->email;

        $msg = "Following is the Delivery information which was missed by customer during checkout.<br>" .
                "<b>Order Id:</b>" . $order_number . "<br>" .
                "<b>Delivery Date:</b>" . $delivery_date . "<br>";
        if (!empty($delivery_time)) {
            $msg .= "<b>Delivery Time:</b>" . $delivery_time;
        }
        $msg .= "<br>To view delivery details, click on below link.<br><a href='https://$shop/admin/orders/$order_number'>Click Here</a>";

        if ($delivery_date == "" || empty($delivery_date) || $delivery_date == "0000-00-00" || $delivery_date == "1969-12-31") {
            if ($config_settings->require_option == "1") {
                //DB::table('order_details')->where(['shop_id' => $shop_id , 'order_id' => $order_number ])->update(['delivery_info_status' => 1]);	
            } else {
                
            }
        } else {
            $delivery_array = array();
            $delivery_array['Delivery-Date'] = $delivery_date;
            if (!empty($delivery_time)) {
                $delivery_array['Delivery-Time'] = $delivery_time;
            }
            $order_data = [
                "order" => [
                    "id" => $order_number,
                    "note_attributes" => $delivery_array
                ]
            ];
            $delivery_date = date('Y-m-d', strtotime($delivery_date));
            $sh->call(['URL' => "/admin/orders/$order_number.json", 'METHOD' => 'PUT', 'DATA' => $order_data]);
            //mail("girish.zestard@gmail.com","Regarding Order #".$request->input('order_number'), $msg ,$headers);	
            mail($email_address, "Regarding Order #" . $request->input('order_number'), $msg, $headers);
            DB::table('order_details')->where(['shop_id' => $shop_id, 'order_id' => $order_number])->update(['delivery_info_status' => 1, 'delivery_date' => $delivery_date, 'delivery_time' => $delivery_time]);
            echo $shop_id;
        }
    }

    public function check_delivery_info(Request $request) {
        $order_number = $request->input('order_number');
        $order_detail = DB::table('order_details')->where(['order_id' => $order_number])->first();
        if ($order_detail->delivery_info_status == 1) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function thank_you_page(Request $request) {
        return view('thank_you_page');
    }

    public function save_delivery_info(Request $request) {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $shop = $request->input('shop_name');
        $order_number = $request->input('order_number');
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name', $shop)->first();

        $shop_id = $shop_find->id;

        $app_settings = AppSettings::where('id', 1)->first();
        $config_settings = block_config::where('shop_id', $shop_id)->first();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find
                    ->access_token]);
        $store_details = $sh->call(['URL' => 'shop.json', 'METHOD' => 'GET']);
        $email_address = $store_details->shop->email;

        $delivery_date = $request->input('delivery_date');
        $delivery_time = $request->input('delivery_time');
        $delivery_array = array();
        $delivery_array['Delivery-Date'] = $delivery_date;
        if (isset($delivery_time)) {
            $delivery_array['Delivery-Time'] = $delivery_time;
        }
        $order_data = [
            "order" => [
                "id" => $order_number,
                "note_attributes" => $delivery_array
            ]
        ];
        $sh->call(['URL' => "/admin/orders/$order_number.json", 'METHOD' => 'PUT', 'DATA' => $order_data]);
        $msg = "Following is the Delivery information which was missed by customer on order confirmation page.<br>" .
                "<b>Order Id:</b>" . $order_number . "<br>" .
                "<b>Delivery Date:</b>" . $delivery_date . "<br>";
        if (isset($delivery_time)) {
            $msg .= "<b>Delivery Time:</b>" . $delivery_time;
        }
        $msg .= "<br>To view delivery details, click on below link.<br><a href='https://$shop/admin/orders/$order_number'>Click Here</a>";
        mail("girish.zestard@gmail.com", "Regarding Order #" . $request->input('order_number'), $msg, $headers);
        //mail("zankar.zestard@gmail.com","Regarding Order #".$request->input('order_number'), $msg ,$headers);
        mail($email_address, "Regarding Order #" . $request->input('order_number'), $msg, $headers);
        $updated_data['delivery_info_status'] = 1;
        $updated_data['delivery_date'] = date("Y-m-d", strtotime(str_replace("/", "-", $delivery_date)));
        if (isset($delivery_time)) {
            $updated_data['delivery_time'] = $delivery_time;
        }
        DB::table('order_details')->where(['shop_id' => $shop_id, 'order_id' => $order_number])->update($updated_data);
    }
    
    /*public function newdash(){
        $shop = session('shop');
		$user_settings = ShopModel::where('store_name' , $shop)->first();						
		$configuration = block_config::where(['shop_id' => $user_settings->id])->first();	
		
		$product_settings = ProductSettings::where('shop' , $shop)->get();		
		$product_settings_array = array();
		foreach($product_settings as $product)
		{
			$product_id = $product->product_id;
			/* 
			$product_settings_array[$product_id] = DB::table('product_settings')->select('product_id', 'blocked_dates', 'blocked_days', 'pre_order_time', 'cut_off_hours', 'cut_off_minutes')->where('product_id', $product_id)->first();	 
			*/
			/*$product_settings_array[$product_id] = ProductSettings::where('product_id', $product_id)->first();	
		}		
		//dd($product_settings_array);	
		return view('test',['product_settings' => json_encode($product_settings_array), 'configuration' => $configuration, 'count' => count($product_settings_array), 'shop' => $shop]);
    }*/
   
}


