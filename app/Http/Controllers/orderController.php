<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\block_config;
use App\field_config;
use Response;

class orderController extends Controller
{    
	var $test_array;
	public function index(Request $request)
	{			
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		
		$shop = session('shop');				
		if(empty($shop))
		{						
			/* 
				print_r($_GET);
				die; 
			*/
			//$shop=$_GET['shop'];			
			/* $shop=$request->input('shop');			
			session(['shop' => $shop]);			 */
			
				if(isset($_GET['shop']))
				{
					$shop=$_GET['shop'];						
				}
				else if(isset($_GET['store']))
				{
					$shop=$_GET['store'];						
				}
				session(['shop' => $shop]); 
			
		}						
		$records_per_page = ShopModel::select('records_per_page')->where('store_name' , $shop)->first();
		$app_settings = DB::table('appsettings')->where('id', 1)->first();
		$shop_model = new ShopModel;      
		$shop_find = DB::table('usersettings')->where('store_name' , $shop)->first();	  
		$shop_id = $shop_find->id;	
		$app_version = $shop_find->app_version;	
		$setting  = [];
		$setting  = block_config::where('shop_id' , $shop_id)->first();    
		$current_date_format  = $setting['date_format'];
		
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' =>$app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find
		->access_token]);
		// For Calculating todays and this weeks orders
		
		//$day = date("w",strtotime("20-12-2017"));
		
		$todays_date = date("d-m-Y");
		$day = date("w",strtotime($todays_date));
		
		$week = 6-$day;			 
		$end_of_week = strtotime("+$week days", strtotime($todays_date));		
		$start_of_week = strtotime("-$day days", strtotime($todays_date));           
		$orders = [];				
		$orders = $sh->call(['URL' => 'orders.json?status=any&limit='.$records_per_page->records_per_page .'&page=1', 'METHOD' => 'GET']);
		
		$all_orders = $orders->orders;	  
		$orders_array=array();
		$todays_orders_array=array();		  
		$todays_orders=0;
		$weeks_orders=0;	
		$useremail="";
		$fullname="";
		foreach ($all_orders as $order)
		{			
			if(!empty($order->note_attributes))
			{					 
				foreach ($order->note_attributes as $atributes)
				{
					if($atributes->name == "Shipping-Date" OR $atributes->name == "date" OR$atributes->name == "Delivery-Date")
					{
						if(strtotime($atributes->value) == '')
						{                                               
							$date_string = (string)$atributes->value;
							$date_final = str_replace("/","-",$date_string);
							
							if($current_date_format == 'dd/mm/yy')
							{	
								$delivery_date=$date_final;
							}						
							if($current_date_format == 'mm/dd/yy')
							{								
								$temp_delivery_date = explode("-",$date_final);
								$delivery_date = $temp_delivery_date[1]."-".$temp_delivery_date[0]."-".$temp_delivery_date[2];
							}
							if($current_date_format == 'yy/mm/dd')
							{								
								$delivery_date  = date("d-m-Y", strtotime($date_final));	
							}														
						}
						else					
						{						              
							$date_final = $atributes->value;
														
							if($current_date_format == 'dd/mm/yy')
							{	
								$delivery_date=str_replace("/","-",$date_final);
							}						
							if($current_date_format == 'mm/dd/yy')
							{								
								$temp_delivery_date = explode("/",$date_final);
								$delivery_date = $temp_delivery_date[1]."-".$temp_delivery_date[0]."-".$temp_delivery_date[2];
							}
							if($current_date_format == 'yy/mm/dd')
							{								
								$delivery_date  = date("d-m-Y", strtotime($date_final));	
							}
						}
						if(strtotime($delivery_date) == strtotime(date('d-m-Y')))	
						{				
							++$todays_orders;
							/* array_push($orders_array,array("#".$order->order_number,$fullname,$useremail,$product_name->title,$delivery_date));  */
							
						}
						if(strtotime($delivery_date) >= $start_of_week && strtotime($delivery_date) <= $end_of_week)	
						{							
							$weeks_orders++;						
						}													
						foreach ($order->line_items as $product_name) 
						{
																						
						}
					}						   
				}
			}
		}			
		//Api call for Order
		$orders = [];
		$count = $sh->call(['URL' => '/admin/orders/count.json?status=any','METHOD' => 'GET']);
		$orders_count = (array)$count;
		$order_count = $orders_count['count'];
				
		$orders = $sh->call(['URL' => 'orders.json?status=any&limit='.$records_per_page->records_per_page .'&page=1', 'METHOD' => 'GET']);
		$fiels_record = field_config::where('shop_id' , $shop_id)->first();	  
		if(count($fiels_record)>0)
		{
			$field_data = json_decode($fiels_record->fields);
		}
		else
		{
			$field_data = ["0","16"];
		}    
		if($current_date_format == 'dd/mm/yy')
		{	
			$start_of_week=date("d/m/Y",$start_of_week);
			$end_of_week=date("d/m/Y",$end_of_week);
		}						
		if($current_date_format == 'mm/dd/yy')
		{								
			$start_of_week = date("d/m/Y", $start_of_week);			
			$end_of_week = date("d/m/Y", $end_of_week);			
		}
		if($current_date_format == 'yy/mm/dd')
		{								
			$start_of_week  = date("Y/m/d", $start_of_week);	
			$end_of_week  = date("Y/m/d", $end_of_week);	
		}						
		
		return view('order', ['orderlist'=> $orders,'shop_id'=>$shop_id, 'shop' => $shop, 'app_version' => $app_version, 'settings'=>$setting,'fields'=>$field_data,'active'=>'order','order_count' => $order_count,'page_number' => 1,'records_per_page' => $records_per_page->records_per_page, 'weeks_orders' => $weeks_orders, 'todays_orders' => $todays_orders,'todays_date' => $todays_date, 'start_of_week' => $start_of_week, 'end_of_week' => $end_of_week]);
	}
	
	public function orders(Request $request)
    {	
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		$shop = session('shop');
		if(empty($shop))
		{
			/* $shop=$_GET['shop'];
			session(['shop' => $shop]); */			
			if(isset($_GET['shop']))
			{
				$shop=$_GET['shop'];						
			}
			else if(isset($_GET['store']))
			{
				$shop=$_GET['store'];						
			}
			session(['shop' => $shop]); 			
		}
		if($request->input('records_length'))
		{
			DB::table('usersettings')->where('store_name',$shop)->update(['records_per_page' =>$request->input('records_length')]);
		}
		$app_settings = DB::table('appsettings')->where('id', 1)->first();
		$shop_model = new ShopModel;      
		$shop_find = DB::table('usersettings')->where('store_name' , $shop)->first();	  
		$shop_id = $shop_find->id;
		$app_version = $shop_find->app_version;
		$setting = [];
		$setting = block_config::where('shop_id' , $shop_id)->first();    
		$current_date_format = $setting['date_format'];
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);		
		$records_per_page = ShopModel::select('records_per_page')->where('store_name' , $shop)->first();			
		if($request->input('page'))
		{
			$page=$request->input('page');		
		}
		else
		{
			$page=1;	
		}
		// For Calculating todays and this weeks orders
		$todays_date = date("d-m-Y");
		$day = date("w",strtotime($todays_date));						
		$week = 6-$day;	
		$temp_date = date("d-m-Y",strtotime("20-12-2017"));			
		$start_of_week = strtotime("-$day days", strtotime($todays_date));           
		$end_of_week = strtotime("+$week days", strtotime($todays_date));		
		
		$orders = [];				
		$orders = $sh->call(['URL' => 'orders.json?status=any&limit='.$records_per_page->records_per_page .'&page='.$page, 'METHOD' => 'GET']);
		
		$all_orders = $orders->orders;	  
		$orders_array=array();
		$todays_orders_array=array();		  
		$todays_orders=0;
		$weeks_orders=0;	
		$useremail="";
		$fullname="";
		foreach ($all_orders as $order)
		{			
			if(!empty($order->note_attributes))
			{					 
				foreach ($order->note_attributes as $atributes)
				{
					if($atributes->name == "Shipping-Date" OR $atributes->name == "date" OR$atributes->name == "Delivery-Date")
					{
						if(strtotime($atributes->value) == '')
						{                                               
							$date_string = (string)$atributes->value;
							$date_final = str_replace("/","-",$date_string);
							
							if($current_date_format == 'dd/mm/yy')
							{	
								$delivery_date=$date_final;
							}						
							if($current_date_format == 'mm/dd/yy')
							{								
								$temp_delivery_date = explode("-",$date_final);
								$delivery_date = $temp_delivery_date[1]."-".$temp_delivery_date[0]."-".$temp_delivery_date[2];
							}
							if($current_date_format == 'yy/mm/dd')
							{								
								$delivery_date  = date("d-m-Y", strtotime($date_final));	
							}														
						}
						else					
						{						              
							$date_final = $atributes->value;
														
							if($current_date_format == 'dd/mm/yy')
							{	
								$delivery_date=str_replace("/","-",$date_final);
							}						
							if($current_date_format == 'mm/dd/yy')
							{								
								$temp_delivery_date = explode("/",$date_final);
								$delivery_date = $temp_delivery_date[1]."-".$temp_delivery_date[0]."-".$temp_delivery_date[2];
							}
							if($current_date_format == 'yy/mm/dd')
							{								
								$delivery_date  = date("d-m-Y", strtotime($date_final));	
							}
						}
						if(strtotime($delivery_date) == strtotime(date('d-m-Y')))	
						{				
							++$todays_orders;						
						}
						if(strtotime($delivery_date) >= $start_of_week && strtotime($delivery_date) <= $end_of_week)	
						{
							$weeks_orders++;
							//array_push($todays_orders_array,array("#".$order->order_number,$fullname,$useremail,$product_name->title,$delivery_date));								
						}				
						foreach ($order->line_items as $product_name)
						{
						}
					}						   
				}
			}
		}				
		//Api call for Order
		$orders = [];
				
		$count = $sh->call(['URL' => '/admin/orders/count.json?status=any','METHOD' => 'GET']);
		$orders_count = (array)$count;
		$order_count = $orders_count['count'];  
						
		$orders = $sh->call(['URL' => 'orders.json?status=any&limit='.$records_per_page->records_per_page .'&page='.$page, 'METHOD' => 'GET']);
		$fiels_record = field_config::where('shop_id' , $shop_id)->first();	  
		if(count($fiels_record)>0)
		{
			$field_data = json_decode($fiels_record->fields);
		}
		else
		{
			$field_data = ["0","16"];
		}      	
		if($current_date_format == 'dd/mm/yy')
		{	
			$start_of_week=date("d/m/Y",$start_of_week);
			$end_of_week=date("d/m/Y",$end_of_week);
		}						
		if($current_date_format == 'mm/dd/yy')
		{								
			$start_of_week = date("d/m/Y", $start_of_week);
			
			$end_of_week = date("d/m/Y", $end_of_week);
			
		}
		if($current_date_format == 'yy/mm/dd')
		{								
			$start_of_week  = date("Y/m/d", $start_of_week);	
			$end_of_week  = date("Y/m/d", $end_of_week);	
		}				
		return view('order', ['orderlist'=> $orders,'shop_id'=>$shop_id, 'shop' => $shop, 'app_version' => $app_version, 'settings'=>$setting,'fields'=>$field_data,'active'=>'order','order_count' => $order_count,'page_number' => $page,'records_per_page' => $records_per_page->records_per_page, 'weeks_orders' => $weeks_orders, 'todays_orders' => $todays_orders,'todays_date' => $todays_date, 'start_of_week' => $start_of_week, 'end_of_week' => $end_of_week]);
    }		
	
	public function order_export(Request $request)
	{					
		$shop = session('shop');
		if(empty($shop))
		{
			if(isset($_GET['shop']))
			{
				$shop=$_GET['shop'];	
			}
			else
			{
				$shop=$request->input('shop');
			}
			session(['shop' => $shop]);
		}	
		$app_settings = DB::table('appsettings')->where('id', 1)->first();
		$shop_model = new ShopModel;
		$shop_find = ShopModel::where('store_name' , $shop)->first();
		$shop_id = $shop_find->id;
		$settings = [];
		$settings = block_config::where('shop_id' , $shop_id)->first();    
		
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);            
		$orders = [];		
		$orders = $sh->call(['URL' => 'orders.json?status=any&limit=250', 'METHOD' => 'GET']);
		$all_orders = $orders->orders;	  
		$orders_array=array();
		$delivery_time_enabled = $settings->admin_time_status;			
		$timestamp_flag = 0;
				
		$ii=0;	
		$start_date=$request->input('min');
		$end_date=$request->input('max');	
		if(empty($start_date) || empty($end_date))	
		{
			$filename = "Orders.csv";
		}
		else
		{
			$filename = "Orders_from_".$start_date."_to_".$end_date.".csv ";
		}
		$old_order_no=0;	
		$new_order_no=0;
		$useremail="";
		$fullname="";
		if($shop=="the-bunched-co.myshopify.com")
		{
			if($delivery_time_enabled == 1)
			{
				$orders_header=array("Order Number","Name","Email","Product Name(Quantity)","Delivery Date","Delivery Time", "Delivery Address");  
			}		
			else
			{
				$orders_header=array("Order Number","Name","Email","Product Name(Quantity)","Delivery Date", "Delivery Address");  
			}
			foreach ($all_orders as $order)
			{
				$time_attribute = "";
				$product_delivery_date_array = array();
				$temp_product_delivery_date_array = array();
				$product_delivery_time_array = array();
				$product_details_array = array();	
				$address = empty($order->billing_address) ? '' : ($order->billing_address->address1 . ' ' . $order->billing_address->address2 . ' ' . $order->billing_address->city . ' ' . $order->billing_address->zip . ' ' . $order->billing_address->province . ' ' . $order->billing_address->country );		
				if(empty($order->customer))
				{
					$fullname="";
					$useremail="";
				}
				else
				{
					$fullname=$order->customer->first_name ." ". $order->customer->last_name;
					$useremail=$order->email;
				}	
				foreach($order->line_items as $product_name) 
				{											
					$product_delivery_date = " ";			
					$product_delivery_time = " ";					
					foreach($product_name->properties as $property)
					{
						if($property->name == "Delivery-Date")
						{								
							$product_delivery_date = ", ".$property->value;
							array_push($product_delivery_date_array, strtotime(str_replace("/","-",$property->value)));
							array_push($temp_product_delivery_date_array, str_replace("/","-",$property->value));
						}							
						if($property->name == "Delivery-Time")
						{
							array_push($product_delivery_time_array, ($property->value));
							$product_delivery_time = ", ".$property->value;
							
						}								
					}
					if($product_delivery_time == " ")
					{
						array_push($product_delivery_time_array, "-");
					}
					$product_details = $product_name->title."(".$product_name->quantity."".$product_delivery_date."".$product_delivery_time.")";
					array_push($product_details_array, $product_details);
				}
				if($request->input('repeat'))
				{
					//Do Nothing
				}
				else
				{
					$new_order_no=$order->order_number;
					if($new_order_no == $old_order_no)
					{						
						$fullname="";
						$useremail="";
					}
				}		
				$old_order_no=$new_order_no;																	
				if($start_date=="" || $end_date=="")
				{
					if($delivery_time_enabled == 1)
					{								
						array_push($orders_array,array("#".$order->order_number,$fullname,$useremail, implode("\n", $product_details_array), implode("\n", $temp_product_delivery_date_array), implode("\n", $product_delivery_time_array), $address));								
					}
					else
					{						
						array_push($orders_array,array("#".$order->order_number,$fullname, $useremail, implode("\n", $product_details_array), implode("\n", $temp_product_delivery_date_array), $address));						
					}
				}
				else
				{								
					if($request->input('current_format') == 'd/m/Y')
					{	
						$start_date=str_replace("/","-",$start_date);
						$end_date=str_replace("/","-",$end_date);
					}						
					if($request->input('current_format') == 'm/d/Y')
					{
						$temp_start_date = explode("/",$start_date);
						$temp_end_date = explode("/",$end_date);
						$start_date = $temp_start_date[1]."-".$temp_start_date[0]."-".$temp_start_date[2];
						$end_date = $temp_end_date[1]."-".$temp_end_date[0]."-".$temp_end_date[2];
					}
					if($request->input('current_format') == 'Y/m/d')
					{
						$start_date = date("d-m-Y", strtotime($start_date));
						$end_date   = date("d-m-Y", strtotime($end_date));	
					}												
					if(count($product_delivery_date_array) > 0)
					{
						foreach($product_delivery_date_array as $timestamp)
						{
							if(!empty($timestamp))
							{
								if($timestamp >= strtotime($start_date) && $timestamp <= strtotime($end_date))
								{
									$timestamp_flag = 1;
								}								
							}
						}					
					}										
					if($timestamp_flag == 1)
					{										
						if($delivery_time_enabled == 1)
						{								
							$product_details = $product_name->title."(".$product_name->quantity.",".$product_delivery_date.", ".$product_delivery_time.")";						
							array_push($orders_array,array("#".$order->order_number,$fullname, $useremail, implode("\n", $product_details_array),implode("\n", $temp_product_delivery_date_array), implode("\n", $product_delivery_time_array), $address));			
						}
						else
						{							
							$product_details = $product_name->title."(".$product_name->quantity.",".$product_delivery_date.")";				
							array_push($orders_array,array("#".$order->order_number,$fullname, $useremail, implode("\n", $product_details_array), implode("\n", $temp_product_delivery_date_array), $address));
						}												
						$timestamp_flag = 0;
					}					
				}		
			}
		}
		else
		{
			if($delivery_time_enabled == 1)
			{
				$orders_header=array("Order Number","Name","Email","Product Name(Quantity)","Delivery Date","Delivery Time");  
			}		
			else
			{
				$orders_header=array("Order Number","Name","Email","Product Name(Quantity)","Delivery Date");  
			}
			foreach ($all_orders as $order)
			{
				$time_attribute = "";
				if(!empty($order->note_attributes))
				{				
					foreach ($order->note_attributes as $atributes)
					{
						
					
						if($atributes->name == "Delivery-Date")
						{
							
							if(strtotime($atributes->value) === '')
							{									
								$date_string = (string)$atributes->value;
								$date_final = str_replace("/","-",$date_string);										
								if($request->input('current_format') == 'd/m/Y')
								{	
									$delivery_date=$date_final;
								}						
								if($request->input('current_format') == 'm/d/Y')
								{				
												
									$temp_delivery_date = explode("-",$date_final);
									$delivery_date = $temp_delivery_date[1]."-".$temp_delivery_date[0]."-".$temp_delivery_date[2];
								}
								if($request->input('current_format') == 'Y/m/d')
								{								
									$delivery_date  = date("d-m-Y", strtotime($date_final));	
								}														
							}
							else					
							{														
								$date_final = $atributes->value;																						
								if($request->input('current_format') == 'd/m/Y')
								{	
									$delivery_date=str_replace("/","-",$date_final);
								}						
								if($request->input('current_format') == 'm/d/Y')
								{	
									if($shop == 'mamasezz.myshopify.com'){
										$delivery_date  = date("d-m-Y", strtotime($date_final));										
									} else {
										$temp_delivery_date = explode("/",$date_final);																		
										$delivery_date = $temp_delivery_date[1].'-'.$temp_delivery_date[0].'-'.$temp_delivery_date[2];										
									}															
								}
								if($request->input('current_format') == 'Y/m/d')
								{								
									$delivery_date  = date("d-m-Y", strtotime($date_final));	
								}
							}
						}
						if($atributes->name == "Delivery-Time")
						{
							$time_attribute = $atributes->value;
						}	
						if($time_attribute != "" || !(empty($time_attribute)))
						{
							$delivery_time = $time_attribute;
						}
						else
						{
							$delivery_time = "-";
						}	
					}
					if(empty($order->customer))
					{
						$fullname="";
						$useremail="";
					}
					else
					{
						$fullname=$order->customer->first_name ." ". $order->customer->last_name;
						$useremail=$order->email;
					}	
					foreach($order->line_items as $product_name) 
					{											
						if($request->input('repeat'))
						{
							//Do Nothing
						}
						else
						{
							$new_order_no=$order->order_number;
							if($new_order_no==$old_order_no)
							{						
								$fullname="";
								$useremail="";
							}
						}
													
					if($start_date=="" || $end_date=="")
					{
						if($delivery_time_enabled == 1)
						{		
							if($shop=="the-bunched-co.myshopify.com")
							{
								$product_details = $product_name->title."(".$product_name->quantity.", ".$product_delivery_date.", ".$product_delivery_time.")";
								$product_delivery_date = "-";			
								$product_delivery_time = "-";					
								foreach($product_name->properties as $property)
								{
									if($property->name == "Delivery-Date")
									{
										$product_delivery_date = $property->value;
									}							
									if($property->name == "Delivery-Time")
									{
										$product_delivery_time = $property->value;
									}							
								}
								array_push($orders_array,array("#".$order->order_number,$fullname,$useremail, $product_details,"-", "-"));		
							}
							else
							{	
								$product_details = $product_name->title."(".$product_name->quantity.")";
								array_push($orders_array,array("#".$order->order_number,$fullname, $useremail, $product_details, $delivery_date, $delivery_time));
							}
						}
						else
						{
							if($shop=="the-bunched-co.myshopify.com")
							{
								$product_details = $product_name->title."(".$product_name->quantity.", ".$product_delivery_date.")";
								$product_delivery_date = "-";			
								$product_delivery_time = "-";					
								foreach($product_name->properties as $property)
								{
									if($property->name == "Delivery-Date")
									{
										$product_delivery_date = $property->value;
									}							
									if($property->name == "Delivery-Time")
									{
										$product_delivery_time = $property->value;
									}							
								}
								array_push($orders_array,array("#".$order->order_number,$fullname, $useremail, $product_details, "-"));
							}
							else
							{				
								$product_details = $product_name->title."(".$product_name->quantity.")";
								array_push($orders_array,array("#".$order->order_number,$fullname, $useremail, $product_details, $delivery_date));
							}
						}
					}
					else
					{								
						if($request->input('current_format') == 'd/m/Y')
						{	
							$start_date=str_replace("/","-",$start_date);
							$end_date=str_replace("/","-",$end_date);
						}						
						if($request->input('current_format') == 'm/d/Y')
						{
							$temp_start_date = explode("/",$start_date);
							$temp_end_date = explode("/",$end_date);
							$start_date = $temp_start_date[1]."-".$temp_start_date[0]."-".$temp_start_date[2];
							$end_date = $temp_end_date[1]."-".$temp_end_date[0]."-".$temp_end_date[2];
						}
						if($request->input('current_format') == 'Y/m/d')
						{
							$start_date = date("d-m-Y", strtotime($start_date));
							$end_date   = date("d-m-Y", strtotime($end_date));	
						}																	$product_delivery_date = "-";			
						$product_delivery_time = "-";					
						foreach($product_name->properties as $property)
						{
							if($property->name == "Delivery-Date")
							{
								$product_delivery_date = $property->value;
							}							
							if($property->name == "Delivery-Time")
							{
								$product_delivery_time = $property->value;
							}							
						}				
						if((strtotime($delivery_date) >= strtotime($start_date)) && (strtotime($delivery_date) <= strtotime($end_date)))	
						{										
							if($delivery_time_enabled == 1)
							{	
								if($shop=="the-bunched-co.myshopify.com")
								{								
									array_push($orders_array,array("#".$order->order_number,$fullname,$useremail,$product_name->title."(".$product_name->quantity.", ".$product_delivery_date.", ".$product_delivery_time.")","-", "-"));						
								}
								else
								{												
									array_push($orders_array,array("#".$order->order_number,$fullname,$useremail,$product_name->title."(".$product_name->quantity.")",$delivery_date, $delivery_time));
								}						
							}
							else
							{
								if($shop=="the-bunched-co.myshopify.com")
								{								
									array_push($orders_array,array("#".$order->order_number,$fullname,$useremail,$product_name->title."(".$product_name->quantity.", ".$product_delivery_date.")", "-"));			
								}
								else
								{
									array_push($orders_array,array("#".$order->order_number,$fullname,$useremail,$product_name->title."(".$product_name->quantity.")",$delivery_date));
								}
							}												
						}
					}
					$old_order_no=$new_order_no;
				}			
			}
			else
			{												
				if($start_date=="" || $end_date=="")
				{
					if(empty($order->customer))
					{
						$fullname="";
						$useremail="";
					}
					else
					{
						$fullname=$order->customer->first_name ." ". $order->customer->last_name;
						$useremail=$order->email;
					}	
					foreach ($order->line_items as $product_name) 
					{		
						if($request->input('repeat'))
						{
							//Do Nothing
						}
						else
						{
							$new_order_no=$order->order_number;
							if($new_order_no==$old_order_no)
							{						
								$fullname="";
								$useremail="";
							}
						}							
						if($shop=="the-bunched-co.myshopify.com")
						{
							$product_delivery_date = "-";			
							$product_delivery_time = "-";					
							foreach($product_name->properties as $property)
							{
								if($property->name == "Delivery-Date")
								{
									$product_delivery_date = $property->value;
								}							
								if($property->name == "Delivery-Time")
								{
									$product_delivery_time = $property->value;
								}							
							}
							array_push($orders_array,array("#".$order->order_number,$fullname,$useremail,$product_name->title."(".$product_name->quantity.", ".$product_delivery_date.", ".$product_delivery_time.")","-", "-"));						
						}
						else
						{
							array_push($orders_array,array("#".$order->order_number,$fullname,$useremail,$product_name->title."(".$product_name->quantity.")","-", "-"));
						}
						$old_order_no=$new_order_no;	
					}					
				}			
			}
		}	
	}		
	$callback = function() use ($orders_array, $orders_header)
	{
		$fp = fopen('php://output', 'w');				
		fputcsv($fp, $orders_header);
		foreach($orders_array as $order_row)
		{
			fputcsv($fp,$order_row);
		}
		fclose($fp);	
	};	
	$headers = array(
		"Content-type" => "text/csv",
		"Content-Disposition" => "attachment; filename=$filename",
		"Pragma" => "no-cache",
		"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
		"Expires" => "0"
	);		
	return Response::stream($callback, 200, $headers);
	}
	 
}
