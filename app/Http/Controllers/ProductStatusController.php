<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RocketCode\Shopify\API;
use App;
use DB;
use App\ProductSettings;
use App\ShopModel;

class ProductStatusController extends Controller
{
    public function get_product_status(Request $request)
    {
        $product_id = $request['product'];
        $shop = $request['store'];
        $user_settings = ShopModel::where('store_name',$shop)->first();
        $shop_id = $user_settings->id;
        $check_status = ProductSettings::where('shop_id', $shop_id)->where('product_id', $product_id)->first();
        
        $current_status = 0;
        if(count($check_status) > 0){
            if($check_status->status == 1){
                $current_status = 1;
            }
        }
        return $current_status;
    }
}
