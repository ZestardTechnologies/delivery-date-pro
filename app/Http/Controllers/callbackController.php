<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\block_f;
use App\app_config;
use App\block_config;
use Mail;
use App\AppSettings;
use App\TrialInfo;
use App\DevelopmentStore; 

class callbackController extends Controller {

    public function index(Request $request) {

        $sh = App::make('ShopifyAPI');

        //getting record of the api key and secreat key
        $app_settings = AppSettings::where('id', 1)->first();

        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];
            $select_store = ShopModel::where('store_name', $shop)->first();

            //check if app is already installed in store or not
            if (count($select_store) > 0) {
                session(['shop' => $shop]);
                //Remove coment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
                $id = $select_store->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store->charge_id;
                $charge_status = $select_store->status;

                //check that app is active or not
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    //check the version of app selected or not
                    if ($select_store->app_version > 0 && $select_store->type_of_app > 0) {
                        session(['shop' => $shop]);
                        return redirect()->route('dashboard', ['shop' => $shop]);
                    } else {
                        return redirect()->route('plans');
                    }
                } else {//if app is not active then redirect user to the payment process 
                    return redirect()->route('payment_process');
                }
            } else {
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
                //setting the scope for the app
                if ($shop == "new-delivery-date-pro-demo.myshopify.com" || $shop == "dipal-test.myshopify.com" || $shop == "zankar-test.myshopify.com" || $shop == "product-delivery-date-demo.myshopify.com") {
                    $permission_url = $sh->installURL(['permissions' => array('read_orders','read_all_orders', 'write_orders', 'read_products', 'write_products', 'write_themes', 'read_themes', 'read_script_tags', 'write_script_tags', 'read_draft_orders', 'write_draft_orders'), 'redirect' => $app_settings->redirect_url]);
                } else {
                    $permission_url = $sh->installURL(['permissions' => array('read_orders','read_all_orders', 'write_orders', 'read_products', 'write_products', 'write_themes', 'read_themes', 'read_script_tags', 'write_script_tags', 'read_draft_orders', 'write_draft_orders'), 'redirect' => $app_settings->redirect_url]);                    
                }
                return redirect($permission_url);
            }
        }
    }

    public function redirect(Request $request) {
        //$app_settings = DB::table('appsettings')->where('id', 1)->first();
        $app_settings = AppSettings::where('id', 1)->first();
        if (!empty($request->input('shop')) && !empty($request->input('code'))) {
            $shop = $request->input('shop'); //shop name            
            //Default Trial Days
            $new_trial_days = 3;
            //Check if trial is still running
            $check_trial = TrialInfo::where('store_name', $shop)->first();

            if (count($check_trial) > 0) {
                $total_trial_days = $check_trial->trial_days;
                $trial_activated_date = $check_trial->activated_on;
                $trial_over_date = $check_trial->trial_ends_on;
                $current_date = date("Y-m-d");
                //$current_date = "2019-01-06";

                if (strtotime($current_date) < strtotime($trial_over_date)) {
                    $date1 = date_create($trial_over_date);
                    $date2 = date_create($current_date);
                    $trial_remain = date_diff($date2, $date1);
                    $new_trial_days = $trial_remain->format("%a");
                } else {
                    $new_trial_days = 0;
                }
            }
            $select_store = ShopModel::where('store_name', $shop)->first();


            if (count($select_store) > 0) {
                //Remove comment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
                $id = $select_store->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store->charge_id;
                $charge_status = $select_store->status;

                //prevent the multiple store entry 
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard', ['shop' => $shop]);
                } else {
                    return redirect()->route('payment_process');
                }
            }
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
            try {
                //check that request is Generated from shopify or not
                $verify = $sh->verifyRequest($request->all());
                if ($verify) {
                    $code = $request->input('code');
                    $accessToken = $sh->getAccessToken($code);
                    ShopModel::insert(['access_token' => $accessToken, 'store_name' => $shop]);

                    $shop_find = ShopModel::where('store_name', $shop)->first();
                    $shop_id = $shop_find->id;
                    $shop_encrypt = crypt($shop_id, "ze");
                    $finaly_encrypt = str_replace(['/', '.'], "Z", $shop_encrypt);
                    ShopModel::where('id', $shop_id)->update(['store_encrypt' => $finaly_encrypt]);
                    $shop_encrypted = ShopModel::where('store_name', $shop)->first();

                    //inserting the default configuration
                    block_config::insert(['block_date' => null, 'alloved_month' => 6, 'date_interval' => 1, 'days' => null, 'hours' => 0, 'minute' => 0, 'shop_id' => $shop_id, 'app_title' => 'Delivery Date', 'date_error_message' => 'Please Enter Delivery Date', 'date_format' => 'yy/mm/dd', 'app_status' => 'Active', 'show_datepicker_label' => 1, 'datepicker_label' => 'Pick a delivery date:', 'add_delivery_information' => 0, 'language_locale' => 'en']);
//                    DB::table('app_config')->insert(['date_format' => 'yy/mm/dd', 'shop_id' => $shop_id]);
                    session(['shop' => $shop]);
                    $sh->setup(['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $accessToken]);

                    //for creating the uninstall webhook
                    $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json'; {
                        $webhookData = [
                            'webhook' => [
                                'topic' => 'app/uninstalled',
                                'address' => config('app.url') . 'uninstall.php',
                                'format' => 'json'
                            ]
                        ];
                    }
                    $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);
                    {
						//webhook for checkout                    
                        $webhookData = [
                            'webhook' => [
                                'topic' => 'orders/create',
                                'address' => config('app.url') . 'checkout.php',
                                'format' => 'json'
                            ]
                        ];
                    }
                    $checkout = $sh->appUninstallHook($accessToken, $url, $webhookData);

                    //api call for get theme info
                    //Commented by Anuj Dalal
                    $main_theme_flag = false;
                    $theme = $sh->call(['URL' => '/admin/themes/', 'METHOD' => 'GET']); //'DATA' => ['role' => 'main']     
                    foreach ($theme->themes as $themeData) {
                        if ($themeData->role == 'main') {
                            $theme_id = $themeData->id;
                            $content = View('snippets')->render();
                            $view = (string) $content;
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/delivery-date.liquid', 'value' => $view]]]);
                        }
                    }
                    //creating the Recuring charge for app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    //if(stristr($shop, "zestard") && 1==2)
                    if ($shop == "dipal-test.myshopify.com" || $shop == "product-delivery-date-demo.myshopify.com" || $shop == "zankar-test.myshopify.com" || $shop == "davidaustinroses-dev.myshopify.com") {
                        $charge = $sh->call([
                            'URL' => $url,
                            'METHOD' => 'POST',
                            'DATA' => array(
                                'recurring_application_charge' => array(
                                    'name' => 'Delivery Date Pro',
                                    'price' => 0.01,
                                    'return_url' => url('payment_success'),
                                    "capped_amount" => 30,
                                    "terms" => "Additional charges as per the plan",
                                    'test' => true,
                                    'trial_days' => $new_trial_days,
                                )
                            )
                                ], false);
                    } else {
                        $charge = $sh->call([
                            'URL' => $url,
                            'METHOD' => 'POST',
                            'DATA' => array(
                                'recurring_application_charge' => array(
                                    'name' => 'Delivery Date Pro',
                                    'price' => 7.99,
                                    'return_url' => url('payment_success'),
                                    "capped_amount" => 30,
                                    "terms" => "Additional charges as per the plan",
                                    //'test' => true,
                                    'trial_days' => $new_trial_days,
                                )
                            )
                                ], false);
                    }
                    $create_charge = ShopModel::where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                    //redirecting to the Shopify payment page
                    $shopi_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    //for the installation follow up mail for cliant
                    $subject = "Zestard Installation Greetings :: Delivery Date Pro";
                    $sender = "support@zestard.com";
                    $sender_name = "Zestard Technologies";
                    $app_name = "Delivery Date Pro";
                    $logo = config('app.url') . 'public/image/zestard-logo.png';
                    $installation_follow_up_msg = '<html>

                    <head>
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <style>
                            @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i");
                            @media only screen and (max-width:599px) {
                                table {
                                    width: 100% !important;
                                }
                            }
                            
                            @media only screen and (max-width:412px) {
                                h2 {
                                    font-size: 20px;
                                }
                                p {
                                    font-size: 13px;
                                }
                                .easy-donation-icon img {
                                    width: 120px;
                                }
                            }
                        </style>
                    
                    </head>
                    
                    <body style="background: #f4f4f4; padding-top: 57px; padding-bottom: 57px;">
                        <table class="main" border="0" cellspacing="0" cellpadding="0" width="600px" align="center" style="border: 1px solid #e6e6e6; background:#fff; ">
                            <tbody>
                                <tr>
                                    <td style="padding: 30px 30px 10px 30px;" class="review-content">
                                        <p class="text-align:left;"><img src="' . $logo . '" alt=""></p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px; line-height: 25px; margin-top: 0px;"><b>Hi ' . $shopi_info->shop->shop_owner . '</b>,</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Thanks for Installing Zestard Application ' . $app_name . '</p>
                                        <p style="font-family: \'Helvetica\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We appreciate your kin interest for choosing our application and hope that you have a wonderful experience.</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Please don\'t feel hesitate to reach us in case of any queries or questions at <a href="mailto:support@zestard.com" style="text-decoration: none;color: #1f98ea;font-weight: 600;">support@zestard.com</a>.</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We also do have live chat support services for quick response and resolution of queries.</p>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">(Please Note: Support services are available according to the IST Time Zone(i.e GMT 5:30+) as we reside in India. Timings are from 10:00am to 7:00pm)</p>
                    
                                    </td>
                                </tr>
                    
                                <tr>
                                    <td style="padding: 20px 30px 30px 30px;">
                    
                                        <br>
                                        <p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 26px; margin-bottom:0px;">Thanks,<br>Zestard Support</p>
                                    </td>
                                </tr>
                    
                            </tbody>
                        </table>
                    </body>';
                    $receiver = $shopi_info->shop->email;
                    try {
//                        Mail::raw([], function ($message) use($sender, $sender_name, $receiver, $subject, $installation_follow_up_msg) {
//                            $message->from($sender, $sender_name);
//                            $message->to($receiver)->subject($subject);
//                            $message->setBody($installation_follow_up_msg, 'text/html');
//                        });
                    } catch (\Exception $ex) {
                        echo "error";
                    }
                    $msg = '<table>
                            <tr>
                                <th>Shop Name</th>
                                <td>' . $shopi_info->shop->name . '</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>' . $shopi_info->shop->email . '</td>
                            </tr>
                            <tr>
                                <th>Domain</th>
                                <td>' . $shopi_info->shop->domain . '</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>' . $shopi_info->shop->phone . '</td>
                            </tr>
                            <tr>
                                <th>Shop Owner</th>
                                <td>' . $shopi_info->shop->shop_owner . '</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>' . $shopi_info->shop->country_name . '</td>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <td>' . $shopi_info->shop->plan_name . '</td>
                            </tr>
                          </table>';
                    $dev_store = DevelopmentStore::where('shop_id', $shop_id)->count();
                    if ($dev_store <= 0) {
                        mail("support@zestard.com", "Delivery Date Pro App Installed", $msg, $headers);                        
                    } else {
                        mail("support@zestard.com", "Delivery Date Pro App Installed", $msg, $headers);                        
                    }
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                }
            } catch (Exception $e) {
                echo '<pre>Error: ' . $e->getMessage() . '</pre>';
            }
        }
    }

    public function dashboard() {
        $shop = session('shop');
        $app_settings = AppSettings::where('id', 1)->first();
        $shop_find = ShopModel::where('store_name', $shop)->first();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

        return view('dashboard');
    }

    public function payment_method(Request $request) {
        $shop = session('shop');
        $app_settings = AppSettings::where('id', 1)->first();
        $select_store = ShopModel::where('store_name', $shop)->first();

        if (count($select_store) > 0) {
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);

            $charge_id = $select_store->charge_id;
            $url = 'admin/recurring_application_charges/' . $charge_id . '.json';
            $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
            if (count($charge) > 0) {
                if ($charge->recurring_application_charge->status == "pending") {
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "declined" || $charge->recurring_application_charge->status == "expired") {
                    //creating the new Recuring charge after declined app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'recurring_application_charge' => array(
                                'name' => 'Delivery Date Pro',
                                'price' => 7.99,
                                'return_url' => url('payment_success'),
                                "capped_amount" => 30,
                                "terms" => "Additional charges as per the plan",
                            //'test' => true
                            )
                        )
                            ], false);
                    //updating the charge info in the database
                    $create_charge = ShopModel::where('store_name', $shop)->update(['charge_id' => (string) $charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "accepted") {//if user accepted the charge
                    $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
                    $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
                    $Activatecharge_array = get_object_vars($Activate_charge);
                    $active_status = $Activatecharge_array['recurring_application_charge']->status;
                    $update_charge_status = ShopModel::where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);

                    //check that user selected any plan or not
                    if ($select_store->app_version > 0 && $select_store->type_of_app > 0) {
                        session(['shop' => $shop]);
                        return redirect()->route('dashboard', ['shop' => $shop]);
                    } else {//Redirect user on the plan page where user can select the relevant plan
                        return redirect()->route('plans');
                    }
                }
            }
        }
    }

    public function payment_compelete(Request $request) {
        $app_settings = AppSettings::where('id', 1)->first();
        $shop = session('shop');
        $select_store = ShopModel::where('store_name', $shop)->first();
        $shop_id = $select_store->id;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store->access_token]);
        $charge_id = $_GET['charge_id'];
        $url = 'admin/recurring_application_charges/#{' . $charge_id . '}.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET',]);
        $status = $charge->recurring_application_charges[0]->status;

        $update_charge_status = ShopModel::where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $status]);

        if ($status == "accepted") {
            $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
            $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
            $Activatecharge_array = get_object_vars($Activate_charge);


            $active_status = $Activatecharge_array['recurring_application_charge']->status;
            $trial_start = $Activatecharge_array['recurring_application_charge']->activated_on;
            $trial_end = $Activatecharge_array['recurring_application_charge']->trial_ends_on;
            $trial_days = $Activatecharge_array['recurring_application_charge']->trial_days;
            $update_charge_status = ShopModel::where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status, 'activated_on' => $trial_start, 'trial_ends_on' => $trial_end]);
            //return redirect()->route('dashboard');
            //check if any trial info is exists or not
            if ($trial_days > 0) {
                $check_trial = TrialInfo::where('store_name', $shop)->first();
                if (count($check_trial) > 0) {
                    TrialInfo::where('store_name', $shop)->update(['shop_id' => $shop_id, 'trial_days' => $trial_days, 'activated_on' => $trial_start, 'trial_ends_on' => $trial_end]);
                } else {
                    TrialInfo::insert([
                        'store_name' => $shop,
                        'shop_id' => $shop_id,
                        'trial_days' => $trial_days,
                        'activated_on' => $trial_start,
                        'trial_ends_on' => $trial_end
                    ]);
                }
            }

            //check that user selected any plan or not
            if ($select_store->app_version > 0 && $select_store->type_of_app > 0) {
                session(['shop' => $shop]);
                return redirect()->route('dashboard', ['shop' => $shop]);
            } else {
                return redirect()->route('plans');
            }
        } elseif ($status == "declined") {//Redirect user to the confirmation page
            echo '<script>window.top.location.href="https://' . $shop . '/admin/apps"</script>';
        }
    }

    public function help(Request $request) {//for the help page
        $shop = session('shop');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $setting = app_config::where('shop_id', $shop_find->id)->first();
        $app_version = $shop_find->app_version;
        return view('help', ['active' => 'order', 'id' => $shop_find->id, 'settings' => $setting, 'app_version' => $app_version, 'shop' => $shop, 'active' => 'help']);
    }

    public function usage_charge(Request $request, $plan_id) {//to create the usage charge as per the plan selected by user
        $shop = session('shop');
        if (empty($shop)) {
            $shop = $_GET['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettings::where('id', 1)->first();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $id = $shop_find->charge_id;
        $url = 'admin/recurring_application_charges/' . $id . '.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);

        $recuring_id = (string) $charge->recurring_application_charge->id;
        if ($plan_id == 1) {//For Basic plan
            if ($shop == "new-delivery-date-pro-demo.myshopify.com") {
                $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
                $plan_argument = [
                    'usage_charge' => [
                        'description' => 'Professional',
                        'price' => 0.01,
                        'test' => true
                    ]
                ];
                $plan1 = $sh->call(['URL' => $url, 'METHOD' => 'POST', 'DATA' => $plan_argument]);
                //update the database entry
                $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['usage_price' => '0.00', 'app_version' => 1, 'type_of_app' => 1]);
            }
            return redirect()->route('dashboard', ['shop' => $shop]);
        } else if ($plan_id == 2) {
            //For Professional plan
            if ($shop == "new-delivery-date-pro-demo.myshopify.com" || $shop == "new-delivery-date-pro-demo.myshopify.com" || $shop == "dipal-test.myshopify.com" || $shop == "vijay-test.myshopify.com") {
                $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
                $plan_argument = [
                    'usage_charge' => [
                        'description' => 'Professional',
                        'price' => 0.01,
                        'test' => true
                    ]
                ];
                $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['app_version' => 2, 'type_of_app' => 2]);
            } else {
                $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
                $plan_argument = [
                    'usage_charge' => [
                        'description' => 'Professional',
                        'price' => 3.00,
                    //'test' => true
                    ]
                ];
                $plan2 = $sh->call(['URL' => $url, 'METHOD' => 'POST', 'DATA' => $plan_argument]);
                $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['usage_charge_id' => $plan2->usage_charge->id, 'usage_price' => $plan2->usage_charge->price, 'app_version' => 2, 'type_of_app' => 2]);
            }
            return redirect()->route('dashboard', ['shop' => $shop]);
        } else if ($plan_id == 3) {
            //For Enterprise plan
            if ($shop == "new-delivery-date-pro-demo.myshopify.com" || $shop == "new-delivery-date-pro-demo.myshopify.com" || $shop == "dipal-test.myshopify.com" || $shop == "vijay-test.myshopify.com") {
                $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
                $plan_argument = [
                    'usage_charge' => [
                        'description' => 'Enterprise',
                        'price' => 0.01,
                        'test' => true
                    ]
                ];
                $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['app_version' => 3, 'type_of_app' => 3]);
            } else {
                $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
                $plan_argument = [
                    'usage_charge' => [
                        'description' => 'Enterprise',
                        'price' => 7.00,
                    //'test' => true
                    ]
                ];
                $plan3 = $sh->call(['URL' => $url, 'METHOD' => 'POST', 'DATA' => $plan_argument]);
                $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['usage_charge_id' => $plan3->usage_charge->id, 'usage_price' => $plan3->usage_charge->price, 'app_version' => 3, 'type_of_app' => 3]);
            }
            return redirect()->route('dashboard', ['shop' => $shop]);
        } else if ($plan_id == 4) {
            //For Ultimate Plan
            if ($shop == "new-delivery-date-pro-demo.myshopify.com" || $shop == "new-delivery-date-pro-demo.myshopify.com" || $shop == "dipal-test.myshopify.com" || $shop == "vijay-test.myshopify.com") {
                $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
                $plan_argument = [
                    'usage_charge' => [
                        'description' => 'Ultimate',
                        'price' => 8.00,
                        'test' => true
                    ]
                ];
                $permission_url = $sh->installURL(['permissions' => array('read_orders', 'read_all_orders', 'write_orders', 'read_products', 'write_products', 'write_themes', 'read_themes', 'read_script_tags', 'write_script_tags', 'read_draft_orders', 'write_draft_orders'), 'redirect' => $app_settings->redirect_url]);
                //$permission_url = $sh->installURL(['permissions' => array('read_products', 'write_products', 'write_themes', 'read_themes', 'read_script_tags', 'write_script_tags'), 'redirect' => $app_settings->redirect_url]);

                $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['app_version' => 4, 'type_of_app' => 4, 'upgrade_status' => "Y"]);

                echo '<script>window.top.location.href="' . $permission_url . '"</script>';
                //return redirect($permission_url);
            } else {
                $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
                $plan_argument = [
                    'usage_charge' => [
                        'description' => 'Ultimate',
                        'price' => 8.00,
                    ]
                ];
                $plan4 = $sh->call(['URL' => $url, 'METHOD' => 'POST', 'DATA' => $plan_argument]);
                $permission_url = $sh->installURL(['permissions' => array('read_orders', 'read_all_orders', 'write_orders', 'read_products', 'write_products', 'write_themes', 'read_themes', 'read_script_tags', 'write_script_tags', 'read_draft_orders', 'write_draft_orders'), 'redirect' => $app_settings->redirect_url]);
                //$permission_url = $sh->installURL(['permissions' => array('read_products', 'write_products', 'write_themes', 'read_themes', 'read_script_tags', 'write_script_tags'), 'redirect' => $app_settings->redirect_url]);
                $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['usage_charge_id' => $plan4->usage_charge->id, 'usage_price' => $plan4->usage_charge->price, 'app_version' => 4, 'type_of_app' => 4, 'upgrade_status' => "Y"]);

                echo '<script>window.top.location.href="' . $permission_url . '"</script>';
            }
            $theme = $sh->call(['URL' => '/admin/themes/', 'METHOD' => 'GET']); //'DATA' => ['role' => 'main']                     
            foreach ($theme->themes as $themeData) {
                if ($themeData->role == 'main') {
                    $theme_id = $themeData->id;
                    $content = View('product_snippets')->render();
                    $view = (string) $content;
                    $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/product-delivery-date.liquid', 'value' => $view]]]);
                }
            }
        } else {
            /*
              $url = '/admin/recurring_application_charges/'.$recuring_id.'/usage_charges.json';
              $plan_argument = [
              'usage_charge' => [
              'description' => 'Ultimate',
              'price' => 0.01,
              'test' => true
              ]
              ];
              $plan3 = $sh->call(['URL' => $url,'METHOD' => 'POST','DATA' => $plan_argument ]);
              $update_usagecharge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['usage_charge_id' => $plan1->usage_charge->id,'usage_price' => $plan1->usage_charge->price,'app_version' => 4]);
              return redirect()->route('dashboard');
             */
        }
    }

    public function plans() {
        //when user select any plan from the plan page
        $shop = session('shop');
        $select_store = ShopModel::where('store_name', $shop)->first();
        $app_version = $select_store->app_version;
        if ($app_version > 0) {
            return redirect()->route('dashboard');
        } else {
            return view('plans', ['shop' => $shop, 'trial_days' => $select_store->trial_days, 'app_version' => $app_version]);
        }
    }

    //For showing plans page when changing version
    public function change_plan() {
        //when user select any plan from the plan page
        $shop = session('shop');
        $select_store = ShopModel::where('store_name', $shop)->first();
        $app_version = $select_store->app_version;
        return view('plans', ['shop' => $shop, 'trial_days' => $select_store->trial_days, 'app_version' => $app_version]);
    }

    // For Sending email on confirmation of usage charge during trial period
    public function send_confirm_email(Request $request) {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $shop = $request->input('shop');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettings::where('id', 1)->first();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $shop_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
        $store_owner = $shop_info->shop->shop_owner;
        $store_email = $shop_info->shop->email;
        $support_email = "support@zestard.com";
        $msg = "Hello, $store_owner<br><br>Greetings from Zestard Technologies!<br><br>Thanks for Installing our Delivery Date Pro app.
        <br><br>We are here to notify about the Usage charges which will get apply during trial period for the version you have selected. <br><br>Please have a wonderful exprience during the trial period and reach us at support@zestard.com if you have any questions. <br><br>Thanks, <br>Zestard team";
        mail("$store_email", "Delivery Date Pro App Usage Charge Confirmation", $msg, $headers);

        $msg = "Hello Admin, <br><br>Notification for App Installation!(Just FYI)
        <br><br>This is to notify that $store_owner has Installed Delivery Date Pro and has subscribed and agreed for the usage charges that will incur during free trial period of application. <br><br>Please note and consider this message incase you need any informtion! <br><br>Thanks, <br>Zestard Notification!";
        mail("$support_email", "Delivery Date Pro App Usage Charge Confirmation", $msg, $headers);
    }

    public function basic(Request $request) {
        $shop = session('shop');
        if (empty($shop)) {
            $shop = $_GET['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettings::where('id', 1)->first();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $id = $shop_find->charge_id;
        $url = 'admin/recurring_application_charges/' . $id . '.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);

        $recuring_id = (string) $charge->recurring_application_charge->id;
        //if($shop == "new-delivery-date-pro-demo.myshopify.com")
        {
            $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
            $plan_argument = [
                'usage_charge' => [
                    'description' => 'Professional',
                    'price' => 0.01,
                    'test' => true
                ]
            ];
        }
        //if($shop == "new-delivery-date-pro-demo.myshopify.com")
        {
            // echo "<pre>";
            // print_r($plan_argument);	
            // echo "<pre>";
            // print_r($charge);
            $plan1 = $sh->call(['URL' => $url, 'METHOD' => 'POST', 'DATA' => $plan_argument]);
            // dd($plan1);
        }
        ShopModel::where('store_name', $shop)->where('store_name', $shop)->update(['usage_price' => '0.01', 'app_version' => 1, 'type_of_app' => 1]);
        return redirect()->route('dashboard');
    }

    public function professional(Request $request) {//For Redirecting to the confirm page
        $shop = $request->input('shop');
        $user_settings = ShopModel::where('store_name', $shop)->first();
        $app_version = $user_settings->app_version;
        if ($app_version == 2) {
            return redirect()->route('dashboard', ['shop' => $shop]);
        } else {
            return view('plan_confirm', ['plan' => $request->input('two'), 'shop' => $request->input('shop')]);
        }
        //return view('plan_confirm', ['plan' => $request->input('two'), 'shop' => $request->input('shop')]);
    }

    public function enterprise(Request $request) {//For Redirecting to the confirm page        
        $shop = $request->input('shop');
        $user_settings = ShopModel::where('store_name', $shop)->first();
        $app_version = $user_settings->app_version;
        if ($app_version == 3) {
            return redirect()->route('dashboard', ['shop' => $shop]);
        } else {
            return view('plan_confirm', ['plan' => $request->input('three'), 'shop' => $shop]);
        }
    }

    public function ultimate(Request $request) {//For Redirecting to the confirm page
        $shop = $request->input('shop');
        $user_settings = ShopModel::where('store_name', $shop)->first();
        $app_version = $user_settings->app_version;
        if ($app_version == 4) {
            return redirect()->route('dashboard', ['shop' => $shop]);
        } else {
            return view('plan_confirm', ['plan' => $request->input('four'), 'shop' => $request->input('shop')]);
        }
        /* DB::table('usersettings')->where('store_name', $shop)->update(['usage_price' => '0.00', 'app_version' => 0, 'type_of_app' => 0]);        
          return view('plan_confirm', ['plan' => $request->input('four'), 'shop' => $request->input('shop')]); */
    }

    public function feature_upgrade(Request $request) {
        $shop = session('shop');
        if (empty($shop)) {
            $shop = $_GET['shop'];
        }
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_version = $shop_find->app_version;
        if ($app_version == 4) {
            ShopModel::where('store_name', $shop)->update(['upgrade_status' => "Y"]);
            return redirect()->route('dashboard');
        } else {
            return view('feature_upgrade', ['shop' => $shop]);
        }
    }

    public function upgrade_to_ultimate(Request $request) {
        $shop = $request->input('shop');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettings::where('id', 1)->first();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $id = $shop_find->charge_id;
        $url = 'admin/recurring_application_charges/' . $id . '.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
        $recuring_id = (string) $charge->recurring_application_charge->id;
        $url = '/admin/recurring_application_charges/' . $recuring_id . '/usage_charges.json';
        $plan_argument = [
            'usage_charge' => [
                'description' => 'Enterprise',
                'price' => 13.00,
            ]
        ];
        $plan4 = $sh->call(['URL' => $url, 'METHOD' => 'POST', 'DATA' => $plan_argument]);

        $update_usagecharge_status = ShopModel::where('store_name', $shop)->where('charge_id', $recuring_id)->update(['usage_charge_id' => $plan4->usage_charge->id, 'usage_price' => $plan4->usage_charge->price, 'app_version' => 4, 'type_of_app' => 4]);
        return redirect()->route('dashboard');
    }

    /* Put shortcode directly in product snippet file */

    public function SnippetCreateProduct(Request $request) {
        $shop = session('shop');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_version = $shop_find->app_version;
        $app_settings = AppSettings::where('id', 1)->first();
        $is_exist = '';
        $str_to_insert = '';
        if ($app_version == 1 || $app_version == 2 || $app_version == 3) {
            $is_exist = "{% include 'delivery-date' %}";
            $str_to_insert = " {% include 'delivery-date' %} ";
        } else {
            if ($request->template_id == 2) { // product page
                $is_exist = "{% include 'product-delivery-date' %}";
                $str_to_insert = " {% include 'product-delivery-date' %} ";
            } else {
                $is_exist = "{% include 'delivery-date' %}";
                $str_to_insert = " {% include 'delivery-date' %} ";
            }
        }
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        //api call for get theme info
        $theme = $sh->call(['URL' => '/admin/themes.json?role=main', 'METHOD' => 'GET']);
        if ($theme->themes[0]->role == 'main') {
            $theme_id = $theme->themes[0]->id;
            $product_template = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=sections/product-template.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);
            $old_str = $product_template->asset->value;
            if (strpos($old_str, $is_exist) === false) {
                $find = "{{ product.title }}";
                if (strpos($old_str, $find) !== false) {
                    //if find string available in liquide file                    
                    $pos = strpos($old_str, $find) + 30;
                    $newstr = substr_replace($old_str, $str_to_insert, $pos, 0);
                    $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'sections/product-template.liquid', 'value' => $newstr]]]);
                } else {
                    //if find string NOT available in liquide file                    
                    $find1 = "{% if section.settings.product_quantity_enable %}";
                    $pos1 = strpos($old_str, $find1);                    
                    $newstr = substr_replace($old_str, $str_to_insert, $pos1,0);
                    $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'sections/product-template.liquid', 'value' => $newstr]]]);
                }
            } else {
                Session::flash('error', 'Your Shortcode has been already pasted in the product template page. If the datepicker section still does not appear, contact us for more support.');
                return redirect()->route('dashboard', ['shop' => $shop]);
            }
        } else {
            Session::flash('error', 'Someting went wrong, Please try manual process.');
            return redirect()->route('dashboard', ['shop' => $shop]);
        }
        Session::flash('success', 'Your shortcode has been added successfully in product template page');
        return redirect()->route('dashboard', ['shop' => $shop]);
    }

    /* Put shortcode directly in cart snippet file */

    public function SnippetCreateCart(Request $request) {

        $template = $request->input('template');
        $shop = session('shop');
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $app_settings = AppSettings::where('id', 1)->first();
        $sh = app('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        //api call for get theme info
        $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);       

        if($template == 'checkout'){            
            foreach ($theme->themes as $themeData) {
                if ($themeData->role == 'main') {
                    $theme_id = $themeData->id;
                    $product_template = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=layout/checkout.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);
                    $string_value = $product_template->asset->value;
                    $contain_section = '<div class="sidebar" role="complementary">';                         
                    if (strpos($string_value, $contain_section) !== false) {                                                                       
                        $is_exist = "{% include 'delivery-date' %}";                                                
                        if (strpos($string_value, $is_exist) !== false) {                            
                            Session::flash('error', 'Your Shortcode has been already pasted in the checkout template page. If the datepicker section still does not appear, contact us for more support.');
                            return redirect()->route('dashboard', ['shop' => $shop]);                           
                        } else {                                                        
                            $str_to_insert = "{% include 'delivery-date' %}";
                            $find = '<div class="sidebar__header">';
                            if (strpos($string_value, $find) !== false) {
                                //if find string available in liquide file                            
                                $pos = strpos($string_value, $find);
                                $newstr = substr_replace($string_value, $str_to_insert, $pos, 0);
                                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'layout/checkout.liquid', 'value' => $newstr]]]);
                            } 
                        }
                    }   
                }             
            }
        } else {
            foreach ($theme->themes as $themeData) {
                if ($themeData->role == 'main') {
                    $theme_id = $themeData->id;
                    $product_template = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=templates/cart.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);
                    $string_value = $product_template->asset->value;
                    $contain_section = "{% section 'cart-template' %}";
                    if (strpos($string_value, $contain_section) !== false) {
                        //if cart-template.liquid available
                        $product_template = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=sections/cart-template.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);
                        $old_str = $product_template->asset->value;
                        $is_exist = "{% include 'delivery-date' %}";
                        if (strpos($old_str, $is_exist) === false) {
                            $str_to_insert = " {% include 'delivery-date' %} ";
                            $find = '<div class="cart__footer">';
                            if (strpos($old_str, $find) !== false) {
                                //if find string available in liquide file                            
                                $pos = strpos($old_str, $find);
                                $newstr = substr_replace($old_str, $str_to_insert, $pos, 0);
                                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'sections/cart-template.liquid', 'value' => $newstr]]]);
                            } else {
                                //if find string NOT available in liquide file
                                $find1 = "{{ cart.note }}";
                                $pos1 = strpos($old_str, $find1) + 30;
                                $newstr = substr_replace($old_str, $str_to_insert, $pos1, 0);
                                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'sections/cart-template.liquid', 'value' => $newstr]]]);
                            }
                        } else {
                            Session::flash('error', 'Your Shortcode has been already pasted in the cart template page. If the datepicker section still does not appear, contact us for more support.');
                            return redirect()->route('dashboard', ['shop' => $shop]);
                        }
                    } else {
                        //if cart-template.liquid not available
                        $product_template = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=templates/cart.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);
                        $old_str = $product_template->asset->value;
                        $is_exist = "{% include 'delivery-date' %}";
                        if (strpos($old_str, $is_exist) === false) {
    
                            $str_to_insert = " {% include 'delivery-date' %} ";
                            $find = "{{ cart.note }}";
                            $pos = strpos($old_str, $find) + 100;
                            $newstr = substr_replace($old_str, $str_to_insert, $pos, 0);
                            //api call for creating snippets                 
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'templates/cart.liquid', 'value' => $newstr]]]);
                        } else {
                            Session::flash('error', 'Your Shortcode has been already pasted in the cart template page. If the datepicker section still does not appear, contact us for more support.');
                            return redirect()->route('dashboard', ['shop' => $shop]);
                        }
                    }
                }
            }
        }
        Session::flash('success', 'Your shortcode has been added successfully in cart template page');
        return redirect()->route('dashboard', ['shop' => $shop]);
    }

    /* Update shop id in every table */

    public function update_db(Request $request) {

        /* cutoff table */
        $db_record = DB::table('cutoff_settings')->where('update_shopid_status', 0)->get();
        foreach ($db_record as $db) {
            $user_setting = DB::table('usersettings')->where('store_name', $db->store)->first();
            if ($user_setting) {
                $shop_id = $user_setting->id;
                $update = DB::table('cutoff_settings')->where('store', $db->store)->update(['shop_id' => $shop_id, 'update_shopid_status' => '1']);
            } else {
                DB::table('cutoff_settings')->where('store', $db->store)->where('update_shopid_status', 0)->delete();
            }
        }

        /* trial info table */
        $db_record = DB::table('trial_info')->where('update_shopid_status', 0)->get();
        foreach ($db_record as $db) {
            $user_setting = DB::table('usersettings')->where('store_name', $db->store_name)->first();
            if ($user_setting) {
                $shop_id = $user_setting->id;
                $update = DB::table('trial_info')->where('store_name', $db->store_name)->update(['shop_id' => $shop_id, 'update_shopid_status' => '1']);
            } else {
                DB::table('trial_info')->where('store_name', $db->store_name)->where('update_shopid_status', 0)->delete();
            }
        }

        /* delivery setting table */
        $db_record = DB::table('deliverytime_settings')->where('update_shopid_status', 0)->get();
        foreach ($db_record as $db) {
            $user_setting = DB::table('usersettings')->where('store_name', $db->store)->first();
            if ($user_setting) {
                $shop_id = $user_setting->id;
                $update = DB::table('deliverytime_settings')->where('store', $db->store)->update(['shop_id' => $shop_id, 'update_shopid_status' => '1']);
            } else {
                DB::table('deliverytime_settings')->where('store', $db->store)->where('update_shopid_status', 0)->delete();
            }
        }

        /* product setting table */
        $db_record = DB::table('product_settings')->where('update_shopid_status', 0)->get();
        foreach ($db_record as $db) {
            $user_setting = DB::table('usersettings')->where('store_name', $db->shop)->first();
            if ($user_setting) {
                $shop_id = $user_setting->id;
                $update = DB::table('product_settings')->where('shop', $db->shop)->update(['shop_id' => $shop_id, 'update_shopid_status' => '1']);
            } else {
                DB::table('product_settings')->where('shop', $db->shop)->where('update_shopid_status', 0)->delete();
            }
        }

        /* visitors_log table */
        $db_record = DB::table('visitors_log')->where('update_shopid_status', 0)->get();
        foreach ($db_record as $db) {
            $user_setting = DB::table('usersettings')->where('store_name', $db->store_name)->first();
            if ($user_setting) {
                $shop_id = $user_setting->id;
                $update = DB::table('visitors_log')->where('store_name', $db->store_name)->update(['shop_id' => $shop_id, 'update_shopid_status' => '1']);
            } else {
                DB::table('visitors_log')->where('store_name', $db->store_name)->where('update_shopid_status', 0)->delete();
            }
        }

        /* development_stores table */
        $db_record = DB::table('development_stores')->where('update_shopid_status', 0)->get();
        foreach ($db_record as $db) {
            $user_setting = DB::table('usersettings')->where('store_name', $db->dev_store_name)->first();
            if ($user_setting) {
                $shop_id = $user_setting->id;
                $update = DB::table('development_stores')->where('dev_store_name', $db->dev_store_name)->update(['shop_id' => $shop_id, 'update_shopid_status' => '1']);
            } else {
                DB::table('development_stores')->where('dev_store_name', $db->dev_store_name)->where('update_shopid_status', 0)->delete();
            }
        }

        /* app_log table */
        $db_record = DB::table('app_log')->where('update_shopid_status', 0)->get();
        foreach ($db_record as $db) {
            $user_setting = DB::table('usersettings')->where('store_name', $db->store_name)->first();
            if ($user_setting) {
                $shop_id = $user_setting->id;
                $update = DB::table('app_log')->where('store_name', $db->store_name)->update(['shop_id' => $shop_id, 'update_shopid_status' => '1']);
            } else {
                DB::table('app_log')->where('store_name', $db->store_name)->where('update_shopid_status', 0)->delete();
            }
        }
        echo 'success';
        exit;
    }

}
