<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\block_config;
use App\DeliveryTimeSettings;
use App\AppSettings;

class deliverytimeController extends Controller
{	
	public function __construct(Request $request)
	{				
	}
	public function delivery_time(Request $request)
	{					
		$shop = session('shop');
		if(empty($shop))
		{
		  $shop = $_GET['shop'];
		}
		$app_settings = AppSettings::where('id', 1)->first();
		$shop_find = ShopModel::where('store_name' , $shop)->first();
		$shop_id = $shop_find->id;		
		$config = [];
		$config = block_config::where('shop_id' , $shop_id)->first();
		$data['config'] = $config;
		$data['active'] = 'date';
		$settings = DeliveryTimeSettings::where('shop_id', $shop_id)->get();
		$data['settings'] = $settings;
		$data['count_flag']=$shop_find->delivery_time_count_status;				
		$data['global_flag']=$shop_find->global_delivery_time_status;				
		return view('delivery_time_settings',$data);
	}
		
	public function save_delivery_times(Request $request)
	{					
		
		$shop = session('shop');		
		if(empty($shop))
		{
		  $shop = $_GET['shop'];
		}		
		$temp = ShopModel::where('store_name' , $shop)->first();
                $shop_id = $temp->id;
		if($request->input('count_flag'))
		{			
			$temp->delivery_time_count_status=1;
			$temp->save();
		}
		else
		{			
			$temp->delivery_time_count_status=0;
			$temp->save();
		}	
		if($request->input('global_delivery_time'))
		{			
			$temp->global_delivery_time_status=1;
			$temp->save();
		}
		else
		{			
			$temp->global_delivery_time_status=0;
			$temp->save();
		}	
		for($i=0;$i<7;$i++)
		{			
			$flag_array =array();														
			$delivery_times=json_encode($request->input('delivery_time_'.$i));		
			if(!empty($request->input('count_'.$i)))
			{
				$count=implode(",",$request->input('count_'.$i));
			}
			else
			{
				$count=0;
			}			
			$info=array('store' => $shop, 'day' => $i,'delivery_times' => $delivery_times ,'count' => $count, 'shop_id' => $shop_id);				
			$count=DeliveryTimeSettings::where(['day' => $i, 'store' => $shop])->count();
			if($count > 0)
			{
				DeliveryTimeSettings::where(['day' => $i, 'store' => $shop])->update($info);
			}	
			else
			{
				DeliveryTimeSettings::insert($info);
			}								
		}		
		$notification = array(
		'message' => 'Settings Saved Successfully.',
		'alert-type' => 'success');  		
		
		return redirect()->route('delivery-time')->with('notification',$notification);
	}
			
}