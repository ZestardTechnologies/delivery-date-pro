<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\settings_log;
use App\block_config;
use App\ProductSettings;
use App\AppSettings;

class Product_Controller extends Controller
{    		
	public function save_product_settings(Request $request)
	{		
                //dd($request);
		$selected_products_count = count($request->input('selected_product'));
		$selected_products = $request->input('selected_product');
		$all_products = $request->input('all_product_ids');
		$all_products_count = count($request->input('all_product_ids'));
		$shop = session('shop');
		$global_product_delivery_time 	= $request->input('global_product_delivery_time');
		$global_product_blocked_dates 	= $request->input('global_product_blocked_dates');
		$global_product_blocked_days 	= $request->input('global_product_blocked_days');
		$global_product_pre_order 		= $request->input('global_product_pre_order');
		$global_product_date_interval 	= $request->input('global_product_date_interval');
		$global_product_cut_off 		= $request->input('global_product_cut_off');
		$data = array();
		$data['global_product_delivery_time'] = isset($global_product_delivery_time) ? $global_product_delivery_time : 0;
		$data['global_product_blocked_dates'] = isset($global_product_blocked_dates) ? $global_product_blocked_dates : 0;
		$data['global_product_blocked_days'] = isset($global_product_blocked_days) ? $global_product_blocked_days : 0;
		$data['global_product_pre_order'] = isset($global_product_pre_order) ? $global_product_pre_order : 0;
		$data['global_product_date_interval'] = isset($global_product_date_interval) ? $global_product_date_interval : 0;
		$data['global_product_cut_off'] = isset($global_product_cut_off) ? $global_product_cut_off : 0;
				
		$user_settings = ShopModel::where('store_name' , $shop)->first();	
                $shop_id = $user_settings->id;
		block_config::where(['shop_id' => $user_settings->id])->update($data);                		
		for($i=0; $i < $all_products_count; $i++)
		{
			$data = array();			
			$product_id = $all_products[$i];				
			{				
				if($selected_products_count > 0)
				{
					if(in_array($product_id, $selected_products))
					{
						$data['status'] = 1;
					}					
					else
					{
						$data['status'] = 0;
					}
				}
				else
				{
					$data['status'] = 0;
				}
                                /* code by dipal */
                                $product_settings = ProductSettings::where(['shop_id' => $user_settings->id, 'product_id' => $product_id])->first();
                                //echo '<pre>';print_r($product_settings);exit;
                                if($request->input('delivery_time_'.$product_id) != ''){
                                    $data['delivery_times'] = $request->input('delivery_time_'.$product_id);
                                } else {
                                    $data['delivery_times'] = !empty($product_settings) ? $product_settings->delivery_times : '';
                                }			
                                /* end code */
                                
                                $blocked_dates = $request->input('blockeddate_'.$product_id);	
				if($blocked_dates)
				{
					$data['blocked_dates'] = str_replace("/", "-", implode(",", $request->input('blockeddate_'.$product_id)));
				}
				if(count($request->input('days_'.$product_id)) > 0)
				{
					$data['blocked_days'] = implode(",", $request->input('days_'.$product_id));					
				}
				$pre_order_time  =  $request->input('allowed_month_'.$product_id);
				$date_interval   =  $request->input('intervel_'.$product_id);	
				if($date_interval == 0 || $date_interval == ''){
					$date_interval = 0;
				} else {
					$date_interval = $date_interval;
				}
				$cut_off_hours   =  $request->input('hour_'.$product_id);			
				$cut_off_minutes =  $request->input('minute_'.$product_id);			
				$data['pre_order_time']  = !empty($pre_order_time) ? $pre_order_time :  6;
				$data['date_interval']   = $date_interval;
                $data['shop_id'] = $shop_id;
				$data['cut_off_hours']   = !empty($cut_off_hours) ? $cut_off_hours : 0;	
				$data['cut_off_minutes'] = !empty($cut_off_minutes) ? $cut_off_minutes : (!empty($product_settings) ? $product_settings->cut_off_minutes : 0);	

				$count = ProductSettings::where(['shop' => $shop, 'product_id' => $product_id])->count();		
				// echo $count;
				// dd($data);
				if($count > 0)
				{
					ProductSettings::where(['shop' => $shop, 'product_id' => $product_id])->update($data);	
				}
				else 		
				{				
					$data['shop'] = $shop;
                                        $data['shop_id'] = $shop_id;
					$data['product_id'] = $product_id;				
					ProductSettings::insert($data);	
				}
			}
		}		
		$notification = array(
			'message' => 'Settings Saved Successfully.',
			'alert-type' => 'success'
		);		  
		return redirect()->route('product-settings', ['shop' => $shop])->with('notification', $notification);
	}
	public function product_settings()
	{
		$shop = session('shop');
		$user_settings = ShopModel::where('store_name' , $shop)->first();						
		$configuration = block_config::where(['shop_id' => $user_settings->id])->first();	
		
		$product_settings = ProductSettings::where('shop' , $shop)->get();		
		$product_settings_array = array();
		foreach($product_settings as $product)
		{
			$product_id = $product->product_id;
			/* 
			$product_settings_array[$product_id] = DB::table('product_settings')->select('product_id', 'blocked_dates', 'blocked_days', 'pre_order_time', 'cut_off_hours', 'cut_off_minutes')->where('product_id', $product_id)->first();	 
			*/
			$product_settings_array[$product_id] = ProductSettings::where('product_id', $product_id)->first();	
		}		
		//dd($product_settings_array);	
		return view('product_settings',['product_settings' => json_encode($product_settings_array), 'configuration' => $configuration, 'count' => count($product_settings_array), 'shop' => $shop]);
	}
	public function get_product_settings(Request $request)
	{                       
		$product_settings = ProductSettings::where(['shop' => $request->input('shop_name'), 'product_id' => $request->input('product_id')])->first();	
		$store_settings = ShopModel::where(['store_name' => $request->input('shop_name')])->first();	
		$app_config = block_config::where(['shop_id' => $store_settings->id])->first();	
		$settings = array();
		$settings['product_settings'] =  $product_settings;
		$settings['app_config'] =  $app_config;	                
		if($store_settings->app_version == 4)
		{
			return json_encode($settings);
		}
		else
		{
			return 0;
		}		
	}
	public function product_data(Request $request)
	{
		$sh = App::make('ShopifyAPI');
        $app_settings = AppSettings::where('id', 1)->first();
        $shop = session('shop');
		if(!isset($shop))
		{
			$shop = $_GET['shop'];
		}
        $select_store = ShopModel::where('store_name', $shop)->get();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $count = $sh->call(['URL' => '/admin/products/count.json', 'METHOD' => 'GET']);
        $products_count = (array) $count;
        $product_count = $products_count['count'];
        $limit = $request['length'];
        $draw = $request['draw'];
        $start = $request['start'];
        $current_page = ceil($start / $limit) + 1;
        $default_image = url('/image/default.png');
        $search = $request['search']['value'];
        $total_products = array('draw' => $draw, 'recordsTotal' => $product_count, 'recordsFiltered' => $product_count);

        $val = $start + 1;
        if ($search) {			
            $pages = ceil($product_count / 250);
            $limit = 250;
			$count = 0;	
            for($i = 0; $i < $pages; $i++) {
                $current_page = $i + 1;
                $product_list = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
                foreach ($product_list->products as $product) {
					$count++;	
                    if(stristr($product->title, $search)) 
					{
                        if(isset($product->images[0])) 
						{
                            $image = $product->images[0]->src;
                        } 
						else 
						{
                            $image = "";
                        }
                        $total_products['data'][] = array('product_id' => $product->id, 'product_image' => $image, 'product_name' => $product->title);
                    }
                    $val++;
                }				
            }
			$total_products['draw'] = $draw;
			$total_products['recordsTotal'] = $count;
			$total_products['recordsFiltered'] = $count;
        } 
		else 
		{
            $product_list = $sh->call(['URL' => '/admin/products.json?limit=' . $limit . '&page=' . $current_page, 'METHOD' => 'GET']);
            $count = 0;
			foreach($product_list->products as $product) {
				$count++;
                if(isset($product->images[0])) 
				{
                    $image = $product->images[0]->src;
                } 
				else 
				{
                    $image = "";
                }                
                $total_products['data'][] = array('product_id' => $product->id, 'product_image' => $image, 'product_name' => $product->title);
                $val++;
            }
			/* $total_products['draw'] = $draw;
			$total_products['recordsTotal'] = $count;
			$total_products['recordsFiltered'] = $count; */
        }
        return json_encode($total_products);        
	}
	public function get_product_data(Request $request)
	{
		$product_settings = ProductSettings::select('product_id')->get();		
		$product_settings_array = array();
		foreach($product_settings as $product)
		{
			$product_id = $product->product_id;
			/* $product_settings_array[$product_id] = DB::table('product_settings')->select('product_id', 'blocked_dates', 'blocked_days', 'pre_order_time', 'cut_off_hours', 'cut_off_minutes')->where('product_id', $product_id)->first();	 */
			$product_settings_array[$product_id] = ProductSettings::where('product_id', $product_id)->first();	
		}		
		return json_encode($product_settings_array);
	}
	public function check_data(Request $request)
	{
		$product_data = array();
		parse_str($request->input('data'), $product_data);		
		if(isset($product_data['selected_product']))
		{
			$selected_products_count = count($product_data['selected_product']);
			$selected_products = $product_data['selected_product'];
			$shop = session('shop');		
			$data = array();
			$data['global_product_delivery_time'] = isset($product_data['global_product_delivery_time']) ? 1 : 0;
			$data['global_product_blocked_dates'] = isset($product_data['global_product_blocked_dates']) ? 1 : 0;
			$data['global_product_blocked_days'] = isset($product_data['global_product_blocked_days']) ? 1 : 0;
			$data['global_product_pre_order'] = isset($product_data['global_product_pre_order']) ? 1 : 0;
			$data['global_product_date_interval'] = isset($product_data['global_product_date_interval']) ? 1 : 0;
			$data['global_product_cut_off'] = isset($product_data['global_product_cut_off']) ? 1 : 0;
					
			$user_settings = ShopModel::where('store_name' , $shop)->first();						
			block_config::where(['shop_id' => $user_settings->id])->update($data);	
			/* DB::table('product_settings')->truncate(); */
			/* dd($selected_products);	 */
			for($i=0; $i < $selected_products_count; $i++)
			{
				$data = array();
				$product_id = $selected_products[$i];						
				if(isset($product_data['delivery_time_'.$product_id]))
				{
					$data['delivery_times'] = $product_data['delivery_time_'.$product_id];	
				}
				else
				{
					$data['delivery_times'] = "";
				}
				$data['blocked_dates'] = str_replace("/", "-", implode(",", $product_data['blockeddate_'.$product_id]));
				if(isset($product_data['days_'.$product_id]))
				{
					if(count($product_data['days_'.$product_id]) > 0)
					{
						$data['blocked_days']  = implode(",", $product_data['days_'.$product_id]);
					}
				}
				$data['pre_order_time'] = $product_data['allowed_month_'.$product_id];			
				$data['date_interval'] = $product_data['intervel_'.$product_id];			
				$data['cut_off_hours'] = empty($product_data['hour_'.$product_id]) ? 0 : $product_data['hour_'.$product_id];			
				$data['cut_off_minutes'] = empty($product_data['minute_'.$product_id]) ? 0 : $product_data['minute_'.$product_id];						
			
				$count = ProductSettings::where(['shop' => $shop, 'product_id' => $product_id])->count();			
				if($count > 0)
				{
					$data = ProductSettings::where(['shop' => $shop, 'product_id' => $product_id])->update($data);	
				}
				else 		
				{				
					$data['shop'] = $shop;
					$data['product_id'] = $product_id;				
					$data = ProductSettings::insert($data);	
				}
			}
		}
	}	
}