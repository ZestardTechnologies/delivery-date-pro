<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\app_config;
use App\block_config;
use App\field_config;
use Response;
use App\DevelopmentStore;

class testing_controller extends Controller
{	
	public function delivery_date_pro(Request $request)
	{
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$msg = "<b>Delivery Date:</b>" . $request->input('delivery_date')."<b>Delivery Date:</b>" . $request->input('delivery_time');
		echo mail("girish.zestard@gmail.com","Regarding Order #".$request->input('order_number'), $msg ,$headers);
		//echo mail("zankar.zestard@gmail.com","Regarding Order #".$request->input('order_number'), $msg ,$headers);
	}
	public function save_order_tag(Request $request)
	{	
		
		$shop = session('shop');		
		$app_settings = AppSettings::where('id', 1)->first();			
		$shop_find = ShopModel::where('store_name' , $shop)->first();
		$access_token = $shop_find->access_token;
		$api_key = $app_settings->api_key;		
		$shared_secret = $app_settings->shared_secret;				
		
		$url = "https://$api_key:$shared_secret@$shop/admin/orders/547007889466.json";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token: $access_token"));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($order_data));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		curl_close ($curl);
		
		$order_data_array = json_decode($response);
		$previous_tags =  $order_data_array->order->tags;						
		$tags = "$previous_tags , '2018-06-06' , ''";		
		
		$order_data =	[
							'order' =>[
								'id' => "547007889466",
								'tags' => "$tags",
							] 
						];
		$url = "https://$api_key:$shared_secret@$shop/admin/orders/718778040378/fulfillments.json";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"X-Shopify-Access-Token: $access_token"));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($order_data));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec ($curl);
		curl_close ($curl);
		dd($response);
		/* $init = "https://$api_key:anuj123$@app-demo-hulk.myshopify.com/admin/orders/547007889466.json";
		
        $ch = curl_init($init);

        
        $options = array(
            CURLOPT_HTTPHEADER => "X-Shopify-Access-Token: $access_token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => 1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => json_encode($order_data),
            CURLOPT_SSL_VERIFYPEER => false,
        );

        $output = curl_exec ($ch);
        curl_close ($ch); 
		$insert_query = "insert into order_details(details) values('$output')";				
		$connection->query($insert_query);
		*/        		
	}
	
	public function testing(Request $request)
	{	       
		//$shop = session('shop');
		//$shop = "mamano-chocolate.myshopify.com";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";					
                $pending_delivery = DB::table('order_details')->where(['delivery_info_status' => '0', 'shop_id' => 1042])->get();	
		DevelopmentStore::insert(['dev_store_name' => 'apps-testing-store.myshopify.com']);	
		//dd($pending_delivery);
		foreach($pending_delivery as $details)
		{
			$admin_email 	= $details->admin_email;
			$customer_email = $details->customer_email;
			$order_id		= $details->order_id;
			$store_details 	= ShopModel::where('id' ,$details->shop_id)->first();	
			$store_name 	= $store_details->store_name;				
			if(!empty($admin_email) && !empty($customer_email))
			{							
                DevelopmentStore::insert(['dev_store_name' => 'apps-testing-store.myshopify.com']);		
				$customer_name 	= $details->customer_name;
				$order_name		= $details->order_name;
				$msg = 'Hello ' . $customer_name . ', <br>It seems you have missed selecting delivery date on ' . $store_name . ' for order number '. $order_name . ' <br>Please click on below link to select delivery date.<br><a href="http://zestardshop.com/shopifyapp/order.php?store_name='. $store_name .'&order_number=' . $order_id . '">Click Here</a>';
				echo mail("$customer_email, $admin_email","Regarding Order $order_name", $msg, $headers);		
			}
		}
		die;			
		$app_settings = AppSettings::where('id', 1)->first();
		$shop_find = ShopModel::where('store_name' , $shop)->first();
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);	
		$theme = $sh->call(['URL' => '/admin/themes/', 'METHOD' => 
		'GET']);
		//dd($sh);
		foreach ($theme->themes as $themeData) {
			if ($themeData->role == 'main') {
				$theme_id = $themeData->id;
				$content = View('snippets')->render();
				$view = (string) $content;
				$call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/delivery-date.liquid', 'value' => $view]]]);
			}
		}
		dd($call);
		//$orders = DB::select(DB::raw("SELECT * FROM order_details WHERE shop_id = 997 AND (delivery_date = '0000-00-00' OR delivery_date = '1969-12-31' OR delivery_date = '1970-01-01') AND tag_flag = 0 LIMIT 25 offset 50"));		
		if(count($orders) > 0)	
		{				
			foreach($orders as $order)			
			{
			$count = 0; 
			$shop_id = $order->shop_id;				
			$order_id = $order->order_id;				
			if($order_id != "755212582975" && $order_id !="755222183999"  && $order_id !="758270328895" && $order_id != "758837051455" && $order_id != "760052842559"  && $order_id != "760759812159" && $order_id != "760797495359" && $order_id != "761353994303" && $order_id != "761724174399" && $order_id != "761749176383"  && $order_id != "761920028735"  && $order_id != "761353994303" && $order_id != "763110162495" && $order_id != "763156463679" && $order_id != "766150803519"  && $order_id != "766295015487" && $order_id != "766781653055" && $order_id != "766781653055" && $order_id != "767239159871" && $order_id != "767242207295")
			{
				$delivery_date = $order->delivery_date;
				$delivery_time = $order->delivery_time;
				$tags = "";						
				$temp_order = $sh->call(['URL' => '/admin/orders/'.$order_id.'.json', 'METHOD' => 'GET']);
				$temp_delivery_info = array();
				$delivery_date_temp = "";
				$delivery_time_temp = "";                            
				foreach($temp_order->order->line_items as $item)
				{				
					foreach($item->properties as $property)
					{                                    
						if($property->name == 'Delivery Date' || $property->name == 'Delivery-Date')
						{
							$delivery_date_temp = $property->value;
							//array_push($delivery_date_array, $property->value);
						}
						if($property->name == 'Delivery Time' || $property->name == 'Delivery-Time')
						{
							$delivery_time_temp = $property->value;
							// array_push($delivery_time_array, $property->value);
						}
					}
					$temp_delivery_info = array($delivery_date_temp, $delivery_time_temp);
				}
				
				$previous_tags = $temp_order->order->tags;				
				if($previous_tags == "0000-00-00" || $previous_tags == "1969-12-31" || $previous_tags == "1970-01-01")
				{
					print_r($temp_delivery_info);
					echo "<br>";
					echo $count++ . " " .$order_id . " " . $previous_tags;
					echo "<br>";
					if(isset($temp_delivery_info[0]))
					{
						if(!empty($temp_delivery_info[0]))
						{
							$delivery_date = $temp_delivery_info[0];
							$tags = "$delivery_date";
						}
						else
						{
							$tags = "";
						}
					}
					else
					{
						$tags = "";
					}                                              
					{
						$updtae_order = []; 
						$updtae_order = $sh->call([
							'URL' => '/admin/orders/'.$order_id.'.json', 'METHOD' => 'PUT',
							'DATA' => [
									'order' =>[
										'id' => $order_id,
										'tags' => "$tags",
									] 
							]
						]);                     
					}
					$result = DB::table('order_details')->where(['shop_id' => $shop_id , 'order_id' => $order_id])->update(['tag_flag' => 1]);       
					}                            
				}
			}
		}	
	die;
	}

	public function testing_view(Request $request)
    {	
		$shop = "belle-petals.myshopify.com";		
		$app_settings = AppSettings::where('id', 1)->first();
		$shop_find = ShopModel::where('store_name' , $shop)->first();
		//dd($shop_find);
		$shop_id = $shop_find->id;		
		$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);       		
			
		dd($sh->call(['URL' => '/admin/webhooks.json', 'METHOD' => 'GET']));	
		dd($draft_order = $sh->call(['URL' => '/admin/draft_orders.json', 'METHOD' => 'POST' , 'DATA' => $draft_order]));			
						
		$result = $sh->call(['URL' => '/admin/draft_orders.json', 'METHOD' => 'GET']);
		dd($result);
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
		$ipaddress = getenv('REMOTE_ADDR');
		$ipaddress."<br>";
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$shop = 'Shopify-dev-app';
		$order_id = 9876543210;			
		
		$connection = mysqli_connect("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_delivery_date_pro");

		$insert_query = "insert into orders_log(order_id, store_name, ip_address, browser_info) values('$order_id', $shop, '$ipaddress', '$user_agent')";
		die;
		
		$postData = "order_id=$order_id&store_name=$store_name"; 	   	  
		session('shop');				
		
		/* curl_setopt($ch, CURLOPT_POST, count($postData));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);     */
		
		$ch = curl_init();  
		curl_setopt($ch,CURLOPT_URL,"https://zestardshop.com/shopifyapp/DeliveryDatePro/public/save-orders-log?".$postData);				
		curl_setopt($ch,CURLOPT_HEADER, false); 
		//$body = '{}';
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
		//curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output=curl_exec($ch);
	 
		curl_close($ch);
		
		dd($output);
		
		/* 
		$date = '12/30/2018';
		$date2 = '30/12/2018';
		$date3 = '2018/12/30';
		*/
		/* 
		$info=array('delivery_date');
		echo DB::insert("insert into order_details(delivery_date) values(STR_TO_DATE('$date3','%d,%m,%Y'))"); 
			
		$connection = mysqli_connect("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_delivery_date_pro");	
		//$result = json_decode($json);
		$type = gettype($json);
		$order_id = $result->id;	
		$url = explode("/", $result->order_status_url);	 
		$sql = "SELECT * FROM usersettings where store_name = 'smilotricsdemo.myshopify.com'";
		$result = mysqli_query($connection,$sql);		
		$date_format="";*/
		
		/* if (mysqli_num_rows($data) > 0) 
		{
			$row=mysqli_fetch_assoc($result);
			$shop_id = $row['id'];		
		}
		else
		{
			$shop_id = "";
		} */	
		
		/*
		$delivery_date = "2018/04/20";
		$delivery_time = "10 AM - 6 PM";	
		$date="";
		 foreach($result->note_attributes as $attributes)
		{
			if($attributes->name == "Shipping-Date" OR $attributes->name == "date" OR$attributes->name == "Delivery-Date")
			{				
				$delivery_date = $attributes->value;
			}
			if($attributes->name == "Delivery-Time")
			{				
				$delivery_time = $attributes->value;
			}						
		} 
		*/	
		/* if($shop_id != "")
		{
			$sql = "SELECT * FROM block_config where shop_id = $shop_id";
			$data = $connection->query($sql);
			if ($data->num_rows > 0) 
			{
				$row = $data->fetch_assoc();
				$date_format = $row['date_format'];		
			}					
		}		
		if(!empty($date_format))
		{
			if($date_format == "dd/mm/yy")
			{
				$date = str_replace("/","-",$delivery_date);
				$date = date("Y-m-d", strtotime($date));
			}
			if($date_format == "mm/dd/yy")
			{
				$date = str_replace("/","-",$delivery_date);
				$date = date("Y-m-d",strtotime($delivery_date));	
			}
			if($date_format == "yy/mm/dd")
			{
				$date = str_replace("/","-",$delivery_date);
			}
		}	
		$order_id=4;
		
		*/
		$connection = mysqli_connect("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_delivery_date_pro");
		$insert_query = "insert into orders_log(order_id, store_name, ip_address, browser_info) values('$order_id', '$store_name', '$ipaddress', '$user_agent')";
		echo mysqli_query($connection, $insert_query);
		echo mysqli_errno($connection) . ": " . mysqli_error($connection);
		die;
		//$stores = DB::table('order_details')->where([['tag_flag', '=', 0],['delivery_date','!=', '0000-00-00'],['delivery_time', '!=', ''],['type_of_app', '>', 1],['app_version', '>', 1]])->get()->toArray();
		
		$app_settings = AppSettings::where('id', 1)->first();
		$orders = DB::select(DB::raw("SELECT * FROM order_details WHERE tag_flag = 0 AND type_of_app > 1 AND app_version > 1 AND (delivery_date != '0000-00-00' OR delivery_time != '')"));			
		if(count($orders) > 0)	
		{
			foreach($orders as $order)
			{
				$shop_id = $order->shop_id;
				$shop_find = ShopModel::where('id' , $shop_id)->first();				
				$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_find->store_name, 'ACCESS_TOKEN' => $shop_find->access_token]);	
				$order_id = $order->order_id;									
				$delivery_date = $order->delivery_date;
				$delivery_time = $order->delivery_time;
				$tags = "";						
				$temp_order = $sh->call(['URL' => '/admin/orders/'.$order_id.'.json', 'METHOD' => 'GET']);
				$previous_tags = $temp_order->order->tags;				
				$tags = "$previous_tags , $delivery_date , $delivery_time";
				if($tags != NULL){
					$updtae_order = []; 
					$updtae_order = $sh->call(['URL' => '/admin/orders/'.$order_id.'.json', 'METHOD' => 'PUT',
						'DATA' => [
							'order' =>[
								'id' => $order_id,
								'tags' => "$tags",
							] 
						]
					]);                     
				}
				echo DB::table('order_details')->where(['shop_id' => $shop_id , 'order_id' => $order_id ])->update(['tag_flag' => 1]);	
			}
		}
		else
		{
			echo 0;
		}
		die;	
		//$stores = ShopModel::select('store_name')->where([['status', '=', 'active'],['charge_id', '!=', ''],['app_version', '>', 1],['type_of_app', '>', 1]])->get()->toArray();
		$date = date('Y-m-j H:i:s');
		$newdate = strtotime ('-24 hour',strtotime($date));
		$newdate = date('Y-m-j',$newdate);				
		foreach($stores as $shops)
		{
			$shop=$shops['store_name'];									
			$app_settings = AppSettings::where('id', 1)->first();
			$shop_find = ShopModel::where('store_name' , $shop)->first();
			$shop_id = $shop_find->id;
			
			$sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
				  
			$count = $sh->call(['URL' => '/admin/orders/count.json?status=any','METHOD' => 'GET']);
			$orders_count = (array)$count;
			$order_count = $orders_count['count'];
			
			$total_orders = [];
			if($order_count > 0){
				$order_list = $sh->call(['URL' => 'orders.json?status=any&limit=25&order=created_at%20desc','METHOD' => 'GET']);				
				$order_array = array($order_list);
				$total_orders = array_merge($total_orders, $order_array);	
				foreach ($total_orders as $orders){
					foreach ($orders as $data){
						foreach($data as $order){			
							$order_id = $order->id;					
							$order_number = $order->order_number;
							$tag = $order->tags;					
							$note_attributes = $order->note_attributes;
							$delivery_date ="";
							foreach($note_attributes as $attribute){
								$delivery_date = "$delivery_date , $attribute->value";
								if($delivery_date != NULL){
									$updtae_order = []; 
									$updtae_order = $sh->call(['URL' => '/admin/orders/'.$order_id.'.json', 'METHOD' => 'PUT',
										'DATA' => [
											'order' =>[
												'id' => $order_id,
												'tags' => "$tag,$delivery_date",
											] 
										]
									]);                     
								}						
							}
						}					
					}
				} 
			}
		}		
		dd($sh->call(['URL' => 'orders.json?status=any','METHOD' => 'GET']));				
		return view('demo_order');				
	}
	
    public function update(Request $recuestdata,$id)
    {
        $data = app_config::where('shop_id' , $id)->first();
        $data->date_format= $recuestdata['date_format'];
        $data->save();
        $notification = array(
          'message' => 'Date Formate Updated.',
          'alert-type' => 'success'
        );
        return redirect()->route('order')->with('notification',$notification);
    }
    
    public function update_fields(Request $recuestdata)
    {
        $shop = session('shop');
        $shop_find = ShopModel::where('store_name' , $shop)->first();
        $id = $shop_find->id;
        $fields = json_encode($recuestdata->display_field);
        $check_exist = field_config::where('shop_id' , $id)->first();
        if(count($check_exist)>0):
            $data = field_config::where('shop_id' , $id)->first();
            $data->fields = $fields;    
            $data->save();
        else:
            $data = new field_config;
            $data->shop_id = $id;
            $data->fields = $fields;
            $data->save();
        endif;       
        return redirect()->route('order');
    }

    public function update_order (Request $recuestdata)
    {
        $shop = session('shop');
        $app_settings = AppSettings::where('id', 1)->first();
        $shop_find = ShopModel::where('store_name' , $shop)->first();
        $shop_id = $shop_find->id;
       
        $setting = [];
        $setting = app_config::where('shop_id' , $shop_id)->first();    
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
      
        $orders = [];
        $orders = $sh->call(['URL' => 'orders.json', 'METHOD' => 'GET']); 
        
        foreach ($orders as $data){
            foreach($data as $order){
                $order_id = $order->id;
                $order_number = $order->order_number;
                $tag = $order->tags;
                $note_attributes = $order->note_attributes;
                
                foreach($note_attributes as $attribute){
                    $delivery_date = $attribute->value;
                    
                    if($delivery_date != NULL){
                        $updtae_order = []; 
                        $updtae_order = $sh->call(['URL' => '/admin/orders/'.$order_id.'.json', 'METHOD' => 'PUT',
                            'DATA' => [
                                'order' =>[
                                    'id' => $order_id,
                                    'tags' => "$tag,$delivery_date",
                                ] 
                            ]
                        ]);                     
                    }
                   //echo '<pre>'; print_r($delivery_date); 
                }   
                
            }
        } 
        return redirect()->route('order');
    }
}
