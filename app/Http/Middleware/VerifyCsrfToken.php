<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
    **/
    protected $except = [
        'getconfig', 'edit/*', 'saveconfig', 'update-modal-status', 'order-demo', 'save-cut-off', 'check-cart', 'delivery-date-pro', 'save-delivery-info', 'check-delivery-info', 'check-data', 'get-product-data','save-delivery-times', 'edit/*','save-product-settings', 'upgrade-modal-status', 'upgrade-status', 'ultimate'
    ];
}
