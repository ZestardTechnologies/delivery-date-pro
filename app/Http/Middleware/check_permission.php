<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\block_config;

use Closure;

class check_permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //echo session('shop'); 
		$shop = session('shop');
		if(empty($shop))
		{
		  $shop = $_GET['shop'];
		}				
		$shop_find = ShopModel::where('store_name' , $shop)->first();				
		if($shop_find->type_of_app > 2 && $shop_find->app_version > 2)
		{
			
		}
		else
		{			
			return redirect('denied');
		}
        
        return $next($request);
    }
}
