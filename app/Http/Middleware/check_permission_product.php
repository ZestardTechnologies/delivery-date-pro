<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\block_config;

use Closure;

class check_permission_product
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($request->all());
		$shop = session('shop');
		if(empty($shop))
		{
		    $shop = $request['shop'];
		}			
		$shop_find = ShopModel::where('store_name' , $shop)->first();
		// echo "<pre>";
		// print_r($shop_find);
		// echo $shop_find['type_of_app'];
		// echo gettype($shop_find['type_of_app']);					
		// die;
		//echo intval($shop_find['type_of_app']) > 3 && intval($shop_find['app_version']) > 3 ;
		// echo intval($shop_find['type_of_app']);
		// echo gettype(intval($shop_find['type_of_app']));		
		// echo intval($shop_find['app_version']);
		// echo gettype(intval($shop_find['app_version']));		
		// echo intval($shop_find['type_of_app']) > 3 && intval($shop_find['app_version']) > 3 ;
		// die;
		if(intval($shop_find['type_of_app']) > 3 && intval($shop_find['app_version']) > 3)
		{
			
		}
		else
		{			
			return redirect('denied');
		}
        
        return $next($request);
    }
}
