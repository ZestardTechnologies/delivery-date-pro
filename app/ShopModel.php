<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopModel extends Model {

    protected $table = 'usersettings';
    public $timestamps = false;    
    protected $fillable = [
        'access_token', 'store_name', 'store_encrypt', 'charge_id', 'api_client_id' ,'price', 'status', 'billing_on', 'payment_created_at', 'activated_on', 'trial_ends_on', 'cancelled_on', 'trial_days', 'decorated_return_url', 'confirmation_url', 'domain', 'write_orders_access', 'records_per_page', 'delivery_time_count_status', 'global_delivery_time_status' ,'global_cutoff_time_status', 'app_version', 'type_of_app', 'usage_charge_id', 'usage_price', 'new_install', 'upgrade_status', 'upgrade_modal_status', 'language_locale'
    ];

    public function block_config()
    {
        return $this->hasOne('App\block_config','shop_id','id');
    }
    
}
