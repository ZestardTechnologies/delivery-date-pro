<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class app_config extends Model
{
  protected $table = 'app_config';
  public $timestamps = false;
  //protected $primaryKey = 'id';
  protected $fillable =[
    'date_format'
  ];
  
}
