<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryTimeSettings extends Model
{
  protected $table = 'deliverytime_settings';
  public $timestamps = false;
  //protected $primaryKey = 'id';
  protected $fillable =[
    'store', 'shop_id', 'day', 'delivery_times', 'count', 'update_shopid_status'
  ];
  
}
