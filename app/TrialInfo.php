<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TrialInfo extends Model
{
  protected $table = 'trial_info';
  public $timestamps = false;
  //protected $primaryKey = 'id';
  protected $fillable =[
    'store_name', 'shop_id', 'trial_days', 'activated_on', 'trial_ends_on', 'update_shopid_status'
  ];
  
}
