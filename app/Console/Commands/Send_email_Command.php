<?php
namespace App\Console\Commands;

//namespace App\Http\Controllers;

use Illuminate\Console\Command;
use DB;

use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;

class Send_email_Command extends Command
{	
    /**
     * The name and signature of the console command.
     *
     * @var string
    **/
    protected $signature = 'send:email';
    /**
     * The console command description.
     *
     * @var string
    **/
    protected $description = '';
    /**
     * Create a new command instance.
     *
     * @return void
    **/
    public function __construct()
    {
        parent::__construct();
    }
    /*
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {	
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";					
        $pending_delivery = DB::table('order_details')->where('delivery_info_status', '0')->get();	
        DB::table('development_stores')->insert(['dev_store_name' => 'apps-testing-store.myshopify.com']);	
		foreach($pending_delivery as $details)
		{
			$admin_email 	= $details->admin_email;
			$customer_email = $details->customer_email;
			$order_id		= $details->order_id;
			$store_details 	= DB::table('usersettings')->where('id' ,$details->shop_id)->first();	
			$store_name 	= $store_details->store_name;				
			if(!empty($admin_email) && !empty($customer_email))
			{							
                DB::table('development_stores')->insert(['dev_store_name' => 'apps-testing-store.myshopify.com']);		
				$customer_name 	= $details->customer_name;
				$order_name		= $details->order_name;
				$msg = 'Hello ' . $customer_name . ', <br>It seems you have missed selecting delivery date on ' . $store_name . ' for order number '. $order_name . ' <br>Please click on below link to select delivery date.<br><a href="http://zestardshop.com/shopifyapp/order.php?store_name='. $store_name .'&order_number=' . $order_id . '">Click Here</a>';
				mail("$customer_email","Regarding Order $order_name", $msg, $headers);		
			}
		}							
    }
}

?>