<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /*
     * The Artisan commands provided by your application.
     *
     * @var array
    */
    protected $commands = [
        \App\Console\Commands\Send_email_Command::class
    ];
    /*
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
    */
    protected function schedule(Schedule $schedule)
	{	  	//$schedule->command('import:tag')->weekly()->wednesdays()->timezone('Asia/Kolkata')->at('18:02');						
	$schedule->command('send:email')->cron('0 * * * *');
    }
    /*
     * Register the Closure based commands for the application.
     *
     * @return void
    */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}