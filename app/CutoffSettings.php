<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CutoffSettings extends Model
{
  protected $table = 'cutoff_settings';
  public $timestamps = false;
  //protected $primaryKey = 'id';
  protected $fillable =[
    'store', 'shop_id', 'cutoff_day', 'cutoff_hour', 'cutoff_minute', 'update_shopid_status'
  ];
  
}
