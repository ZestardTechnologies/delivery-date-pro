<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DevelopmentStore extends Model
{
  protected $table = 'development_stores';
  public $timestamps = false;
  //protected $primaryKey = 'id';
  protected $fillable =[
    'dev_store_name', 'shop_id', 'update_shopid_status'
  ];
  
}
